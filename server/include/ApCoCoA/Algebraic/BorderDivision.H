//
// This file is part of the source of ApCoCoALib, the ApCoCoA Library.
//
//   Copyright (c) ApCoCoA Project (Prof. Dr. Martin Kreuzer, Uni Passau)
//
//   Author: 2008 Stefan Kaspar
//
// Visit http://apcocoa.org/ for more information regarding ApCoCoA
// and ApCoCoALib.
// Visit http://www.apcocoa.org/wiki/ApCoCoA:KnownIssues for bugs, problems 
// and known issues.
//
// ApCoCoALib is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License (version 3 or later)
// as published by the Free Software Foundation. A copy of the full
// licence may be found in the file COPYING in this directory.
//
// ApCoCoALib is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ApCoCoALib; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

#ifndef ApCoCoA_BorderDivision_H
#define ApCoCoA_BorderDivision_H

#include "ApCoCoA/Algebraic/OrderIdeal.H"
#include "CoCoA/library.H"

// #include <cstddef> // Included in ring.H
// using std::size_t;
#include <utility>
// using std::pair;
// #include <vector> // Included in ring.H
// using std::vector;

namespace ApCoCoA
{
  namespace AlgebraicAlgorithms
  {
    /*-----------------------------------------------------------------*/
    /** \include BorderDivision.txt  */
    /*-----------------------------------------------------------------*/

    /*! \brief Apply Border Division Algorithm to a polynomial.
     *
     *  Let \f$P = K[x_1, \dots, x_n]\f$,
     *  \f$\mathcal{O}\f$ be an order ideal in \f$T^n\f$,
     *  \f$\{ g_1, \dots, g_k \}\f$ be an \f$\mathcal{O}\f$-border prebasis,
     *  and \f$f \in P\f$. This function computes a vector of tuples
     *  \f$((\alpha_1, f_{\alpha_1}), \dots, ({\alpha_m}, f_{\alpha_m}))\f$
     *  and a polynomial \f$r \in P\f$ such that
     *  \f$\alpha_1, \dots, \alpha_m \in \{ 1, \dots, k \}\f$,
     *  \f$\alpha_1 < \dots < \alpha_m\f$, and
     *  \f$f = f_{\alpha_1}g_{\alpha_1} + \cdots + f_{\alpha_m}g_{\alpha_m} + r\f$
     *  where \f$f_{\alpha_i}g_{\alpha_i} \ne 0\f$ and
     *  \f$\textnormal{deg}(f_{\alpha_i}) \leq \textnormal{ind}_\mathcal{O}(f) - 1\f$
     *  for all \f$i \in \{ 1, \dots, m \}\f$.
     *
     *  \param quotients Will hold the vector of tuples
     *                    \f$((\alpha_1, f_{\alpha_1}), \dots, ({\alpha_m}, f_{\alpha_m}))\f$.
     *  \param r Will hold the normal \f$\mathcal{O}\f$-remainder of
     *           <code>f</code>.
     *  \param f The polynomial which should be decomposed.
     *  \param O The order ideal necessary to apply the algorithm.
     *  \param prebasis A vector of polynomials representing an
     *                  \f$\mathcal{O}\f$-border prebasis of the given order
     *                  ideal <code>O</code>.
     *  \param ComputeRemainderOnly If set to <code>true</code>, the function
     *                              will only compute the normal
     *                              \f$\mathcal{O}\f$-remainder <code>r</code>
     *                              and <code>quotients</code> will remain
     *                              unchanged. The default value is
     *                              <code>false</code>.
     */
    void BorderDivision(std::vector< std::pair<std::size_t, CoCoA::RingElem> >& quotients,
                        CoCoA::RingElem r,
                        CoCoA::ConstRefRingElem f,
                        const AlgebraicCore::OrderIdeal& O,
                        const std::vector<CoCoA::RingElem>& prebasis,
                        const bool ComputeRemainderOnly = false);


    /*! \brief Apply Border Division Algorithm to a polynomial.
     *
     *  Let \f$P = K[x_1, \dots, x_n]\f$,
     *  \f$\mathcal{O}\f$ be an order ideal in \f$T^n\f$,
     *  \f$\{ g_1, \dots, g_k \}\f$ be an \f$\mathcal{O}\f$-border prebasis,
     *  and \f$f \in P\f$. This function computes a vector of tuples
     *  \f$((\alpha_1, f_{\alpha_1}), \dots, ({\alpha_m}, f_{\alpha_m}))\f$
     *  and a polynomial \f$r \in P\f$ such that
     *  \f$\alpha_1, \dots, \alpha_m \in \{ 1, \dots, k \}\f$,
     *  \f$\alpha_1 < \dots < \alpha_m\f$, and
     *  \f$f = f_{\alpha_1}g_{\alpha_1} + \cdots + f_{\alpha_m}g_{\alpha_m} + r\f$
     *  where \f$f_{\alpha_i}g_{\alpha_i} \ne 0\f$ and
     *  \f$\textnormal{deg}(f_{\alpha_i}) \leq \textnormal{ind}_\mathcal{O}(f) - 1\f$
     *  for all \f$i \in \{ 1, \dots, m \}\f$.
     *
     *  \param quotients Will hold the vector of tuples
     *                    \f$((\alpha_1, f_{\alpha_1}), \dots, ({\alpha_m}, f_{\alpha_m}))\f$.
     *  \param r Will hold the normal \f$\mathcal{O}\f$-remainder of
     *           <code>f</code>.
     *  \param f The polynomial which should be decomposed.
     *  \param O The order ideal necessary to apply the algorithm.
     *  \param prebasis A vector of tuples representing an
     *                  \f$\mathcal{O}\f$-border prebasis of the given order
     *                  ideal. Each tuple \f$(g_i, b_i)\f$ must contain a
     *                  prebasis polynomial \f$g_i\f$ and a term \f$b_i\f$ where
     *                  \f$b_i\f$ is an element of the border of the given
     *                  order ideal <code>O</code>.
     *  \param ComputeRemainderOnly If set to <code>true</code>, the function
     *                              will only compute the normal
     *                              \f$\mathcal{O}\f$-remainder <code>r</code>
     *                              and <code>quotients</code> will remain
     *                              unchanged. The default value is
     *                              <code>false</code>.
     */
    void BorderDivision(std::vector< std::pair<std::size_t, CoCoA::RingElem> >& quotients,
                        CoCoA::RingElem r,
                        CoCoA::ConstRefRingElem f,
                        const AlgebraicCore::OrderIdeal& O,
                        const std::vector< std::pair<CoCoA::RingElem, CoCoA::PPMonoidElem> >& prebasis,
                        const bool ComputeRemainderOnly = false);
  } // end of namespace AlgebraicAlgorithms
} // end of namespace ApCoCoA

#endif
