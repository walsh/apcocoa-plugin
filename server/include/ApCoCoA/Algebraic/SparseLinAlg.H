//
// This file is part of the source of ApCoCoALib, the ApCoCoA Library.
//
//   Copyright (c) ApCoCoA Project (Prof. Dr. Martin Kreuzer, Uni Passau)
//
//   Author: 2009 Xingqiang XIU, Ehsan Ullah
//
// Visit http://apcocoa.org/ for more information regarding ApCoCoA
// and ApCoCoALib.
// Visit http://www.apcocoa.org/wiki/ApCoCoA:KnownIssues for bugs, problems
// and known issues.
//
// ApCoCoALib is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License (version 3 or later)
// as published by the Free Software Foundation. A copy of the full
// licence may be found in the file COPYING in this directory.
//
// ApCoCoALib is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ApCoCoALib; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

#ifndef APCOCOA_SPARSELINALG_H
#define APCOCOA_SPARSELINALG_H

#include <set>
using std::set;
#include<vector>
using std::vector;
//#include <iostream>
//using std::cout;
//using std::endl;

namespace ApCoCoA {
  namespace AlgebraicAlgorithms {

enum SGE { GE, GE_v2, GE_v3, SGE0, SGE1, SGE2, Mat_tr };


//! ============================ RowInfo class ============================
/*!
 * Class of RowInfo.
 * It is used in class SparseMatrix (normal Gaussian elimination)
 */
class RowInfo {
private:
  unsigned int row_index_; //row index
  unsigned int leading_column_index_; //the first column index of the row
public:
  RowInfo();
  RowInfo( const unsigned int, const unsigned int );
  RowInfo( const RowInfo & );
  ~RowInfo();
  RowInfo & operator=( const RowInfo & );
  //! Comparing operator.
  /*!
   * It is a well-ordering. ri1 < ri2 iff
   * (ri1.leading_column_index_ < ri2.leading_column_index_) OR
   * (ri1.leading_column_index_ == ri2.leading_column_index_ AND ri1.row_index_ < ri2.row_index_)
   */
  bool operator<( const RowInfo & ) const;
  unsigned int getRowIndex() const;
  unsigned int getLeadingColIndex() const;
};



//! ============================ SparseMatrix class ============================
/*!
 * Class of SparseMatrix (over F2).
 * Class for normal Gaussian elimination.
 */
class SparseMatrix {
private:
  unsigned int num_rows_; //! number of rows of original matrix (maybe useless!)
  unsigned int num_cols_; //! number of columns of original matrix (maybe useless!)
  //! indices start with 1
  vector<set<unsigned int> > m_;
  set<RowInfo> s_ri_;
public:
  SparseMatrix();
  /*!
   * Note: only nonzero entries are stored in SparseMatrix!
   * Zero rows will be deleted during constructing.
   */
  SparseMatrix( const unsigned int, const unsigned int, const vector<set<unsigned int> > & );
  SparseMatrix( const SparseMatrix & );
  ~SparseMatrix();
  SparseMatrix & operator=( const SparseMatrix & );
  vector<set<unsigned int> > getRows() const;
  /*!
   * It seems to declare member data m_ to be public can save memory copy!?
   */
  set<RowInfo> getRowInfo() const;
  void GaussElim();

//  void print() const {
//    cout << "Matrix : (r, c) = (" << num_rows_ << ", " << num_cols_ << ")" << endl;
//    for ( set<RowInfo>::iterator s_it = s_ri_.begin(); s_it != s_ri_.end(); ++s_it )
//    {
//      set<unsigned int> row_vector = m_.at(s_it->getRowIndex() - 1);
//      for ( set<unsigned int>::iterator s_eit = row_vector.begin(); s_eit != row_vector.end(); ++s_eit )
//        cout << *s_eit << "\t";
//      cout << endl;
//      row_vector.clear();
//    }
//    return;
//  }
};



//! ============================ RowInfo_v2 class ============================
class RowInfo_v2 {
private:
  unsigned int row_index_; //! row index
  unsigned int num_of_elems_; //! number of non-zero elements
  unsigned int leading_column_index_; //! the first column index
  RowInfo_v2();
public:
  RowInfo_v2( const unsigned int, const unsigned int, const unsigned int );
  RowInfo_v2( const RowInfo_v2 & );
  ~RowInfo_v2();
  RowInfo_v2 & operator=( const RowInfo_v2 & );
  //! compare operator.
  /*!
   * ri1 < ri2 (well-ordering) if and only if
   * (ri1.leading_column_index_ < ri2.leading_column_index_) OR
   * (ri1.leading_column_index_ == ri2.leading_column_index_ AND ri1.num_of_elems_ < ri2.num_of_elems_) OR
   * (ri1.leading_column_index_ == ri2.leading_column_index_ AND ri1.num_of_elems_ == ri2.num_of_elems_ AND
   * ri1.row_index_ < ri2.row_index_)
   */
  bool operator<( const RowInfo_v2 & ) const;
  unsigned int getRowIndex() const;
  unsigned int getNumOfElems() const;
  unsigned int getLeadingColumnIndex() const;
};



//! ============================ SparseMatrix_v2 class ============================
class SparseMatrix_v2 {
private:
  unsigned int row_num_; //! number of rows of original matrix (maybe useless!)
  unsigned int col_num_; //! number of columns of original matrix (maybe useless!)
  vector<set<unsigned int> > m_;
  set<RowInfo_v2> s_ri_;
  SparseMatrix_v2();
public:
  /*!
   * Note: only nonzero entries are stored in SparseMatrix_v2!
   * Zero rows will be deleted during constructing.
   */
  SparseMatrix_v2( const unsigned int, const unsigned int, const vector<set<unsigned int> > & );
  SparseMatrix_v2( const SparseMatrix_v2 & );
  ~SparseMatrix_v2();
  SparseMatrix_v2 & operator=( const SparseMatrix_v2 & );
  unsigned int getRowNum() const;
  unsigned int getColNum() const;
  vector<set<unsigned int> > getRows() const;
  set<RowInfo_v2> getRowInfo() const;
  void GaussElim();
  void GaussElim_trace();
};



//! ============================ SBMatrix class ============================
/*!
 * Class of SBMatrix (over F2).
 * Especially for structured Gaussian elimination.
 */
class SBMatrix {
private:
  unsigned int num_rows_; //! number of rows
  unsigned int num_cols_; //! number of columns
  //! indices start with 1
  vector<set<unsigned int> > mat_rows_; //! matrix stored by rows (of non-zero elements)
  vector<set<unsigned int> > mat_cols_; //! matrix stored by columns (of non-zero elements)
  set<unsigned int> s_light_; //! indices of light columns
  set<unsigned int> s_heavy_; //! indices of heavy columns
  vector<unsigned int> v_num_in_light_; //! number of non-zero elements in light part
  set<unsigned int> s_indices_unit_in_light_; // indices of rows which have only 1 non-zero element in light part
  SBMatrix();
public:
  SBMatrix( const unsigned int, const unsigned int, const vector<set<unsigned int> > & );
  SBMatrix( const SBMatrix & rhs );
  ~SBMatrix();
  SBMatrix & operator=( const SBMatrix & rhs );
  vector<set<unsigned int> > getRows() const;
//  unsigned int getRowNum() const { return num_rows_; }
//  unsigned int getColNum() const { return num_cols_; }
//  vector<set<unsigned int> > getRowMat() const { return mat_rows_; }
private:
/*!
 * Note: i tried to design it in the way that each steps can be combined freely.
 */
  /*!
   * Step 1:
   * delete all columns that have a single non-zero coefficient and
   * the rows in which those columns have non-zero coefficients.
   */
  void Step1();
  /*!
   * Step 2:
   * declare some additional light columns to be heavy,
   * choosing the heaviest ones.
   */
  void Step2();
  /*!
   * Step 3:
   * delete some of the rows, selecting those which have
   * the largest number of non-zero elements in the light columns.
   */
  void Step3();
  /*!
   * Step 4:
   * for any row which has only a single non-zero coefficient equal to +/-1
   * in the light column, substract appropriate multiples of that row from
   * all other rows that have non-zero coefficients on that column so as to
   * make those coefficients 0;
   */
  void Step4();
  inline void deleteRow( const unsigned int );
  inline void addRow( const unsigned int, const set<unsigned int> & );
public:
  //! structured Gaussian elimination
  void StructuredGaussElim( const SGE );

//  bool SBMatrix::checkMatrix() const {
//    size_t i;
//    set<unsigned int>::iterator it;
//    for ( i = 0; i != mat_rows_.size(); ++i )
//      for ( it = mat_rows_.at(i).begin(); it != mat_rows_.at(i).end(); ++it )
//        if ( mat_cols_.at(*it - 1).find(i+1) == mat_cols_.at(*it - 1).end() )
//          return false;
//    for ( i = 0; i != mat_cols_.size(); ++i )
//      for ( it = mat_cols_.at(i).begin(); it != mat_cols_.at(i).end(); ++it )
//        if ( mat_rows_.at(*it - 1).find(i+1) == mat_rows_.at(*it - 1).end() )
//          return false;
//    return true;
//  }
};



//! ============================ functions called by ApCoCoA ============================
/*!
 * I defined the functions like these, 'cause i want to limit the affection
 * of above classes only in this file.
 */
void SEF( const unsigned int, const unsigned int, vector<set<unsigned int> > & );
void SEF_v2( const unsigned int, const unsigned int, vector<set<unsigned int> > & );
void SEF_v3( const unsigned int, const unsigned int, vector<set<unsigned int> > & );
void SGEF( const unsigned int, const unsigned int, vector<set<unsigned int> > &, const SGE & );

void getTransportMatrix( const unsigned int, const unsigned int, vector<set<unsigned int> > & );



  } // end of namespace AlgebraicAlgorithms
} // end of namespace ApCoCoA

#endif /* APCOCOA_SPARSELINALG_H */
