//
// This file is part of the source of ApCoCoALib, the ApCoCoA Library.
//
//   Copyright (c) ApCoCoA Project (Prof. Dr. Martin Kreuzer, Uni Passau)
//
//   Author: 2013 X. XIU
//
// Visit http://apcocoa.org/ for more information regarding ApCoCoA
// and ApCoCoALib.
// Visit http://www.apcocoa.org/wiki/ApCoCoA:KnownIssues for bugs, problems
// and known issues.
//
// ApCoCoALib is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License (version 3 or later)
// as published by the Free Software Foundation. A copy of the full
// licence may be found in the file COPYING in this directory.
//
// ApCoCoALib is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ApCoCoALib; if not, write to the Free Software

#ifndef ApCoCoA_NCPolynomial_H
#define ApCoCoA_NCPolynomial_H

#include "ApCoCoA/Noncommutative/NCPolynomialRing.H"
#include "ApCoCoA/Noncommutative/FiniteField.H"
#include "CoCoA/QQ.H"
#include <iostream>
#include <map>
#include <math.h> //fabs()
#include <sstream> //stingstream
#include <stdexcept> //invalid_argument()
#include <utility> //pair
using std::map;
using std::ostream;
using std::size_t;


namespace ApCoCoA {
	namespace NoncommutativeAlgorithms {

    ////////////////////////////////////////////////////////////
    //	class NCMonomial
    ////////////////////////////////////////////////////////////

	template <typename T>
    class NCMonomial
    {
    public:
		T myCoeff;
		Word myWord;
		NCMonomial(){}
		NCMonomial(const T c, const Word word)
			: myCoeff(c), myWord(word) {}
		NCMonomial(const NCMonomial & rhs)
			: myCoeff(rhs.myCoeff), myWord(rhs.myWord) {}
		~NCMonomial(){}
		NCMonomial & operator=(const NCMonomial & rhs)
		{
			if (&rhs != this)
		    {
				myCoeff = rhs.myCoeff;
				myWord = rhs.myWord;
		    }
			return *this;
		}
	    //Overload operator << for the sake of output.
	    template <typename K>
	    friend ostream & operator<<(ostream & out, const NCMonomial<K> & m);
    };

    //Overload operator << for the sake of output.
    template <typename K>
    ostream & operator<<(ostream & out, const NCMonomial<K> & m)
    {
    	out << m.myCoeff << "*"<< m.myWord;
    	return out;
    }
        

    ////////////////////////////////////////////////////////////
    //	class NCPolynomial
    ////////////////////////////////////////////////////////////

    /*! class NCPolynomial defines a non-commutative polynomial
     * 	in a non-commutative polynomial ring.
     *
     *  Notice that
     *  (a) a word is represented as a vector of indices of
     *  indeterminates.
     *  (b) the names of indeterminates are stored in the
     *  	corresponding object of class NCPolynomialRing.
     *  (c) 1-1 correspondence between indeterminate and its
     *  	index is interpreted by an Interpreter.
     * 	(d) we define the degree of constant to be 0! In other
     * 		words, a constant c is considered to be c*1.
     * 	(e) the default word ordering is the length-lexicographic
     * 		ordering LLEX.
     */
	template <typename T>
    class NCPolynomial
    {
    public:
		NCPolynomial() {}
		NCPolynomial(const map<Word, T> & supp);
	    NCPolynomial(const NCPolynomial & rhs);
	    ~NCPolynomial();
	    NCPolynomial & operator=(const NCPolynomial & rhs);
	    NCPolynomial & operator+=(const NCPolynomial & rhs);
	    const NCPolynomial operator+(const NCPolynomial & rhs) const;
	    NCPolynomial & operator-=(const NCPolynomial & rhs);
	    const NCPolynomial operator-(const NCPolynomial & rhs) const;
	    NCPolynomial & operator*=(const NCPolynomial & rhs);
	    const NCPolynomial operator*(const NCPolynomial & rhs) const;
//	    NCPolynomial & operator/=(const NCPolynomial & rhs);
//	    const NCPolynomial operator/(const NCPolynomial & rhs) const;
	    NCPolynomial & operator+=(const NCMonomial<T> & m); //NCPolynomial+NCMonomial
	    const NCPolynomial operator+(const NCMonomial<T> & m) const;
	    NCPolynomial & operator-=(const NCMonomial<T> & m); //NCPolynomial-NCMonomial
	    const NCPolynomial operator-(const NCMonomial<T> & m) const;
	    NCPolynomial & operator*=(const NCMonomial<T> & m); //NCPolynomial*NCMonomial
	    const NCPolynomial operator*(const NCMonomial<T> & m) const;
	    NCPolynomial & operator*=(const Word & w); //NCPolynomial*Word
	    const NCPolynomial operator*(const Word & w) const;
//	    NCPolynomial & operator/=(const NCMonomial<T> & m); //NCPolynomial/NCMonomial
//	    const NCPolynomial operator/(const NCMonomial<T> & m) const;
	    NCPolynomial & operator*=(const T & c); //NCPolynomial*c
	    const NCPolynomial operator*(const T & c) const;
	    NCPolynomial & operator/=(const T & c); //NCPolynomial/c
	    const NCPolynomial operator/(const T & c) const;
	    //Overloaded operator< and operator== ONLY compare words in supports
	    //Question: if f1=x+y and f2=x+2y, then f1 < f2 or f1 = f2 or f1 > f2 ?
	    bool operator<(const NCPolynomial & rhs) const;
	    bool operator==(const NCPolynomial & rhs) const;
	    /* access functions */
	    size_t degree() const; //standard degree
	    Word LW() const;
	    T LC() const;
	    T getCoefficient(const Word & w) const;
	    const map<Word, T> & getSupp() const;
	    bool isUnit() const;
	    bool isZero() const;
	    bool isHomogeneous() const;
	    /* friend functions */
	    //negative operator
	    template <typename K>
	    friend NCPolynomial<K> operator-(const NCPolynomial<K> & poly);
	    //NCMonomial+NCPolynomial
	    template <typename K>
	    friend NCPolynomial<K> operator+(const NCMonomial<K> & m, const NCPolynomial<K> & poly);
	    //NCMonomial-NCPolynomial
	    template <typename K>
	    friend NCPolynomial<K> operator-(const NCMonomial<K> & m, const NCPolynomial<K> & poly);
	    //NCMonomial*NCPolynomial
	    template <typename K>
	    friend NCPolynomial<K> operator*(const NCMonomial<K> & m, const NCPolynomial<K> & poly);
        //Word*NCPolynomial
	    template <typename K>
	    friend NCPolynomial<K> operator*(const Word & w, const NCPolynomial<K> & poly);
	    //c*NCPolynomial
	    template <typename K>
	    friend NCPolynomial<K> operator*(const K & c, const NCPolynomial<K> & poly);
	    //Overload operator << for the sake of output.
	    template <typename K>
	    friend ostream & operator<<(ostream & out, const NCPolynomial<K> & poly);
    private:
	    /* auxiliary functions */
	    void removeZero(); //remove word whose coefficient is zero
    public:
	    bool isUnit(const float elem) const { return (fabs(elem) > 0.0000001); }
	    bool isZero(const float elem) const { return (fabs(elem) < 0.0000001); }
	    bool isUnit(const double elem) const { return (fabs(elem) > 0.0000001); }
	    bool isZero(const double elem) const { return (fabs(elem) < 0.0000001); }
	    bool isUnit(const FiniteField & elem) const { return !elem.isZero(); }
	    bool isZero(const FiniteField & elem) const { return elem.isZero(); }
        bool isUnit(const CoCoA::QQ & elem) const { return !CoCoA::IsZero(elem); }
        bool isZero(const CoCoA::QQ & elem) const { return CoCoA::IsZero(elem); }
    private:
		map<Word, T> mySupp;
    };

	template <typename T>
	NCPolynomial<T>::NCPolynomial(const map<Word, T> & supp)
	{
		typename map<Word, T>::const_iterator cit;
	    for (cit = supp.begin(); cit != supp.end(); ++cit)
	    {
	    	if (!isZero(cit->second))
	    		mySupp[cit->first] = cit->second;
	    }
	}

	template <typename T>
	NCPolynomial<T>::NCPolynomial(const NCPolynomial<T> & rhs)
		: mySupp(rhs.mySupp)
	{}

	template <typename T>
	NCPolynomial<T>::~NCPolynomial()
	{
		if (!mySupp.empty())
			mySupp.clear();
	}

	template <typename T>
	NCPolynomial<T> & NCPolynomial<T>::operator=(const NCPolynomial<T> & rhs)
	{
		if (&rhs != this)
	    {
			if (!mySupp.empty())
				mySupp.clear(); //a potential error might occur here
			mySupp = rhs.mySupp;
	    }
		return *this;
	}

	template <typename T>
    NCPolynomial<T> & NCPolynomial<T>::operator+=(const NCPolynomial & rhs)
    {
		if (!rhs.mySupp.empty()) //check if rhs is zero
	    {
			typename map<Word,T>::const_iterator cit;
			for (cit = rhs.mySupp.begin(); cit != rhs.mySupp.end(); ++cit)
			{
				typename map<Word,T>::iterator fi = mySupp.find(cit->first);
				if (mySupp.end() != fi)
					fi->second = fi->second + cit->second;
				else
					mySupp[cit->first] = cit->second;
			}
			removeZero();
	    }
		return *this;
    }

	template <typename T>
	const NCPolynomial<T> NCPolynomial<T>::operator+(const NCPolynomial<T> & rhs) const
	{
		NCPolynomial<T> p(*this);
	    p += rhs;
	    return p;
	}

	template <typename T>
    NCPolynomial<T> & NCPolynomial<T>::operator-=(const NCPolynomial & rhs)
    {
		if (!rhs.mySupp.empty()) //check if rhs is zero
	    {
			typename map<Word,T>::const_iterator cit;
			for (cit = rhs.mySupp.begin(); cit != rhs.mySupp.end(); ++cit)
			{
				typename map<Word,T>::iterator fi = mySupp.find(cit->first);
				if (mySupp.end() != fi)
					fi->second = fi->second - cit->second;
				else
					mySupp[cit->first] = - cit->second;
			}
			removeZero();
	    }
		return *this;
    }

	template <typename T>
	const NCPolynomial<T> NCPolynomial<T>::operator-(const NCPolynomial<T> & rhs) const
	{
		NCPolynomial<T> p(*this);
	    p -= rhs;
	    return p;
	}

	template <typename T>
    NCPolynomial<T> & NCPolynomial<T>::operator*=(const NCPolynomial & rhs)
    {
	    if (!mySupp.empty() && !rhs.mySupp.empty())
	    {
	    	map<Word,T> supp;
	    	typename map<Word,T>::iterator it;
	    	typename map<Word,T>::const_iterator cit;
	    	for (it = mySupp.begin(); it != mySupp.end(); ++it)
	    	{
	    		for (cit = rhs.mySupp.begin(); cit != rhs.mySupp.end(); ++cit)
	    		{
	    			Word w = it->first * cit->first;
	    			T c = it->second * cit->second;
	    			typename map<Word,T>::iterator fi = supp.find(w);
	    			if (supp.end() != fi)
	    				fi->second = fi->second + c;
	    			else
	    				supp[w] = c;
	    		}
	    	}
	    	mySupp.clear(); mySupp = supp; supp.clear();
	    	removeZero();
	    }
	    else if (!mySupp.empty())
	    {
	    	mySupp.clear();
	    }
		return *this;
    }

	template <typename T>
	const NCPolynomial<T> NCPolynomial<T>::operator*(const NCPolynomial<T> & rhs) const
	{
		NCPolynomial<T> p(*this);
	    p *= rhs;
	    return p;
	}

	template <typename T>
    NCPolynomial<T> & NCPolynomial<T>::operator+=(const NCMonomial<T> & m)
    {
		if (isZero(m.myCoeff))
			return *this;
		typename map<Word,T>::iterator fi = mySupp.find(m.myWord);
		if (mySupp.end() != fi)
		{
			if (isZero(fi->second - m.myCoeff))
				mySupp.erase(fi);
			else
				fi->second = fi->second + m.myCoeff;
		}
		else
		{
			mySupp[m.myWord] = m.myCoeff;
		}
		return *this;
    }

	template <typename T>
	const NCPolynomial<T> NCPolynomial<T>::operator+(const NCMonomial<T> & m) const
	{
		NCPolynomial<T> p(*this);
	    p += m;
	    return p;
	}

	template <typename T>
    NCPolynomial<T> & NCPolynomial<T>::operator-=(const NCMonomial<T> & m)
    {
		if (isZero(m.myCoeff))
			return *this;
		typename map<Word,T>::iterator fi = mySupp.find(m.myWord);
		if (mySupp.end() != fi)
		{
			if (isZero(fi->second - m.myCoeff))
				mySupp.erase(fi);
			else
				fi->second = fi->second - m.myCoeff;
		}
		else
		{
			mySupp[m.myWord] = - m.myCoeff;
		}
		return *this;
    }

	template <typename T>
	const NCPolynomial<T> NCPolynomial<T>::operator-(const NCMonomial<T> & m) const
	{
		NCPolynomial<T> p(*this);
	    p -= m;
	    return p;
	}

	template <typename T>
    NCPolynomial<T> & NCPolynomial<T>::operator*=(const NCMonomial<T> & m)
    {
		if (isZero(m.myCoeff))
		{
			if (!mySupp.empty())
				mySupp.clear();
			return *this;
		}
		map<Word,T> supp;
		typename map<Word,T>::iterator it;
		for (it = mySupp.begin(); it != mySupp.end(); ++it)
		{
			supp.insert(std::pair<Word, T>(it->first*m.myWord,
					it->second*m.myCoeff));
		}
		mySupp.clear(); mySupp = supp; supp.clear();
		return *this;
    }

	template <typename T>
	const NCPolynomial<T> NCPolynomial<T>::operator*(const NCMonomial<T> & m) const
	{
		NCPolynomial<T> p(*this);
	    p *= m;
	    return p;
	}
        
        template <typename T>
        NCPolynomial<T> & NCPolynomial<T>::operator*=(const Word & w)
        {
            if (w.isEmpty())
                return *this;
            map<Word,T> supp;
            typename map<Word,T>::iterator it;
            for (it = mySupp.begin(); it != mySupp.end(); ++it)
            {
                supp.insert(std::pair<Word, T>(it->first*w,it->second));
            }
            mySupp.clear(); mySupp = supp; supp.clear();
            return *this;
        }
        
        template <typename T>
        const NCPolynomial<T> NCPolynomial<T>::operator*(const Word & w) const
        {
            NCPolynomial<T> p(*this);
            p *= w;
            return p;
        }

	template <typename T>
    NCPolynomial<T> & NCPolynomial<T>::operator*=(const T & c)
    {
		if (isZero(c))
		{
			if (!mySupp.empty())
				mySupp.clear();
			return *this;
		}
		typename map<Word,T>::iterator it;
		for (it = mySupp.begin(); it != mySupp.end(); ++it)
		{
			it->second *= c;
		}
		return *this;
    }

	template <typename T>
	const NCPolynomial<T> NCPolynomial<T>::operator*(const T & c) const
	{
		NCPolynomial<T> p(*this);
	    p *= c;
	    return p;
	}

	template <typename T>
    NCPolynomial<T> & NCPolynomial<T>::operator/=(const T & c)
    {
		if (isZero(c))
		{
			throw std::invalid_argument("Divided by 0 : "
					"NCPolynomial<T> & NCPolynomial<T>::operator/=(const T & c)");
		}
		typename map<Word,T>::iterator it;
		for (it = mySupp.begin(); it != mySupp.end(); ++it)
		{
			it->second /= c;
		}
		return *this;
    }

	template <typename T>
	const NCPolynomial<T> NCPolynomial<T>::operator/(const T & c) const
	{
		NCPolynomial<T> p(*this);
	    p /= c;
	    return p;
	}

	template <typename T>
    bool NCPolynomial<T>::operator<(const NCPolynomial<T> & rhs) const
	{
	    typename map<Word,T>::const_reverse_iterator critL = mySupp.rbegin();
	    typename map<Word,T>::const_reverse_iterator critR = rhs.mySupp.rbegin();
	    for ( ; critL != mySupp.rend() && critR != rhs.mySupp.rend(); ++critL,++critR)
	    {
	    	if (critL->first < critR->first)
	    		return true;
	    	else if (critR->first < critL->first)
	    		return false;
	    }
	    if (rhs.mySupp.rend() != critR) //thus we have 0 < c
	    	return true;
	    return false;
	}

	template <typename T>
    bool NCPolynomial<T>::operator==(const NCPolynomial<T> & rhs) const
	{
		if (mySupp.size() != rhs.mySupp.size())
			return false;
	    typename map<Word, T>::const_iterator citL = mySupp.begin();
	    typename map<Word, T>::const_iterator citR = rhs.mySupp.begin();
	    for ( ; citL != mySupp.end(); )
	    {
	    	if (citL->first == citR->first)
	    	{
	    		++citL; ++citR;
	    	}
	    	else
	    	{
	    		return false;
	    	}
	    }
	    return true;
	}

    /* access functions */
	template <typename T>
	size_t NCPolynomial<T>::degree() const
	{
		size_t d = 0;
		if (!mySupp.empty())
		{
			typename map<Word,T>::const_iterator cit = mySupp.begin();
			for ( ; cit != mySupp.end(); ++cit)
				d = (d >= cit->first.getDegree()) ? d : cit->first.getDegree();
		}
	    return d;
	}

	template <typename T>
	Word NCPolynomial<T>::LW() const
	{
		if (mySupp.empty())
		{
			throw std::invalid_argument("Leading word of 0 is undefined : "
					"Word NCPolynomial<T>::LW() const");
		}
		return mySupp.rbegin()->first;
	}

	template <typename T>
	T NCPolynomial<T>::LC() const
	{
		if (!mySupp.empty())
			return mySupp.rbegin()->second;
		else
			return T();
	}

	template <typename T>
	T NCPolynomial<T>::getCoefficient(const Word & w) const
	{
		typename map<Word, T>::const_iterator fi = mySupp.find(w);
		if (mySupp.end() != fi)
			return fi->second;
		else
			return T();
	}

	template <typename T>
	const map<Word, T> & NCPolynomial<T>::getSupp() const
	{
		return mySupp;
	}

	template <typename T>
	bool NCPolynomial<T>::isUnit() const
	{
		return (1 == mySupp.size() &&
				mySupp.begin()->first.isEmpty() && isUnit(mySupp.begin()->second));
	}

    template <typename T>
    bool NCPolynomial<T>::isZero() const
    {
    	return mySupp.empty();
    }

    template <typename T>
    bool NCPolynomial<T>::isHomogeneous() const
    {
		if (!mySupp.empty())
		{
			typename map<Word,T>::const_iterator cit = mySupp.begin();
			size_t d = cit->first.length();
	    	for( ; cit != mySupp.end(); ++cit)
	    	{
	    		if (cit->first.length() != d)
	    			return false;
	    	}
		}
    	return true;
    }

    /* friend functions */

    template <typename K>
    NCPolynomial<K> operator-(const NCPolynomial<K> & poly)
    {
    	NCPolynomial<K> neg_poly;
    	typename map<Word,K>::const_iterator cit = poly.mySupp.begin();
    	for ( ; cit != poly.mySupp.end(); ++cit)
    		neg_poly.mySupp.insert(std::make_pair(cit->first, - cit->second));
    	return neg_poly;
    }

    template <typename K>
    NCPolynomial<K> operator+(const NCMonomial<K> & m, const NCPolynomial<K> & poly)
    {
    	return poly+m;
    }

    template <typename K>
    NCPolynomial<K> operator-(const NCMonomial<K> & m, const NCPolynomial<K> & poly)
    {
    	return -poly+m;
    }

    template <typename K>
    NCPolynomial<K> operator*(const NCMonomial<K> & m, const NCPolynomial<K> & poly)
    {
    	NCPolynomial<K> ret_poly;
    	if (poly.isZero(m.myCoeff))
    	{
    		return ret_poly;
    	}
    	typename map<Word,K>::const_iterator cit = poly.mySupp.begin();
        for ( ; cit != poly.mySupp.end(); ++cit)
        {
        	ret_poly.mySupp.insert(std::make_pair(m.myWord * cit->first,
                                                  m.myCoeff * cit->second));
        }
        return ret_poly;
    }
        
        template <typename K>
        NCPolynomial<K> operator*(const Word & w, const NCPolynomial<K> & poly)
        {
            if (w.isEmpty())
                return poly;
            NCPolynomial<K> ret_poly;
            typename map<Word,K>::const_iterator cit = poly.mySupp.begin();
            for ( ; cit != poly.mySupp.end(); ++cit)
            {
                ret_poly.mySupp.insert(std::make_pair(w*cit->first,cit->second));
            }
            return ret_poly;
        }

    template <typename K>
    NCPolynomial<K> operator*(const K & c, const NCPolynomial<K> & poly)
    {
    	return poly*c;
    }

    //Overload operator << for the sake of output.
    template <typename K>
    ostream & operator<<(ostream & out, const NCPolynomial<K> & poly)
    {
    	if (poly.mySupp.empty())
    	{
    		out << 0;
    		return out;
    	}
    	typename map<Word,K>::const_reverse_iterator crit = poly.mySupp.rbegin();
    	for ( ; crit != poly.mySupp.rend(); ++crit)
    	{
            std::stringstream ss;
            ss << crit->second;
            if ('-' == ss.str().at(0))
            	out << crit->second << "*" << crit->first;
            else
            {
            	if (poly.mySupp.rbegin() == crit)
            		out << crit->second << "*" << crit->first;
            	else
            		out << "+" << crit->second << "*" << crit->first;
            }
    	}
    	return out;
    }

	/* auxiliary functions */
	template <typename T>
	inline void NCPolynomial<T>::removeZero()
	{
		typename map<Word,T>::iterator it;
	    for (it = mySupp.begin(); it != mySupp.end(); )
	    {
	    	if (isZero(it->second))
	    		mySupp.erase(it++);
	    	else
	    		++it;
	    }
	    return;
	}


    ////////////////////////////////////////////////////////////
    //	class NCQuotient
    ////////////////////////////////////////////////////////////

	//! =============================== class NCQuotient ===============================
	/*! \brief Basic data structure for storing information during division algorithm.
	 * $p=\sum_{pos}c_{pos}l_{pos}f_{pos}r_{pos}+r$
	 */
	template<typename T>
	class NCQuotient
	{
	public:
	    size_t pos_;
	    T c_;
	    Word l_;
	    Word r_;
	    NCQuotient()
	    	: pos_(0) {}
	    NCQuotient(const size_t pos, const T c, const Word & l, const Word & r)
	    	: pos_(pos), c_(c), l_(l), r_(r) {}
	    NCQuotient(const NCQuotient & rhs)
	    	: pos_(rhs.pos_), c_(rhs.c_), l_(rhs.l_), r_(rhs.r_) {}
	    ~NCQuotient() {}
	    NCQuotient & operator=(const NCQuotient & rhs)
	    {
	    	if (&rhs != this)
	    	{
	    		pos_ = rhs.pos_; c_ = rhs.c_; l_ = rhs.l_; r_ = rhs.r_;
	    	}
	    	return *this;
	    }
	    bool operator<(const NCQuotient & rhs) const
	    {
	    	if (pos_ < rhs.pos_ || (pos_ == rhs.pos_ && l_ < rhs.l_) ||
	    			(pos_ == rhs.pos_ && l_ == rhs.l_ && r_ < rhs.r_))
	    		return true;
	    	else
	    		return false;
	    }
	};





	} /* end of namespace NoncommutativeAlgorithms */
} /* end of namespace ApCoCoA */

#endif /* ApCoCoA_NCPolynomial_H */
