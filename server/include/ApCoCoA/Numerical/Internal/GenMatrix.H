//
// This file is part of the source of ApCoCoALib, the ApCoCoA Library.
//
//   Copyright (c) ApCoCoA Project (Prof. Dr. Martin Kreuzer, Uni Passau)
//
//   Author: 2010 Jan Limbeck
//
// Visit http://apcocoa.org/ for more information regarding ApCoCoA
// and ApCoCoALib.
// Visit http://www.apcocoa.org/wiki/ApCoCoA:KnownIssues for bugs, problems 
// and known issues.
//
// ApCoCoALib is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License (version 3 or later)
// as published by the Free Software Foundation. A copy of the full
// licence may be found in the file COPYING in this directory.
//
// ApCoCoALib is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ApCoCoALib; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 US

#ifndef ApCoCoA_GENMATRIX_H
#define ApCoCoA_GENMATRIX_H

#include <string.h>
#include "CoCoA/convert.H"


namespace ApCoCoA {

namespace NumericalAlgorithms {

/**
   \brief This class is used as a generic interface to matrices. 
   
   This class is used as a generic interface to matrices, allowing for a higher level of abstraction
   
*/
template <class a_type> class GenMatrix
   {
       protected:
	   	const std::size_t numRows;
        const std::size_t numColumns;
		std::vector< a_type > elements;
		 
       public: 
		
		GenMatrix(const std::size_t rows, const std::size_t columns, const a_type *pValues = NULL): numRows(rows), numColumns(columns) 
		{ 	  
			const size_t num = rows*columns;
			if (num != 0)
			{ 
				elements.resize(num);
				if ( pValues != NULL)
				{
					memcpy(&(elements[0]), pValues, num*sizeof(a_type));
				}
			}
		};
		 
		 inline size_t getNumCols() const {return numColumns;}
         inline size_t getNumRows() const {return numRows;}
		 inline a_type getElements() {return &(elements[0]); };
		 
		 /*virtual *GenMatrix sum(&GenMatrix);
		 virtual void sum(&GenMatrix, &GenMatrix);
		 virtual *GenMatrix mul(&GenMatrix);
		 virtual void mul(&GenMatrix, &GenMatrix);
		 virtual *GenMatrix kernel();
		 virtual double getColumnNorm2(const std::size_t numColumn);*/

		 
         inline a_type* getColumn(const std::size_t numColumn){return &(elements[numRows*numColumn]);};
         inline a_type getElement(const std::size_t pNumRow, const std::size_t pNumColumn){
			if ( pNumRow >= numRows || pNumColumn >= numColumns)
			{
				CoCoA_ERROR(CoCoA::ERR::BadIndetIndex,"Numerical::GenMatrix::getElement:index out of bounds");
			}
			return elements[pNumRow + pNumColumn*numRows];	
		 };
		 
		 virtual void setElement(const a_type value, const std::size_t row, const std::size_t column)
		 { elements[row + column*numRows] = value; };
		 
   }; 

}  // end of namespace NumericalAlgorithms
}  // end of namespace ApCoCoA
#endif
