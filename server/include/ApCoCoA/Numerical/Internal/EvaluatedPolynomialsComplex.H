//
// This file is part of the source of ApCoCoALib, the ApCoCoA Library.
//
//   Copyright (c) ApCoCoA Project (Prof. Dr. Martin Kreuzer, Uni Passau)
//
//   Author: 2006, 2007, 2008 Daniel Heldt, 2009 Jan Limbeck
//
// Visit http://apcocoa.org/ for more information regarding ApCoCoA
// and ApCoCoALib.
// Visit http://www.apcocoa.org/wiki/ApCoCoA:KnownIssues for bugs, problems 
// and known issues.
//
// ApCoCoALib is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License (version 3 or later)
// as published by the Free Software Foundation. A copy of the full
// licence may be found in the file COPYING in this directory.
//
// ApCoCoALib is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ApCoCoALib; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 US

#ifndef ApCoCoA_EvaluatedPolynomialsComplex_H 
#define ApCoCoA_EvaluatedPolynomialsComplex_H 

#include <complex>
 
////////////////////////////////////////////////////////////////////////////////
namespace ApCoCoA {
namespace NumericalAlgorithms{
namespace ABM{

   class EvaluatedPolynomialComplex{
      private:
      std::vector< std::complex<double> > myEvaluation;
      CoCoA::RingElem myPolynomial;
   
	  public:
      EvaluatedPolynomialComplex(CoCoA::RingElem polynomial, std::size_t NumPoints):myPolynomial(polynomial)
      {
        myEvaluation.resize(NumPoints);
      }
	   
      ~EvaluatedPolynomialComplex(){}
      std::vector< std::complex<double> >& getMyEvaluation(){return myEvaluation;}
      CoCoA::RingElem &	getMyPolynomial(){return myPolynomial;}
      void setMyPolynomial(CoCoA::RingElem r){myPolynomial = r;}
   };
   
   ////////////////////////////////////////////////////////////////////////////////

   typedef std::map<CoCoA::PPMonoidElem, EvaluatedPolynomialComplex* > EvaluatedPolynomialsBaseComplex;
 
   typedef std::list<EvaluatedPolynomialComplex*> OrderIdealComplex;
 
}  // end of namespace ABM
}  // end of namespace NumericalAlgorithms
}  // end of namespace ApCoCoA

#endif
