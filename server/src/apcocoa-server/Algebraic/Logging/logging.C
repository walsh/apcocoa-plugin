//
// This file is part of the source of ApCoCoALib, the ApCoCoA Library.
//
//   Copyright (c) ApCoCoA Project (Prof. Dr. Martin Kreuzer, Uni Passau)
//
//   Author: 2010 Stefan Kaspar
//
// Visit http://apcocoa.org/ for more information regarding ApCoCoA
// and ApCoCoALib.
// Visit http://www.apcocoa.org/wiki/ApCoCoA:KnownIssues for bugs, problems
// and known issues.
//
// ApCoCoALib is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License (version 3 or later)
// as published by the Free Software Foundation. A copy of the full
// licence may be found in the file COPYING in this directory.
//
// ApCoCoALib is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ApCoCoALib; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 US

#include "ApCoCoA/Logging/logging.H"

#include "CoCoA/library.H"

#include <boost/iostreams/device/null.hpp>
#include <boost/iostreams/stream.hpp>

#include <iostream>
using std::ostream;
using std::endl;
#include <string>
using std::string;

namespace ApCoCoA
{
  namespace Logging
  {
    namespace LogLevel
    {
      // Prefixes of log messages
      string prefixes[] = {
                            "[FATAL]  ",
                            "[ERROR]  ",
                            "[WARN]   ",
                            "[INFO]   ",
                            "[DEBUG]  "
                          };

      // Current log level; default: ERROR
      LoggingLevel CurrentLogLevel = ERROR;
    } // end of namespace LogLevel

    namespace { // anonymous namespace
      // Return reference to Boost null stream
      // (file local class)
      class NullStreamWrapper
      {
      public:
        NullStreamWrapper() { myNullStream.open(boost::iostreams::null_sink()); }
        ~NullStreamWrapper() { myNullStream.close(); }

        ostream& myGetNullStream() { return myNullStream; }

      private:
        boost::iostreams::stream<boost::iostreams::null_sink> myNullStream;
      };
      // (file local object)
      NullStreamWrapper NullStream;

      // Where to log to (file local pointer)
      ostream* LogOutputStreamPtr = &(CoCoA::LogStream());
      //ostream* LogOutputStreamPtr = &(CoCoA::GlobalLogput());
    } // end of anonymous namespace

    // Set logging output stream
    void SetLogOutputStream(ostream& out)
    {
      LogOutputStreamPtr = &out;
    }

    // Get global log level
    LogLevel::LoggingLevel GetLogLevel()
    {
      return LogLevel::CurrentLogLevel;
    }

    // Set global log level
    void SetLogLevel(const LogLevel::LoggingLevel lvl)
    {
      if (lvl >= LogLevel::FATAL && lvl <= LogLevel::DEBUG)
        LogLevel::CurrentLogLevel = lvl;
      else
        CoCoA_THROW_ERROR(CoCoA::ERR::BadArg, "SetLogLevel: Invalid new log level.");
    }

    // Main logging function
    ostream& log(const LogLevel::LoggingLevel lvl)
    {
      if (lvl <= GetLogLevel())
      {
        if (lvl < LogLevel::FATAL)
          CoCoA_THROW_ERROR(CoCoA::ERR::BadArg, "log: Invalid log level specified.");

        *LogOutputStreamPtr << LogLevel::prefixes[lvl];
        return *LogOutputStreamPtr;
      }

      return NullStream.myGetNullStream();
    }

    // Convenience logging functions
    ostream& LogFatal()
    {
      return log(LogLevel::FATAL);
    }

    ostream& LogError()
    {
      return log(LogLevel::ERROR);
    }

    ostream& LogWarning()
    {
      return log(LogLevel::WARNING);
    }

    ostream& LogInfo()
    {
      return log(LogLevel::INFO);
    }

    ostream& LogDebug()
    {
      return log(LogLevel::DEBUG);
    }
  } // end of namespace Logging
} // end of namespace ApCoCoA
