//
// This file is part of the source of ApCoCoALib, the ApCoCoA Library.
//
//   Copyright (c) ApCoCoA Project (Prof. Dr. Martin Kreuzer, Uni Passau)
//
//   Author: 2009 Stefan Kaspar
//
// Visit http://apcocoa.org/ for more information regarding ApCoCoA
// and ApCoCoALib.
// Visit http://www.apcocoa.org/wiki/ApCoCoA:KnownIssues for bugs, problems
// and known issues.
//
// ApCoCoALib is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License (version 3 or later)
// as published by the Free Software Foundation. A copy of the full
// licence may be found in the file COPYING in this directory.
//
// ApCoCoALib is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ApCoCoALib; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

#include "ApCoCoA/Algebraic/BorderBasis.H"
using ApCoCoA::AlgebraicCore::OrderIdeal;

#include "CoCoA/DenseMatrix.H"
#include "CoCoA/matrix.H"
#include "CoCoA/SparsePolyRing.H"
using namespace CoCoA;

#include <algorithm>
using std::count_if;
using std::find;
using std::find_if;
using std::greater;
//#include <cstddef> // Included in SparsePolyRing.H
using std::size_t;
#include <functional>
using std::binary_function;
//#include <iostream>
using std::ostream;
//#include <map>
using std::map;
//#include <set> // Included by OrderIdeal.H
using std::set;
#include <utility>
using std::make_pair;
//#include <vector>
using std::vector;
#include <iterator>
using std::distance;


namespace ApCoCoA
{
  namespace AlgebraicAlgorithms
  {
    namespace // anonymous namespace for file local definitions
    {
      // The classes below are used in the calls of std::count_if
      // and std::find_if
      // TODO: Remove duplicate code; see also BorderBasis.C.

      // Tests if the LPP of two polynomials are equal
      class LPPEquals
      {
      public:
        explicit LPPEquals(ConstRefRingElem f);
        bool operator()(ConstRefRingElem candidate);
      private:
        RingElem myPolynomial;
      };

      inline LPPEquals::LPPEquals(ConstRefRingElem f)
        : myPolynomial(f)
      {}

      inline bool LPPEquals::operator()(ConstRefRingElem candidate)
      {
        return (LPP(myPolynomial) == LPP(candidate));
      }

      // Tests if LPP(f) = t for a polynomial f and a term t
      class LPPEqualsTerm
      {
      public:
        explicit LPPEqualsTerm(ConstRefPPMonoidElem t);
        bool operator()(ConstRefRingElem f);
      private:
        PPMonoidElem myTerm;
      };

      inline LPPEqualsTerm::LPPEqualsTerm(ConstRefPPMonoidElem t)
        : myTerm(t)
      {}

      inline bool LPPEqualsTerm::operator()(ConstRefRingElem f)
      {
        return (LPP(f) == myTerm);
      }

      // Tests if a term is a factor of another term
      class IsFactorOf
      {
      public:
        explicit IsFactorOf(ConstRefPPMonoidElem t);
        bool operator()(ConstRefPPMonoidElem t);
      private:
        PPMonoidElem myTerm;
      };

      inline IsFactorOf::IsFactorOf(ConstRefPPMonoidElem t)
        : myTerm(t)
      {}

      inline bool IsFactorOf::operator()(ConstRefPPMonoidElem t)
      {
        return IsDivisible(myTerm, t);
      }

      // Test if leading term of a polynomial is not contained in an order ideal
      class LPPIsNotInOrderIdeal
      {
      public:
        explicit LPPIsNotInOrderIdeal(const OrderIdeal& O);
        bool operator()(ConstRefRingElem f);
      private:
        const OrderIdeal& myO;
      };

      inline LPPIsNotInOrderIdeal::LPPIsNotInOrderIdeal(const OrderIdeal& O):
          myO(O)
      {}

      inline bool LPPIsNotInOrderIdeal::operator()(ConstRefRingElem f)
      {
        return !myO.myContains(LPP(f));
      }

      // Tests if LPP(f1) < LPP(f2) for polynomials f1, f2
      class LPPLess : binary_function<RingElem, RingElem, bool>
      {
      public:
        result_type operator()(first_argument_type f1, second_argument_type f2)
        {
          return (LPP(f1) < LPP(f2));
        }
      };

    } // end of anonymous namespace

    /******************************************************************
     * Implementation of border basis computer class using polynomial *
     * representation                                                 *
     ******************************************************************/

    /************************
     * Public query methods *
     ************************/

    // Get all polynomials in V with specific term in their support
    void MatBorderBasisComputer::myGetVPolysWithTermInSupport(vector<RingElem>& polys,
                                                               ConstRefPPMonoidElem t) const
    {
      vector<RingElem> VPolys;
      myGetV(VPolys);

      vector<RingElem> TmpPolys;
      for (size_t i = 0; i < VPolys.size(); ++i)
        for (SparsePolyIter PolyIter = BeginIter(VPolys[i]); !IsEnded(PolyIter); ++PolyIter)
          if (PP(PolyIter) == t)
          {
            TmpPolys.push_back(VPolys[i]);
            break;
          }

      polys.swap(TmpPolys);
    }

    // Get all polynomials in V
    void MatBorderBasisComputer::myGetV(vector<RingElem>& polys) const
    {
      vector<RingElem> TmpPolys;
      for (BBReducerMap::const_iterator iter = myV.begin(); iter != myV.end(); ++iter)
        TmpPolys.push_back(iter->second);

      polys.swap(TmpPolys);
    }

    // Get leading terms of V
    void MatBorderBasisComputer::myGetVLTs(vector<PPMonoidElem>& LTs, long deg) const
    {
      vector<PPMonoidElem> TmpLTs;
      if (deg == 0)
        for (BBReducerMap::const_iterator iter = myV.begin(); iter != myV.end(); ++iter)
          TmpLTs.push_back(LPP(iter->second));
      else
        for (BBReducerMap::const_iterator iter = myV.begin(); iter != myV.end(); ++iter)
          if (StdDeg(LPP(iter->second)) == deg)
            TmpLTs.push_back(LPP(iter->second));

      LTs.swap(TmpLTs);
    }

    // Get leading terms of W'
    void MatBorderBasisComputer::myGetWPrimeLTs(vector<PPMonoidElem>& LTs,
                                                 long deg) const
    {
      vector<PPMonoidElem> TmpLTs;
      if (deg == 0)
        for (size_t i = 0; i < myWPrime.size(); ++i)
          TmpLTs.push_back(LPP(myWPrime[i]));
      else
        for (size_t i = 0; i < myWPrime.size(); ++i)
          if (StdDeg(LPP(myWPrime[i])) == deg)
            TmpLTs.push_back(LPP(myWPrime[i]));

      LTs.swap(TmpLTs);
    }

    // Output status information
    void MatBorderBasisComputer::myOutputSelf(ostream& out) const
    {
      out << "MatBorderBasisComputer("
          << "No. of steps executed=" << myGetNoStepsExecuted()
          << ",Current computation step=I" << myGetPrevCompStep()
          << ",Next computation step=I" << myGetNextCompStep()
          << ",Interactive computation=";
      if (IamComputingInteractively())
        out << "true";
      else
        out << "false";
      out << ",Border basis computation finished=";
      if (IamFinishedWithBBComp())
        out << "true";
      else
        out << "false";
      out << ")";
    }

    // Reset computation variables
    void MatBorderBasisComputer::myResetComputationVariables()
    {
      myUPtr->myClear();
      myV.clear();
      myOPtr->myClear();
      myWPrime.clear();
      myW.clear();
    }

    /**********************************************
     * Border Basis functions for steps (I1)-(I9) *
     **********************************************/

    typedef map< PPMonoidElem, RingElem, greater<PPMonoidElem> > BBReducerMap;

    // Compute a row echelon form of a matrix in a such a way that the first
    // k rows of the matrix are not being touched (but nevertheless are used
    // for reductions)
    void MatBorderBasisComputer::myREFByKeepingRows(matrix& R,
                                                    const ConstMatrixView& M,
                                                    const size_t k)
    {
      CoCoA_ASSERT(k < NumRows(M));

      const ring& K = CoCoA::RingOf(M);
      const size_t NUM_ROWS = NumRows(M),
                   NUM_COLS = NumCols(M);

      // Create working copy of M
      matrix RTmp = NewDenseMat(K, NUM_ROWS, NUM_COLS);
      for (size_t i = 0; i < NUM_ROWS; ++i)
        for (size_t j = 0; j < NUM_COLS; ++j)
          SetEntry(RTmp, i, j, M(i, j));

      // The matrix M is divided like this:
      // sector A = rows 0 to (k-1), sector B = rows k to (NUM_ROWS-1)
      size_t StartRowSecA = 0,
             StartRowSecB = k,
             col = 0;

      while (StartRowSecA + (StartRowSecB - k) < NUM_ROWS && col < NUM_COLS)
      {
        bool ReducerRowFoundInSecA = false;

        if (StartRowSecA < k)
          // Look for pivot element in section A
          if (!IsZero(RTmp(StartRowSecA, col)))
            ReducerRowFoundInSecA = true; // Section A is already in row echelon
                                        // form

        if (ReducerRowFoundInSecA)
        {
          // Reduce rows of section B with row of section A
          for (size_t i = StartRowSecB; i < NUM_ROWS; ++i)
            if (!IsZero(RTmp(i, col)))
              RTmp->myAddRowMul(i, StartRowSecA, -RTmp(i, col));

          ++StartRowSecA; // This row in section A is finished
        }
        else
          if (StartRowSecB < NUM_ROWS)
          {
            // Look for pivot element in section B
            if (IsZero(RTmp(StartRowSecB, col)))
            {
              size_t i = StartRowSecB + 1;
              for ( ; i < NUM_ROWS; ++i)
                if (!IsZero(RTmp(i, col)))
                {
                  RTmp->mySwapRows(i, StartRowSecB);
                  break;
                }

              // No pivot element found
              if (i == NUM_ROWS)
              {
                ++col;
                continue;
              }
            }

            // Reduce rows of section B with row of section B
            RTmp->myRowMul(StartRowSecB, one(K) / RTmp(StartRowSecB, col));
            for (size_t i = StartRowSecB + 1; i < NUM_ROWS; ++i)
              RTmp->myAddRowMul(i, StartRowSecB, -RTmp(i, col));

            ++StartRowSecB; // This row in section B is finished
          }

        ++col;
      }

      // Swap result into R
      swap(R, RTmp);
    }

    // Compute a set of polynomials W out of G such that elements of W \cup V have
    // pairwise different leading terms and leading coefficients equal to 1.
    //
    // V must be a set of non zero polynomials with pairwise different leading terms
    // and leading coefficients equal to 1.
    void MatBorderBasisComputer::myComputeKBasisExtension(vector<RingElem>& W,
                                                          const vector<RingElem>& G,
                                                          const BBReducerMap& V)
    {
      // Check if V meets the conditions stated above.
      // TODO: Use CoCoA_ASSERT here.
//      for (BBReducerMap::const_iterator iter = V.begin(); iter != V.end(); ++iter)
//      {
//        if (IsZero(iter->second))
//          CoCoA_ERROR(ERR::ZeroRingElem,
//                      "ComputeKBasisExtension: set V must not contain zero polynomial");
//        if (!IsOne(LC(iter->second)))
//          CoCoA_ERROR(ERR::nonstandard,
//                      "ComputeKBasisExtension: set V must not contain polynomials with leading coefficient not equal to 1");
//      }

      const std::size_t GSize = G.size();
      if (GSize == 0)
      {
        W.clear();
        return;
      }

      const SparsePolyRing& Kx = SparsePolyRing(owner(G[0]));

      // Write polynomials of V and G in matrix form M such that the first
      // k = V.size() rows correspond to the polynomials of V and form a row
      // echelon form, and that the other rows correspond to the polynomials
      // of G
      // TODO: Use a vector here instead of a set.
      typedef set< PPMonoidElem, greater<PPMonoidElem> > DescPPMElemSet;

      DescPPMElemSet SetOfAllTerms;
      for (BBReducerMap::const_iterator f = V.begin(); f != V.end(); ++f)
        for (SparsePolyIter PolyIter = BeginIter(f->second); !IsEnded(PolyIter); ++PolyIter)
          SetOfAllTerms.insert(PP(PolyIter));
      for (size_t i = 0; i < G.size(); ++i)
        for (SparsePolyIter PolyIter = BeginIter(G[i]); !IsEnded(PolyIter); ++PolyIter)
          SetOfAllTerms.insert(PP(PolyIter));

      size_t NUM_ROWS = V.size() + G.size(),
             NUM_COLS =  SetOfAllTerms.size();
      matrix M = NewDenseMat(CoeffRing(Kx), NUM_ROWS, NUM_COLS);
      DescPPMElemSet::const_iterator IterToTerm;
      size_t row = 0;
      for (BBReducerMap::const_iterator f = V.begin(); f != V.end(); ++f, ++row)
        for (SparsePolyIter PolyIter = BeginIter(f->second); !IsEnded(PolyIter); ++PolyIter)
        {
          IterToTerm = SetOfAllTerms.find(PP(PolyIter));
          SetEntry(M, row, distance<DescPPMElemSet::const_iterator>(SetOfAllTerms.begin(), IterToTerm), coeff(PolyIter));
        }
      for (size_t i = 0; i < G.size(); ++i, ++row)
        for (SparsePolyIter PolyIter = BeginIter(G[i]); !IsEnded(PolyIter); ++PolyIter)
        {
          IterToTerm = SetOfAllTerms.find(PP(PolyIter));
          SetEntry(M, row, distance<DescPPMElemSet::const_iterator>(SetOfAllTerms.begin(), IterToTerm), coeff(PolyIter));
        }
// VERBOSE
//      // Print some information about how sparse the matrix "M" is
//      cout << "Matrix size: " << NUM_ROWS << " x " << NUM_COLS << endl;
//      size_t NonZeros = 0;
//      for (size_t i = 0; i < NUM_ROWS; ++i)
//        for (size_t j = 0; j < NUM_COLS; ++j)
//          if (!IsZero(M(i, j)))
//              ++NonZeros;
//      cout << "Non-zero entries: " << NonZeros << " / " << (NUM_ROWS * NUM_COLS)
//           << " ("
//           << ((double) NonZeros) * 100.0 / ((double) NUM_ROWS * NUM_COLS)
//           << " %)" << endl;
// VERBOSE

      // Compute a row echelon form R of M in a special way
      matrix R = NewDenseMat(CoeffRing(Kx), NUM_ROWS, NUM_COLS);
      myREFByKeepingRows(R, M, V.size());

      // Extract polynomials != 0 from R beginning with row k+1
      vector<RingElem> WTmp;
      RingElem f(zero(Kx));
      // The following is not really clever...
      vector<PPMonoidElem> VectorOfAllTerms(SetOfAllTerms.begin(), SetOfAllTerms.end());
      for (size_t i = V.size(); i < NUM_ROWS; ++i)
      {
        for (size_t j = 0; j < NUM_COLS; ++j)
          if (!IsZero(R(i, j)))
          {
            vector<long> expv;
            exponents(expv, VectorOfAllTerms[j]);
            PushBack(f, R(i, j), expv);
          }

        if (!IsZero(f))
          WTmp.push_back(f);

        f = zero(Kx);
      }

      W.swap(WTmp);
    }

    // (I1) Create order ideal U spanned by Supp(gens)
    // !!! Might not be exception safe !!!
    void MatBorderBasisComputer::myDoStepI1(OrderIdeal& U,
                                            const vector<RingElem>& gens)
    {
      set<PPMonoidElem> SupportOfGens;

      for (size_t i = 0; i < gens.size(); ++i)
        for (SparsePolyIter PolyIter = BeginIter(gens[i]); !IsEnded(PolyIter); ++PolyIter)
          SupportOfGens.insert(PP(PolyIter));

      U.myEnlarge(SupportOfGens); // Exception safe?
    }

    // (I2) Compute a vector space basis V of <gens>_K
    void MatBorderBasisComputer::myDoStepI2(BBReducerMap& V,
                                            const vector<RingElem>& gens)
    {
      vector<RingElem> VAsVector;
      BBReducerMap VTmp;

      myComputeKBasisExtension(VAsVector, gens, V);

      for (size_t i = 0; i < VAsVector.size(); ++i)
        VTmp.insert(make_pair(LPP(VAsVector[i]), VAsVector[i]));

      V.swap(VTmp);
    }

    // (I3) Compute basis extension W for V^+
    void MatBorderBasisComputer::myDoStepI3(vector<RingElem>& WPrime,
                                            const BBReducerMap& V)
    {
      CoCoA_ASSERT(!V.empty());

      // Compute V^+
      const SparsePolyRing& Kx = SparsePolyRing(owner(V.begin()->second));
      const vector<RingElem>& x = indets(Kx);
      vector<RingElem> VPlus;
      for (BBReducerMap::const_iterator v = V.begin(); v != V.end(); ++v)
        for (long i = 0; i < NumIndets(Kx); ++i)
          VPlus.push_back(x[i] * v->second);

      myComputeKBasisExtension(WPrime, VPlus, V);
    }

    // (I4) Collect in W all polynomials from WPrime whose leading terms are
    // contained in U
    void MatBorderBasisComputer::myDoStepI4(vector<RingElem>& W,
                                            const vector<RingElem>& WPrime,
                                            const OrderIdeal& U)
    {
      vector<RingElem> WTmp;
      remove_copy_if(WPrime.begin(),
                     WPrime.end(),
                     back_inserter(WTmp),
                     LPPIsNotInOrderIdeal(U));
      W.swap(WTmp);
    }

    // (I5) If Supp(W) is not contained in U then replace U with the order
    // ideal spanned by U and Supp(W)
    // !!! Might not be exception safe !!!
    bool MatBorderBasisComputer::myDoStepI5(OrderIdeal& U,
                                            const vector<RingElem>& W)
    {
      // For each w in W collect terms in support of w that are not contained
      // in U; these terms will be used to enlarge U later
      set<PPMonoidElem> UEnlarger;
      for (size_t i = 0; i < W.size(); ++i)
        for (SparsePolyIter PolyIter = BeginIter(W[i]); !IsEnded(PolyIter); ++PolyIter)
          if (!U.myContains(PP(PolyIter)))
            // Found a term in support of w which will be used to enlarge U later
            UEnlarger.insert(PP(PolyIter));

      // If the union of support terms of all polynomials in W is not contained
      // in U then U must be enlarged, otherwise break out of the loop
      if (UEnlarger.empty())
        return false;

      U.myEnlarge(UEnlarger); // Exception safe?

      return true;
    }

    // (I6) Check if W is empty and replace V with V \cup W if necessary
    // !!! Not exception safe !!!
    bool MatBorderBasisComputer::myDoStepI6(BBReducerMap& V,
                                            const vector<RingElem>& W)
    {
      if (W.empty())
        return false;

      // Exception safety violated below
      for (size_t i = 0; i < W.size(); ++i)
        V.insert(make_pair(LPP(W[i]), W[i]));

      return true;
    }

    // (I7) Construct order ideal O := U \ { LPP(v) | v in V } (see
    // "BorderBasis.txt" for details)
    // !!! Not exception safe !!!
    void MatBorderBasisComputer::myDoStepI7(OrderIdeal& O,
                                            const BBReducerMap& V,
                                            const OrderIdeal& U)
    {
      set<PPMonoidElem> LPPV, OEnlarger;
      // Collect leading terms of polynomials in V
      for (BBReducerMap::const_iterator v = V.begin(); v != V.end(); ++v)
        LPPV.insert(v->first);

      // Collect each generator term in U that is not divisible by any v in
      // LPPV
      for (set<PPMonoidElem>::const_iterator u = U.myGenerators().begin();
           u != U.myGenerators().end();
           ++u)
        if (find_if(LPPV.begin(), LPPV.end(), IsFactorOf(*u)) == LPPV.end())
          OEnlarger.insert(*u);

      // Collect possible generators of O
      const SparsePolyRing& Kx = SparsePolyRing(owner(V.begin()->second));
      const PPMonoid& PPMon = PPM(Kx);
      for (set<PPMonoidElem>::const_iterator v = LPPV.begin(); v != LPPV.end(); ++v)
        for (long xi = 0; xi < NumIndets(Kx); ++xi)
          if (exponent(*v, xi) != 0)
            if (find_if(LPPV.begin(), LPPV.end(), IsFactorOf((*v) / indet(PPMon, xi))) == LPPV.end())
              OEnlarger.insert((*v) / indet(PPMon, xi));

      // TODO: Write swap method for class OrderIdeal
      O.myClear();
      O.myEnlarge(OEnlarger);
    }

    // (I8) Check if border is contained in U (for details see "BorderBasis.txt")
    // !!! Might not be exception safe !!!
    bool MatBorderBasisComputer::myDoStepI8(OrderIdeal& U,
                                            size_t EnlargementCounter,
                                            const OrderIdeal& O,
                                            const size_t N)
    {
      const PPMonoid& PPMon = O.myPPM();
      const vector<PPMonoidElem>& x = indets(PPMon);
      set<PPMonoidElem> UEnlarger;
      for (set<PPMonoidElem>::const_iterator t = O.myGenerators().begin();
           t != O.myGenerators().end();
           ++t)
        for (long i = 0; i < NumIndets(PPMon); ++i)
            if (!U.myContains((*t) * x[i]))
              UEnlarger.insert((*t) * x[i]);

      // Enlarge U if necessary
      if (UEnlarger.empty())
        return false;

      // Every N-th loop the computing universe U will be exchanged for U^+
      ++EnlargementCounter;
      if (EnlargementCounter == N)
      {
        EnlargementCounter = 0;

        // Compute terms that will enlarge U
        UEnlarger.clear();
        for (set<PPMonoidElem>::const_iterator u = U.myGenerators().begin();
             u != U.myGenerators().end();
             ++u)
          for (long i = 0; i < NumIndets(PPMon); ++i)
            UEnlarger.insert(x[i] * (*u));
      }
      U.myEnlarge(UEnlarger); // Exception safe?

      return true;
    }

    // (I9) Apply Final Reduction Algorithm and return the border basis
    // polynomials computed by it
    void MatBorderBasisComputer::myDoStepI9(vector<RingElem>& BBasis,
                                            const BBReducerMap& V,
                                            const OrderIdeal& O)
    {
      if (V.empty())
        return;

      vector<RingElem> VAsVector;
      for (BBReducerMap::const_iterator v = V.begin(); v != V.end(); ++v)
        VAsVector.push_back(v->second);

      // Extract a O_\sigma{Id(gens)}-border basis from a given set of
      // polynomials V and an order ideal O
      const SparsePolyRing Kx = SparsePolyRing(owner(VAsVector[0]));
      const ring& K = CoeffRing(Kx);
      const RingHom& EmbedCoeff = CoeffEmbeddingHom(Kx);

      vector<RingElem> VSorted(VAsVector.begin(), VAsVector.end());
      sort(VSorted.begin(), VSorted.end(), LPPLess());
      vector<RingElem> TmpBBasis, W;
      RingElem reducer(Kx), LeadingCoeff(K);
      SparsePolyIter t = BeginIter(VSorted[0]); // Initialization cannot be omitted here

      for (size_t i = 0; i < VSorted.size(); ++i)
      {
        // Polynomial that will be used to reduce v if necessary
        reducer = 0;

        // Determine the terms in v that need to be reduced
        t = BeginIter(VSorted[i]);
        LeadingCoeff = coeff(t); // Save LC(VSorted[i])
        ++t; // Ignore LPP(VSorted[i])
        for (; !IsEnded(t); ++t)
        {
          // Check if term is element of O
          if (O.myContains(PP(t)))
            continue;

          // If t is not an element of O there is an element w in W with
          // LPP(w) = t
          vector<RingElem>::iterator w = find_if(W.begin(), W.end(), LPPEqualsTerm(PP(t)));
          CoCoA_ASSERT(w != W.end());

          // Update reducing polynomial
          reducer -= EmbedCoeff(coeff(t)) * (*w);
        }

        W.push_back((VSorted[i] + reducer) / EmbedCoeff(LeadingCoeff));
      }

      // Collect border basis polynomials
      const vector<PPMonoidElem>& x = indets(PPM(Kx));
      PPMonoidElem LT(PPM(Kx));
      for (size_t i = 0; i < W.size(); ++i)
      {
        // Check if W[i] is a border polynomial
        LT = LPP(W[i]);
        for (long j = 0; j < NumIndets(Kx); ++j)
          if (IsDivisible(LT, x[j]) && O.myContains(LT / x[j]))
          {
            TmpBBasis.push_back(W[i]);
            break;
          }
      }

      BBasis.swap(TmpBBasis);
    }

    // Output status of border basis computer object
    ostream& operator<<(ostream& out, const MatBorderBasisComputer& comp)
    {
      comp.myOutputSelf(out);
      return out;
    }
  } // end of namespace AlgebraicAlgorithms
} // end of namespace ApCoCoA
