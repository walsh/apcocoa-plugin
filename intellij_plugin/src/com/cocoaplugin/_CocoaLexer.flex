package com.cocoaplugin;

import com.intellij.lexer.FlexLexer;
import com.intellij.psi.tree.IElementType;

import static com.intellij.psi.TokenType.BAD_CHARACTER;
import static com.intellij.psi.TokenType.WHITE_SPACE;
import static com.cocoaplugin.psi.CocoaTypes.*;

%%

%{
  public _CocoaLexer() {
    this((java.io.Reader)null);
  }
%}

%public
%class _CocoaLexer
%implements FlexLexer
%function advance
%type IElementType
%unicode

EOL=\R
WHITE_SPACE=\s+

WHITE_SPACE=[ \t\n\x0B\f\r]+
COMMENT=--.*|"//".*
BLOCK_COMMENT="/"\*(.|\n)*\*"/"
NUMBER=[0-9]+(\.[0-9]*)?
STRING=('([^'\\]|\\.)*'|\"([^\"\\]|\\.)*\")
OPERATOR=\+|-|\*|"/"|%|\^|<|>|<>|=|>=|<=|and|And|or|Or
RESERVED=export|DegLex|DegRevLex|Lex|Xel|Alias|[Aa]nd|ciao|try|endtry|[Ee]nd[Ff]oreach|[Ee]ndif|[Ee]nd[Pp]ackage|[Ee]nd[Rr]epeat|[Ee]nd[Ww]hile|[Ff]alse|[Ff]or[Ee]ach|[Ii]f|[Ii]n|isin|[Nn]ot|[O]or|[Pp]ackage|[Rr]epeat|record|[Rr]eturn|[Ss]ource|[Tt]hen|[Tt]o|[Tt]rue|[Uu]ntil|[Uu]se|[Ww]hile|QQ|ZZ|[Dd]efine|[Ee]nd[Dd]efine|[Ff]or|[Ee]nd[Ff]or|[Ee]lse|[Ee]nd[Ii]f|[Dd]o
IDENTIFIER=[_a-zA-Z][_a-zA-Z0-9]*

%%
<YYINITIAL> {
  {WHITE_SPACE}        { return WHITE_SPACE; }

  "="                  { return OP_EQ; }
  "::="                { return OP_RING; }
  ":="                 { return OP_ASSIGN; }
  ":"                  { return OP_COLON; }
  "or"                 { return OR; }
  "and"                { return AND; }
  "not"                { return NOT; }
  "true"               { return TRUE; }
  "false"              { return FALSE; }
  "define"             { return DEFINE; }
  "enddefine"          { return ENDDEFINE; }
  "for"                { return FOR; }
  "endfor"             { return ENDFOR; }
  "do"                 { return DO; }
  "if"                 { return IF; }
  "endif"              { return ENDIF; }
  "else"               { return ELSE; }
  "elif"               { return ELIF; }
  "while"              { return WHILE; }
  "enwhile"            { return ENDWHILE; }
  "?"                  { return OP_QUERY; }
  ";"                  { return SEMICOLON; }
  "{"                  { return LEFT_BRACE; }
  "}"                  { return RIGHT_BRACE; }
  "["                  { return LEFT_BRACKET; }
  "]"                  { return RIGHT_BRACKET; }
  "("                  { return LEFT_PAREN; }
  ")"                  { return RIGHT_PAREN; }
  "<<"                 { return EXTERNAL_START; }
  ">>"                 { return EXTERNAL_END; }
  "<"                  { return LT; }
  ">"                  { return GT; }
  "<="                 { return LEQ; }
  ">="                 { return GEQ; }
  ","                  { return COMMA; }

  {WHITE_SPACE}        { return WHITE_SPACE; }
  {COMMENT}            { return COMMENT; }
  {BLOCK_COMMENT}      { return BLOCK_COMMENT; }
  {NUMBER}             { return NUMBER; }
  {STRING}             { return STRING; }
  {OPERATOR}           { return OPERATOR; }
  {RESERVED}           { return RESERVED; }
  {IDENTIFIER}         { return IDENTIFIER; }

}

[^] { return BAD_CHARACTER; }
