package com.cocoaplugin;

import com.cocoaplugin.language.CocoaLanguage;
import com.intellij.openapi.fileTypes.LanguageFileType;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;

public class CocoaPackageFileType extends LanguageFileType {
    public static final CocoaPackageFileType INSTANCE = new CocoaPackageFileType();

    private CocoaPackageFileType() {
        super(CocoaLanguage.INSTANCE);
    }

    @NotNull
    @Override
    public String getName() {
        return "Cocoa5 Package file";
    }

//    @NotNull
//    @Override
//    public String getDisplayName() {
//        return "Cocoa5 Package";
//    }

    @NotNull
    @Override
    public String getDescription() {
        return "Cocoa5 Package file";
    }

    @NotNull
    @Override
    public String getDefaultExtension() {
        return "cpkg5";
    }

    @Nullable
    @Override
    public Icon getIcon() {
        return CocoaIcons.FILE;
    }
}