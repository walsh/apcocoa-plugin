package com.cocoaplugin;

import com.cocoaplugin.language.CocoaLanguage;
import com.intellij.formatting.*;
import com.intellij.lang.ASTNode;
import com.intellij.openapi.util.TextRange;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiFile;
import com.intellij.psi.codeStyle.CodeStyleSettings;
import com.cocoaplugin.psi.CocoaTypes;
import org.jetbrains.annotations.*;

public class CocoaFormattingModelBuilder implements FormattingModelBuilder {

//    @Override
//    public @NotNull FormattingModel createModel(@NotNull FormattingContext formattingContext) {
//        final CodeStyleSettings codeStyleSettings = formattingContext.getCodeStyleSettings();
//        return FormattingModelProvider
//                .createFormattingModelForPsiFile(formattingContext.getContainingFile(),
//                        new CocoaBlock(formattingContext.getNode(),
//                                Wrap.createWrap(WrapType.NONE, false),
//                                Alignment.createAlignment(),
//                                createSpaceBuilder(codeStyleSettings)),
//                        codeStyleSettings);
//    }

    private static SpacingBuilder createSpaceBuilder(CodeStyleSettings settings) {
        return new SpacingBuilder(settings, CocoaLanguage.INSTANCE)
                .around(CocoaTypes.OPERATOR)
                .spaceIf(settings.getCommonSettings(CocoaLanguage.INSTANCE.getID()).SPACE_AROUND_ASSIGNMENT_OPERATORS)
                .before(CocoaTypes.IDENTIFIER)
                .none();
    }

    @NotNull
    @Override
    public FormattingModel createModel(PsiElement psiElement, CodeStyleSettings codeStyleSettings) {
        return null;
    }

    @Nullable
    @Override
    public TextRange getRangeAffectingIndent(PsiFile file, int offset, ASTNode elementAtOffset) {
        return FormattingModelBuilder.super.getRangeAffectingIndent(file, offset, elementAtOffset);
    }
}
