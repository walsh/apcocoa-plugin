package com.cocoaplugin.language;
import com.cocoaplugin.CocoaLexerAdapter;
import com.intellij.lexer.Lexer;
import com.intellij.openapi.editor.*;
import com.intellij.openapi.editor.colors.TextAttributesKey;
import com.intellij.openapi.fileTypes.SyntaxHighlighterBase;
import com.intellij.psi.TokenType;
import com.intellij.psi.tree.IElementType;
import com.cocoaplugin.psi.CocoaTypes;
import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.intellij.openapi.editor.colors.TextAttributesKey.createTextAttributesKey;

public class CocoaSyntaxHighlighter extends SyntaxHighlighterBase {
    public static final TextAttributesKey LEFT_PAREN =
            createTextAttributesKey("LEFT_PAREN", DefaultLanguageHighlighterColors.BRACKETS);
    public static final TextAttributesKey RIGHT_PAREN =
            createTextAttributesKey("RIGHT_PAREN", DefaultLanguageHighlighterColors.BRACKETS);
    public static final TextAttributesKey OPERATOR =
            createTextAttributesKey("COCOA_SEPARATOR", DefaultLanguageHighlighterColors.OPERATION_SIGN);
    public static final TextAttributesKey RESERVED =
            createTextAttributesKey("COCOA_KEY", DefaultLanguageHighlighterColors.KEYWORD);
    public static final TextAttributesKey STRING =
            createTextAttributesKey("COCOA_VALUE", DefaultLanguageHighlighterColors.STRING);
    public static final TextAttributesKey COMMENT =
            createTextAttributesKey("COCOA_COMMENT", DefaultLanguageHighlighterColors.LINE_COMMENT);
    public static final TextAttributesKey BLOCK_COMMENT =
            createTextAttributesKey("COCOA_BLOCK_COMMENT", DefaultLanguageHighlighterColors.BLOCK_COMMENT);
    public static final TextAttributesKey BAD_CHARACTER =
            createTextAttributesKey("COCOA_BAD_CHARACTER", HighlighterColors.BAD_CHARACTER);

    private static final TextAttributesKey[] BAD_CHAR_KEYS = new TextAttributesKey[]{BAD_CHARACTER};
    private static final TextAttributesKey[] OPERATOR_KEYS = new TextAttributesKey[]{OPERATOR};
    private static final TextAttributesKey[] RESERVED_KEYS = new TextAttributesKey[]{RESERVED};
    private static final TextAttributesKey[] STRING_KEYS = new TextAttributesKey[]{STRING};
    private static final TextAttributesKey[] COMMENT_KEYS = new TextAttributesKey[]{COMMENT};
    private static final TextAttributesKey[] BLOCK_COMMENT_KEYS = new TextAttributesKey[]{BLOCK_COMMENT};
    private static final TextAttributesKey[] LEFT_PAREN_KEYS = new TextAttributesKey[]{LEFT_PAREN};
    private static final TextAttributesKey[] RIGHT_PAREN_KEYS = new TextAttributesKey[]{RIGHT_PAREN};




    private static final TextAttributesKey[] EMPTY_KEYS = new TextAttributesKey[0];

    @NotNull
    @Override
    public Lexer getHighlightingLexer() {
        return new CocoaLexerAdapter();
    }

    @NotNull
    @Override
    public TextAttributesKey[] getTokenHighlights(IElementType tokenType) {
        List<IElementType> reserved = Stream.of(CocoaTypes.RESERVED, CocoaTypes.AND, CocoaTypes.OR, CocoaTypes.NOT,
                                                CocoaTypes.FOR, CocoaTypes.ENDFOR, CocoaTypes.WHILE, CocoaTypes.ENDWHILE,
                                                CocoaTypes.DEFINE, CocoaTypes.ENDDEFINE).collect(Collectors.toList());
        if (reserved.contains(tokenType)) {
            return RESERVED_KEYS;
        } else if (tokenType.equals(CocoaTypes.STRING)) {
            return STRING_KEYS;
        } else if (tokenType.equals(CocoaTypes.COMMENT)) {
            return COMMENT_KEYS;
        }
        else if (tokenType.equals(CocoaTypes.BLOCK_COMMENT)) {
            return BLOCK_COMMENT_KEYS;
        }
        else if (tokenType.equals(CocoaTypes.LEFT_PAREN)) {
            return LEFT_PAREN_KEYS;
        }
        else if (tokenType.equals(CocoaTypes.RIGHT_PAREN)) {
            return RIGHT_PAREN_KEYS;
        }
        else if (tokenType.equals(TokenType.BAD_CHARACTER)) {
            return BAD_CHAR_KEYS;
        }
        else {
            return EMPTY_KEYS;
        }
    }
}
