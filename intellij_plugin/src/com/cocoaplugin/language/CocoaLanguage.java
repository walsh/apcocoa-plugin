package com.cocoaplugin.language;

import com.intellij.lang.Language;

public class CocoaLanguage extends Language {
    public static final CocoaLanguage INSTANCE = new CocoaLanguage();

    private CocoaLanguage() {
        super("Cocoa");
    }
}
