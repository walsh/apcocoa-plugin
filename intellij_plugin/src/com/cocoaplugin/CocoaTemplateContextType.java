package com.cocoaplugin;


import com.cocoaplugin.language.CocoaLanguage;
import com.intellij.codeInsight.template.TemplateContextType;
import com.intellij.psi.PsiFile;
import com.intellij.psi.util.PsiUtilCore;
import org.jetbrains.annotations.NotNull;

public class CocoaTemplateContextType extends TemplateContextType {

    protected CocoaTemplateContextType() {
        super("COCOA", "CoCoA");
    }

    @Override
    public boolean isInContext(@NotNull final PsiFile file, final int offset) {
       return PsiUtilCore.getLanguageAtOffset(file, offset).isKindOf(CocoaLanguage.INSTANCE);
    }

}