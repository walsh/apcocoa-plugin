package com.cocoaplugin.psi;

import com.cocoaplugin.CocoaFileType;
import com.cocoaplugin.language.CocoaLanguage;
import com.intellij.extapi.psi.PsiFileBase;
import com.intellij.openapi.fileTypes.FileType;
import com.intellij.psi.FileViewProvider;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;

public class CocoaFile extends PsiFileBase {
    public CocoaFile(@NotNull FileViewProvider viewProvider) {
        super(viewProvider, CocoaLanguage.INSTANCE);
    }

    @NotNull
    @Override
    public FileType getFileType() {
        return CocoaFileType.INSTANCE;
    }

    @Override
    public String toString() {
        return "Cocoa File";
    }

    @Override
    public Icon getIcon(int flags) {
        return super.getIcon(flags);
    }
}
