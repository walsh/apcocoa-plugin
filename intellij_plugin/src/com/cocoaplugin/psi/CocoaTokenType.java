package com.cocoaplugin.psi;

import com.cocoaplugin.language.CocoaLanguage;
import com.intellij.psi.tree.IElementType;
import org.jetbrains.annotations.NonNls;
import org.jetbrains.annotations.NotNull;

public class CocoaTokenType extends IElementType {
    public CocoaTokenType(@NotNull @NonNls String debugName) {
        super(debugName, CocoaLanguage.INSTANCE);
    }

    @Override
    public String toString() {
        return "CocoaTokenType." + super.toString();
    }
}