package com.cocoaplugin;

import com.cocoaplugin.language.CocoaLanguage;
import com.intellij.openapi.fileTypes.LanguageFileType;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;

public class CocoaFileType extends LanguageFileType {
    public static final CocoaFileType INSTANCE = new CocoaFileType();

    private CocoaFileType() {
        super(CocoaLanguage.INSTANCE);
    }

    @NotNull
    @Override
    public String getName() {
        return "Cocoa file";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Cocoa language file";
    }

    @NotNull
    @Override
    public String getDefaultExtension() {
        return "cocoa5";
    }

    @Nullable
    @Override
    public Icon getIcon() {
        return CocoaIcons.FILE;
    }
}