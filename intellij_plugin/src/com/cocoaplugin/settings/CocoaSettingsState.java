package com.cocoaplugin.settings;

import com.cocoaplugin.CocoaResources;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.components.PersistentStateComponent;
import com.intellij.openapi.components.ServiceManager;
import com.intellij.openapi.components.State;
import com.intellij.openapi.components.Storage;
import com.intellij.util.xmlb.XmlSerializerUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@State(
        name = "com.cocoaplugin.settings.CocoaSettingsState",
        storages = @Storage("SdkSettingsPlugin.xml")
)
public class CocoaSettingsState implements PersistentStateComponent<CocoaSettingsState> {

    public String interpreterPath = CocoaResources.getInterpreterPath();
    public String wslPath = CocoaResources.getWslPath();
    public boolean useWsl = false;
    public String cocoaParams = CocoaResources.getDefaultParams();
    public String serverParams = CocoaResources.getDefaultServerParams();

    public static CocoaSettingsState getInstance() {
        return ServiceManager.getService(CocoaSettingsState.class);
        //return ApplicationManager.getApplication().getService(CocoaSettingsState.class,true);
    }

    @Nullable
    @Override
    public CocoaSettingsState getState() {
        return this;
    }

    @Override
    public void loadState(@NotNull CocoaSettingsState state) {
        XmlSerializerUtil.copyBean(state, this);
    }

}
