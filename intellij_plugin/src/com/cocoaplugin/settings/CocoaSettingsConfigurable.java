package com.cocoaplugin.settings;

import com.intellij.openapi.options.Configurable;
import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;


public class CocoaSettingsConfigurable implements Configurable {

    private CocoaSettingsComponent mySettingsComponent;

    // A default constructor with no arguments is required because this implementation
    // is registered as an applicationConfigurable EP

    @Nls(capitalization = Nls.Capitalization.Title)
    @Override
    public String getDisplayName() {
        return "ApCoCoA Settings";
    }

    @Override
    public JComponent getPreferredFocusedComponent() {
        return mySettingsComponent.getPreferredFocusedComponent();
    }

    @Nullable
    @Override
    public JComponent createComponent() {
        mySettingsComponent = new CocoaSettingsComponent();
        return mySettingsComponent.getPanel();
    }

    @Override
    public boolean isModified() {
        CocoaSettingsState settings = CocoaSettingsState.getInstance();
        boolean modified = !mySettingsComponent.getInterpreterPath().equals(settings.interpreterPath);
        modified |= mySettingsComponent.getWslStatus() != settings.useWsl;
        modified |= mySettingsComponent.getCocoaParams() != settings.cocoaParams;
        modified |= mySettingsComponent.getServerParams() != settings.serverParams;
        modified |= mySettingsComponent.getWslPath() != settings.wslPath;
        return modified;
    }

    @Override
    public void apply() {
        CocoaSettingsState settings = CocoaSettingsState.getInstance();
        settings.useWsl = mySettingsComponent.getWslStatus();
        settings.cocoaParams = mySettingsComponent.getCocoaParams();
        settings.serverParams = mySettingsComponent.getServerParams();
        settings.wslPath = mySettingsComponent.getWslPath();
        settings.interpreterPath = mySettingsComponent.getInterpreterPath();
    }

    @Override
    public void reset() {
        CocoaSettingsState settings = CocoaSettingsState.getInstance();
        mySettingsComponent.setInterpreterPath(settings.interpreterPath);
        mySettingsComponent.setWslPath(settings.wslPath);
        mySettingsComponent.setWslStatus(settings.useWsl);
        mySettingsComponent.setCocoaParams(settings.cocoaParams);
        mySettingsComponent.setServerParams(settings.serverParams);
    }

    @Override
    public void disposeUIResources() {
        mySettingsComponent = null;
    }

}
