package com.cocoaplugin.settings;

import com.intellij.ui.components.JBCheckBox;
import com.intellij.ui.components.JBLabel;
import com.intellij.ui.components.JBTextField;
import com.intellij.util.ui.FormBuilder;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;


public class CocoaSettingsComponent {

    private final JPanel myMainPanel;
    private final JBTextField interpreterPath = new JBTextField();
    private final JBTextField wslPath = new JBTextField();
    private final JBCheckBox wslStatus = new JBCheckBox("Use Windows Subsystem for Linux (WSL)");
    private final JBTextField cocoaParams = new JBTextField();
    private final JBTextField serverParams = new JBTextField();

    public CocoaSettingsComponent() {
        myMainPanel = FormBuilder.createFormBuilder()
                .addLabeledComponent(new JBLabel("Interpreter Path:"), interpreterPath, 1, true)
                .addLabeledComponent(new JBLabel("WSL Path:"), wslPath, 1, true)
                .addComponent(wslStatus, 1)
                .addLabeledComponent(new JBLabel("CoCoA Parameters:"), cocoaParams, 1, true)
                .addLabeledComponent(new JBLabel("ApCoCoA Server Parameters:"), serverParams, 1, true)
                .addComponentFillVertically(new JPanel(), 0)
                .getPanel();
        String OS = System.getProperty("os.name").toLowerCase();
        if (!OS.contains("windows")){
            wslPath.setEnabled(false);
            wslStatus.setEnabled(false);
        }
    }

    public JPanel getPanel() {
        return myMainPanel;
    }

    public JComponent getPreferredFocusedComponent() {
        return interpreterPath;
    }

    @NotNull
    public String getInterpreterPath() {
        return interpreterPath.getText();
    }

    public void setInterpreterPath(@NotNull String newText) {
        interpreterPath.setText(newText);
    }

    @NotNull
    public String getWslPath() {
        return wslPath.getText();
    }

    public void setWslPath(@NotNull String newText) {
        wslPath.setText(newText);
    }

    public boolean getWslStatus() {return wslStatus.isSelected();}

    public void setWslStatus(boolean newStatus) {
        wslStatus.setSelected(newStatus);
    }

    public String getCocoaParams() {return cocoaParams.getText();}

    public void setCocoaParams(String newText) {cocoaParams.setText(newText);}

    public String getServerParams() {return serverParams.getText();}

    public void setServerParams(String newText) {serverParams.setText(newText);}
}