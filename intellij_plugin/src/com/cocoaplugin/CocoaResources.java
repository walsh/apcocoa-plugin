package com.cocoaplugin;

import org.yaml.snakeyaml.util.UriEncoder;

import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;

public class CocoaResources {

    private static final String WINDOWS_INTERPRETER = "cocoa/CoCoAInterpreter.exe";
    private static final String LINUX_INTERPRETER = "cocoa/CoCoAInterpreter";
    private static final String LINUX_SERVER = "cocoa/packages/apcocoa-packages/binaries/ApCoCoAServer1";
    private static final String WINDOWS_SERVER = "cocoa/packages/apcocoa-packages/binaries/ApCoCoAServer1.exe";
    private static final String COCOA_PARAMS = "--no-readline --enable-system-cmd --no-preamble --prompt §%##---------------Done---------------§%##";
    private static final String SERVER_PARAMS = "-p 49344";

    private static String getPathtoResources() {
        // this is an ugly workaround to find the path to the plugin jar file
        // in the same folder under "cocoa" all external resources are located
        String resourcesPath = CocoaResources.class.getClassLoader().getResource("file").getFile();
        resourcesPath = URLDecoder.decode(resourcesPath, StandardCharsets.UTF_8);
        //remove "/file:" at the beginning of the path
        resourcesPath = resourcesPath.substring(5,resourcesPath.length()-24);
        return resourcesPath;
    }

    // this converts a path to the correct format for the Windows Subsystem for Linux
    // it assumes that the path is of the form /C:/...
    // if the name of the drive is not a single letter, this code does not work
    private static String convertToWslPath(String path) {
        String driveLetter = String.valueOf(path.charAt(1));
        driveLetter = driveLetter.toLowerCase();
        return "/mnt/"+driveLetter+path.substring(3);
    }

    // returns the path to the cocoa executable
    public static String getInterpreterPath() {
        String OS = System.getProperty("os.name").toLowerCase();
        String resourcesPath = getPathtoResources();
        if(OS.contains("windows")) {
            return resourcesPath + WINDOWS_INTERPRETER;
        }
        else {
            return resourcesPath + LINUX_INTERPRETER;
        }
    }

    public static String getWslPath() {
        String resourcesPath = getPathtoResources();
        return convertToWslPath(resourcesPath) + LINUX_INTERPRETER;
    }

    public static String getWslServerPath() {
        String resourcesPath = getPathtoResources();
        return convertToWslPath(resourcesPath) + LINUX_SERVER;
    }

    //returns the path to the server executable
    public static String getServerPath() {
        String OS = System.getProperty("os.name").toLowerCase();
        String serverPath = getPathtoResources();
        if(OS.contains("windows")) {
            serverPath += WINDOWS_SERVER;
        }
        else {
            serverPath += LINUX_SERVER;
        }
        return serverPath;
    }

    public static String getInterpreterDir() {
        return getPathtoResources()+"cocoa";
    }
    public static String getHelpPath() {
        return getPathtoResources()+"cocoa/CoCoAManual/html/toc.html";
    }
    public static String getApcocoaHelpPath() {
        return getPathtoResources()+"cocoa/apcocoa_doc/toc_cmdref.html";
    }
    public static String getCommandHistoryPath() { return UriEncoder.encode(getInterpreterDir()+"/commandhistory.txt");}
    public static String getDefaultParams() {return COCOA_PARAMS;}
    public static String getDefaultServerParams() {return SERVER_PARAMS;}
}
