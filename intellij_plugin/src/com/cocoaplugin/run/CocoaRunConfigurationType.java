package com.cocoaplugin.run;

import com.intellij.execution.configurations.ConfigurationFactory;
import com.intellij.execution.configurations.ConfigurationType;
import com.intellij.openapi.util.IconLoader;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;

public class CocoaRunConfigurationType implements ConfigurationType {
    public static final CocoaRunConfigurationType INSTANCE = new CocoaRunConfigurationType();

    @Override
    public String getDisplayName() {
        return "Cocoa";
    }

    @Override
    public String getConfigurationTypeDescription() {
        return "Cocoa Run Configuration Type";
    }

    @Override
    public Icon getIcon() {
        return IconLoader.getIcon("/com/cocoaplugin/icons/cocoa_logo.png");
    }

    @NotNull
    @Override
    public String getId() {
        return "COCOA_RUN_CONFIGURATION";
    }

    @Override
    public ConfigurationFactory[] getConfigurationFactories() {
        return new ConfigurationFactory[]{new CocoaConfigurationFactory(this)};
    }
}