package com.cocoaplugin.run;


import com.intellij.execution.ExecutionException;
import com.intellij.execution.Executor;
import com.intellij.execution.configuration.EnvironmentVariablesComponent;
import com.intellij.execution.configurations.*;
import com.intellij.execution.filters.TextConsoleBuilder;
import com.intellij.execution.runners.ExecutionEnvironment;

import com.intellij.openapi.options.SettingsEditor;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.roots.*;
import com.intellij.openapi.util.InvalidDataException;
import com.intellij.openapi.util.JDOMExternalizerUtil;
import com.intellij.openapi.util.WriteExternalException;
import com.intellij.openapi.util.text.StringUtil;
import com.intellij.openapi.vfs.*;

import org.jdom.Element;
import org.jetbrains.annotations.NotNull;

import java.io.File;

import java.util.HashMap;
import java.util.Map;
import org.jetbrains.annotations.Nullable;



public class CocoaRunConfiguration extends RunConfigurationBase implements CommonCocoaRunConfigurationParams, CocoaRunConfigurationParams{
    // common config
    private String interpreterOptions = "";
    private String workingDirectory = "";
    private boolean passParentEnvs = true;
    private Map<String, String> envs = new HashMap<String, String>();
    private String interpreterPath = "";
    private String interpreterDir="";

    // run config
    private String scriptName;
    private String scriptParameters;

    protected CocoaRunConfiguration(Project project, ConfigurationFactory factory, String name) {
        super(project, factory, name);
    }

    @NotNull
    @Override
    public SettingsEditor<? extends RunConfiguration> getConfigurationEditor() {
        return new CocoaRunConfigurationEditor(this);
    }



    @Nullable
    @Override
    public RunProfileState getState(@NotNull final Executor executor, @NotNull final ExecutionEnvironment env) throws ExecutionException {
        CocoaCommandLineState state = null;



        if (state == null) {

                state = new CocoaCommandLineState(this, env);
        }

        TextConsoleBuilder textConsoleBuilder = new CocoaTextConsoleBuilder(getProject());
        state.setConsoleBuilder(textConsoleBuilder);
        return state;
    }
    public static void copyParams(CommonCocoaRunConfigurationParams from, CommonCocoaRunConfigurationParams to) {
        to.setEnvs(new HashMap<String, String>(from.getEnvs()));
        to.setInterpreterOptions(from.getInterpreterOptions());
        to.setWorkingDirectory(from.getWorkingDirectory());
        to.setInterpreterPath(from.getInterpreterPath());
        to.setInterpreterDir(from.getInterpreterDir());
//        to.setUsingCocoaJInterpreter(from.isUsingCocoaJInterpreter());
        //to.setPassParentEnvs(from.isPassParentEnvs());
    }

    public static void copyParams(CocoaRunConfigurationParams from, CocoaRunConfigurationParams to) {
        copyParams(from.getCommonParams(), to.getCommonParams());

        to.setScriptName(from.getScriptName());
        to.setScriptParameters(from.getScriptParameters());
    }

    @Override
    public void readExternal(Element element) throws InvalidDataException {
        super.readExternal(element);

        // common config
        interpreterOptions = JDOMExternalizerUtil.readField(element, "INTERPRETER_OPTIONS");
        interpreterPath = JDOMExternalizerUtil.readField(element, "INTERPRETER_PATH");
        workingDirectory = JDOMExternalizerUtil.readField(element, "WORKING_DIRECTORY", getProject().getBasePath());
        interpreterDir=JDOMExternalizerUtil.readField(element, "INTERPRETER_DIR");

        String str = JDOMExternalizerUtil.readField(element, "PARENT_ENVS");
        if (str != null) {
            passParentEnvs = Boolean.parseBoolean(str);
        }
        str = JDOMExternalizerUtil.readField(element, "ALTERNATE_INTERPRETER");

        EnvironmentVariablesComponent.readExternal(element, envs);

        // ???

        // run config
        scriptName = JDOMExternalizerUtil.readField(element, "SCRIPT_NAME");
        scriptParameters = JDOMExternalizerUtil.readField(element, "PARAMETERS");
    }

    @Override
    public void writeExternal(Element element) throws WriteExternalException {
        super.writeExternal(element);

        // common config
        JDOMExternalizerUtil.writeField(element, "INTERPRETER_OPTIONS", interpreterOptions);
        JDOMExternalizerUtil.writeField(element, "INTERPRETER_PATH", interpreterPath);
        JDOMExternalizerUtil.writeField(element, "WORKING_DIRECTORY", workingDirectory);
        JDOMExternalizerUtil.writeField(element, "PARENT_ENVS", Boolean.toString(passParentEnvs));
        JDOMExternalizerUtil.writeField(element, "INTERPRETER_DIR", interpreterDir);

        EnvironmentVariablesComponent.writeExternal(element, envs);

        // ???

        // run config
        JDOMExternalizerUtil.writeField(element, "SCRIPT_NAME", scriptName);
        JDOMExternalizerUtil.writeField(element, "PARAMETERS", scriptParameters);
    }


    @Override
    public void checkConfiguration() throws RuntimeConfigurationException {
        super.checkConfiguration();


        if (StringUtil.isEmptyOrSpaces(scriptName)) {
            throw new RuntimeConfigurationException("No script name given.");
        }

        String name = getScriptName();
        final String dir = getWorkingDirectory();
        if (StringUtil.isNotEmpty(dir))
            name = dir + '/' + name;
        final VirtualFile file = LocalFileSystem.getInstance().findFileByPath(name);

        if (file == null) throw new RuntimeConfigurationException("Script file does not exist");

        final ProjectRootManager rootManager = ProjectRootManager.getInstance(getProject());

        if (!rootManager.getFileIndex().isInContent(file))
            throw new RuntimeConfigurationException("File is not in the current project");

    }

    public String getInterpreterOptions() {
        return interpreterOptions;
    }

    public void setInterpreterOptions(String interpreterOptions) {
        this.interpreterOptions = interpreterOptions;
    }

    public String getWorkingDirectory() {
        return workingDirectory;
    }

    public void setWorkingDirectory(String workingDirectory) {
        this.workingDirectory = workingDirectory;
    }

    public boolean isPassParentEnvs() {
        return passParentEnvs;
    }

    public void setPassParentEnvs(boolean passParentEnvs) {
        this.passParentEnvs = passParentEnvs;
    }

    public Map<String, String> getEnvs() {
        return envs;
    }

    public void setEnvs(Map<String, String> envs) {
        this.envs = envs;
    }

    public String getInterpreterPath() {
        return interpreterPath;
    }

    public void setInterpreterPath(String path) {
        this.interpreterPath = path;
    }

    public CommonCocoaRunConfigurationParams getCommonParams() {
        return this;
    }

    public String getScriptName() {
        return scriptName;
    }

    public void setScriptName(String scriptName) {
        this.scriptName = scriptName;
    }

    public String getScriptParameters() {
        return scriptParameters;
    }

    public void setScriptParameters(String scriptParameters) {
        this.scriptParameters = scriptParameters;
    }
    public String getInterpreterDir() {
        return interpreterDir;
    }

    public void setInterpreterDir(String dir) {
        this.interpreterDir = dir;
    }


}
