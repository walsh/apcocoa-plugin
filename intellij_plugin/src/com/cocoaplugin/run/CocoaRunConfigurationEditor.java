package com.cocoaplugin.run;
import com.intellij.openapi.options.ConfigurationException;
import com.intellij.openapi.options.SettingsEditor;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;


public class CocoaRunConfigurationEditor extends SettingsEditor<CocoaRunConfiguration> {
    private CocoaRunConfigurationForm myForm;

    public CocoaRunConfigurationEditor(CocoaRunConfiguration cocoaRunConfiguration) {
        this.myForm = new CocoaRunConfigurationForm(cocoaRunConfiguration);
    }

    @Override
    protected void resetEditorFrom(CocoaRunConfiguration runConfiguration) {
        CocoaRunConfiguration.copyParams(runConfiguration, myForm);
    }

    @Override
    protected void applyEditorTo(CocoaRunConfiguration runConfiguration) throws ConfigurationException {
        CocoaRunConfiguration.copyParams(myForm, runConfiguration);
    }

    @Override
    @NotNull
    protected JComponent createEditor() {
        return myForm.getRootPanel();
    }

    @Override
    protected void disposeEditor() {
        myForm = null;
    }
}