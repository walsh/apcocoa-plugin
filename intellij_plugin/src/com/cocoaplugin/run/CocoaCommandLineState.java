package com.cocoaplugin.run;
import com.intellij.execution.ExecutionException;
import com.intellij.execution.configurations.CommandLineState;
import com.intellij.execution.configurations.GeneralCommandLine;
import com.intellij.execution.configurations.RunConfiguration;
import com.intellij.execution.process.OSProcessHandler;
import com.intellij.execution.process.ProcessHandler;
import com.intellij.execution.process.ProcessTerminatedListener;
import com.intellij.execution.runners.ExecutionEnvironment;
import com.intellij.openapi.util.text.StringUtil;
import org.jetbrains.annotations.NotNull;

import java.io.File;

public class CocoaCommandLineState extends CommandLineState {
    public ExecutionEnvironment getExecutionEnvironment() {
        return executionEnvironment;
    }

    private final RunConfiguration     runConfiguration;
    private final ExecutionEnvironment executionEnvironment;
    public CocoaCommandLineState(RunConfiguration runConfiguration, ExecutionEnvironment env) {
        super(env);
        this.runConfiguration = runConfiguration;
        this.executionEnvironment = env;
    }

    @NotNull
    @Override
    protected ProcessHandler startProcess() throws ExecutionException {
        GeneralCommandLine commandLine = generateCommandLine();

        OSProcessHandler osProcessHandler =
                new CocoaProcessHandler(commandLine.createProcess(), commandLine.getCommandLineString());
        ProcessTerminatedListener.attach(osProcessHandler, runConfiguration.getProject());

        return osProcessHandler;
    }

    protected GeneralCommandLine generateCommandLine() {
        GeneralCommandLine commandLine = new GeneralCommandLine();
        final CocoaRunConfiguration cfg = (CocoaRunConfiguration) runConfiguration;



        if (!StringUtil.isEmptyOrSpaces(cfg.getInterpreterPath()))
            commandLine.setExePath(cfg.getInterpreterPath());


        //commandLine.setEnvParams(cfg.getEnvs());
        //commandLine.setPassParentEnvs(cfg.isPassParentEnvs());

        return configureCommandLine(commandLine);
    }

    protected GeneralCommandLine configureCommandLine(GeneralCommandLine commandLine) {
        final CocoaRunConfiguration configuration = (CocoaRunConfiguration) runConfiguration;
        commandLine.getParametersList().addParametersString(configuration.getInterpreterOptions());

            commandLine.setWorkDirectory(configuration.getInterpreterDir());


        if (!StringUtil.isEmptyOrSpaces(configuration.getScriptName()) && !StringUtil.isEmptyOrSpaces(configuration.getWorkingDirectory())) {
            commandLine.addParameter(configuration.getWorkingDirectory()+"/"+configuration.getScriptName());
        }

        commandLine.getParametersList().addParametersString(configuration.getScriptParameters());

        return commandLine;
    }


    protected RunConfiguration getRunConfiguration() {
        return runConfiguration;
    }
}