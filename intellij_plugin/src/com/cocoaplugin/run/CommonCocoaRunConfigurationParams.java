package com.cocoaplugin.run;

import java.util.Map;

public interface CommonCocoaRunConfigurationParams {
    public String getInterpreterOptions();

    public void setInterpreterOptions(String options);

    public String getWorkingDirectory();

    public void setWorkingDirectory(String workingDirectory);

    public Map<String, String> getEnvs();

    public void setEnvs(Map<String, String> envs);

    public String getInterpreterPath();

    public void setInterpreterPath(String path);
    public String getInterpreterDir();

    public void setInterpreterDir(String dir);

}
