package com.cocoaplugin.run;
import com.intellij.execution.filters.Filter;
import com.intellij.execution.filters.TextConsoleBuilder;
import com.intellij.execution.impl.ConsoleViewImpl;
import com.intellij.execution.ui.ConsoleView;
import com.intellij.openapi.project.Project;

import java.util.ArrayList;


public class CocoaTextConsoleBuilder extends TextConsoleBuilder {
    private final Project myProject;
    private final ArrayList<Filter> myFilters = new ArrayList<Filter>();
    private boolean myViewer;

    public CocoaTextConsoleBuilder(final Project project) {
        myProject = project;
    }

    public ConsoleView getConsole() {
        final ConsoleView consoleView = createConsole();
        for (final Filter filter : myFilters) {
            consoleView.addMessageFilter(filter);
        }

        return consoleView;
    }

    @Override
    public void addFilter(Filter filter) {
        myFilters.add(filter);
    }

    protected ConsoleViewImpl createConsole() {
        ConsoleViewImpl view = new ConsoleViewImpl(myProject, myViewer);
        // TODO view.setHistorySize(10);
        return view;
    }

    @Override
    public void setViewer(boolean isViewer) {
        myViewer = isViewer;
    }
}

