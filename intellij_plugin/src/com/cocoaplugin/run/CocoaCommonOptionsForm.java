package com.cocoaplugin.run;

import com.intellij.execution.configuration.EnvironmentVariablesComponent;
import com.intellij.ide.util.BrowseFilesListener;
import com.intellij.openapi.ui.TextFieldWithBrowseButton;
import com.intellij.ui.RawCommandLineEditor;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Map;


public class CocoaCommonOptionsForm implements CommonCocoaRunConfigurationParams {
    private JPanel rootPanel;
    private RawCommandLineEditor interpreterOptions;
    private EnvironmentVariablesComponent environmentVariablesEdit;
    private TextFieldWithBrowseButton cocoaInterpreterEdit;
    private TextFieldWithBrowseButton interpreterDirEdit;
    private TextFieldWithBrowseButton workingDirEdit;

    public CocoaCommonOptionsForm(CocoaRunConfiguration cocoaRunConfiguration) {
        cocoaInterpreterEdit.addBrowseFolderListener("Select Cocoa Interpreter", "", cocoaRunConfiguration.getProject(), BrowseFilesListener.SINGLE_FILE_DESCRIPTOR);
        workingDirEdit.addBrowseFolderListener("Select Working Directory", "", cocoaRunConfiguration.getProject(), BrowseFilesListener.SINGLE_DIRECTORY_DESCRIPTOR);
        interpreterDirEdit.addBrowseFolderListener("Select Interpreter Directory", "", cocoaRunConfiguration.getProject(), BrowseFilesListener.SINGLE_DIRECTORY_DESCRIPTOR);

    }



    @Override
    public String getInterpreterOptions() {
        return interpreterOptions.getText();
    }

    @Override
    public void setInterpreterOptions(String options) {
        interpreterOptions.setText(options);
    }

    @Override
    public String getWorkingDirectory() {
        return workingDirEdit.getText();
    }

    @Override
    public void setWorkingDirectory(String workingDirectory) {
        workingDirEdit.setText(workingDirectory);
    }

    @Override
    public Map<String, String> getEnvs() {
        return environmentVariablesEdit.getEnvs();
    }

    @Override
    public void setEnvs(Map<String, String> envs) {
        environmentVariablesEdit.setEnvs(envs);
    }

    @Override
    public String getInterpreterPath() {
        return cocoaInterpreterEdit.getText();
    }

    @Override
    public void setInterpreterPath(String path) {
        this.cocoaInterpreterEdit.setText(path);
    }
    @Override
    public String getInterpreterDir() {
        return interpreterDirEdit.getText();
    }
    @Override
    public void setInterpreterDir(String dir) {
        interpreterDirEdit.setText(dir);
    }


    public JComponent getRootPanel() {
        return rootPanel;
    }

    private void createUIComponents() {
        // TODO: place custom component creation code here
    }
}
