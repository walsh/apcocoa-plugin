package com.cocoaplugin.run;

import com.cocoaplugin.CocoaFileType;
import com.cocoaplugin.CocoaResources;
import com.intellij.execution.actions.ConfigurationContext;
import com.intellij.execution.actions.RunConfigurationProducer;
import com.intellij.execution.configurations.ConfigurationFactory;
import com.intellij.execution.configurations.RunConfiguration;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.util.Ref;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.PsiFile;
import org.jetbrains.annotations.NotNull;


public class CocoaRunConfigurationProducer extends RunConfigurationProducer/*, LocatableConfigurationBase*/ {

    final String INTERPRETER_PATH= CocoaResources.getInterpreterPath();
    final String INTERPRETER_DIR= CocoaResources.getInterpreterDir();

    public CocoaRunConfigurationProducer() {
        super(true);
    }

    @Override
    protected boolean setupConfigurationFromContext(RunConfiguration runConfiguration, ConfigurationContext configurationContext, Ref ref) {
        PsiFile sourceFile = configurationContext.getPsiLocation().getContainingFile();

        if (sourceFile != null && sourceFile.getFileType().equals(CocoaFileType.INSTANCE)) {
            Project project = sourceFile.getProject();
            VirtualFile file = sourceFile.getVirtualFile();

            if (file != null) {
                runConfiguration.setName(file.getName());
                ((CocoaRunConfiguration)runConfiguration).setInterpreterPath(INTERPRETER_PATH);
                ((CocoaRunConfiguration)runConfiguration).setInterpreterDir(INTERPRETER_DIR);

                ((CocoaRunConfiguration)runConfiguration).setScriptName(file.getName());
                final String dir = configurationContext.getProject().getBasePath();
                if (dir != null)
                    ((CocoaRunConfiguration)runConfiguration).setWorkingDirectory(dir);
            }
            return true;
        }
        return false;
    }

    @Override
    public boolean isConfigurationFromContext(RunConfiguration runConfiguration, ConfigurationContext configurationContext) {
        PsiFile sourceFile = configurationContext.getPsiLocation().getContainingFile();
        VirtualFile file = sourceFile.getVirtualFile();
        return (((CocoaRunConfiguration)runConfiguration).getWorkingDirectory().equals(configurationContext.getProject().getBasePath()) &&
                runConfiguration.getName().equals(file.getName()));
    }

    @NotNull
    @Override
    public ConfigurationFactory getConfigurationFactory() {
        return CocoaConfigurationFactory.INSTANCE;
    }
}