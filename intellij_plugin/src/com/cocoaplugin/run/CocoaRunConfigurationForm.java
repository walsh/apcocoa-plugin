package com.cocoaplugin.run;

import com.intellij.ide.util.BrowseFilesListener;
import com.intellij.openapi.ui.TextFieldWithBrowseButton;
import com.intellij.ui.RawCommandLineEditor;

import javax.swing.*;
import java.awt.*;


public class CocoaRunConfigurationForm implements CocoaRunConfigurationParams {
    private JPanel rootPanel;
    private TextFieldWithBrowseButton scriptNameEdit;
    private RawCommandLineEditor commandLineEdit;
    private JPanel commonOptionsPlaceholder;
    private CocoaCommonOptionsForm commonOptionsForm=null;
    private CocoaRunConfiguration myCocoaRunConfiguration;

    public CocoaRunConfigurationForm(CocoaRunConfiguration cocoaRunConfiguration) {
        this.myCocoaRunConfiguration = cocoaRunConfiguration;

        assert myCocoaRunConfiguration != null;

        try {
            commonOptionsForm = new CocoaCommonOptionsForm(myCocoaRunConfiguration);
            commonOptionsPlaceholder.add(commonOptionsForm.getRootPanel(), BorderLayout.CENTER);
        } catch (Throwable unused) {}
        scriptNameEdit.addBrowseFolderListener("Select script", "", myCocoaRunConfiguration.getProject(), BrowseFilesListener.SINGLE_FILE_DESCRIPTOR);
    }

    public CommonCocoaRunConfigurationParams getCommonParams() {
        return commonOptionsForm;
    }

    public String getScriptName() {
        return scriptNameEdit.getText();
    }

    public void setScriptName(String scriptName) {
        this.scriptNameEdit.setText(scriptName);
    }

    public String getScriptParameters() {
        return commandLineEdit.getText();
    }

    public void setScriptParameters(String scriptParameters) {
        commandLineEdit.setText(scriptParameters);
    }

    public JComponent getRootPanel() {
        return rootPanel;
    }
}
