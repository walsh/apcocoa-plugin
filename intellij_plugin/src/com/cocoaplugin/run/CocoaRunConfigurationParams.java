package com.cocoaplugin.run;

public interface CocoaRunConfigurationParams {
    CommonCocoaRunConfigurationParams getCommonParams();

    String getScriptName();

    void setScriptName(String scriptName);

    String getScriptParameters();

    void setScriptParameters(String scriptParameters);
}
