package com.cocoaplugin.run;

import com.intellij.execution.configurations.ConfigurationFactory;
import com.intellij.execution.configurations.ConfigurationType;
import com.intellij.execution.configurations.RunConfiguration;
import com.intellij.openapi.project.Project;

public class CocoaConfigurationFactory extends ConfigurationFactory {
    private static final String FACTORY_NAME = "Cocoa configuration factory";
    public static final CocoaConfigurationFactory INSTANCE = new CocoaConfigurationFactory(CocoaRunConfigurationType.INSTANCE);


    protected CocoaConfigurationFactory(ConfigurationType type) {
        super(type);
    }

    @Override
    public RunConfiguration createTemplateConfiguration(Project project) {
        return new CocoaRunConfiguration(project, this, "Cocoa");
    }

    @Override
    public String getName() {
        return FACTORY_NAME;
    }
}
