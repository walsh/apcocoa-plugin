package com.cocoaplugin.run;

import com.intellij.execution.process.CapturingProcessHandler;
import com.intellij.openapi.vfs.CharsetToolkit;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class CocoaProcessHandler extends CapturingProcessHandler {
    public CocoaProcessHandler(@NotNull Process process, @Nullable String commandLine) {
        super(process, CharsetToolkit.UTF8_CHARSET,  commandLine);
    }

    @Override
    protected boolean shouldDestroyProcessRecursively() {
        return true;
    }


}

