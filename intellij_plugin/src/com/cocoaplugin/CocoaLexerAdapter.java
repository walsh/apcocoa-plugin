package com.cocoaplugin;

import com.intellij.lexer.FlexAdapter;

import java.io.Reader;

public class CocoaLexerAdapter extends FlexAdapter {
    public CocoaLexerAdapter() {
        super(new _CocoaLexer((Reader) null));
    }
}
