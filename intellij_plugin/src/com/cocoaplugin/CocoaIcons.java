package com.cocoaplugin;

import com.intellij.openapi.util.IconLoader;

import javax.swing.*;

public class CocoaIcons {
    public static final Icon FILE = IconLoader.getIcon("/com/cocoaplugin/icons/cocoa_logo.png");
}
