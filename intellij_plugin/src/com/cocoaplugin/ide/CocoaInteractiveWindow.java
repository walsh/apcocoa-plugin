package com.cocoaplugin.ide;


import com.cocoaplugin.CocoaResources;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.project.ProjectManager;
import com.intellij.openapi.project.ProjectManagerListener;
import com.intellij.openapi.wm.ToolWindow;
import com.intellij.openapi.wm.ToolWindowFactory;
import com.intellij.openapi.wm.ToolWindowManager;
import com.intellij.ui.JBColor;
import com.intellij.ui.components.JBScrollPane;
import com.intellij.ui.content.Content;
import com.intellij.ui.content.ContentFactory;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;
import javax.swing.text.AbstractDocument;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.Element;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class CocoaInteractiveWindow implements ToolWindowFactory, ActionListener, KeyListener, ProjectManagerListener {
    private JPanel terminalPanel;
    private JTextArea terminalArea;
    private Project project;
    private JScrollPane scroll;
    private static int historyOffset;
    private static List<String> history;
    private static final int HISTORY_LENGTH =100;

    @Override
    public void createToolWindowContent(@NotNull Project project, @NotNull ToolWindow toolWindow) {
        ProjectManager.getInstance().addProjectManagerListener(this);
        this.project=project;
        //initializing history
        history=new ArrayList<>();
        try{
            File history_file = new File(CocoaResources.getCommandHistoryPath());
            history = Files.readAllLines(Paths.get(history_file.getPath()));
        }catch (Exception e){
            e.printStackTrace();
        }
        historyOffset=history.size();

        terminalPanel=new JPanel();
        terminalPanel.setLayout(new BorderLayout());
        terminalArea = new JTextArea("\n > ");
        terminalArea.setCaretPosition(terminalArea.getDocument().getLength());

        terminalArea.setBackground(new JBColor(new Color(237,244,252), new Color(237,244,252)));
        terminalArea.setForeground(JBColor.BLACK);
        scroll = new JBScrollPane(terminalArea);
        terminalArea.setLineWrap(true);
        terminalArea.setWrapStyleWord(true);
        terminalPanel.add(scroll);
        AbstractDocument document = (AbstractDocument) terminalArea.getDocument();
        document.setDocumentFilter(new TerminalFilter());
        terminalArea.setNavigationFilter(new TerminalNavigationFilter(terminalArea.getDocument()));

        terminalArea.addKeyListener(this);
        ContentFactory contentFactory = ContentFactory.SERVICE.getInstance();

        Content content = contentFactory.createContent(terminalPanel, "INPUT", false);
        toolWindow.getContentManager().addContent(content);
    }

    @Override
    public void actionPerformed(ActionEvent e) {

    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {

        if(e.getKeyCode()==KeyEvent.VK_ENTER){
            ToolWindowManager toolWindowManager=ToolWindowManager.getInstance(project);
            ToolWindow cocoaWindow = toolWindowManager.getToolWindow("Cocoa Output Window");
            CocoaProcessManager.startProcess();
            cocoaWindow.show(null);

            //set caret at the end
            terminalArea.setCaretPosition(terminalArea.getDocument().getLength());
            //get last line
            String toSend="";
            Document document = terminalArea.getDocument();
            Element rootElem = document.getDefaultRootElement();
            int numLines = rootElem.getElementCount();
            Element lineElem = rootElem.getElement(numLines - 1);
            int lineStart = lineElem.getStartOffset()+3;
            int lineEnd = lineElem.getEndOffset();
            try {
                toSend = document.getText(lineStart, lineEnd - lineStart);
                toSend = toSend.replace("\n", "").replace("\r", "");

            }catch (BadLocationException blex){
                blex.printStackTrace();
            }
            if(toSend.equals("cls")){terminalArea.setText(""); return;}
            writeInHistory(toSend);
            CocoaProcessManager.execute(toSend);

        }else if(e.getKeyCode()==KeyEvent.VK_UP){
            String upHistory=getUpHistory();
            if(!upHistory.equals("EndOfHistory")){
                updateTextareaHistory(upHistory);
            }
        }else if(e.getKeyCode()==KeyEvent.VK_DOWN){
            String downHistory=getDownHistory();
            if(!downHistory.equals("EndOfHistory")){
                updateTextareaHistory(downHistory);
            }
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {

        if(e.getKeyCode()==KeyEvent.VK_ENTER) {
            terminalArea.append(" > ");
            terminalArea.setCaretPosition(terminalArea.getDocument().getLength());
        }
    }

    public void writeInHistory(String data){
        history.add(data);
        historyOffset=history.size();
    }

    public void updateTextareaHistory(String command){
        String pastText="";
        Document document = terminalArea.getDocument();
        Element rootElem = document.getDefaultRootElement();
        int numLines = rootElem.getElementCount();
        Element lineElem = rootElem.getElement(numLines - 1);
        int lineStart = lineElem.getStartOffset()+2;
       // int lineEnd = lineElem.getEndOffset();
        try {
            pastText = document.getText(0, lineStart+1);

        }catch (BadLocationException blex){
            blex.printStackTrace();
        }
        terminalArea.setText(pastText+command);
    }

    public String getUpHistory(){
        historyOffset=historyOffset-1;
        if(historyOffset==-1){
            historyOffset=0;
            return "EndOfHistory";
        }
        return history.get(historyOffset);
    }

    public String getDownHistory(){
        historyOffset=historyOffset+1;
        if(historyOffset>=history.size()){
            historyOffset=history.size();
            return "";
        }
        return history.get(historyOffset);
    }

    @Override
    public void projectClosing(@NotNull Project project) {
        CocoaProcessManager.stopCocoaProcess();
        //saving the history
        try {
            File history=new File(CocoaResources.getCommandHistoryPath());
            history.createNewFile();
            FileWriter fw = new FileWriter(CocoaResources.getCommandHistoryPath());
            int start=0;
            int hsize=this.history.size();
            if(hsize> HISTORY_LENGTH)start=hsize- HISTORY_LENGTH;
            for (int i=start; i<hsize; i++) {
                fw.write(this.history.get(i)+System.lineSeparator());
            }
            fw.close();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
