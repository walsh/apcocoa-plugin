package com.cocoaplugin.ide;

import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;

public class StopServer extends AnAction {

    @Override
    public void actionPerformed(AnActionEvent e) {
        ServerProcessManager.stopProcess();
    }

    public void update(AnActionEvent e){
        e.getPresentation().setEnabled(ServerProcessManager.getProcess() != null);
    }
}
