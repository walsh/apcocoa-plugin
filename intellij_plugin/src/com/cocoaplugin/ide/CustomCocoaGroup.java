package com.cocoaplugin.ide;

import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.CommonDataKeys;
import com.intellij.openapi.actionSystem.DefaultActionGroup;
import com.intellij.openapi.editor.Editor;

public class CustomCocoaGroup extends DefaultActionGroup {
    public void update(AnActionEvent event) {
        // Enable/disable depending on whether user is editing
       // Editor editor = event.getData(CommonDataKeys.EDITOR);
        //event.getPresentation().setEnabled(editor != null);
        // Always make visible.
        event.getPresentation().setVisible(true);
    }

}
