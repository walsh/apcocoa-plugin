package com.cocoaplugin.ide;

import com.intellij.execution.filters.TextConsoleBuilderFactory;
import com.intellij.execution.ui.ConsoleView;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.wm.ToolWindow;
import com.intellij.openapi.wm.ToolWindowFactory;
import com.intellij.ui.content.Content;
import org.jetbrains.annotations.NotNull;


public class CocoaToolWindow implements ToolWindowFactory {

    @Override
    public void createToolWindowContent(@NotNull Project project, @NotNull ToolWindow toolWindow) {
        ConsoleView serverView = TextConsoleBuilderFactory.getInstance().createBuilder(project).getConsole();
        ServerProcessManager.setConsoleView(serverView);
        Content serverContent = toolWindow.getContentManager().getFactory().createContent(serverView.getComponent(), "Server", true);
        toolWindow.getContentManager().addContent(serverContent);
        ConsoleView cocoaView = TextConsoleBuilderFactory.getInstance().createBuilder(project).getConsole();
        CocoaProcessManager.setConsoleView(cocoaView);
        Content cocoaContent = toolWindow.getContentManager().getFactory().createContent(cocoaView.getComponent(), "Output", true);
        toolWindow.getContentManager().addContent(cocoaContent);
        toolWindow.getContentManager().setSelectedContent(cocoaContent);
        CocoaProcessManager.startProcess();
    }
}
