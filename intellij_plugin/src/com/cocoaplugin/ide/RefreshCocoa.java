package com.cocoaplugin.ide;

import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.wm.ToolWindow;
import com.intellij.openapi.wm.ToolWindowManager;

public class RefreshCocoa extends AnAction {

    @Override
    public void actionPerformed(AnActionEvent e) {
        ToolWindowManager toolWindowManager=ToolWindowManager.getInstance(e.getProject());
        ToolWindow cocoaWindow = toolWindowManager.getToolWindow("Cocoa Output Window");
        CocoaProcessManager.stopCocoaProcess();
        CocoaProcessManager.startProcess();
        cocoaWindow.show(null);
    }
}
