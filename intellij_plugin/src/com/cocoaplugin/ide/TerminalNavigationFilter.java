package com.cocoaplugin.ide;

import javax.swing.text.*;
public class TerminalNavigationFilter extends NavigationFilter {
    private Document doc;

    public TerminalNavigationFilter(Document doc) {
        super();
        this.doc=doc;
    }

    @Override
    public void setDot(FilterBypass fb, int dot, Position.Bias bias) {
        Document document = this.doc;
        Element rootElem = document.getDefaultRootElement();
        int numLines = rootElem.getElementCount();
        Element lineElem = rootElem.getElement(numLines - 1);
        int lineStart = lineElem.getStartOffset();


        if(dot>=lineStart+3|| dot==lineStart)super.setDot(fb, dot, bias);
    }

    @Override
    public void moveDot(FilterBypass fb, int dot, Position.Bias bias) {
        super.moveDot(fb, dot, bias);
    }

    @Override
    public int getNextVisualPositionFrom(JTextComponent text, int pos, Position.Bias bias, int direction, Position.Bias[] biasRet) throws BadLocationException {
        return super.getNextVisualPositionFrom(text, pos, bias, direction, biasRet);
    }
}
