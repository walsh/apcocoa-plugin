package com.cocoaplugin.ide;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;

public class InterpreterThread extends Thread {
    private volatile Scanner outputScanner=null;
    private volatile Scanner errScanner=null;

    @Override
    public void run() {
        Process process = CocoaProcessManager.getCocoaProcess();
        if(process == null){
            CocoaProcessManager.printErrorMessage();
            return;
        }
        InputStream stderr = process.getErrorStream();
        InputStream stdout = process.getInputStream();
        if(outputScanner==null){
            outputScanner = new Scanner(stdout, String.valueOf(StandardCharsets.UTF_8));
            outputScanner.useDelimiter("\n|(§%## )|(§%##)");
        }
        if(errScanner==null){
            errScanner = new Scanner(stderr, String.valueOf(StandardCharsets.UTF_8));
            errScanner.useDelimiter("\n");
        }
        while (outputScanner.hasNext() || errScanner.hasNext()) {
            if (outputScanner.hasNext())
                CocoaProcessManager.printCocoaline(outputScanner.next());
            else
                CocoaProcessManager.printCocoaError(errScanner.next());
        }
    }
}
