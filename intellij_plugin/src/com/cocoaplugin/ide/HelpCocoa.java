package com.cocoaplugin.ide;

import com.cocoaplugin.CocoaResources;
import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import org.apache.http.client.utils.URIUtils;
import org.yaml.snakeyaml.util.UriEncoder;

import java.net.URI;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

public class HelpCocoa extends AnAction {

    @Override
    public void actionPerformed(AnActionEvent e) {
        String help_file = CocoaResources.getHelpPath();
        String Link = UriEncoder.encode(help_file);
        com.intellij.ide.BrowserUtil.browse(Link);
    }
}
