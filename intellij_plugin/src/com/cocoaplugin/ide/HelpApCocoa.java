package com.cocoaplugin.ide;

import com.cocoaplugin.CocoaResources;
import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import org.yaml.snakeyaml.util.UriEncoder;

import java.net.URI;

public class HelpApCocoa extends AnAction {

    @Override
    public void actionPerformed(AnActionEvent e) {
        String help_file = CocoaResources.getApcocoaHelpPath();
        String Link = UriEncoder.encode(help_file);
        com.intellij.ide.BrowserUtil.browse(Link);
    }
}
