package com.cocoaplugin.ide;

import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;

public class InterruptCocoa extends AnAction {

    @Override
    public void actionPerformed(AnActionEvent e) {
        CocoaProcessManager.sendInterrupt();
    }

    public void update(AnActionEvent e){
        String OS = System.getProperty("os.name").toLowerCase();
        e.getPresentation().setEnabledAndVisible(!OS.contains("windows"));
    }
}
