package com.cocoaplugin.ide;

import com.intellij.execution.filters.TextConsoleBuilderFactory;
import com.intellij.execution.ui.ConsoleView;
import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.wm.ToolWindow;
import com.intellij.openapi.wm.ToolWindowManager;
import com.intellij.ui.content.Content;

public class StartApcocoaServer extends AnAction {

    @Override
    public void actionPerformed(AnActionEvent e) {
        if (ServerProcessManager.getConsoleView() == null) {
            ToolWindowManager toolWindowManager=ToolWindowManager.getInstance(e.getProject());
            ToolWindow cocoaWindow = toolWindowManager.getToolWindow("Cocoa Output Window");
            ConsoleView vv = TextConsoleBuilderFactory.getInstance().createBuilder(e.getProject()).getConsole();
            ServerProcessManager.setConsoleView(vv);
            Content content = cocoaWindow.getContentManager().getFactory().createContent(vv.getComponent(), "Server", true);
            cocoaWindow.getContentManager().addContent(content);
        }
        ServerProcessManager.startProcess();
    }
    public void update(AnActionEvent e){
        e.getPresentation().setEnabled(ServerProcessManager.getProcess() == null);
    }
}
