package com.cocoaplugin.ide;

import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;

public class ServerThread extends Thread {
    private volatile Scanner scanner=null;

    @Override
    public void run() {
        Process process = ServerProcessManager.getProcess();
        if(process == null){
            ServerProcessManager.printErrorMessage();
            return;
        }
        InputStream stdout = process.getErrorStream();
        if(scanner==null){
            scanner = new Scanner(stdout, String.valueOf(StandardCharsets.UTF_8));
            scanner.useDelimiter("\n");
        }
        while (scanner.hasNext()) {
            ServerProcessManager.printString(scanner.next());
        }
    }
}
