package com.cocoaplugin.ide;

import com.cocoaplugin.CocoaResources;
import com.cocoaplugin.settings.CocoaSettingsState;
import com.intellij.execution.impl.ConsoleViewImpl;
import com.intellij.execution.ui.ConsoleView;
import com.intellij.execution.ui.ConsoleViewContentType;
import com.intellij.openapi.project.Project;
import org.apache.commons.lang.ArrayUtils;
import java.io.*;


public class CocoaProcessManager {

    private static Process process;
    private static InterpreterThread cocoaThread;
    private static ConsoleView consoleView;

    public static void startProcess(){
        if (process == null) {
            try {
                String OS = System.getProperty("os.name").toLowerCase();
                String interpreterDir = CocoaResources.getInterpreterDir();
                ProcessBuilder builder;
                String path;
                if (OS.contains("windows") && CocoaSettingsState.getInstance().useWsl) {
                    path = CocoaSettingsState.getInstance().wslPath;
                    String[] command = {"wsl", path};
                    String[] params = CocoaSettingsState.getInstance().cocoaParams.split(" ");
                    String[] final_command = (String[]) ArrayUtils.addAll(command, params);
                    builder = new ProcessBuilder(final_command);
                } else {
                    path = CocoaSettingsState.getInstance().interpreterPath;
                    String[] command = {path};
                    String[] params = CocoaSettingsState.getInstance().cocoaParams.split(" ");
                    String[] final_command = (String[]) ArrayUtils.addAll(command, params);
                    builder = new ProcessBuilder(final_command);
                }
                builder.directory(new File(interpreterDir));
                process = builder.start();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        if (cocoaThread == null) {
            cocoaThread = new InterpreterThread();
            cocoaThread.start();
        }
    }

    public static Process getCocoaProcess(){
        return process;
    }

    public static void stopCocoaProcess() {
        if (process != null) {
            process.destroyForcibly();
            process = null;
            consoleView.clear();
        }
        cocoaThread = null;
    }

    public static void sendInterrupt() {
        String OS = System.getProperty("os.name").toLowerCase();
        if (!OS.contains("windows")) {
            long pid = process.pid();
            try {
                Runtime.getRuntime().exec("kill -SIGINT " + pid);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void printCocoaline(String token){
        consoleView.print(token+System.lineSeparator(), ConsoleViewContentType.NORMAL_OUTPUT);
        ((ConsoleViewImpl)consoleView).requestScrollingToEnd();
    }

    public static void printCocoaError(String token){
        consoleView.print(token+System.lineSeparator(), ConsoleViewContentType.ERROR_OUTPUT);
        ((ConsoleViewImpl)consoleView).requestScrollingToEnd();
    }

    public static void printErrorMessage(){
        consoleView.print("Ops, CoCoA Process could not be started."+System.lineSeparator(), ConsoleViewContentType.ERROR_OUTPUT);
    }

    public static void setConsoleView(ConsoleView consoleView) {
        CocoaProcessManager.consoleView = consoleView;
    }

    public static void execute(String input){
        if(process == null){
            printErrorMessage();
            return;
        }
        OutputStream stdin = process.getOutputStream();
        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(stdin));
        consoleView.print(input+ System.lineSeparator(), ConsoleViewContentType.USER_INPUT);
        ((ConsoleViewImpl)consoleView).requestScrollingToEnd();
        try {
            writer.write(input);
            writer.write(System.lineSeparator());
            writer.flush();
        }catch (IOException ioex){ioex.printStackTrace();}
    }
}
