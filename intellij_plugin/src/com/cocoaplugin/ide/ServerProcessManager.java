package com.cocoaplugin.ide;

import com.cocoaplugin.CocoaResources;
import com.cocoaplugin.settings.CocoaSettingsState;
import com.intellij.execution.ui.ConsoleView;
import com.intellij.execution.ui.ConsoleViewContentType;
import org.apache.commons.lang.ArrayUtils;

import java.io.File;
import java.io.IOException;

public class ServerProcessManager {
    private static Process process;
    private static ServerThread thread;
    private static ConsoleView consoleView;

    public static void startProcess(){
        //do not restart Server if it is already running (user has to Stop Server first)
        if(process == null) {
            try {
                String interpreterDir = CocoaResources.getInterpreterDir();
                String OS = System.getProperty("os.name").toLowerCase();
                ProcessBuilder builder;
                if (OS.contains("windows") && CocoaSettingsState.getInstance().useWsl){
                    String[] command = {"wsl", CocoaResources.getWslServerPath()};
                    String[] params = CocoaSettingsState.getInstance().serverParams.split(" ");
                    String[] final_command = (String[]) ArrayUtils.addAll(command, params);
                    builder = new ProcessBuilder(final_command);
                }
                else {
                    String[] command = {CocoaResources.getServerPath()};
                    String[] params = CocoaSettingsState.getInstance().serverParams.split(" ");
                    String[] final_command = (String[]) ArrayUtils.addAll(command, params);
                    builder = new ProcessBuilder(final_command);
                }
                builder.directory(new File(interpreterDir));
                process = builder.start();
                // make sure to destroy the process when intellij is closed
                Thread closeChildThread = new Thread() {
                    public void run() {
                        process.destroyForcibly();
                    }
                };
                Runtime.getRuntime().addShutdownHook(closeChildThread);
            }catch (IOException ioex) {ioex.printStackTrace();}
            if (thread == null) {
                thread = new ServerThread();
                thread.start();
            }
        }
    }

    public static Process getProcess(){ return process; }

    public static void stopProcess() {
        if(process != null) {
            process.destroy();
            process = null;
            consoleView.clear();
        }
        thread=null;
    }
    public static void printErrorMessage(){
        consoleView.print("Ops, ApCoCoA Server could not be started."+System.lineSeparator(), ConsoleViewContentType.ERROR_OUTPUT);
    }

    public static void printString(String token){ consoleView.print(token+"\n", ConsoleViewContentType.NORMAL_OUTPUT);}

    public static void setConsoleView(ConsoleView consoleView) {
        ServerProcessManager.consoleView = consoleView;
    }

    public static ConsoleView getConsoleView() {return consoleView;}

}
