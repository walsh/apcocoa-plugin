package com.cocoaplugin.ide;

import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.CommonDataKeys;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.util.TextRange;
import com.intellij.openapi.wm.ToolWindow;
import com.intellij.openapi.wm.ToolWindowManager;

public class SendToCocoa extends AnAction {

    @Override
    public synchronized void actionPerformed(AnActionEvent e) {
        if(e.getData(CommonDataKeys.EDITOR)==null||e.getData(CommonDataKeys.EDITOR).getSelectionModel()==null)
            return;
        ToolWindowManager toolWindowManager=ToolWindowManager.getInstance(e.getProject());
        ToolWindow cocoaWindow = toolWindowManager.getToolWindow("Cocoa Output Window");
        CocoaProcessManager.startProcess();

        Editor editor = e.getData(CommonDataKeys.EDITOR);
        String selectedText = editor.getSelectionModel().getSelectedText();
        if(selectedText==null){
            //send line of cursor position if nothing is selected
            int startOffset = editor.getCaretModel().getVisualLineStart();
            int endOffset = editor.getCaretModel().getVisualLineEnd();
            TextRange tr = new TextRange(startOffset,endOffset);
            selectedText = editor.getDocument().getText(tr);
        }
        CocoaProcessManager.execute(selectedText);
        cocoaWindow.show(null);
    }
}
