package com.cocoaplugin.ide;

import javax.swing.text.*;
import java.awt.*;

public class TerminalFilter extends DocumentFilter {

    @Override
    public void remove(FilterBypass fb, int offset, int length) throws BadLocationException {
        Document document = fb.getDocument();
        Element rootElem = document.getDefaultRootElement();
        int numLines = rootElem.getElementCount();
        Element lineElem = rootElem.getElement(numLines - 1);
        int lineStart = lineElem.getStartOffset();
        int lineEnd = lineElem.getEndOffset();
        if(offset<lineStart+3){
            Toolkit.getDefaultToolkit().beep();

        }
        else{
            super.remove(fb, offset, length);

        }

    }



    @Override
    public void replace(FilterBypass fb, int offset, int length, String text, AttributeSet attrs) throws BadLocationException {
        Document document = fb.getDocument();
        Element rootElem = document.getDefaultRootElement();
        int numLines = rootElem.getElementCount();
        Element lineElem = rootElem.getElement(numLines - 1);
        int lineStart = lineElem.getStartOffset();
        int lineEnd = lineElem.getEndOffset();
        if(offset==0){
            super.replace(fb, offset, length, text, attrs);
            return;
        }
        if(offset<lineStart+3){
            Toolkit.getDefaultToolkit().beep();
        }
        else{
            super.replace(fb, offset, length, text, attrs);

        }
    }
}
