// This is a generated file. Not intended for manual editing.
package com.cocoaplugin.psi;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElement;

public interface CocoaIterationBlock extends PsiElement {

  @Nullable
  CocoaAssignmentStatement getAssignmentStatement();

  @NotNull
  List<CocoaStatement> getStatementList();

  @Nullable
  PsiElement getIdentifier();

}
