// This is a generated file. Not intended for manual editing.
package com.cocoaplugin.psi;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElement;

public interface CocoaOrExpression extends PsiElement {

  @NotNull
  List<CocoaAndExpression> getAndExpressionList();

}
