// This is a generated file. Not intended for manual editing.
package com.cocoaplugin.psi;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElement;

public interface CocoaExpression extends PsiElement {

  @Nullable
  CocoaElement getElement();

  @Nullable
  CocoaExpression getExpression();

  @Nullable
  CocoaFunctionInvocation getFunctionInvocation();

  @Nullable
  PsiElement getOperator();

}
