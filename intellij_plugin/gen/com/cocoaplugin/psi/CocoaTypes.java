// This is a generated file. Not intended for manual editing.
package com.cocoaplugin.psi;

import com.intellij.psi.tree.IElementType;
import com.intellij.psi.PsiElement;
import com.intellij.lang.ASTNode;
import com.cocoaplugin.psi.impl.*;

public interface CocoaTypes {

  IElementType ROOT = new CocoaElementType("ROOT");

  IElementType AND = new CocoaTokenType("and");
  IElementType BLOCK_COMMENT = new CocoaTokenType("BLOCK_COMMENT");
  IElementType COMMA = new CocoaTokenType(",");
  IElementType COMMENT = new CocoaTokenType("COMMENT");
  IElementType DEFINE = new CocoaTokenType("define");
  IElementType DO = new CocoaTokenType("do");
  IElementType ELIF = new CocoaTokenType("elif");
  IElementType ELSE = new CocoaTokenType("else");
  IElementType ENDDEFINE = new CocoaTokenType("enddefine");
  IElementType ENDFOR = new CocoaTokenType("endfor");
  IElementType ENDIF = new CocoaTokenType("endif");
  IElementType ENDWHILE = new CocoaTokenType("enwhile");
  IElementType EXTERNAL_END = new CocoaTokenType(">>");
  IElementType EXTERNAL_START = new CocoaTokenType("<<");
  IElementType FALSE = new CocoaTokenType("false");
  IElementType FOR = new CocoaTokenType("for");
  IElementType GEQ = new CocoaTokenType(">=");
  IElementType GT = new CocoaTokenType(">");
  IElementType IDENTIFIER = new CocoaTokenType("IDENTIFIER");
  IElementType IF = new CocoaTokenType("if");
  IElementType LEFT_BRACE = new CocoaTokenType("{");
  IElementType LEFT_BRACKET = new CocoaTokenType("[");
  IElementType LEFT_PAREN = new CocoaTokenType("(");
  IElementType LEQ = new CocoaTokenType("<=");
  IElementType LT = new CocoaTokenType("<");
  IElementType NOT = new CocoaTokenType("not");
  IElementType NUMBER = new CocoaTokenType("NUMBER");
  IElementType OPERATOR = new CocoaTokenType("OPERATOR");
  IElementType OP_ASSIGN = new CocoaTokenType(":=");
  IElementType OP_COLON = new CocoaTokenType(":");
  IElementType OP_EQ = new CocoaTokenType("=");
  IElementType OP_QUERY = new CocoaTokenType("?");
  IElementType OP_RING = new CocoaTokenType("::=");
  IElementType OR = new CocoaTokenType("or");
  IElementType RESERVED = new CocoaTokenType("RESERVED");
  IElementType RIGHT_BRACE = new CocoaTokenType("}");
  IElementType RIGHT_BRACKET = new CocoaTokenType("]");
  IElementType RIGHT_PAREN = new CocoaTokenType(")");
  IElementType SEMICOLON = new CocoaTokenType(";");
  IElementType STRING = new CocoaTokenType("STRING");
  IElementType TRUE = new CocoaTokenType("true");
  IElementType WHILE = new CocoaTokenType("while");

  class Factory {
    public static PsiElement createElement(ASTNode node) {
      IElementType type = node.getElementType();
      if (type == ROOT) {
        return new CocoaRootImpl(node);
      }
      throw new AssertionError("Unknown element type: " + type);
    }
  }
}
