// This is a generated file. Not intended for manual editing.
package com.cocoaplugin.psi;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElement;

public interface CocoaFunctionDefinition extends PsiElement {

  @NotNull
  CocoaFunctionBody getFunctionBody();

  @Nullable
  CocoaParams getParams();

  @NotNull
  PsiElement getIdentifier();

}
