// This is a generated file. Not intended for manual editing.
package com.cocoaplugin.psi.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static com.cocoaplugin.psi.CocoaTypes.*;
import com.intellij.extapi.psi.ASTWrapperPsiElement;
import com.cocoaplugin.psi.*;

public class CocoaRootImpl extends ASTWrapperPsiElement implements CocoaRoot {

  public CocoaRootImpl(@NotNull ASTNode node) {
    super(node);
  }

  public void accept(@NotNull CocoaVisitor visitor) {
    visitor.visitRoot(this);
  }

  @Override
  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof CocoaVisitor) accept((CocoaVisitor)visitor);
    else super.accept(visitor);
  }

}
