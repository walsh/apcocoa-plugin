// This is a generated file. Not intended for manual editing.
package com.cocoaplugin.parser;

import com.intellij.lang.PsiBuilder;
import com.intellij.lang.PsiBuilder.Marker;
import static com.cocoaplugin.psi.CocoaTypes.*;
import static com.intellij.lang.parser.GeneratedParserUtilBase.*;
import com.intellij.psi.tree.IElementType;
import com.intellij.lang.ASTNode;
import com.intellij.psi.tree.TokenSet;
import com.intellij.lang.PsiParser;
import com.intellij.lang.LightPsiParser;

@SuppressWarnings({"SimplifiableIfStatement", "UnusedAssignment"})
public class CocoaParser implements PsiParser, LightPsiParser {

  public ASTNode parse(IElementType t, PsiBuilder b) {
    parseLight(t, b);
    return b.getTreeBuilt();
  }

  public void parseLight(IElementType t, PsiBuilder b) {
    boolean r;
    b = adapt_builder_(t, b, this, null);
    Marker m = enter_section_(b, 0, _COLLAPSE_, null);
    r = parse_root_(t, b);
    exit_section_(b, 0, m, t, r, true, TRUE_CONDITION);
  }

  protected boolean parse_root_(IElementType t, PsiBuilder b) {
    return parse_root_(t, b, 0);
  }

  static boolean parse_root_(IElementType t, PsiBuilder b, int l) {
    return CocoaFile(b, l + 1);
  }

  /* ********************************************************** */
  // Root*
  static boolean CocoaFile(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "CocoaFile")) return false;
    while (true) {
      int c = current_position_(b);
      if (!Root(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "CocoaFile", c)) break;
    }
    return true;
  }

  /* ********************************************************** */
  // 'regexp:[\s\S]*' | '?' | '.' | '|'
  public static boolean Root(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "Root")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, ROOT, "<root>");
    r = consumeToken(b, "regexp:[\\s\\S]*");
    if (!r) r = consumeToken(b, OP_QUERY);
    if (!r) r = consumeToken(b, ".");
    if (!r) r = consumeToken(b, "|");
    exit_section_(b, l, m, r, false, null);
    return r;
  }

}
