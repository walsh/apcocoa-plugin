//
// This file is part of the source of ApCoCoALib, the ApCoCoA Library.
//
//   Copyright (c) ApCoCoA Project (Prof. Dr. Martin Kreuzer, Uni Passau)
//
//   Author: 2008-2010,2013 X. XIU
//
// Visit http://apcocoa.org/ for more information regarding ApCoCoA
// and ApCoCoALib.
// Visit http://www.apcocoa.org/wiki/ApCoCoA:KnownIssues for bugs, problems
// and known issues.
//
// ApCoCoALib is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License (version 3 or later)
// as published by the Free Software Foundation. A copy of the full
// licence may be found in the file COPYING in this directory.
//
// ApCoCoALib is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ApCoCoALib; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

#include "ApCoCoA/Noncommutative/Ufnarovski.H"


namespace ApCoCoA {
    namespace NoncommutativeAlgorithms {
      
        
		////////////////////////////////////////////////////////////
        //	Functions for the Ufnarovski techniques.
        ////////////////////////////////////////////////////////////
        
    	//========== for the ApCoCoA package gbmr ==========
        
    	bool IsReduced(const string & w, const set<string> & M)
        {
            for (set<string>::iterator it = M.begin(); it != M.end(); ++it)
            {
                if (string::npos != w.find(*it))
                    return false;
            }
            return true;
        }
        
        void UfnVertices(const string & X, const set<string> & M,
                         vector<string> & V)
        {
        	V.clear();
        	size_t k = 0;
        	for (set<string>::iterator it = M.begin(); it != M.end(); ++it)
        		k = (it->length() > k) ? it->length() : k;
        	if (0 == k || 1 == k) //M is empty or a subset of {1,a,b,...}
                return;
        	--k; // k=-1+max{len(w)|w in M}
        	V.push_back("");
        	for (size_t i = 1; i <= k; ++i)
        	{
        		vector<string> tmpV(V);
        		V.clear();
        		for (size_t j = 0; j != tmpV.size(); ++j)
        			for (size_t l = 0; l != X.length(); ++l)
        			{
        				string s = tmpV.at(j) + X.substr(l,1);
        				if (IsReduced(s,M))
        					V.push_back(s);
        			}
        		tmpV.clear();
        	}
        	return;
        }
        
        void UfnGraph(const set<string> & M, const vector<string> & V,
                      vector<vector<size_t> > & adLists)
        {
            adLists.clear();
            for (size_t i = 0; i != V.size(); ++i)
            {
                vector<size_t> adList;
                for (size_t j = 0; j != V.size(); ++j)
                {
                    string be = V.at(i)+V.at(j).substr(V.at(j).length()-1,string::npos);
                    string en = V.at(i).substr(0,1)+V.at(j);
                    if (be == en && IsReduced(be,M))
                        adList.push_back(j);
                }
                adLists.push_back(adList); //adList might be empty
                adList.clear();
            }
            return;
        }
        
        void adMatrix(const string & X, const set<string> & M,
                      vector<string> & V, vector<vector<size_t> > & adMat)
        {
            adMat.clear();
            cout << "Constructing vertices..." << endl;
            UfnVertices(X,M,V);
            cout << "Constructing vertices finished." << endl;
            size_t n = V.size();
            cout << "Vertices (" << n << "):" << endl;
            for (size_t i = 0; i != n; ++i)
                cout << V.at(i) << "  ";
            cout << endl;
            cout << "Constructing adjacency matrix..." << endl;
            vector<vector<size_t> > adLists;
            UfnGraph(M,V,adLists);
            for (size_t i = 0; i != n; ++i)
            {
                vector<size_t> tmpV; tmpV.assign(n,0);
                for (size_t j = 0; j != adLists.at(i).size(); ++j)
                    tmpV.at(adLists.at(i).at(j)) = 1;
                adMat.push_back(tmpV);
                tmpV.clear();
            }
            cout << "Constructing adjacency matrix finished." << endl;
            adLists.clear();
            return;
        }
        
        
        //========== for the ApCoCoA package ncpoly ==========
        
        bool IsReduced(const Word & w, const set<Word> & M)
        {
            for (set<Word>::iterator it = M.begin(); it != M.end(); ++it)
            {
                if (IsDivisible(w,*it))
                	return false;
            }
            return true;
        }
        
        void UfnVertices(const vector<size_t> & inds, const set<Word> & M,
                         vector<Word> & V)
        {
        	V.clear();
        	size_t k = 0;
        	for (set<Word>::iterator it = M.begin(); it != M.end(); ++it)
        		k = (it->length() > k) ? it->length() : k;
        	if (0 == k || 1 == k) //M is empty or a subset of {1,a,b,...}
                return;
        	--k; // k=-1+max{len(w)|w in M}
        	ORDER order = M.begin()->getOrder();
        	V.push_back(Word(order));
        	for (size_t len = 1; len <= k; ++len)
        	{
        		vector<Word> tmpV(V);
        		V.clear();
        		for (size_t i = 0; i != inds.size(); ++i)
        		{
        			for (size_t j = 0; j != tmpV.size(); ++j)
        			{
        				vector<size_t> w = tmpV.at(j).getWord();
        				w.push_back(inds.at(i));
        				Word new_w (w,order);
        				if (IsReduced(new_w,M))
        					V.push_back(new_w);
        				w.clear();
        			}
        		}
        		tmpV.clear();
        	}
        	return;
        }
        
        void UfnGraph(const set<Word> & M, const vector<Word> & V,
                      vector<vector<size_t> > & adLists)
        {
            adLists.clear();
            if (V.empty())
            	return;
        	size_t len = V.at(0).length();
        	ORDER order = V.at(0).getOrder();
            for (size_t i = 0; i != V.size(); ++i)
            {
                vector<size_t> adList;
                for (size_t j = 0; j != V.size(); ++j)
                {
                	vector<size_t> be = V.at(i).getWord();
                	be.push_back(V.at(j).getWord().at(len-1));
                	vector<size_t> en;
                	en.push_back(V.at(i).getWord().at(0));
                	en.insert(en.begin()+1,
                              V.at(j).getWord().begin(),V.at(j).getWord().end());
                    if (be == en && IsReduced(Word(be,order),M))
                        adList.push_back(j);
                    be.clear(); en.clear();
                }
                adLists.push_back(adList); //adList might be empty
                adList.clear();
            }
            return;
        }
        
        void adMatrix(const vector<size_t> & inds, const set<Word> & M,
                      vector<Word> & V, vector<vector<size_t> > & adMat)
        {
            adMat.clear();
            cout << "Constructing vertices..." << endl;
            UfnVertices(inds,M,V);
            cout << "Constructing vertices finished." << endl;
            cout << "Vertices (" << V.size() << "):" << endl;
            for (size_t i = 0; i != V.size(); ++i)
                cout << V.at(i) << "  ";
            cout << endl;
            cout << "Constructing adjacency matrix..." << endl;
            vector<vector<size_t> > adLists;
            UfnGraph(M,V,adLists);
            for (size_t i = 0; i != V.size(); ++i)
            {
                vector<size_t> tmpV; tmpV.assign(V.size(),0);
                for (size_t j = 0; j != adLists.at(i).size(); ++j)
                    tmpV.at(adLists.at(i).at(j)) = 1;
                adMat.push_back(tmpV);
                tmpV.clear();
            }
            cout << "Constructing adjacency matrix finished." << endl;
            adLists.clear();
            return;
        }
        
        
        //========== for the ApCoCoA packages gbmr and ncpoly ==========
        
        bool findCycles(const vector<vector<size_t> > & adLists,
        				vector<vector<size_t> > & cycles)
        {
            cycles.clear();
            set<set<size_t> > ss_cycles;
            set<size_t> startPoints;
            for (size_t i = 0; i != adLists.size(); ++i)
            {
                if (!adLists.at(i).empty())
                    startPoints.insert(i);
            }
            while (!startPoints.empty())
            {
                vector<vector<size_t> > pathsToBeAppended;
                vector<size_t> r; //path with a single start point;
                r.push_back(*startPoints.begin());
                startPoints.erase(startPoints.begin());
                pathsToBeAppended.push_back(r);
                r.clear();
                while (!pathsToBeAppended.empty()) //breadth-first searching
                {
                    vector<size_t> path = pathsToBeAppended.back();
                    pathsToBeAppended.pop_back();
                    size_t curPos = path.back();
                    if (!adLists.at(curPos).empty())
                    {
                        //add one more vertex into path
                        for (size_t j = 0; j != adLists.at(curPos).size(); ++j)
                        {
                            //insert one more vertex
                            vector<size_t> path_plus = path;
                            size_t nextPos = adLists.at(curPos).at(j);
                            path_plus.push_back(nextPos);
                            //delete nextPos from startPoints to avoid searching
                            //twice starting with nextPos.
                            set<size_t>::iterator fi = startPoints.find(nextPos);
                            if (startPoints.end() != fi)
                                startPoints.erase(fi);
                            //check if path_plus contains a cycle
                            bool isCyclic = false;
                            size_t pos = 0;
                            for (pos = 0; pos != path.size(); ++pos)
                            {
                                if (nextPos == path.at(pos))
                                {
                                    isCyclic = true;
                                    break;
                                }
                            }
                            if (isCyclic)
                            {
                                vector<size_t> cycle;
                                set<size_t> s_cycle;
                                for (size_t k = pos; k != path.size(); ++k)
                                {
                                	cycle.push_back(path.at(k));
                                	s_cycle.insert(path.at(k));
                                }
                                if (ss_cycles.end() == ss_cycles.find(s_cycle))
                                {
                                	cycles.push_back(cycle);
                                	ss_cycles.insert(s_cycle);
                                }
                                s_cycle.clear();
                                cycle.clear();
                            }
                            else
                            {
                                pathsToBeAppended.push_back(path_plus);
                            }
                            path_plus.clear();
                        }
                    }
                } //end: while (!pathsToBeAppended.empty())
            } //end: while (!startPoints.empty())
            ss_cycles.clear();
            return !cycles.empty();
        }
        
        
        bool checkIntersection(const vector<vector<size_t> > & cycles)
        {
        	if (cycles.size() <= 1)
        		return true;
        	set<size_t> vertices;
        	vector<vector<size_t> >::const_iterator cit = cycles.begin();
        	vertices.insert(cit->begin(),cit->end());
        	++cit;
            for ( ; cit != cycles.end(); ++cit)
            {
            	for (size_t i = 0; i != cit->size(); ++i)
            	{
            		if (vertices.end() == vertices.find(cit->at(i)))
            			vertices.insert(cit->at(i));
            		else
            		{
            			vertices.clear();
            			return true;
            		}
            	}
            }
            return false;
        }
        
        
        bool areConnected(const vector<vector<size_t> > & adLists,
                          const vector<size_t> & C1, const vector<size_t> & C2,
                          set<vector<size_t> > & paths)
        {
        	paths.clear();
        	size_t startVertex = C1.at(0);
        	size_t endVertex = C2.at(0);
        	set<vector<size_t> > paths_temp;
        	vector<size_t> path;
        	path.push_back(startVertex);
        	paths_temp.insert(path);
        	path.clear();
        	size_t maxLen = adLists.size();
        	while (!paths_temp.empty() && maxLen-- >= 0)
        	{
        		set<vector<size_t> > paths_temp_plus;
        		set<vector<size_t> >::iterator it;
        		for (it = paths_temp.begin(); it != paths_temp.end(); ++it)
        		{
        			set<size_t> s_path;
        			s_path.insert(it->begin(),it->end());
        			size_t pos = it->at(it->size()-1);
        			if (adLists.at(pos).empty()) //path ends
        			{
        				s_path.clear();
        				continue;
        			}
        			//breadth-first searching
        			for (size_t i = 0; i != adLists.at(pos).size(); ++i)
        			{
        				path = *it;
        				size_t pos_next = adLists.at(pos).at(i);
        				if (pos_next == endVertex) //find a path from C1 to C2
        				{
        					path.push_back(pos_next);
        					paths.insert(path);
        					s_path.clear();
        					continue;
        				}
        				else if (s_path.end() != s_path.find(pos_next)) //a cycle
        				{
        					continue;
        				}
        				else
        				{
        					//add next vertex into path
        					path.push_back(pos_next);
        					paths_temp_plus.insert(path);
        					path.clear();
        				}
        			}
        			s_path.clear();
        		} //for (it = paths_temp.begin(); it != paths_temp.end(); ++it)
        		paths_temp = paths_temp_plus;
        		paths_temp_plus.clear();
        	} //while (maxLen-- >= 0)
        	paths_temp.clear();
        	if (!paths.empty())
        	{
        		paths_temp = paths;
        		paths.clear();
        		set<size_t> V1, V2;
        		V1.insert(C1.begin(),C1.end());
        		V2.insert(C2.begin(),C2.end());
        		set<vector<size_t> >::iterator it;
        		for (it = paths_temp.begin(); it != paths_temp.end(); ++it)
        		{
        			path = *it;
        			while (path.size()>2 &&
                           V1.end() != V1.find(path.at(0)) &&
                           V1.end() != V1.find(path.at(1)))
						path.erase(path.begin());
					while (path.size()>2 &&
                           V2.end() != V2.find(path.at(path.size()-2)) &&
                           V2.end() != V2.find(path.at(path.size()-1)))
						path.pop_back();
					paths.insert(path);
					path.clear();
        		}
        		V2.clear(), V1.clear();
        	}
        	return !paths.empty();
        }
        
        
        bool Ufnarovskij(const string & X, const set<string> & M)
        {
            if (M.empty()) return false; //M=(0) and <X>/M=<X>
            bool hasUnit = false;
            size_t k = 0;
            for (set<string>::iterator it = M.begin(); it != M.end(); ++it)
            {
                if (0 == it->length()) { hasUnit = true; break; }
                if (it->length() > k) k = it->length();
            }
            if (hasUnit) return true; //M=(1) and <X>/M=1
            if (1 == k) return X.length() == M.size(); //M is a subset of X
            cout << "Constructing vertices..." << endl;
            vector<string> V;
            UfnVertices(X,M,V);
            cout << "Constructing vertices finished." << endl;
            cout << "Vertices (" << V.size() << " vertices):" << endl;
            for (size_t i = 0; i != V.size(); ++i)
                cout << "(" << i << ") " << V.at(i) << "  ";
            cout << endl;
            cout << "Constructing adjacency list..." << endl;
            vector<vector<size_t> > adLists;
            UfnGraph(M,V,adLists);
            cout << "Constructing adjacency list finished." << endl;
            cout << "Adjacency lists:" << endl;
            for (size_t i = 0; i != adLists.size(); ++i)
            {
                cout << V.at(i) << " :-> ";
                for (size_t j = 0; j != adLists.at(i).size(); ++j)
                    cout << V.at(adLists.at(i).at(j)) << "  ";
                cout << endl;
            }
            cout << "Searching cycle(s) in the Ufnarovski graph..." << endl;
            vector<vector<size_t> > cycles;
            bool isFound = findCycles(adLists,cycles);
            cout << "Searching Ufnarovski graph finished." << endl;
            if (isFound)
            {
                cout << "Found " << cycles.size() << " cycles:"<< endl;
                vector<vector<size_t> >::iterator it;
                for (it = cycles.begin(); it != cycles.end(); ++it)
                {
                    for (size_t i = 0; i != it->size(); ++i)
                        cout << V.at(it->at(i)) << "  ";
                    cout << endl;
                }
                if (checkIntersection(cycles))
                    cout << "The group has exponential growth." << endl;
                else
                    cout << "The group has polynomial growth of at most degree "
                    << cycles.size() << "." << endl;
            }
            else
            {
            	cout << "No cycle was found." << endl;
            	cout << "The group is finite." << endl;
            }
            cycles.clear(); adLists.clear(); V.clear();
            return !isFound;
        }
        
        bool Ufnarovskij(const vector<size_t> & inds, const set<Word> & M)
        {
            if (M.empty()) return false; //M=(0) and <X>/M=<X>
            bool hasUnit = false;
            size_t k = 0;
            for (set<Word>::iterator it = M.begin(); it != M.end(); ++it)
            {
                if (it->isEmpty()) { hasUnit = true; break; }
                if (it->length() > k) k = it->length();
            }
            if (hasUnit) return true; //M=(1) and <X>/M=1
            if (1 == k) return inds.size() == M.size(); //M is a subset of X
            cout << "Constructing vertices..." << endl;
            vector<Word> V;
            UfnVertices(inds,M,V);
            cout << "Constructing vertices finished." << endl;
            cout << "Vertices (" << V.size() << " vertices):" << endl;
            for (size_t i = 0; i != V.size(); ++i)
                cout << "(" << i << ") " << V.at(i) << "  ";
            cout << endl;
            cout << "Constructing adjacency list..." << endl;
            vector<vector<size_t> > adLists;
            UfnGraph(M,V,adLists);
            cout << "Constructing adjacency list finished." << endl;
            cout << "Adjacency lists:" << endl;
            for (size_t i = 0; i != adLists.size(); ++i)
            {
                cout << V.at(i) << " :-> ";
                for (size_t j = 0; j != adLists.at(i).size(); ++j)
                    cout << V.at(adLists.at(i).at(j)) << "  ";
                cout << endl;
            }
            cout << "Searching cycle(s) in the Ufnarovski graph..." << endl;
            vector<vector<size_t> > cycles;
            bool isFound = findCycles(adLists,cycles);
            cout << "Searching Ufnarovski graph finished." << endl;
            if (isFound)
            {
                cout << "Found " << cycles.size() << " cycles:"<< endl;
                vector<vector<size_t> >::iterator it;
                for (it = cycles.begin(); it != cycles.end(); ++it)
                {
                    for (size_t i = 0; i != it->size(); ++i)
                        cout << V.at(it->at(i)) << "  ";
                    cout << endl;
                }
                if (checkIntersection(cycles))
                    cout << "The group has exponential growth." << endl;
                else
                    cout << "The group has polynomial growth of at most degree "
                    << cycles.size() << "." << endl;
            }
            else
            {
            	cout << "No cycle was found." << endl;
            	cout << "The group is finite." << endl;
            }
            cycles.clear(); adLists.clear(); V.clear();
            return !isFound;
        }


  
    } // end of namespace NoncommutativeAlgorithms
} // end of namespace ApCoCoA
