//
// This file is part of the source of ApCoCoALib, the ApCoCoA Library.
//
//   Copyright (c) ApCoCoA Project (Prof. Dr. Martin Kreuzer, Uni Passau)
//
//   Author: 2013 X. XIU
//
// Visit http://apcocoa.org/ for more information regarding ApCoCoA
// and ApCoCoALib.
// Visit http://www.apcocoa.org/wiki/ApCoCoA:KnownIssues for bugs, problems
// and known issues.
//
// ApCoCoALib is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License (version 3 or later)
// as published by the Free Software Foundation. A copy of the full
// licence may be found in the file COPYING in this directory.
//
// ApCoCoALib is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ApCoCoALib; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

#include "ApCoCoA/Noncommutative/NCBPolynomial.H"
#include <stdexcept> //invalid_argument


namespace ApCoCoA {
	namespace NoncommutativeAlgorithms {

	NCBPolynomial::NCBPolynomial()
	{

	}

	NCBPolynomial::NCBPolynomial(const set<Word> & supp)
  		: mySupp(supp)
	{

	}

	NCBPolynomial::NCBPolynomial(const NCBPolynomial & rhs)
  		: mySupp(rhs.mySupp)
	{

	}

	NCBPolynomial::~NCBPolynomial()
	{
		if (!mySupp.empty())
			mySupp.clear();
	}

	NCBPolynomial & NCBPolynomial::operator=(const NCBPolynomial & rhs)
	{
		if (&rhs != this)
		{
			if (!mySupp.empty())
				mySupp.clear(); //a potential error might occur here
			mySupp = rhs.mySupp;
		}
		return *this;
	}

	NCBPolynomial & NCBPolynomial::operator+=(const NCBPolynomial & rhs)
	{
		if (!rhs.mySupp.empty())
		{
			set<Word>::const_iterator cit;
			for (cit = rhs.mySupp.begin(); cit != rhs.mySupp.end(); ++cit)
			{
				set<Word>::iterator fi = mySupp.find(*cit);
				if (mySupp.end() == fi)
					mySupp.insert(*cit);
				else
					mySupp.erase(fi);
			}
		}
		return *this;
	}

	const NCBPolynomial NCBPolynomial::operator+(const NCBPolynomial & rhs) const
	{
		NCBPolynomial p(*this);
	    p += rhs;
	    return p;
	}

	NCBPolynomial & NCBPolynomial::operator*=(const NCBPolynomial & rhs)
	{
		if (!mySupp.empty() && !rhs.mySupp.empty()) //! lhs != and rhs != 0
		{
			set<Word> supp;
			set<Word>::iterator it;
			set<Word>::const_iterator cit;
			for (it = mySupp.begin(); it != mySupp.end(); ++it)
			{
				for (cit = rhs.mySupp.begin(); cit != rhs.mySupp.end(); ++cit)
				{
					Word w = (*it) * (*cit);
					set<Word>::iterator fi = supp.find(w);
					if (supp.end() == fi)
						supp.insert(w);
					else
						supp.erase(fi);
				}
			}
			mySupp.clear(); mySupp = supp; supp.clear();
		}
		else if (!mySupp.empty()) //! lhs*rhs = 0, otherwise
		{
			mySupp.clear();
		}
		return *this;
	}

	const NCBPolynomial NCBPolynomial::operator*(const NCBPolynomial & rhs) const
	{
		NCBPolynomial p(*this);
	    p *= rhs;
	    return p;
	}

	NCBPolynomial & NCBPolynomial::operator+=(const Word & w)
	{
		set<Word>::iterator fi = mySupp.find(w);
		if (mySupp.end() == fi)
			mySupp.insert(w);
		else
			mySupp.erase(fi);
		return *this;
	}

	const NCBPolynomial NCBPolynomial::operator+(const Word & w) const
	{
		NCBPolynomial p(*this);
	    p += w;
	    return p;
	}

	NCBPolynomial & NCBPolynomial::operator*=(const Word & w)
	{
		if (!mySupp.empty() && !w.isEmpty())
		{
			set<Word> supp;
			set<Word>::iterator it;
			for (it = mySupp.begin(); it != mySupp.end(); ++it)
			{
				supp.insert((*it) * w);
			}
			mySupp.clear(); mySupp = supp; supp.clear();
		}
		return *this;
	}

	const NCBPolynomial NCBPolynomial::operator*(const Word & w) const
	{
		NCBPolynomial p(*this);
	    p *= w;
	    return p;
	}

	bool NCBPolynomial::operator<(const NCBPolynomial & rhs) const
	{
		set<Word>::const_reverse_iterator ritL = mySupp.rbegin();
		set<Word>::const_reverse_iterator ritR = rhs.mySupp.rbegin();
		for ( ; ritL != mySupp.rend() && ritR != rhs.mySupp.rend(); ++ritL, ++ritR )
		{
			if ((*ritL) < (*ritR))
				return true;
			else if ((*ritR) < (*ritL))
				return false;
		}
		if (rhs.mySupp.rend() != ritR)
			return true;
		return false;
	}

	bool NCBPolynomial::operator==(const NCBPolynomial & rhs) const
	{
		if (mySupp.size() != rhs.mySupp.size())
			return false;
	    set<Word>::const_iterator citL = mySupp.begin();
	    set<Word>::const_iterator citR = rhs.mySupp.begin();
	    for ( ; citL != mySupp.end(); )
	    {
	    	if (*citL == *citR)
	    	{
	    		++citL; ++citR;
	    	}
	    	else
	    	{
	    		return false;
	    	}
	    }
	    return true;
	}

    /* access functions */
	size_t NCBPolynomial::degree() const
	{
		size_t d = 0;
	    if (!mySupp.empty())
	    {
	    	set<Word>::const_reverse_iterator crit;
	    	for (crit = mySupp.rbegin(); crit != mySupp.rend(); ++crit)
	    	{
	    		d = (d >= crit->length()) ? d : crit->length();
	    	}
	    }
	    return d;
	}

	Word NCBPolynomial::LW() const
	{
		if (mySupp.empty())
		{
			throw std::invalid_argument("Leading word of 0 is undefined : "
					"Word NCBPolynomial::LW() const");
		}
		return *(mySupp.rbegin());
	}

    bool NCBPolynomial::isInSupp(const Word & w) const
    {
    	return mySupp.end() != mySupp.find(w);
    }

    const set<Word> & NCBPolynomial::getSupp() const
    {
    	return mySupp;
    }

    bool NCBPolynomial::isUnit() const
    {
    	return (1 == mySupp.size() && mySupp.begin()->isEmpty());
    }

    bool NCBPolynomial::isZero() const
	{
    	return mySupp.empty();
	}

    bool NCBPolynomial::isHomogeneous() const
    {
		if (!mySupp.empty())
		{
			set<Word>::const_iterator cit = mySupp.begin();
			size_t d = cit->length();
	    	for( ; cit != mySupp.end(); ++cit)
	    	{
	    		if (cit->length() != d)
	    			return false;
	    	}
		}
    	return true;
    }

    /* friend functions */
    NCBPolynomial operator+(const Word & w, const NCBPolynomial & poly)
    {
    	return poly+w;
    }

    NCBPolynomial operator*(const Word & w, const NCBPolynomial & poly)
    {
    	NCBPolynomial ret_poly;
    	set<Word>::const_iterator cit = poly.mySupp.begin();
        for ( ; cit != poly.mySupp.end(); ++cit)
        {
        	ret_poly.mySupp.insert(w * (*cit));
        }
        return ret_poly;
    }

    ostream & operator<<(ostream & out, const NCBPolynomial & poly)
    {
    	if (poly.mySupp.empty())
    	{
    		out << 0;
    		return out;
    	}
    	set<Word>::const_reverse_iterator crit = poly.mySupp.rbegin();
    	out << *crit;
    	while (++crit != poly.mySupp.rend())
    	{
    		out << "+" << *crit;
    	}
    	return out;
    }


	} /* end of namespace NoncommutativeAlgorithms */
} /* end of namespace ApCoCoA */
