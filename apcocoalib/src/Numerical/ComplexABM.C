//
// This file is part of the source of ApCoCoALib, the ApCoCoA Library.
//
//   Copyright (c) ApCoCoA Project (Prof. Dr. Martin Kreuzer, Uni Passau)
//
//   Author: 2009-2012 Jan Limbeck
//
// Visit http://apcocoa.org/ for more information regarding ApCoCoA
// and ApCoCoALib.
// Visit http://www.apcocoa.org/wiki/ApCoCoA:KnownIssues for bugs, problems 
// and known issues.
//
// ApCoCoALib is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License (version 3 or later)
// as published by the Free Software Foundation. A copy of the full
// licence may be found in the file COPYING in this directory.
//
// ApCoCoALib is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ApCoCoALib; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

#include "ApCoCoA/Numerical/Internal/EvaluatedPolynomialsComplexABM.H"
#include "ApCoCoA/Numerical/Internal/EvaluatedPolynomialsExact.H"
#include "ApCoCoA/Numerical/ComplexABM.H"
#include "ApCoCoA/Numerical/AVIUtilities.H"
#include "ApCoCoA/Numerical/Internal/ComplexDenseMatrix.H"

#include "CoCoA/io.H"

#include "CoCoA/DenseMatrix.H"
#include "CoCoA/RingQ.H"
#include "ApCoCoA/Numerical/NumericalFoundation.H"

#include <iostream>
#include <string.h>
#include <vector>

using namespace CoCoA;
using namespace std;

////////////////////////////////////////////////////////////////////////////////

namespace ApCoCoA{
namespace NumericalAlgorithms{


////////////////////////////////////////////////////////////////////////////////
  void ComplexABM(ABM::ComplexDenseMatrix& Points,
                               const SparsePolyRing& polyRing, 
                               PolyList &Basis,
                               PolyList *OrderIdeal,
							   const OptionsABM &options)
  {
    PolyList GivenBasis;
    GivenBasis.push_back(one(polyRing));
	ComplexSubABM(Points, polyRing, 
                  Basis, GivenBasis,
                  OrderIdeal, options);
  }
  
////////////////////////////////////////////////////////////////////////////////

  void ComplexExtABM(ABM::ComplexDenseMatrix &points,
                            ABM::ComplexDenseMatrix &funValues,
                            const SparsePolyRing &polyRing, 
                            PolyList &basis,
                            PolyList *orderIdeal,
							const OptionsABM &options)
  {
    PolyList givenBasis;
    givenBasis.push_back(one(polyRing));
		 
	ComplexSubExtABM(points,
                     funValues,
                     polyRing, 
                     basis,
                     givenBasis,
                     orderIdeal,
					 options);
  }

  
////////////////////////////////////////////////////////////////////////////////

  void ComplexSubABM(ABM::ComplexDenseMatrix& Points,
                     const SparsePolyRing& polyRing, 
                     PolyList &Basis,
                     PolyList &GivenBasis,
                     PolyList *OrderIdeal,
					 const OptionsABM &options)
  {

	//GlobalLogput() << "[Log] System=CABM Step=1" << endl;

	int deg = 0;
    try 
    {
		if(!Basis.empty())
		{
			Basis.clear();
		}
		
		if(OrderIdeal != NULL && !OrderIdeal->empty())
		{
          OrderIdeal->clear();
		}
		
		//GlobalLogput() << "[Log] System=CABMAction=createUniverse" << endl;

		ABM::EvaluatedPolynomialsComplexABM Universe(polyRing, GivenBasis, options.basisType(), Points);

		//the main iteration loop:
		while(!Universe.empty())
		{
		
		   // check if we have reached the maximal degree
		   if (deg > options.maxDeg())
		   {
			GlobalLogput() << "We have reached the maximum degree! Only a partial result will be returned" << endl;
			break;
		   }
		    ++deg;

			vector<bool> isLT;
			isLT.resize(Universe.size() + GivenBasis.size(), false);

			//GlobalLogput() << "[Log] System=CABMAction=buildRealtions" << endl;
			// we distinguish between the BB-ABM and the normal ABM
			if (options.isBBABM())
			{
				Universe.buildRelationsBB(Basis, isLT, options);
			} else
			{
			    Universe.buildRelations(Basis, isLT, options);
		    } 

			//GlobalLogput() << "[Log] System=CABMAction=ExtendTerms" << endl;

			Universe.extend(isLT);
			
			// in case the GivenBasis is still not empty we have to feed in the 
			// polynomials of the current degree
			// this means that the actual computation may stop before the last polynomial
			// from given basis is fed in. This should be ok as we have alreasy found an
			// approximate border basis.
			
			if (!GivenBasis.empty() && !Universe.empty())
			{ Universe.addBasisElements(GivenBasis); }
	  
		} // end while

		if (OrderIdeal != NULL)
		{
			// the elements are copied into OrderIdeal
			Universe.getMyOrderIdeal(OrderIdeal);
		}
				
		if (options.isNoMoreLinear())
		{
		    // ToDo: add functionality again
			//IsAlmostLinearDependent(&Universe, options.eps());
		}
    }
	catch(const ErrorInfo &err)
    {

		if(!Basis.empty())
		{ // if parts of the results were computed, 
			Basis.clear(); // wipe them out...
		}
	   
		if(OrderIdeal != NULL)
		{
			if (!OrderIdeal->empty())
			OrderIdeal->clear();
		}	   
		throw err;
    }
    catch(...)    // something bad has happend...
    {
	
		if(!Basis.empty())
		{
			// if parts of the results were computed, 
			Basis.clear(); // wipe them out...
		}
  
		if(OrderIdeal != NULL)
		{
			if(!OrderIdeal->empty())
			{
				OrderIdeal->clear();
			}
		}	
		CoCoA_ERROR(ERR::nonstandard,"Numerical::ComplexSubABM:something went wrong");
    } // end catch

  } // end ComplexSubABM()

////////////////////////////////////////////////////////////////////////////////

  void ComplexSubExtABM(ABM::ComplexDenseMatrix &Points,
                        ABM::ComplexDenseMatrix &FunValues,
                        const CoCoA::SparsePolyRing &polyRing, 
                        CoCoA::PolyList &Basis,
                        CoCoA::PolyList &GivenBasis,
                        CoCoA::PolyList *OrderIdeal,
						const OptionsABM &options)
  {
	//  GlobalLogput() << "[Log] System=CEXTABM Step=1" << endl;
    
	int deg = 0;
    try
    {
		Basis.clear();
		
		if(OrderIdeal != NULL)
		{  OrderIdeal->clear();  }

		ABM::EvaluatedPolynomialsComplexABM Universe(polyRing, GivenBasis, options.basisType(), Points);

		//the main iteration loop:
		while(!Universe.empty())
		{
		   // check if we have reached the maximal degree
		   if (deg > options.maxDeg())
		   {
			GlobalLogput() << "We have reached the maximum degree! Only a partial result will be returned." << endl;
			break;
		   }
		    ++deg;

			vector<bool> isLT;
			isLT.resize(Universe.size() + GivenBasis.size(), false);

			Universe.buildRelations(Basis, FunValues, isLT, options); 

			//GlobalLogput() << "[Log] System=CEXTABM=ExtendTerms" << endl;

			Universe.extend(isLT);
			
			// in case the GivenBasis is still not empty we have to feed in the 
			// polynomials of current degree
			
			if (!GivenBasis.empty() && !Universe.empty())
			{ Universe.addBasisElements(GivenBasis); }
	  
		} // end while

		if (OrderIdeal != NULL)
		{
			// the elements are copied into OrderIdeal
			Universe.getMyOrderIdeal(OrderIdeal);
		}
		
    }
	catch(const ErrorInfo &err)
    {

		if(!Basis.empty())
		{ // if parts of the results were computed, 
			Basis.clear(); // wipe them out...
		}
	   
		if(OrderIdeal != NULL)
		{
			if (!OrderIdeal->empty())
			OrderIdeal->clear();
		}	   
		throw err;
    }
    catch(...)    // something bad has happend...
    {
	
		if(!Basis.empty())
		{
			// if parts of the results were computed, wipe them out...
			Basis.clear();
		}
  
		if(OrderIdeal != NULL)
		{
			if(!OrderIdeal->empty())
			{
				OrderIdeal->clear();
			}
		}	
		CoCoA_ERROR(ERR::nonstandard,"Numerical::ComplexSubExtABM:something went wrong");
    } // end catch

  } // end SubIdealOfPoints()
  
 ////////////////////////////////////////////////////////////////////////////////
 
 // ToDo: implement exact version of algorithm 
  /* void ComputeSubIdealOfPointsABM(matrix &Points,
                               const SparsePolyRing &polyRing, 
                               PolyList &Basis,
                               PolyList &GivenBasis,
                               PolyList *OrderIdeal,
							   const OptionsABM &options)
  {

	//    GlobalLogput() << "[Log] System=ABM Step=1" << endl;

    try // just to be sure....
    {
		if(!Basis.empty())
		{
			Basis.clear();
		}
		
		if(OrderIdeal != NULL && !OrderIdeal->empty())
		{
          OrderIdeal->clear();
		}
		
		// we got to do it in a degreee wise fashion if we deal with the subideal version
		ABM::EvaluatedPolynomialsExact Universe(polyRing, GivenBasis, options.basisType(), Points);

		//the main iteration loop:
		while(!Universe.empty())
		{

			vector<bool> isLT;
			// we need to reserve some more space as potentially the size can grow with all the elements of givenbasis
			isLT.resize(Universe.size()+GivenBasis.size(), false);
	  
			//GlobalLogput() << "[Log] created isLT" << endl;

			Universe.buildRelations(Basis, isLT, GivenBasis);

			GlobalLogput() << "[Log] System=ABMAction=ExtendTerms" << endl;

			Universe.extend(isLT);
	  
		} // end while

		if (OrderIdeal!= NULL)
		{
			// the elements are copied into OrderIdeal
			Universe.getMyOrderIdeal(OrderIdeal);
		}
				
    }
	catch(const ErrorInfo &err)
    {

		if(!Basis.empty())
		{ // if parts of the results were computed, 
			Basis.clear(); // wipe them out...
		}
	   
		if(OrderIdeal != NULL)
		{
			if (!OrderIdeal->empty())
			OrderIdeal->clear();
		}	   
		throw err;
    }
    catch(...)    // something bad has happend...
    {
	
		if(!Basis.empty())
		{
			// if parts of the results were computed, 
			Basis.clear(); // wipe them out...
		}
  
		if(OrderIdeal != NULL)
		{
			if(!OrderIdeal->empty())
			{
				OrderIdeal->clear();
			}
		}	
		CoCoA_ERROR(ERR::nonstandard,"Numerical::SubIdealOfPoints:something went wrong");
    } // end catch

  } // end SubIdealOfPoints() */


////////////////////////////////////////////////////////////////////////////////

} // end of namespace NumericalAlgorithms
} // end of namespace ApCoCoA

