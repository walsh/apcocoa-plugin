//
// This file is part of the source of ApCoCoALib, the ApCoCoA Library.
//
//   Copyright (c) ApCoCoA Project (Prof. Dr. Martin Kreuzer, Uni Passau)
//
//   Author: 2007 Daniel Heldt
//
// Visit http://apcocoa.org/ for more information regarding ApCoCoA
// and ApCoCoALib.
// Visit http://www.apcocoa.org/wiki/ApCoCoA:KnownIssues for bugs, problems 
// and known issues.
//
// ApCoCoALib is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License (version 3 or later)
// as published by the Free Software Foundation. A copy of the full
// licence may be found in the file COPYING in this directory.
//
// ApCoCoALib is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ApCoCoALib; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 US


#include "ApCoCoA/Algebraic/PPMonoidModSquares.H"

#include "CoCoA/library.H"

#include <iostream>
#include <iterator>

namespace ApCoCoA{
namespace AlgebraicCore{

typedef CoCoA::PPMonoidElemRawPtr RawPtr;      
typedef CoCoA::PPMonoidElemConstRawPtr ConstRawPtr;

class PPMonoidModSquares: public CoCoA::PPMonoidBase{

private:
  long NumIndets;
  std::size_t myNumBlocks;
  mutable CoCoA::MemPool myMemMgr;     // IMPORTANT: this must come *before* myIndetVector and myOnePtr.
  std::size_t myBlockSize;
  std::auto_ptr<CoCoA::PPMonoidElem> myOnePtr;  
  std::vector<CoCoA::symbol> myIndetNames;
  std::vector<CoCoA::PPMonoidElem> myIndetVector;
  FastSquareFreeOrderings myOrdering;
  
public:
  PPMonoidModSquares(const std::vector<CoCoA::symbol>& IndetNames, FastSquareFreeOrderings ord);
//  PPMonoidModSquares(PPMonoidModSquares & ppm);
  ~PPMonoidModSquares();
  
  virtual RawPtr myNew() const;                     // initialize pp to the identity
  virtual RawPtr myNew(const std::vector<long> & v) const; // initialize pp from exponent vector v
  virtual RawPtr myNew(ConstRawPtr pp1) const;     // initialize pp from pp1
   
  virtual void myDelete(RawPtr pp) const;           // destroy pp, frees memory
  
  virtual void mySwap(RawPtr pp1, RawPtr pp2) const;             // swap the values of pp1 and pp2
  virtual void myAssign(RawPtr pp, ConstRawPtr pp1) const;       // assign the value of pp1 to pp
  virtual void myAssign(RawPtr pp, const std::vector< long>& v) const; // assign to pp the PP with exponent vector v
  virtual void myAssignOne(RawPtr pp) const; 
  
  virtual const CoCoA::PPMonoidElem&  myOne() const;                           // reference to 1 in the monoid
  virtual void myMul(RawPtr pp, ConstRawPtr pp1, ConstRawPtr pp2) const;   // pp = pp1*pp2
  virtual void myMulIndetPower(RawPtr pp, long var, long exp) const;   // pp *= indet(var)^exp
  virtual void myDiv(RawPtr pp, ConstRawPtr pp1, ConstRawPtr pp2) const;   // pp = pp1/pp2 (if it exists)
  virtual void myColon(RawPtr pp, ConstRawPtr pp1, ConstRawPtr pp2) const;// pp = pp1/gcd(pp1,pp2)
  virtual void myGcd(RawPtr pp, ConstRawPtr pp1, ConstRawPtr pp2) const;   // pp = gcd(pp1, pp2)
  virtual void myLcm(RawPtr pp, ConstRawPtr pp1, ConstRawPtr pp2) const;   // pp = lcm(pp1, pp2)
  virtual void myPower(RawPtr pp, ConstRawPtr pp1, unsigned long exp) const;        // pp = pp1^exp
  virtual void myPowerSmallExp(RawPtr pp, ConstRawPtr pp1, long int exp) const;
  
  virtual long myNumIndets() const;                                 // number of indeterminates generating the monoid
  virtual const CoCoA::symbol&  myIndetName(std::size_t var) const;                // name of indet with index var
  virtual const std::vector<CoCoA::PPMonoidElem>& myIndets() const;               ///< std::vector whose n-th entry is n-th indet as PPMonoidElem
  virtual void mySymbols(std::vector<CoCoA::symbol>& SymList) const;
  const CoCoA::symbol& myIndetSymbol(long indet) const;
  virtual long myStdDeg(ConstRawPtr pp1) const;
  
  virtual void myRadical(RawPtr rawpp, ConstRawPtr rawpp1) const; 
  virtual bool myIsRadical(ConstRawPtr rawpp) const;
  
  
  virtual bool myIsCoprime(ConstRawPtr pp1, ConstRawPtr pp2) const;     // true iff gcd(pp1, pp2) is 1
  virtual bool myIsDivisible(ConstRawPtr pp1, ConstRawPtr pp2) const;     // true iff t1 is divisible by t2
  virtual int myCmp(ConstRawPtr pp1, ConstRawPtr pp2)   const;           // result is <0, =0, >0 according as t1 <,=,> t2
  virtual int myHomogCmp(ConstRawPtr pp1, ConstRawPtr pp2)  const;       // as cmp, but assumes t1 and t2 have the same degree
  virtual bool myIsOne(ConstRawPtr pp1) const;      
  virtual bool myIsIndet(long& n, ConstRawPtr pp1) const;
  
  
  virtual void myWDeg(CoCoA::degree&, ConstRawPtr) const;
  virtual int myCmpWDeg(ConstRawPtr, ConstRawPtr) const;
  virtual int myCmpWDegPartial(ConstRawPtr rawpp1, ConstRawPtr rawpp2, long n) const;

  virtual long myExponent(ConstRawPtr pp1, long n) const; // degree in var (in an unsigned type)
  virtual void myZZExponent(CoCoA::BigInt& zz, ConstRawPtr pp1, long n) const;
  void myZZExponents(std::vector<CoCoA::BigInt>& v, ConstRawPtr rawpp) const;
  virtual void myExponents(std::vector<long>& v, ConstRawPtr t)  const;    // get exponents
  
  virtual void myComputeDivMask(CoCoA::DivMask&, const CoCoA::DivMaskRule&, ConstRawPtr) const;    
  virtual void myOutputSelf(std::ostream&) const; 
};


PPMonoidModSquares::PPMonoidModSquares(const std::vector<CoCoA::symbol>& pIndetNames, FastSquareFreeOrderings ord):
 PPMonoidBase( (ord == Lex) ? CoCoA::MakeTermOrd(CoCoA::LexMat(pIndetNames.size())) : 
              ( (ord ==DegLex)?                                               // ToDo:
                CoCoA::MakeTermOrd(CoCoA::StdDegLexMat(pIndetNames.size())):
                CoCoA::MakeTermOrd(CoCoA::StdDegRevLexMat(pIndetNames.size()))
              )),                                                             // taking an ordering as input, etc.pp
              myMemMgr( (   (pIndetNames.size() / (sizeof(unsigned long)*8) ) + 
              (((pIndetNames.size() % (sizeof(unsigned long)*8))  !=0)? 1: 0)
              )
              *sizeof(unsigned long), "PPMonoidModSquares.myMemMgr"),
              myIndetNames(pIndetNames)

            // i don't understand the french, either....
            
{

  // check indet names?
   
  NumIndets = (pIndetNames.size()),
  myOrdering   = ord;
  myBlockSize  = sizeof(unsigned long)*8;

  myNumBlocks =  NumIndets / myBlockSize + (((NumIndets % myBlockSize) != 0)? 1 : 0);

  myOnePtr.reset( new CoCoA::PPMonoidElem(CoCoA::PPMonoid(this)));
  // create indet vector
  {
    CoCoA::PPMonoidElem ppe(CoCoA::PPMonoid(this));
  
    std::vector< long> exp (NumIndets);
    for (long i=0; i < NumIndets; ++i)
    {
      exp[i] = 1;
      myAssign(CoCoA::raw(ppe), exp);
      myIndetVector.push_back(ppe);
      exp[i] = 0;
    }
  }
  myRefCountZero();
}

PPMonoidModSquares::~PPMonoidModSquares(){
}


RawPtr PPMonoidModSquares::myNew() const{
  RawPtr rawptr(myMemMgr.alloc());
  unsigned long * exp  = static_cast<unsigned long*>(rawptr.myRawPtr());
  for (std::size_t i=0; i< myNumBlocks; ++i){
      exp[i] =0;
  }
  return rawptr; 
}

RawPtr PPMonoidModSquares::myNew(const std::vector<long>& v) const{

  if (v.size() != static_cast<std::size_t>(NumIndets))
    CoCoA_ERROR(CoCoA::ERR::BadNumIndets, "PPMonoidModSquares::myNew(const std::vector<long>)");

  RawPtr rawptr(myMemMgr.alloc());
  unsigned long h = static_cast<unsigned long>(1) << (myBlockSize-1);
  std::size_t k =0;
  
  unsigned long * exp  = static_cast<unsigned long*>(rawptr.myRawPtr());
  
  for (std::size_t i=0; i< myNumBlocks; ++i)
  {
    exp[i] =0;
    h= static_cast<unsigned long>(1) << (myBlockSize-1);
        
    for (std::size_t j=0; j < myBlockSize; ++j)
    {
      if ( v[k] != 0 ){
        exp[i] ^= h;
      }
      h >>= 1;
      ++k;
      
      if ( k== static_cast<std::size_t>(NumIndets)){
        return rawptr;
      }
    }
  }

  return rawptr; 
}

const CoCoA::PPMonoidElem& PPMonoidModSquares::myOne()  const
{
  return *myOnePtr;
}

RawPtr PPMonoidModSquares::myNew(ConstRawPtr pp1) const
{
  RawPtr rawptr(myMemMgr.alloc());
  // Warning, what if pp1 is inanother ppmonoid?
  
  unsigned long * exp  = static_cast<unsigned long*>(rawptr.myRawPtr());
  const unsigned long * oldexp  = static_cast<const unsigned long*>(pp1.myRawPtr());

  for (std::size_t i=0; i< myNumBlocks; ++i){ // use rawcopy instead?
    exp[i] =oldexp[i];
  }
  return rawptr;
}  

void PPMonoidModSquares::myDelete(RawPtr pp) const 
{
  myMemMgr.free(pp.myRawPtr());
}

void PPMonoidModSquares::mySwap(RawPtr pp1, RawPtr pp2)const
{
  unsigned long helper;
  unsigned long * expA = static_cast<unsigned long*>(pp1.myRawPtr());
  unsigned long * expB = static_cast<unsigned long*>(pp2.myRawPtr());

  for ( std::size_t i=0; i<myNumBlocks; ++i){
    helper = expA[i]; 
    expA[i] = expB[i];
    expB[i] = helper;
  }
}


void PPMonoidModSquares::myAssignOne(RawPtr pp) const
{
  unsigned long * expA = static_cast<unsigned long*>(pp.myRawPtr());
  for ( std::size_t i=0; i<myNumBlocks; ++i){
    expA[i] = 0; 
  }
}

void PPMonoidModSquares::myAssign(RawPtr pp, ConstRawPtr pp1)const
{
  unsigned long * expA = static_cast<unsigned long*>(pp.myRawPtr());
  const unsigned long * expB = static_cast<const unsigned long*>(pp1.myRawPtr());

  for ( std::size_t i=0; i<myNumBlocks; ++i){ // use rawcopy instead?
    expA[i] = expB[i];     
  }
}

void PPMonoidModSquares::myAssign(RawPtr pp, const std::vector<long>& v)const
{
  if (v.size() != static_cast<std::size_t>(NumIndets))
    CoCoA_ERROR(CoCoA::ERR::BadNumIndets, "PPMonoidModSquares::myAssign(RawPtr, std::vector<long>)");

  unsigned long h= static_cast<unsigned long>(1) <<(myBlockSize -1);
  
  std::size_t k =0;
  unsigned long * exp  = static_cast<unsigned long*>(pp.myRawPtr());
  
  for (std::size_t i=0; i< myNumBlocks; ++i)
  {
    exp[i] = 0;
    for (std::size_t j=0; j < myBlockSize; ++j)
    {
    
      if ( v[k] != 0 )
        exp[i] ^= h;
        
      h >>= 1;
      ++k;
      if ( k== static_cast<std::size_t>(NumIndets)){
         return;
      }
    }
    h= 1 <<( myBlockSize -1);
  }  
}


void PPMonoidModSquares::myMul(RawPtr pp, ConstRawPtr pp1, ConstRawPtr pp2)const
{
  unsigned long * exp        = static_cast<unsigned long*>(pp.myRawPtr());
  const unsigned long * expA = static_cast<const unsigned long*>(pp1.myRawPtr());
  const unsigned long * expB = static_cast<const unsigned long*>(pp2.myRawPtr());
  
  for (std::size_t i=0; i< myNumBlocks;++i){
    exp[i] = expA[i] | expB[i]; 
  }
}


void PPMonoidModSquares::myMulIndetPower(RawPtr pp, long var, long exp) const
{

  unsigned long * expA = static_cast<unsigned long*>(pp.myRawPtr());
  if (exp != 0 ){
    std::size_t block = var / (myBlockSize); //better do this with shifting...
    std::size_t index = var % (myBlockSize);  
    expA[block] |= static_cast<unsigned long>(1) <<(myBlockSize -index -1);
  }  
}

void PPMonoidModSquares::myDiv(RawPtr pp, ConstRawPtr pp1, ConstRawPtr pp2)const
{
  unsigned long * exp        = static_cast<unsigned long*>(pp.myRawPtr());
  const unsigned long * expA = static_cast<const unsigned long*>(pp1.myRawPtr());
  const unsigned long * expB = static_cast<const unsigned long*>(pp2.myRawPtr());

  unsigned long ones =0 ; unsigned long h = 255;            // doing a bitwise not
  for(std::size_t i=0; i<sizeof(unsigned long); ++i)    // via XOR 1 ...
    ones = (ones<<8) | h;                               // is this doable more
                                                        // elegant?
  for (std::size_t i=0; i< myNumBlocks;++i){
    exp[i] = expA[i] & (expB[i] ^ ones); 
  }
}

void PPMonoidModSquares::myColon(RawPtr pp, ConstRawPtr pp1, ConstRawPtr pp2)const
{
  unsigned long * exp        = static_cast<unsigned long*>(pp.myRawPtr());
  const unsigned long * expA = static_cast<const unsigned long*>(pp1.myRawPtr());
  const unsigned long * expB = static_cast<const unsigned long*>(pp2.myRawPtr());
  /*
  <Xore> ;_;
  <Xore> dammit
  <Xore> you know you have problems when...
  <Xore> you write really really sexy code
  <Xore> and you belatedly realize
  <Xore> that your code is a whole heap sexier than you are
  http://bash.org/?808899
  */
                                  
  for (std::size_t i=0; i< myNumBlocks;++i){
    exp[i] = expA[i] & (expA[i] ^ expB[i]);
  }  
}

void PPMonoidModSquares::myGcd(RawPtr pp, ConstRawPtr pp1, ConstRawPtr pp2)const
{
  unsigned long * exp        = static_cast<unsigned long*>(pp.myRawPtr());
  const unsigned long * expA = static_cast<const unsigned long*>(pp1.myRawPtr());
  const unsigned long * expB = static_cast<const unsigned long*>(pp2.myRawPtr());
  
  for (std::size_t i=0; i< myNumBlocks;++i){
    exp[i] = expA[i] & expB[i]; 
  }  
}

void PPMonoidModSquares::myLcm(RawPtr pp, ConstRawPtr pp1, ConstRawPtr pp2) const
{
  unsigned long * exp        = static_cast<unsigned long*>(pp.myRawPtr());
  const unsigned long * expA = static_cast<const unsigned long*>(pp1.myRawPtr());
  const unsigned long * expB = static_cast<const unsigned long*>(pp2.myRawPtr());
  
  for (std::size_t i=0; i< myNumBlocks;++i){
    exp[i] = expA[i] | expB[i]; 
  }  
}


void PPMonoidModSquares::myRadical(RawPtr rawpp, ConstRawPtr rawpp1) const
{
    CoCoA_ERROR(CoCoA::ERR::NYI,"NonCommutativePPMonoidImpl::myRadical");
}

bool PPMonoidModSquares::myIsRadical(ConstRawPtr rawpp) const
{
	CoCoA_ERROR(CoCoA::ERR::NYI,"NonCommutativePPMonoidImpl::myIsRadical");
	return true;
}

void PPMonoidModSquares::myPower(RawPtr pp, ConstRawPtr pp1, unsigned long exp) const
{
  if (exp != 0)
    myAssign(pp,pp1);
  else
    myAssignOne(pp);
}

void PPMonoidModSquares::myPowerSmallExp(RawPtr pp, ConstRawPtr pp1, long int exp) const
{
  if (exp != 0)
    myAssign(pp,pp1);
  else
    myAssignOne(pp);
}

long PPMonoidModSquares::myNumIndets()const
{
  return NumIndets;  
}

const CoCoA::symbol& PPMonoidModSquares::myIndetName(std::size_t var) const
{
  if(var >= static_cast<std::size_t>(NumIndets))
    CoCoA_ERROR(CoCoA::ERR::BadIndetIndex, "PPMonoidModSquares::myIndetName(var)");
  return myIndetNames[var];
}


const std::vector<CoCoA::PPMonoidElem>& PPMonoidModSquares::myIndets() const
{
  return myIndetVector;
}

void PPMonoidModSquares::mySymbols(std::vector<CoCoA::symbol>& SymList) const
{
  std::copy(myIndetNames.begin(), myIndetNames.end(), back_inserter(SymList));
}


bool PPMonoidModSquares::myIsCoprime(ConstRawPtr pp1, ConstRawPtr pp2) const
{
  const unsigned long * expA = static_cast<const unsigned long*>(pp1.myRawPtr());
  const unsigned long * expB = static_cast<const unsigned long*>(pp2.myRawPtr());

  for (std::size_t i=0; i< myNumBlocks;++i){
  
    if ( (expA[i] & expB[i]) != static_cast<unsigned long>(0) )
      return false;
  }  
  return true;
}

bool PPMonoidModSquares::myIsDivisible(ConstRawPtr pp1, ConstRawPtr pp2) const
{
  const unsigned long * expA = static_cast<const unsigned long*>(pp1.myRawPtr());
  const unsigned long * expB = static_cast<const unsigned long*>(pp2.myRawPtr());
   
  for (std::size_t i=0; i< myNumBlocks;++i){
    if ( ((expA[i] ^ expB[i]) & expB[i])  != static_cast<unsigned long>(0) )
      return false;
  }  
  return true;
}

int  PPMonoidModSquares::myCmp(ConstRawPtr pp1, ConstRawPtr pp2)   const
{
  const unsigned long * expA = static_cast<const unsigned long*>(pp1.myRawPtr());
  const unsigned long * expB = static_cast<const unsigned long*>(pp2.myRawPtr());

  switch(myOrdering){
    case DegRevLex:  // it works, but don't ask why ;)
    {
      if(myNumBlocks > 1)
      {
        const std::size_t d1 = myStdDeg(pp1);
        const std::size_t d2 = myStdDeg(pp2);
        if (d1 < d2 )
          return -1;
        if (d1 > d2)
          return 1;

        for (std::size_t i=myNumBlocks; i> 0;--i)
        {
          if ( expA[i-1] != expB[i-1] )
          {
            unsigned long index =static_cast<unsigned long>(1);
            for (std::size_t j=0; j< myBlockSize;++j)
            {
              if( (expA[i-1] & index ) != 0 && (expB[i-1] & index ) == 0)
                return -1;
              if( (expA[i-1] & index ) == 0 && (expB[i-1] & index ) != 0)
                return 1;
              index <<= 1;
            } // for
          } // if
        } // for
        return 0;
      } // if (NumBlocks > 1)
    } // case DegRevLex
        
    case DegLex:
    {
      const std::size_t d1 = myStdDeg(pp1);
      const std::size_t d2 = myStdDeg(pp2);
      if (d1 < d2 )
        return -1;
      if (d1 > d2)
        return 1;
      // check lex part:
    }
    case Lex: // obvious, isn't it? 
    {
      for (std::size_t i=0; i< myNumBlocks;++i){
        if ( expA[i] < expB[i] )
            return -1;
        if ( expA[i] > expB[i] )
            return 1;
      }
      return 0;  
    }  
  } 
  // result is <0, =0, >0 according as t1 <,=,> t2
  return 0;
}

bool PPMonoidModSquares::myIsOne(ConstRawPtr pp1) const
{
  const unsigned long * expA = static_cast<const unsigned long*>(pp1.myRawPtr());
  for (std::size_t i=0; i< myNumBlocks;++i)
    if ( expA[i] !=0 )
      return false;
    
  return true;
}

bool PPMonoidModSquares::myIsIndet(long& n, ConstRawPtr pp1) const
{

  const unsigned long * expA = static_cast<const unsigned long*>(pp1.myRawPtr());
  const unsigned long h = static_cast<unsigned long>(1) <<  (myBlockSize - (n % myBlockSize) -1); 
  const unsigned long t = n / myBlockSize;
  
  for (std::size_t i=0; i< myNumBlocks;++i)
    if ( i != t){
      if( expA[i] !=0 )
        return false;
    }else
      if(expA[i]!= h )
        return false;
  
  return true;  
}

int  PPMonoidModSquares::myHomogCmp(ConstRawPtr pp1, ConstRawPtr pp2)  const
{
  const unsigned long * expA = static_cast<const unsigned long*>(pp1.myRawPtr());
  const unsigned long * expB = static_cast<const unsigned long*>(pp2.myRawPtr());

  for (std::size_t i=0; i< myNumBlocks;++i){
    if ( expA[i] < expB[i] )
      return -1;
    if ( expA[i] > expB[i] )
      return 1;
  }
  return 0;  
}

long PPMonoidModSquares::myStdDeg(ConstRawPtr pp1) const
{
  const unsigned long * expA = static_cast<const unsigned long*>(pp1.myRawPtr());
  unsigned long index = 1;
  std::size_t result=0;
  
  for (std::size_t j=0; j< myBlockSize; ++j){
      
    for (std::size_t i=0; i< myNumBlocks;++i){
      if( (expA[i] & index ) != 0)
        ++result;
    }
    index <<= 1;
  }
  return result;
}


void PPMonoidModSquares::myWDeg(CoCoA::degree& deg, ConstRawPtr pp1) const
{
  CoCoA::SetComponent(deg, 0, myStdDeg(pp1) ); // warning! computes only the StdDeg, 
}                                               // no matter what deg is! fix this

int PPMonoidModSquares::myCmpWDeg(ConstRawPtr pp1, ConstRawPtr pp2) const
{
  const std::size_t a = myStdDeg(pp1);
  const std::size_t b = myStdDeg(pp2);

  if (a > b)
      return 1;

  if( a < b)
      return -1;
  
  return 0;
}

int PPMonoidModSquares::myCmpWDegPartial(ConstRawPtr rawpp1, ConstRawPtr rawpp2, long n) const
{
    // FIXME
	// implement the function correctly
  const std::size_t a = myStdDeg(rawpp1);
  const std::size_t b = myStdDeg(rawpp2);

  if (a > b)
      return 1;

  if( a < b)
      return -1;
  
  return 0;
}


long PPMonoidModSquares::myExponent(ConstRawPtr pp1, long n) const
{
  // warning, check if n is in range!
  const unsigned long * expA = static_cast<const unsigned long*>(pp1.myRawPtr());
  const unsigned long h =  static_cast<unsigned long>(1) << (myBlockSize - (n% myBlockSize) -1);
  const unsigned long t = n / myBlockSize;
  
  if ((expA[t] & h) != 0)
    return 1;
  return 0;
}

void PPMonoidModSquares::myZZExponent(CoCoA::BigInt& zz, ConstRawPtr pp1, long n) const
{
  const unsigned long * expA = static_cast<const unsigned long*>(pp1.myRawPtr());
  const unsigned long h = static_cast<unsigned long>(1) << (myBlockSize - (n % myBlockSize)-1);
  const unsigned long t = n / myBlockSize;  

  if( (expA[t] &h)  !=0 )
    zz =1;
  else
    zz =0;
}

void PPMonoidModSquares::myExponents(std::vector<long>& v, ConstRawPtr pp1)  const
{
  const unsigned long * expA = static_cast<const unsigned long*>(pp1.myRawPtr());
  unsigned long index = static_cast<unsigned long>(1) <<(myBlockSize -1);
  v.resize(NumIndets); // sometimes i REALLY hate c++. took me 3 hours to understand the bug and add this line :(
  
  std::size_t i =0;
  
  for (std::size_t k=0; k< myNumBlocks;++k)
  {
    for (std::size_t j=0; j< myBlockSize; ++j)
    {
      if( (expA[k] & index ) != 0)
        v[i] = 1;
      else
        v[i] = 0;
      
      index >>= 1;
      ++i;
      
      if( i == static_cast<std::size_t>(NumIndets))
        return;
    }
    index = static_cast<unsigned long>(1) << (myBlockSize -1);
  }
}

void PPMonoidModSquares::myComputeDivMask(CoCoA::DivMask& dm, const CoCoA::DivMaskRule& DivMaskImpl, ConstRawPtr rawpp) const
{
  // CoCoA_ERROR(CoCoA::ERR::NYI, "PPMonoidModSquares::myComputeDivMask");
  std::vector<CoCoA::SmallExponent_t>  t;
  t.reserve(NumIndets);
  std::vector<long> t2;
  myExponents(t2,rawpp);
 
  for(long i=0; i < NumIndets; ++i)
    t.push_back(t2[i]);
 
  DivMaskImpl->myAssignFromExpv(dm, &(t.at(0)), NumIndets);
}

void PPMonoidModSquares::myOutputSelf(std::ostream& out) const
{
  out << "PPMonoidModSquares(" << NumIndets << ")";  
}

///////////////////////////////////////////////////////////////

CoCoA::PPMonoid NewPPMonoidModSquares(const std::vector<CoCoA::symbol>& IndetNames, const FastSquareFreeOrderings ord)
{
  for (size_t i=0; i < IndetNames.size(); ++i)
    for (size_t j=i+1; j < IndetNames.size(); ++j)
      if (IndetNames[i] == IndetNames[j])
        CoCoA_ERROR(CoCoA::ERR::BadIndetNames, "NewPPMonoidModSquares(IndetNames,ord)");
  
  return CoCoA::PPMonoid(new PPMonoidModSquares(IndetNames, ord));
}

// Introduced in CoCoALib-0.9940, CoCoALib-0.9941 or CoCoALib-0.9942
const CoCoA::symbol& PPMonoidModSquares::myIndetSymbol(long indet) const
{
  CoCoA_ERROR(CoCoA::ERR::NYI, "This function has not yet been implemented.");
  return myIndetNames[0]; // Should never be reached
}

// Introduced in CoCoALib-0.9940, CoCoALib-0.9941 or CoCoALib-0.9942
void PPMonoidModSquares::myZZExponents(std::vector<CoCoA::BigInt>& v, ConstRawPtr rawpp) const
{
  CoCoA_ERROR(CoCoA::ERR::NYI, "This function has not yet been implemented.");
}
} // end of namespace AlgebraicCore
} // end of namespace ApCoCoA
