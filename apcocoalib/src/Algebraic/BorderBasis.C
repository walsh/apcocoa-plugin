//
// This file is part of the source of ApCoCoALib, the ApCoCoA Library.
//
//   Copyright (c) ApCoCoA Project (Prof. Dr. Martin Kreuzer, Uni Passau)
//
//   Author: 2006, 2007, 2008, 2009 Stefan Kaspar
//
// Visit http://apcocoa.org/ for more information regarding ApCoCoA
// and ApCoCoALib.
// Visit http://www.apcocoa.org/wiki/ApCoCoA:KnownIssues for bugs, problems
// and known issues.
//
// ApCoCoALib is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License (version 3 or later)
// as published by the Free Software Foundation. A copy of the full
// licence may be found in the file COPYING in this directory.
//
// ApCoCoALib is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ApCoCoALib; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

#include "ApCoCoA/Algebraic/BorderBasis.H"
using ApCoCoA::AlgebraicCore::OrderIdeal;
#include "ApCoCoA/Algebraic/error.H"
#include "ApCoCoA/Logging/logging.H"
using namespace ApCoCoA::Logging;

#include "CoCoA/library.H"
using namespace CoCoA;

//#include <boost/bind.hpp>
//#include <boost/ref.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/thread/thread.hpp>

#include <algorithm>
using std::fill_n;
using std::find_if;
using std::greater;
using std::sort;
//#include <cstddef> // Included in SparsePolyRing.H
using std::size_t;
#include <functional>
using std::binary_function;
#include <iostream>
using std::endl;
using std::stringstream;
#include <iterator>
using std::distance;
#include <set>
using std::set;
//#include <vector>
using std::vector;

// Used for debugging function "TestComputedBBasis"
//#include <iostream>
using std::endl;


namespace ApCoCoA
{
  namespace AlgebraicAlgorithms
  {
    namespace // anonymous namespace for file local definitions
    {
      // The classes below are used in the call of std::find_if.
      // TODO: Remove duplicate code; see also PolyBorderBasisComputer.C.

      // Tests if LPP(f) = t for a polynomial f and a term t
      class LPPEqualsTerm
      {
      public:
        explicit LPPEqualsTerm(ConstRefPPMonoidElem t);
        bool operator()(ConstRefRingElem f);
      private:
        PPMonoidElem myTerm;
      };

      inline LPPEqualsTerm::LPPEqualsTerm(ConstRefPPMonoidElem t)
        : myTerm(t)
      {}

      inline bool LPPEqualsTerm::operator()(ConstRefRingElem f)
      {
        return (LPP(f) == myTerm);
      }

      // Tests if a term is a factor of another term
      class IsFactorOf
      {
      public:
        explicit IsFactorOf(ConstRefPPMonoidElem t);
        bool operator()(ConstRefPPMonoidElem t);
      private:
        PPMonoidElem myTerm;
      };

      inline IsFactorOf::IsFactorOf(ConstRefPPMonoidElem t)
        : myTerm(t)
      {}

      inline bool IsFactorOf::operator()(ConstRefPPMonoidElem t)
      {
        return IsDivisible(myTerm, t);
      }
    } // end of anonymous namespace

// DEBUG
//    // THIS FUNCTION IS USED FOR DEBUGGING PURPOSES ONLY.
//    // Test validity of a computed Border Basis.
//    // WARNING: This test only checks certain properties of the polynomials
//    //          of "BBasis". Even if no problem is reported "BBasis" still
//    //          might not be a true border basis!
//    void TestComputedBBasis(const vector<RingElem>& gens, const vector<RingElem>& BBasis)
//    {
//      SparsePolyRing Kx = SparsePolyRing(owner(gens[0]));
//      ideal I(Kx, gens), J(Kx, BBasis);
//      GlobalErrput() << "\n\n------------------\nIdeal equality check:\nI == J? " << (I == J) << endl;
//      const vector<RingElem> GB = TidyGens(J);
//      vector<PPMonoidElem> GBLT;
//      for (size_t i = 0; i < GB.size(); ++i)
//        GBLT.push_back(LPP(GB[i]));
//      const vector<PPMonoidElem>& x = indets(PPM(Kx));
//      PPMonoidElem b(PPM(Kx));
//      for (size_t j = 0; j < BBasis.size(); ++j)
//      {
//        // Check if leading term is a border term:
//        // I == J, therefore every leading term b of a polynomial in BBasis
//        // is divisible by a leading term of a polynomial in GB. We
//        // only have to check if there exists an index i such that b/x[i]
//        // is not divisible by any leading term of the polynomials in GB
//        b = LPP(BBasis[j]);
//        for (long i = 0; i < NumIndets(Kx); ++i)
//          if (IsDivisible(b, x[i]))
//            if (find_if(GBLT.begin(), GBLT.end(), IsFactorOf(b/x[i])) == GBLT.end())
//              goto LeadingTermOK;
//
//        GlobalErrput() << "Bad leading term in " << BBasis[j] << endl;
//
//      LeadingTermOK:
//        // Check if terms in support (without leading term) are elements
//        // of O_\sigma{J}
//        for (SparsePolyIter t = BeginIter(BBasis[j]); !IsEnded(++t); )
//          if (find_if(GBLT.begin(), GBLT.end(), IsFactorOf(PP(t))) != GBLT.end())
//            GlobalErrput() << "Bad support term " << PP(t) << " in " << j << "th polynomial" << endl;
//      }
//      GlobalErrput() << "\n------------------" << endl;
//    }
// DEBUG

    // ********************************************
    // * Border Basis Algorithm (generic version) *
    // ********************************************

    // Compute a O_\sigma{Id(gens)}-border basis
    void BorderBasis(vector<RingElem>& BBasis,
                     const vector<RingElem>& gens,
                     const size_t N)
    {
      MatBorderBasisComputer BBComputer(gens, N);
      BBComputer.myFinishComputation();
      BBComputer.myGetBorderBasis(BBasis);

// DEBUG
      // Uncomment the following for debugging purposes
      // TestComputedBBasis(gens, BBasis);
// DEBUG
    }

    /**********************************************************
     * Implementation of border basis computer skeleton class *
     **********************************************************/

    BorderBasisComputerSkeleton::BorderBasisComputerSkeleton(const vector<RingElem>& gens,
                                                             const size_t N)
      : myGens(gens), myN(N)
    {
      if (myGens.empty())
        CoCoA_ERROR(ERR::BadArg,
                    "BorderBasisComputerSkeleton: Argument \"gens\" must not be empty.");

      myEnlargementCounter = 0;
      myInterruptComputationFlag = false;
      myInterruptCompFlagMutexPtr.reset(new boost::mutex);
      const PPMonoid& PPMon = PPM(SparsePolyRing(owner(myGens[0])));
      myUPtr.reset(new OrderIdeal(PPMon));
      myOPtr.reset(new OrderIdeal(PPMon));
    }

    // Signal interruption request
    void BorderBasisComputerSkeleton::myInterruptComputation()
    {
      boost::lock_guard<boost::mutex> InterruptFlagLock(*myInterruptCompFlagMutexPtr);
      myInterruptComputationFlag = true;
    }

    // Start computation thread
    void BorderBasisComputerSkeleton::myDoSteps(const size_t NumOfSteps)
    {
      size_t StepCounter = 0;
      struct ComputationStatus TmpCompStatus;
      myComputationStatus.myThreadSafeClone(TmpCompStatus);

      while ((StepCounter < NumOfSteps || TmpCompStatus.myRunAllSteps)
             && !TmpCompStatus.myBBCompFinished)
      {
        TmpCompStatus.myCurStep = TmpCompStatus.myNextStep;

        // Check if interruption is requested
        {
          boost::lock_guard<boost::mutex> InterruptFlagLock(*myInterruptCompFlagMutexPtr);
          if (myInterruptComputationFlag)
          {
            myInterruptComputationFlag = false;
            break;
          }
        }

        switch (TmpCompStatus.myNextStep)
        {
          case 1:
            myStepI1();
            TmpCompStatus.myNextStep = 2;
            break;

          case 2:
            myStepI2();
            TmpCompStatus.myNextStep = 3;
            break;

          case 3:
            myStepI3();
            TmpCompStatus.myNextStep = 4;
            break;

          case 4:
            myStepI4();
            TmpCompStatus.myNextStep = 5;
            break;

          case 5:
            if (myStepI5())
              TmpCompStatus.myNextStep = 4;
            else
              TmpCompStatus.myNextStep = 6;
            break;

          case 6:
            if (myStepI6())
              TmpCompStatus.myNextStep = 3;
            else
              TmpCompStatus.myNextStep = 7;
            break;

          case 7:
            myStepI7();
            TmpCompStatus.myNextStep = 8;
            break;

          case 8:
            if (myStepI8())
              TmpCompStatus.myNextStep = 3;
            else
              TmpCompStatus.myNextStep = 9;
            break;

          case 9:
          default:
            myStepI9();
            TmpCompStatus.myNextStep = 1;
            TmpCompStatus.myBBCompFinished = true;
            break;
        }

        // Count total of computation steps executed
        TmpCompStatus.myCompStepCounter = TmpCompStatus.myCompStepCounter + 1;

        if (!TmpCompStatus.myRunAllSteps)
          ++StepCounter;

        // Write back changes of computation status
        myComputationStatus.myThreadSafeUpdate(TmpCompStatus);
      }
    }

    // Finish border basis computation
    void BorderBasisComputerSkeleton::myFinishComputation()
    {
      myComputationStatus.mySetRunAllSteps(true);
      myDoSteps(0);
    }

    // Reset object to state of initialization and pass on computed
    // border basis
    void BorderBasisComputerSkeleton::myReset()
    {
      myResetComputationVariables();

      myEnlargementCounter = 0;

      myComputationStatus.myCurStep = 1;
      myComputationStatus.myNextStep = 1;
      myComputationStatus.myBBCompFinished = false;
      myComputationStatus.myRunAllSteps = false;
      myComputationStatus.myCompStepCounter = 0;
    }

    /*******************************
     * Public status query methods *
     *******************************/

    // Retrieve number of steps executed
    BigInt BorderBasisComputerSkeleton::myGetNoStepsExecuted() const
    {
      return myComputationStatus.myGetCompStepCounter();
    }

    // Retrieve last computation step
    short BorderBasisComputerSkeleton::myGetPrevCompStep() const
    {
      return myComputationStatus.myGetCurStep();
    }

    // Retrieve next computation step
    short BorderBasisComputerSkeleton::myGetNextCompStep() const
    {
      return myComputationStatus.myGetNextStep();
    }

    // Check if border basis computation has finished
    bool BorderBasisComputerSkeleton::IamFinishedWithBBComp() const
    {
      return myComputationStatus.myGetBBCompFinished();
    }

    // Check if border basis is being computed interactively
    bool BorderBasisComputerSkeleton::IamComputingInteractively() const
    {
      return myComputationStatus.myGetRunAllSteps();
    }

    /************************
     * Public query methods *
     ************************/

    // Destructive border basis getter
    void BorderBasisComputerSkeleton::myGetBorderBasis(vector<RingElem>& BBasis)
    {
      if (!myComputationStatus.myBBCompFinished)
        CoCoA_ERROR(ERR::BBCompNotYetFinished,
                    "void myGetBorderBasis(std::vector<CoCoA::RingElem>&)");

      myReset();
      BBasis.swap(myBBasis);
    }

    // Border basis getter
    const vector<RingElem>& BorderBasisComputerSkeleton::myGetBorderBasis() const
    {
      if (!myComputationStatus.myBBCompFinished)
        CoCoA_ERROR(ERR::BBCompNotYetFinished,
                    "const std::vector<CoCoA::RingElem>& myGetBorderBasis()");

      return myBBasis;
    }

    // Get leading terms of border basis
    void BorderBasisComputerSkeleton::myGetBorderBasisLTs(vector<PPMonoidElem>& LTs,
                                                  long deg) const
    {
      if (!myComputationStatus.myBBCompFinished)
        CoCoA_ERROR(ERR::BBCompNotYetFinished, "myGetBorderBasisLTs");

      vector<PPMonoidElem> TmpLTs;
      if (deg == 0)
        for (size_t i = 0; i < myBBasis.size(); ++i)
          TmpLTs.push_back(LPP(myBBasis[i]));
      else
        for (size_t i = 0; i < myBBasis.size(); ++i)
          if (StdDeg(LPP(myBBasis[i])) == deg)
            TmpLTs.push_back(LPP(myBBasis[i]));

      LTs.swap(TmpLTs);
    }

    // Get border basis polynomial with matching leading term
    RingElem BorderBasisComputerSkeleton::myGetBorderBasisPolyByLT(ConstRefPPMonoidElem t) const
    {
      if (!myComputationStatus.myBBCompFinished)
        CoCoA_ERROR(ERR::BBCompNotYetFinished,
                    "myGetBorderBasisPolyWithMatchingLT");

      vector<RingElem>::const_iterator poly = find_if(myBBasis.begin(),
                                                      myBBasis.end(),
                                                      LPPEqualsTerm(t));
      if (poly == myBBasis.end())
        CoCoA_ERROR(ERR::BBNoSuchPoly, "myGetBorderBasisPolyWithMatchingLT");

      return *poly;
    }

    // Constructor of nested status structure
    BorderBasisComputerSkeleton::ComputationStatus::ComputationStatus()
    {
      myComputationStatusMutexPtr.reset(new boost::mutex);
      myCurStep = 1;
      myNextStep = 1;
      myBBCompFinished = false;
      myRunAllSteps = false;
      myCompStepCounter = 0;
    }

    // Thread safe clone method of nested status structure
    void BorderBasisComputerSkeleton::ComputationStatus::myThreadSafeClone(struct ComputationStatus& clone) const
    {
      boost::lock_guard<boost::mutex> ComputationStatusLock(*myComputationStatusMutexPtr);
      clone.myBBCompFinished = myBBCompFinished;
      clone.myCompStepCounter = myCompStepCounter;
      clone.myCurStep = myCurStep;
      clone.myNextStep = myNextStep;
      clone.myRunAllSteps = myRunAllSteps;
    }

    // Thread safe update method of nested status structure
    void BorderBasisComputerSkeleton::ComputationStatus::myThreadSafeUpdate(const struct ComputationStatus& NewStatus)
    {
      boost::lock_guard<boost::mutex> ComputationStatusLock(*myComputationStatusMutexPtr);
      myBBCompFinished = NewStatus.myBBCompFinished;
      myCompStepCounter = NewStatus.myCompStepCounter;
      myCurStep = NewStatus.myCurStep;
      myNextStep = NewStatus.myNextStep;
      myRunAllSteps = NewStatus.myRunAllSteps;
    }

    // Thread safe getter of nested status structure
    bool BorderBasisComputerSkeleton::ComputationStatus::myGetBBCompFinished() const
    {
      boost::lock_guard<boost::mutex> ComputationStatusLock(*myComputationStatusMutexPtr);
      return myBBCompFinished;
    }

    BigInt BorderBasisComputerSkeleton::ComputationStatus::myGetCompStepCounter() const
    {
      boost::lock_guard<boost::mutex> ComputationStatusLock(*myComputationStatusMutexPtr);
      return myCompStepCounter;
    }

    short BorderBasisComputerSkeleton::ComputationStatus::myGetCurStep() const
    {
      boost::lock_guard<boost::mutex> ComputationStatusLock(*myComputationStatusMutexPtr);
      return myCurStep;
    }

    short BorderBasisComputerSkeleton::ComputationStatus::myGetNextStep() const
    {
      boost::lock_guard<boost::mutex> ComputationStatusLock(*myComputationStatusMutexPtr);
      return myNextStep;
    }

    bool BorderBasisComputerSkeleton::ComputationStatus::myGetRunAllSteps() const
    {
      boost::lock_guard<boost::mutex> ComputationStatusLock(*myComputationStatusMutexPtr);
      return myRunAllSteps;
    }

    // Thread safe setter of nested status structure
    void BorderBasisComputerSkeleton::ComputationStatus::mySetRunAllSteps(bool RunAllSteps)
    {
      boost::lock_guard<boost::mutex> ComputationStatusLock(*myComputationStatusMutexPtr);
      myRunAllSteps = RunAllSteps;
    }
  } // end of namespace AlgebraicAlgorithms
} // end of namespace ApCoCoA
