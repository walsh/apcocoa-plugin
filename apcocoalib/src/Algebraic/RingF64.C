//
// This file is part of the source of ApCoCoALib, the ApCoCoA Library.
//
//   Copyright (c) ApCoCoA Project (Prof. Dr. Martin Kreuzer, Uni Passau)
//
//   Author: 2007 Daniel Heldt
//
// Visit http://apcocoa.org/ for more information regarding ApCoCoA
// and ApCoCoALib.
// Visit http://www.apcocoa.org/wiki/ApCoCoA:KnownIssues for bugs, problems
// and known issues.
//
// ApCoCoALib is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License (version 3 or later)
// as published by the Free Software Foundation. A copy of the full
// licence may be found in the file COPYING in this directory.
//
// ApCoCoALib is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ApCoCoALib; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 US

#include "ApCoCoA/Algebraic/RingF64.H"

#include "CoCoA/assert.H"
#include "CoCoA/ideal.H"
#include "CoCoA/FieldIdeal.H"
#include "CoCoA/MemPool.H"
#include "CoCoA/OpenMath.H"
#include "CoCoA/RingHom.H"
#include "CoCoA/convert.H"
#include "CoCoA/ZZ.H"


#include <iostream>
#include <memory>
#include <vector>
#include <algorithm>

namespace ApCoCoA
{
namespace AlgebraicCore
{
    class RingF64Impl: public RingF64Base
    {
    private: // data members
      mutable CoCoA::MemPool myMemMgr;           // MemPool must come before myZeroPtr, myOnePtr, and myMinusOnePtr
      std::auto_ptr<CoCoA::RingElem> myZeroPtr;
      std::auto_ptr<CoCoA::RingElem> myOnePtr;

    private:
      RingF64Impl();
      RingF64Impl(const RingF64Impl&);           ///< NEVER DEFINED -- disable copy ctor
      RingF64Impl operator=(const RingF64Impl&); ///< NEVER DEFINED -- disable assignment
      ~RingF64Impl();
      friend RingF64 NewRingF64(); // the only function allowed to call the constructor

    public:
      virtual void myCharacteristic(CoCoA::BigInt& p) const;
      virtual bool IamCommutative() const;
      virtual bool IamIntegralDomain() const;
      virtual bool IamOrderedDomain() const;
      virtual bool IamField() const;
      virtual CoCoA::ConstRefRingElem myZero() const;
      virtual CoCoA::ConstRefRingElem myOne() const;
      virtual CoCoA::RingElemRawPtr myNew() const;
      virtual CoCoA::RingElemRawPtr myNew(const CoCoA::MachineInt& n) const;
      virtual CoCoA::RingElemRawPtr myNew(const CoCoA::BigInt& n) const;
      virtual CoCoA::RingElemRawPtr myNew(ConstRawPtr rawt) const;
      virtual void myDelete(RawPtr rawx) const;
      virtual void mySwap(RawPtr rawx, RawPtr rawy) const;
      virtual void myAssign(RawPtr rawlhs, ConstRawPtr rawx) const;
      virtual void myAssign(RawPtr rawlhs, const CoCoA::MachineInt& n) const;
      virtual void myAssign(RawPtr rawlhs, const CoCoA::BigInt& N) const;
      virtual void myAssignZero(RawPtr rawlhs) const;
      virtual void myNegate(RawPtr rawlhs, ConstRawPtr rawx) const;
      virtual void myAdd(RawPtr rawlhs, ConstRawPtr rawx, ConstRawPtr rawy) const;
      virtual void mySub(RawPtr rawlhs, ConstRawPtr rawx, ConstRawPtr rawy) const;
      virtual void myMul(RawPtr rawlhs, ConstRawPtr rawx, ConstRawPtr rawy) const;
      virtual void myDiv(RawPtr rawlhs, ConstRawPtr rawx, ConstRawPtr rawy) const;
      virtual bool myIsDivisible(RawPtr rawlhs, ConstRawPtr rawx, ConstRawPtr rawy) const;
      virtual bool myIsUnit(ConstRawPtr rawx) const;
    // virtual void myGcd(...) const;
      virtual void myPowerSmallExp(RawPtr rawlhs, ConstRawPtr rawx, long n) const;
      virtual bool myIsPrintedWithMinus(ConstRawPtr rawx) const;
      virtual void myOutput(std::ostream& out, ConstRawPtr rawx) const;
      virtual void myOutputSelf(std::ostream& out) const;
      virtual void myOutputSelf(CoCoA::OpenMathOutput& OMOut) const;
      virtual void myOutput(CoCoA::OpenMathOutput& OMOut, ConstRawPtr rawx) const;
      virtual bool myIsZero(ConstRawPtr rawx) const;
      virtual bool myIsOne(ConstRawPtr rawx) const;
      virtual bool myIsMinusOne(ConstRawPtr rawx) const;
      virtual bool myIsInteger(CoCoA::BigInt& N, ConstRawPtr rawx) const;
      virtual bool myIsRational(CoCoA::BigInt& N, CoCoA::BigInt& D, ConstRawPtr rawx) const;
      virtual bool myIsRational(CoCoA::BigRat& Q, ConstRawPtr rawx) const;
      virtual bool myIsEqual(ConstRawPtr rawx, ConstRawPtr rawy) const;

      virtual CoCoA::RingElem mySymbolValue(const CoCoA::symbol& sym) const {CoCoA_ERROR("This ring has no symbols", "RingF64Impl::mySymbolValue"); return myZero();}
      virtual CoCoA::ideal myIdealCtor(const std::vector<CoCoA::RingElem>& gens) const;

      virtual CoCoA::RingHom myCompose(const CoCoA::RingHom& phi, const CoCoA::RingHom& theta) const; // phi(theta(...))

      // Added during update to CoCoALib-0.9945
      virtual bool IamExact() const;
      virtual void myRecvTwinFloat(RawPtr rawlhs, ConstRawPtr rawx) const;
    };

    RingF64Impl::RingF64Impl():
      myMemMgr(sizeof(unsigned char), "RingF64Impl.myMemMgr")
    {
      myZeroPtr.reset(new CoCoA::RingElem(CoCoA::ring(this), 0));
      myOnePtr.reset(new CoCoA::RingElem(CoCoA::ring(this), 1));
      myRefCountZero();
    }


    RingF64Impl::~RingF64Impl()
    {
      // nothing to do...
    }


    void RingF64Impl::myCharacteristic(CoCoA::BigInt& p) const
    {
      p = 2;
    }


    bool RingF64Impl::IamCommutative() const
    {
      return true;
    }


    bool RingF64Impl::IamIntegralDomain() const
    {
      return true;
    }


    bool RingF64Impl::IamOrderedDomain() const
    {
      return false;
    }


    bool RingF64Impl::IamField() const
    {
      return true;
    }


    CoCoA::ConstRefRingElem RingF64Impl::myZero() const
    {
      return *myZeroPtr;
    }


    CoCoA::ConstRefRingElem RingF64Impl::myOne() const
    {
      return *myOnePtr;
    }


    CoCoA::RingElemRawPtr RingF64Impl::myNew() const
    {
      unsigned char*  ans = (static_cast<unsigned char *> (myMemMgr.alloc()));

      *ans = 0;

      return CoCoA::RingElemRawPtr(ans);
    }


    CoCoA::RingElemRawPtr RingF64Impl::myNew(const CoCoA::MachineInt& n) const
    {
      unsigned char*  ans = (static_cast<unsigned char*> (myMemMgr.alloc()));

      if (!CoCoA::IsNegative(n))
        *ans = CoCoA::AsUnsignedLong(n) % 64;
      else
        *ans = 64 - (CoCoA::abs(n) % 64);

      if (*ans == 64)
        *ans = 0;

      return CoCoA::RingElemRawPtr(ans);
    }


    CoCoA::RingElemRawPtr RingF64Impl::myNew(const CoCoA::BigInt& N) const
    {
      unsigned char*  ans = (static_cast<unsigned char*> (myMemMgr.alloc()));
      unsigned int h;
      CoCoA::convert(h,N % CoCoA::BigInt(64));
      *ans = static_cast<unsigned char>(h);
      return CoCoA::RingElemRawPtr(ans);
    }


    CoCoA::RingElemRawPtr RingF64Impl::myNew(ConstRawPtr rawcopy) const
    {
      unsigned char*  ans = (static_cast<unsigned char*> (myMemMgr.alloc()));

      *ans = *static_cast<const unsigned char*>(rawcopy.myRawPtr());
      return CoCoA::RingElemRawPtr(ans);
    }


    void RingF64Impl::myDelete(RawPtr rawx) const
    {
      myMemMgr.free(rawx.myRawPtr());
    }


    void RingF64Impl::mySwap(RawPtr rawx, RawPtr rawy) const
    {
      unsigned char h;
      unsigned char * x =  static_cast<unsigned char*>(rawx.myRawPtr());
      unsigned char * y =  static_cast<unsigned char*>(rawy.myRawPtr());

      h = *x;
      *x = *y;
      *y = h;
    }


    void RingF64Impl::myAssign(RawPtr rawlhs, ConstRawPtr rawx) const
    {
      unsigned char *lhs =  static_cast<unsigned char*>(rawlhs.myRawPtr());
      unsigned char  x  =  *static_cast<const unsigned char*>(rawx.myRawPtr());

      *lhs = x;
    }

    void RingF64Impl::myAssign(RawPtr rawlhs, const CoCoA::MachineInt& n) const
    {
      unsigned char * lhs =  static_cast<unsigned char*>(rawlhs.myRawPtr());

      if (!CoCoA::IsNegative(n))
      {
        *lhs = CoCoA::AsUnsignedLong(n) % 64;
        return;
      }

      *lhs = 64 - (CoCoA::abs(n) % 64);

      if (*lhs == 64)
        *lhs = 0;
    }


    void RingF64Impl::myAssign(RawPtr rawlhs, const CoCoA::BigInt& N) const
    {
      unsigned char * lhs =  static_cast<unsigned char*>(rawlhs.myRawPtr());
      unsigned int h;
      CoCoA::convert(h,N % CoCoA::BigInt(64));
      *lhs = static_cast<unsigned char>(h);
     }


    void RingF64Impl::myAssignZero(RawPtr rawlhs) const
    {
      unsigned char * lhs =  static_cast<unsigned char*>(rawlhs.myRawPtr());
      *lhs = 0;
    }


    void RingF64Impl::myNegate(RawPtr rawlhs, ConstRawPtr rawx) const
    {
      unsigned char * lhs =   static_cast<unsigned char*>(rawlhs.myRawPtr()); // To Do: look at this
      unsigned char   x   =  *static_cast<const unsigned char*>(rawx.myRawPtr());

      *lhs = x;
    }


    void RingF64Impl::myAdd(RawPtr rawlhs, ConstRawPtr rawx, ConstRawPtr rawy) const
    {
      unsigned char * lhs =   static_cast<unsigned char*>(rawlhs.myRawPtr());
      unsigned char   x   =  *static_cast<const unsigned char*>(rawx.myRawPtr());
      unsigned char   y   =  *static_cast<const unsigned char*>(rawy.myRawPtr());

      *lhs = x ^ y;
    }


    void RingF64Impl::mySub(RawPtr rawlhs, ConstRawPtr rawx, ConstRawPtr rawy) const
    {
      unsigned char * lhs =   static_cast<unsigned char*>(rawlhs.myRawPtr());
      unsigned char   x   =  *static_cast<const unsigned char*>(rawx.myRawPtr());
      unsigned char   y   =  *static_cast<const unsigned char*>(rawy.myRawPtr());

      *lhs = x ^ y;
    }


    void RingF64Impl::myMul(RawPtr rawlhs, ConstRawPtr rawx, ConstRawPtr rawy) const
    {
      // this matrix was constructed as described in
      // http://apcocoa.org/wiki/HowTo:Construct_fields

      const unsigned char mult_middle[6][64]={
              {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63},
              {0, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30, 32, 34, 36, 38, 40, 42, 44, 46, 48, 50, 52, 54, 56, 58, 60, 62, 3, 1, 7, 5, 11, 9, 15, 13, 19, 17, 23, 21, 27, 25, 31, 29, 35, 33, 39, 37, 43, 41, 47, 45, 51, 49, 55, 53, 59, 57, 63, 61},
              {0, 4, 8, 12, 16, 20, 24, 28, 32, 36, 40, 44, 48, 52, 56, 60, 3, 7, 11, 15, 19, 23, 27, 31, 35, 39, 43, 47, 51, 55, 59, 63, 6, 2, 14, 10, 22, 18, 30, 26, 38, 34, 46, 42, 54, 50, 62, 58, 5, 1, 13, 9, 21, 17, 29, 25, 37, 33, 45, 41, 53, 49, 61, 57},
              {0, 8, 16, 24, 32, 40, 48, 56, 3, 11, 19, 27, 35, 43, 51, 59, 6, 14, 22, 30, 38, 46, 54, 62, 5, 13, 21, 29, 37, 45, 53, 61, 12, 4, 28, 20, 44, 36, 60, 52, 15, 7, 31, 23, 47, 39, 63, 55, 10, 2, 26, 18, 42, 34, 58, 50, 9, 1, 25, 17, 41, 33, 57, 49},
              {0, 16, 32, 48, 3, 19, 35, 51, 6, 22, 38, 54, 5, 21, 37, 53, 12, 28, 44, 60, 15, 31, 47, 63, 10, 26, 42, 58, 9, 25, 41, 57, 24, 8, 56, 40, 27, 11, 59, 43, 30, 14, 62, 46, 29, 13, 61, 45, 20, 4, 52, 36, 23, 7, 55, 39, 18, 2, 50, 34, 17, 1, 49, 33},
              {0, 32, 3, 35, 6, 38, 5, 37, 12, 44, 15, 47, 10, 42, 9, 41, 24, 56, 27, 59, 30, 62, 29, 61, 20, 52, 23, 55, 18, 50, 17, 49, 48, 16, 51, 19, 54, 22, 53, 21, 60, 28, 63, 31, 58, 26, 57, 25, 40, 8, 43, 11, 46, 14, 45, 13, 36, 4, 39, 7, 34, 2, 33, 1}
        };


      unsigned char * lhs   =   static_cast<      unsigned char*>(rawlhs.myRawPtr());
      const unsigned char x =  *static_cast<const unsigned char*>(rawx.myRawPtr());
      const unsigned char y =  *static_cast<const unsigned char*>(rawy.myRawPtr());

      *lhs = 0;
      unsigned char h=1;
      for(std::size_t i=0; i< 6; ++i){
        if ( static_cast<unsigned char>(x & h) != 0){
          *lhs = (*lhs) ^ mult_middle[i][y];
        }
        h <<=1;
      }
    }


    void RingF64Impl::myDiv(RawPtr rawlhs, ConstRawPtr rawx, ConstRawPtr rawy) const
    {
      // this matrix was constructed as described in
      // http://apcocoa.org/wiki/HowTo:Construct_fields

      const unsigned char div_middle[6][64]={
              {0, 1, 33, 62, 49, 43, 31, 44, 57, 37, 52, 28, 46, 40, 22, 25, 61, 54, 51, 39, 26, 35, 14, 24, 23, 15, 20, 34, 11, 53, 45, 6, 63, 2, 27, 21, 56, 9, 50, 19, 13, 47, 48, 5, 7, 30, 12, 41, 42, 4, 38, 18, 10, 29, 17, 60, 36, 8, 59, 58, 55, 16, 3, 32},
              {0, 2, 1, 63, 33, 21, 62, 27, 49, 9, 43, 56, 31, 19, 44, 50, 57, 47, 37, 13, 52, 5, 28, 48, 46, 30, 40, 7, 22, 41, 25, 12, 61, 4, 54, 42, 51, 18, 39, 38, 26, 29, 35, 10, 14, 60, 24, 17, 23, 8, 15, 36, 20, 58, 34, 59, 11, 16, 53, 55, 45, 32, 6, 3},
              {0, 4, 2, 61, 1, 42, 63, 54, 33, 18, 21, 51, 62, 38, 27, 39, 49, 29, 9, 26, 43, 10, 56, 35, 31, 60, 19, 14, 44, 17, 50, 24, 57, 8, 47, 23, 37, 36, 13, 15, 52, 58, 5, 20, 28, 59, 48, 34, 46, 16, 30, 11, 40, 55, 7, 53, 22, 32, 41, 45, 25, 3, 12, 6},
              {0, 8, 4, 57, 2, 23, 61, 47, 1, 36, 42, 37, 63, 15, 54, 13, 33, 58, 18, 52, 21, 20, 51, 5, 62, 59, 38, 28, 27, 34, 39, 48, 49, 16, 29, 46, 9, 11, 26, 30, 43, 55, 10, 40, 56, 53, 35, 7, 31, 32, 60, 22, 19, 45, 14, 41, 44, 3, 17, 25, 50, 6, 24, 12},
              {0, 16, 8, 49, 4, 46, 57, 29, 2, 11, 23, 9, 61, 30, 47, 26, 1, 55, 36, 43, 42, 40, 37, 10, 63, 53, 15, 56, 54, 7, 13, 35, 33, 32, 58, 31, 18, 22, 52, 60, 21, 45, 20, 19, 51, 41, 5, 14, 62, 3, 59, 44, 38, 25, 28, 17, 27, 6, 34, 50, 39, 12, 48, 24},
              {0, 32, 16, 33, 8, 31, 49, 58, 4, 22, 46, 18, 57, 60, 29, 52, 2, 45, 11, 21, 23, 19, 9, 20, 61, 41, 30, 51, 47, 14, 26, 5, 1, 3, 55, 62, 36, 44, 43, 59, 42, 25, 40, 38, 37, 17, 10, 28, 63, 6, 53, 27, 15, 50, 56, 34, 54, 12, 7, 39, 13, 24, 35, 48}
      };

      CoCoA_ASSERT(!myIsZero(rawy));
      unsigned char * lhs   =   static_cast<      unsigned char*>(rawlhs.myRawPtr());
      const unsigned char x =  *static_cast<const unsigned char*>(rawx.myRawPtr());
      const unsigned char y =  *static_cast<const unsigned char*>(rawy.myRawPtr());
      *lhs = 0;
      unsigned char h=1;
      for(std::size_t i=0; i< 6; ++i){
        if ( static_cast<unsigned char>(x & h) != 0){
          *lhs = (*lhs) ^ div_middle[i][y];
         }
        h <<= 1;
      }

    }


    bool RingF64Impl::myIsDivisible(RawPtr rawlhs, ConstRawPtr rawx, ConstRawPtr rawy) const
    {
      if (myIsZero(rawy)) return false;
      myDiv(rawlhs, rawx, rawy);
      return true;
    }


    bool RingF64Impl::myIsUnit(ConstRawPtr rawx) const
    {
      return !myIsZero(rawx);
    }


    void RingF64Impl::myPowerSmallExp(RawPtr rawlhs, ConstRawPtr rawx, long n) const
    {
      this->myAssign(rawlhs,1);
      if (n == 0)
        return;

      unsigned long h= static_cast<unsigned long>(1);
      CoCoA::RingElemRawPtr powers =  this->myNew(rawx);

      for (std::size_t i =1; i <8*sizeof(unsigned long); ++i)
      {
        if ( (h & n) != 0)
        {
          this->myMul(rawlhs, rawlhs, powers);

          n ^= h;
          if ( n == 0)
            break;
        }

        this->myMul(powers, powers,powers);
        h <<= 1;
      }

    }

    bool RingF64Impl::myIsPrintedWithMinus(ConstRawPtr rawx) const
    {
      return false;
    }


    void RingF64Impl::myOutput(std::ostream& out, ConstRawPtr rawx) const
    {
      const unsigned char   x   =  *static_cast<const unsigned char*>(rawx.myRawPtr());
      out << static_cast<unsigned int>( x );
    }


    void RingF64Impl::myOutputSelf(std::ostream& out) const
    {
      out << "RingF64(" << " " << ")";
    }


    void RingF64Impl::myOutputSelf(CoCoA::OpenMathOutput& OMOut) const
    {
      // ToDo.
    }


    void RingF64Impl::myOutput(CoCoA::OpenMathOutput& OMOut, ConstRawPtr rawx) const
    {
      // ToDo.
    }


    bool RingF64Impl::myIsZero(ConstRawPtr rawx) const
    {
      const unsigned char   x   =  *static_cast<const unsigned char*>(rawx.myRawPtr());
      return (x == 0);
    }


    bool RingF64Impl::myIsOne(ConstRawPtr rawx) const
    {
      const unsigned char   x   =  *static_cast<const unsigned char*>(rawx.myRawPtr());
      return (x==1);
    }


    bool RingF64Impl::myIsMinusOne(ConstRawPtr rawx) const
    {
      const unsigned char   x   =  *static_cast<const unsigned char*>(rawx.myRawPtr());
      return (x==1);
    }

    bool RingF64Impl::myIsInteger(CoCoA::BigInt& N, ConstRawPtr rawX) const
    {
      return false;
    }


    bool RingF64Impl::myIsRational(CoCoA::BigInt& N, CoCoA::BigInt& D, ConstRawPtr rawx) const
    {
      return false;
    }
	
	bool RingF64Impl::myIsRational(CoCoA::BigRat& Q, ConstRawPtr rawx) const
    {
      return false;
    }


    bool RingF64Impl::myIsEqual(ConstRawPtr rawx, ConstRawPtr rawy) const
    {
      const unsigned char   x   =  *static_cast<const unsigned char*>(rawx.myRawPtr());
      const unsigned char   y   =  *static_cast<const unsigned char*>(rawy.myRawPtr());

      return x == y;
    }

    //---------------------------------------------------------------------------

    // This function has to exist, but it can never be called,
    // why not?
    CoCoA::RingHom RingF64Impl::myCompose(const CoCoA::RingHom& phi, const CoCoA::RingHom& /*theta*/) const
    {
      CoCoA_ERROR(CoCoA::ERR::SERIOUS, "RingF64Impl::myCompose -- how did you get here?");
      return phi; // just to keep compiler quiet
    }


    CoCoA::ideal RingF64Impl::myIdealCtor(const std::vector<CoCoA::RingElem>& gens) const
    {
      return CoCoA::NewFieldIdeal(CoCoA::ring(this), gens);
    }

    //---------------------------------------------------------------------------

    RingF64::RingF64(const RingF64Base* RingPtr):
      CoCoA::ring(RingPtr)
    {


    }


    RingF64 NewRingF64()
    {
      return RingF64(new RingF64Impl());
    }

    bool IsRingF64(const CoCoA::ring& RR)
    {
      return dynamic_cast<const RingF64Impl*>(RR.myRawPtr()) != 0; // wtf? how to fix this???
    }

    // Added during update to CoCoALib-0.9945
    bool RingF64Impl::IamExact() const
    {
      return true;
    }

    void RingF64Impl::myRecvTwinFloat(RawPtr /*rawlhs*/,
                                      ConstRawPtr /*rawx*/) const
    {
      CoCoA_ERROR(CoCoA::ERR::SERIOUS, "RingF64Impl::myRecvTwinFloat");
    }
} // end of namespace AlgebraicCore
} // end of namespace ApCoCoA

