//
// This file is part of the source of ApCoCoALib, the ApCoCoA Library.
//
//   Copyright (c) ApCoCoA Project (Prof. Dr. Martin Kreuzer, Uni Passau)
//
//   Author: 2007 Daniel Heldt
//
// Visit http://apcocoa.org/ for more information regarding ApCoCoA
// and ApCoCoALib.
// Visit http://www.apcocoa.org/wiki/ApCoCoA:KnownIssues for bugs, problems 
// and known issues.
//
// ApCoCoALib is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License (version 3 or later)
// as published by the Free Software Foundation. A copy of the full
// licence may be found in the file COPYING in this directory.
//
// ApCoCoALib is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ApCoCoALib; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 US

#include "CoCoA/library.H"
#include "./ApCoCoA/Algebraic/RingF1024.H"

void program()
{
//  const unsigned long len = 1024;
//  const unsigned long log = 12;
  const unsigned long NumElementsToTest = 50;

  CoCoA::ring myRing = ApCoCoA::AlgebraicCore::NewRingF1024();
  
  CoCoA::RingElem one = CoCoA::one(myRing);
  CoCoA::RingElem zero = CoCoA::zero(myRing);
  
  std::cout << myRing << std::endl;
  
  std::vector<CoCoA::RingElem> elements;
  elements.reserve(NumElementsToTest);
   
     
  for (std::size_t i=0; i < NumElementsToTest; ++i)
  {
    elements.push_back(CoCoA::RingElem(myRing, rand()));
  } 
     
  for (std::size_t i=0; i < NumElementsToTest; ++i)
  {
    if ( elements[i] != 0 )
      if (elements[i]/elements[i] != one)
        std::cout << "division error: " << i << " -=- " << elements[i] << ", " << elements[i]/elements[i] << std::endl;    

    CoCoA::RingElem J(elements[i]); 
    for ( unsigned long j=1; j<16; j++)
    {
      if ( J != CoCoA::power(elements[i],j))
      {
        std::cout << "exponential error: "<< elements[i] << "^" << j << "(=" << CoCoA::power(elements[i],j)<< ") !=" << J << std::endl;
        return;
        }
      J =J* elements[i];
    }

    for (std::size_t j=1; j <  NumElementsToTest; ++j)
    {
      
      if ( elements[i] *elements[j] != elements[j] * elements[i])
        std::cout << "multiplication error: " << i <<"," << j << " -=- " << elements[i]*elements[j] << ", " << elements[j]*elements[i] << " -> " << elements[j]*elements[i] - elements[i]*elements[j] <<std::endl;
  
      if ( elements[i] + elements[j] != elements[j] + elements[i])
        std::cout << "addition error: " << i <<"," << j << " -=- " << elements[i]+elements[j] << ", " << elements[j]+elements[i] << " -> " << elements[j]+elements[i] - (elements[i]+elements[j]) <<std::endl;

      for (std::size_t k=1; k <  NumElementsToTest; ++k)
      {
        if ( elements[k]*(elements[i] + elements[j] ) != elements[k]*elements[i] + elements[k]*elements[j])
          std::cout << "distributive error: " << i <<", "<< j <<", " << k << "-> " << elements[k]*(elements[i]+elements[j]) << ", " <<elements[k]*elements[i] + elements[k]*elements[j] << std::endl;
          
        if ( elements[k]*(elements[i] * elements[j] ) != (elements[k]*elements[i])*elements[j])
          std::cout << "associative error: " << i <<", "<< j <<", " << k << "-> " << elements[k]*(elements[i]*elements[j]) << ", " <<(elements[k]*elements[i])*elements[j] << std::endl;          
      }

      if ( (elements[i] / elements[j])*elements[j] != elements[i])
        std::cout << "division error: " << i <<"," << j << " -=- " << elements[i] << ", " << elements[j] <<", " << elements[i]/elements[j] << ", "<< (elements[i]/elements[j])*elements[j] <<std::endl;

    }    
  }
}// end of programm


//----------------------------------------------------------------------
// Use main() to handle any uncaught exceptions and warn the user about them.
int main()
{
  try
  {
    program();
    return 0;
  }
  catch (const CoCoA::ErrorInfo& err)
  {
    CoCoA::GlobalErrput() << "***ERROR***  UNCAUGHT CoCoA error";
    ANNOUNCE(CoCoA::GlobalErrput(), err);
  }
  catch (const std::exception& exc)
  {
    CoCoA::GlobalErrput() << "***ERROR***  UNCAUGHT std::exception: " << exc.what() << std::endl;
  }
  catch(...)
  {
    CoCoA::GlobalErrput() << "***ERROR***  UNCAUGHT UNKNOWN EXCEPTION" << std::endl;
  }
  return 1;
}
