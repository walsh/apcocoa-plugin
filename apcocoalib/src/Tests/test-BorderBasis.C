//
// This file is part of the source of ApCoCoALib, the ApCoCoA Library.
//
//   Copyright (c) ApCoCoA Project (Prof. Dr. Martin Kreuzer, Uni Passau)
//
//   Author: 2006, 2007, 2009 Stefan Kaspar
//
// Visit http://apcocoa.org/ for more information regarding ApCoCoA
// and ApCoCoALib.
// Visit http://www.apcocoa.org/wiki/ApCoCoA:KnownIssues for bugs, problems
// and known issues.
//
// ApCoCoALib is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License (version 3 or later)
// as published by the Free Software Foundation. A copy of the full
// licence may be found in the file COPYING in this directory.
//
// ApCoCoALib is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ApCoCoALib; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

#include "ApCoCoA/Algebraic/BorderBasis.H"
#include "ApCoCoA/Algebraic/error.H"
using ApCoCoA::AlgebraicAlgorithms::BorderBasis;

#include "CoCoA/FractionField.H"
#include "CoCoA/GlobalManager.H"
#include "CoCoA/PPMonoidEv.H"
#include "CoCoA/RingDistrMPolyClean.H"
#include "CoCoA/RingHom.H"
#include "CoCoA/RingQ.H"
#include "CoCoA/SparsePolyRing.H"
#include "CoCoA/io.H"
#include "CoCoA/symbol.H"
using namespace CoCoA;

// #include <iostream> // Included by io.H
using std::endl;
// #include <vector> // Included by TmpBorderBasis.H
using std::vector;
using std::exception;
using std::size_t;
using std::string;

void program()
{
  GlobalManager CoCoAFoundations;
  const ring& Q = RingQ();

  /**********************
   * Computation test 1 *
   **********************/

  const PPOrdering DegLex2 = NewStdDegLexOrdering(2);
  const SparsePolyRing Qxy = NewPolyRing(Q, SymbolRange("x", 0, 1), DegLex2);
  const vector<RingElem>& xy = indets(Qxy);
  const RingHom embed1 = CoeffEmbeddingHom(Qxy);

  RingElem f1(Qxy);
  RingElem f2(Qxy);
  RingElem f3(Qxy);
  RingElem f4(Qxy);
  RingElem f5(Qxy);
  RingElem num(Q);
  RingElem den(Q);

  num = 1; den = 2;
  f1 = power(xy[0], 3) - xy[0];
  f2 = power(xy[1], 3) - xy[1];
  f3 = power(xy[0], 2)*xy[1] - embed1(num/den)*xy[1] - embed1(num/den)*power(xy[1], 2);
  f4 = xy[0]*xy[1] - xy[0] - embed1(num/den)*xy[1] + power(xy[0], 2) - embed1(num/den)*power(xy[1], 2);
  f5 = xy[0]*power(xy[1], 2) - xy[0] - embed1(num/den)*xy[1] + power(xy[0], 2) - embed1(num/den)*power(xy[1], 2);

  vector<RingElem> gens1;
  gens1.push_back(f1);
  gens1.push_back(f2);
  gens1.push_back(f3);
  gens1.push_back(f4);
  gens1.push_back(f5);

  GlobalOutput() << "Computation test 1\n------------------\nGenerators:\n";
  for (size_t i = 0; i < gens1.size(); ++i)
    GlobalOutput() << "f_" << i << " = " << gens1[i] << "\n";

  vector<RingElem> BBasis1;
  BorderBasis(BBasis1, gens1); // Convenience function, uses PolyBorderBasisComputer

  GlobalOutput() << "\nComputed O_\\sigma{Id(gens)}-border basis with sigma = DegLex:\n";
  for (size_t i = 0; i < BBasis1.size(); ++i)
    GlobalOutput() << "g_" << i << " = " << BBasis1[i] << "\n";
  GlobalOutput() << "\n----------------------------------------------------------------------\n\n";

  /**********************
   * Computation test 2 *
   **********************/

  const SparsePolyRing Qxyz = NewPolyRing(Q, 3);
  const vector<RingElem>& xyz = indets(Qxyz);
  const RingHom embed2 = CoeffEmbeddingHom(Qxyz);

  RingElem g1(Qxyz);
  RingElem g2(Qxyz);
  RingElem g3(Qxyz);
  RingElem g4(Qxyz);
  RingElem g5(Qxyz);
  RingElem g6(Qxyz);

  num = 1; den = 2;

  g1 = power(xyz[2], 2) + 3*xyz[1] - 7*xyz[2];
  g2 = xyz[1]*xyz[2] - 4*xyz[1];
  g3 = xyz[0]*xyz[2] - 4*xyz[1];
  g4 = power(xyz[1], 2) - 4*xyz[1];
  g5 = xyz[0]*xyz[1] - 4*xyz[1];
  g6 = power(xyz[0], 5) - 8*power(xyz[0], 4) + 14*power(xyz[0], 3) + 8*power(xyz[0], 2) - 15*xyz[0] + 15*xyz[1];

  vector<RingElem> gens2;

  gens2.push_back(g1);
  gens2.push_back(g2);
  gens2.push_back(g3);
  gens2.push_back(g4);
  gens2.push_back(g5);
  gens2.push_back(g6);

  GlobalOutput() << "Computation test 2\n------------------\nGenerators:\n";
  for (size_t i = 0; i < gens2.size(); ++i)
    GlobalOutput() << "f_" << i << " = " << gens2[i] << "\n";

  vector<RingElem> BBasis2;
  BorderBasis(BBasis2, gens2); // Convenience function, uses PolyBorderBasisComputer

  GlobalOutput() << "\nComputed O_\\sigma{Id(gens)}-border basis with sigma = DegRevLex:\n";
  for (size_t i = 0; i < BBasis2.size(); ++i)
    GlobalOutput() << "g_" << i << " = " << BBasis2[i] << "\n";
}

//----------------------------------------------------------------------
// Use main() to handle any uncaught exceptions and warn the user about them.
int main()
{
  try
  {
    program();
    return 0;
  }
  catch (const ErrorInfo& err)
  {
    GlobalErrput() << "***ERROR***  UNCAUGHT CoCoA error" << endl;
    ANNOUNCE(CoCoA::GlobalErrput(), err);
  }
  catch (const exception& exc)
  {
    GlobalErrput() << "***ERROR***  UNCAUGHT std::exception: " << exc.what() << endl;
  }
  catch(...)
  {
    GlobalErrput() << "***ERROR***  UNCAUGHT UNKNOWN EXCEPTION" << endl;
  }
  return 1;
}
