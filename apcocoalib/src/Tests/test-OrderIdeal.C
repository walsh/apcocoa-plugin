//
// This file is part of the source of ApCoCoALib, the ApCoCoA Library.
//
//   Copyright (c) ApCoCoA Project (Prof. Dr. Martin Kreuzer, Uni Passau)
//
//   Author: 2008 Stefan Kaspar
//
// Visit http://apcocoa.org/ for more information regarding ApCoCoA
// and ApCoCoALib.
// Visit http://www.apcocoa.org/wiki/ApCoCoA:KnownIssues for bugs, problems 
// and known issues.
//
// ApCoCoALib is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License (version 3 or later)
// as published by the Free Software Foundation. A copy of the full
// licence may be found in the file COPYING in this directory.
//
// ApCoCoALib is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ApCoCoALib; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

#include "ApCoCoA/Algebraic/OrderIdeal.H"
#include "CoCoA/FractionField.H"
#include "CoCoA/GlobalManager.H"
#include "CoCoA/PPMonoidEv.H"
#include "CoCoA/RingQ.H"
#include "CoCoA/SparsePolyRing.H"
#include "CoCoA/io.H"
#include "CoCoA/symbol.H"

// #include <iostream> // Included by io.H
// using std::endl;
// #include <vector>
// using std::vector;


void program()
{
  CoCoA::GlobalManager CoCoAFoundations;

  // Create some terms
  CoCoA::SparsePolyRing Qx = CoCoA::NewPolyRing(CoCoA::RingQ(), 3);
  CoCoA::PPMonoid PPMon = CoCoA::PPM(Qx);
  std::set<CoCoA::PPMonoidElem> terms;
  terms.insert(CoCoA::IndetPower(PPMon, 0, 4) * CoCoA::IndetPower(PPMon, 1, 1) * CoCoA::IndetPower(PPMon, 2, 1));
  terms.insert(CoCoA::IndetPower(PPMon, 0, 2) * CoCoA::IndetPower(PPMon, 1, 2) * CoCoA::IndetPower(PPMon, 2, 2));

  // Create an order ideal spanned by those terms
  ApCoCoA::AlgebraicCore::OrderIdeal O(PPMon, terms);
  CoCoA::GlobalOutput() << "Order ideal to start with\n-------------------------\n" << O << std::endl;

  // Degree test
  if (O.myGetMaxDeg() != 6)
    CoCoA::GlobalOutput() << "ERROR: OrderIdeal::myGetMaxDeg returns "
                          << O.myGetMaxDeg()
                          << " instead of 6."
                          << std::endl;

  // Compute border of order ideal
  CoCoA::GlobalOutput() << "\nBorder of order ideal\n---------------------" << std::endl;
  std::set<CoCoA::PPMonoidElem> border;
  O.myBorder(border);  
  for (std::set<CoCoA::PPMonoidElem>::iterator b = border.begin(); b != border.end(); ++b)
    CoCoA::GlobalOutput() << *b << std::endl;

  // Enlarge order ideal
  terms.clear();
  terms.insert(CoCoA::IndetPower(PPMon, 0, 5));
  terms.insert(CoCoA::IndetPower(PPMon, 0, 2) * CoCoA::IndetPower(PPMon, 1, 2));
  terms.insert(CoCoA::IndetPower(PPMon, 1, 1));
  terms.insert(CoCoA::IndetPower(PPMon, 0, 3) * CoCoA::IndetPower(PPMon, 1, 1));
  terms.insert(CoCoA::IndetPower(PPMon, 1, 3));
  O.myEnlarge(terms);
  CoCoA::GlobalOutput() << "\nEnlarged order ideal\n--------------------\n" << O << std::endl;

  // Create some terms and check if they are contained in order ideal
  CoCoA::GlobalOutput() << "\nElement test\n------------" << std::endl;
  terms.clear();
  terms.insert(CoCoA::IndetPower(PPMon, 0, 1) * CoCoA::IndetPower(PPMon, 1, 3));
  terms.insert(CoCoA::one(PPMon));
  terms.insert(CoCoA::IndetPower(PPMon, 0, 50));
  terms.insert(CoCoA::IndetPower(PPMon, 0, 3) * CoCoA::IndetPower(PPMon, 1, 3));
  for (std::set<CoCoA::PPMonoidElem>::iterator t = terms.begin(); t != terms.end(); ++t)
    if (O.myContains(*t))
      CoCoA::GlobalOutput() << *t << " is an element of order ideal." << std::endl;
    else
      CoCoA::GlobalOutput() << *t << " is not an element of order ideal." << std::endl;

  // Compute some indices
  CoCoA::GlobalOutput() << "\nIndex test\n----------" << std::endl;
  for (std::set<CoCoA::PPMonoidElem>::iterator t = terms.begin(); t != terms.end(); ++t)
    CoCoA::GlobalOutput() << "Index of term " << *t << " is " << O.myIndex(*t) << "." << std::endl;

  CoCoA::RingElem f(Qx);
  const std::vector<CoCoA::RingElem>& x = CoCoA::indets(Qx);
  f = CoCoA::power(x[0], 3) - x[0];
  CoCoA::GlobalOutput() << "Index of polynomial " << f << " is " << O.myIndex(f) << "." << std::endl;
  f = CoCoA::power(x[0], 1) * CoCoA::power(x[1], 4) * CoCoA::power(x[2], 1)
      + CoCoA::power(x[0], 6) + x[1] - 1;
  CoCoA::GlobalOutput() << "Index of polynomial " << f << " is " << O.myIndex(f) << "." << std::endl;

  CoCoA::GlobalOutput() << "\nError detection test\n--------------------" << std::endl;
  // Zero polynomial input
  try
  {
    O.myIndex(CoCoA::zero(Qx));
    CoCoA::GlobalOutput() << "ERROR: OrderIdeal::myIndex failed in detecting zero polynomial input." << std::endl;
  }
  catch (const CoCoA::ErrorInfo& err)
  {
    // Expected exception
    CoCoA::GlobalOutput() << "OrderIdeal::myIndex successfully detected zero polynomial input." << std::endl;
  }

  // "myIndex" cannot be called by empty order ideal
  try
  {
    O.myClear();
    O.myIndex(f);
    CoCoA::GlobalOutput() << "ERROR: OrderIdeal::myIndex failed in detecting empty order ideal." << std::endl;
  }
  catch (const CoCoA::ErrorInfo& err)
  {
    // Expected exception
    CoCoA::GlobalOutput() << "OrderIdeal::myIndex successfully detected empty order ideal." << std::endl;
  }

  // Degree test
  if (O.myGetMaxDeg() != -1)
    CoCoA::GlobalOutput() << "ERROR: OrderIdeal::myGetMaxDeg returns "
                          << O.myGetMaxDeg()
                          << " instead of -1."
                          << std::endl;
}

//----------------------------------------------------------------------
// Use main() to handle any uncaught exceptions and warn the user about them.
int main()
{
  try
  {
    program();
    return 0;
  }
  catch (const CoCoA::ErrorInfo& err)
  {
    CoCoA::GlobalErrput() << "***ERROR***  UNCAUGHT CoCoA error" << std::endl;
    ANNOUNCE(CoCoA::GlobalErrput(), err);
  }
  catch (const std::exception& exc)
  {
    CoCoA::GlobalErrput() << "***ERROR***  UNCAUGHT std::exception: " << exc.what() << std::endl;
  }
  catch(...)
  {
    CoCoA::GlobalErrput() << "***ERROR***  UNCAUGHT UNKNOWN EXCEPTION" << std::endl;
  }
  return 1;
}
