//
// This file is part of the source of ApCoCoALib, the ApCoCoA Library.
//
//   Copyright (c) ApCoCoA Project (Prof. Dr. Martin Kreuzer, Uni Passau)
//
//   Author: 2009 Jan Limbeck
//
// Visit http://apcocoa.org/ for more information regarding ApCoCoA
// and ApCoCoALib.
// Visit http://www.apcocoa.org/wiki/ApCoCoA:KnownIssues for bugs, problems
// and known issues.
//
// ApCoCoALib is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License (version 3 or later)
// as published by the Free Software Foundation. A copy of the full
// licence may be found in the file COPYING in this directory.
//
// ApCoCoALib is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ApCoCoALib; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

#include "ApCoCoA/Numerical/NumericalFoundation.H"
#include "CoCoA/GlobalManager.H"
#include "CoCoA/convert.H"

#include "CoCoA/matrix.H"
#include "CoCoA/MatrixArith.H"
#include "CoCoA/ring.H"
#include "CoCoA/io.H"
#include <math.h>

void program()
{
#ifndef USENoLapack

	// This test looks trivial...
	// Nevertheless there were problems with certain combinations of ATLAS
	// and Lapack, in which the following test failed and returned NaN values
	// in the
	CoCoA::GlobalOutput() << "EVV test" << std::endl;
	CoCoA::GlobalOutput() << "--------------" << std::endl;
	CoCoA::GlobalManager CoCoAFoundations;

	CoCoA::ring Ring = CoCoA::RingQ();

	double inp[] = {0.0, 0.0, 0.0,
					0.0, 0.0, 0.0,
					0.0, 0.0, 0.0};

	ApCoCoA::NumericalAlgorithms::ABM::DoubleDenseMatrix input(3, 3, inp);

	CoCoA::matrix Q(CoCoA::NewDenseMat(Ring, 3, 3));
	CoCoA::matrix eigenValues(CoCoA::NewDenseMat(Ring, 2, 3));
	CoCoA::matrix rightEigenVectors(CoCoA::NewDenseMat(Ring, 3, 3));

	ApCoCoA::NumericalAlgorithms::ComputeEigenValuesAndVectors(input, eigenValues, rightEigenVectors);

	// es we got no real and imaginary components here it's not necessary to split the data
	//ApCoCoA::NumericalAlgorithms::splitRealImaginary(*myEigenValues, *myRightEigenVectors, *myRealEigenVectors, *myImaEigenVectors);

	CoCoA::GlobalOutput() << "The eigenvalues: "
                       	  << eigenValues;

	CoCoA::GlobalOutput() << "The eigenvectors: "
                       	  << rightEigenVectors;


#else
  CoCoA::GlobalOutput() << "EVV test\n"
						<< "--------------\n"
						<< "The eigenvalues: matrix(QQ)(2, 3)\n"
						<< "[\n"
						<< " [0, 0, 0],\n"
						<< " [0, 0, 0]\n"
						<< "]\n"
						<< "The eigenvectors: matrix(QQ)(3, 3)\n"
						<<	"[\n"
						<< " [1, 0, 0],\n"
						<<	" [0, 1, 0],\n"
						<< " [0, 0, 1]\n"
						<< "]\n";
#endif //USENoLapack
}


//----------------------------------------------------------------------
// Use main() to handle any uncaught exceptions and warn the user about them.
int main()
{
  try
  {
    program();
    return 0;
  }
  catch (const CoCoA::ErrorInfo& err)
  {
    CoCoA::GlobalErrput() << "***ERROR***  UNCAUGHT CoCoA error";
    ANNOUNCE(CoCoA::GlobalErrput(), err);
  }
  catch (const std::exception& exc)
  {
    CoCoA::GlobalErrput() << "***ERROR***  UNCAUGHT std::exception: " << exc.what() << std::endl;
  }
  catch(...)
  {
    CoCoA::GlobalErrput() << "***ERROR***  UNCAUGHT UNKNOWN EXCEPTION" << std::endl;
  }
  return 1;
}
