#!/bin/bash



# DIST_ROOT=/Users/shellcomputer/disttest

CP="cp -v"

DIST_ROOT=`pwd`
echo "Distroot: " $DIST_ROOT
AC_DIST=$DIST_ROOT/dist

CHICN=$DIST_ROOT/changeIcon
echo "$CHICN"


AC_VERSION=1.2
APCOCOA_RELEASE=ApCoCoA-$AC_VERSION
APCOCOA_ROOT=$AC_DIST/$APCOCOA_RELEASE
AC_APP_DIR=$APCOCOA_RELEASE.app
AC_APP_ROOT=$AC_DIST/$APCOCOA_RELEASE/$AC_APP_DIR

###############################################################################
# Take the original CoCoA-Mac-Version from here
###############################################################################
MAC_CC_PRFX=/Users/shellcomputer/disttest
MAC_CC_TXT=$MAC_CC_PRFX/text/CoCoA-4.7
MAC_CC_GUI=$MAC_CC_PRFX/gui/MacCoCoA/MacCoCoA-4.7.app

###############################################################################
# Take the ApCoCoA- / BBFServer from here
###############################################################################
AC_SERVER_ROOT=/Users/shellcomputer/Documents/workspace_cdt/ApCoCoALib-R$AC_VERSION/
AC_SRV=$AC_SERVER_ROOT/src/Server/ApCoCoAServer
AC_BBF_SRV=$AC_SERVER_ROOT/src/BBFServer/ApCoCoABBFServer

###############################################################################
# Take the external precompiled libraries from here:
###############################################################################
GLPK_ROOT=/Applications/ApCoCoA-1.0.0.app/Contents/MacOS/Glpk/
LATTE_ROOT=/Users/shellcomputer/disttest/latte-for-tea-too-1.2-mk-0.9.3/dest
# Note, we do not build a static gnuplot library,
# each user should take its own.
# Here, we only provide a shell script that calls gnuplot.
GNUPLOT_ROOT=
MINISAT_ROOT=/Users/shellcomputer/disttest/minisat/minisat/minisat/core

echo "Creating $AC_DIST"
mkdir $AC_DIST
cd $AC_DIST

# First, copy the contents of the GUI release to here
mkdir $APCOCOA_ROOT
cd $APCOCOA_ROOT
$CP -R $MAC_CC_GUI .

###############################################################################
# Rename all cocoa-names into apcocoa-names
###############################################################################
mv MacCoCoA-4.7.app $AC_APP_DIR 
mv $AC_APP_DIR/Contents/MacOS/MacCoCoA $AC_APP_DIR/Contents/MacOS/ApCoCoA
mv $AC_APP_DIR/Contents/MacOS/MacCoCoA-4.7 $AC_APP_DIR/Contents/MacOS/ApCoCoA-$AC_VERSION

sed -i .orig -e 's/MacCoCoA/ApCoCoA/g' $AC_APP_DIR/Contents/MacOS/ApCoCoA-$AC_VERSION

# The Info.plist determines the behaviour of the MAC application
# Replace any occurence of MacCoCoA with ApCoCoA
echo "Changing Info.plist..."
sed -i .orig -e 's/MacCoCoA/ApCoCoA/g' $AC_APP_DIR/Contents/Info.plist

# Copy the ApCoCoA- and BBFServer
# The ACServer is started with a bash script which sets the paths for Latte
# see below.
# The BBFServer can be startet directlry from the dist root

echo "Copying ApCoCoA- and BBFServer..."
$CP $AC_SRV $AC_APP_DIR/Contents/MacOS/ApCoCoAServer
$CP $AC_BBF_SRV .

# cocoa_text is "hidden" in the Contents/MacOS dir, since it should
# not be started directly.
# the wrapper script 'cocoa' will be copied from the release files
# see below
echo "Copying cocoa_text..."
$CP $MAC_CC_TXT/cocoa_text $AC_APP_DIR/Contents/MacOS/cocoa_text

###############################################################################
# Get the latest ApCoCoA packages into packages
###############################################################################

echo "############################"
echo "# Getting Packages         #"
echo "############################"
cd $AC_DIST

svn co svn://apcocoa.org/ApCoCoAPackages/trunk ApCoCoAPackages > /dev/null
mkdir $AC_APP_ROOT/Contents/MacOS/packages/apcocoa
$CP -R $AC_DIST/ApCoCoAPackages/packages/apcocoa/* $AC_APP_ROOT/Contents/MacOS/packages/apcocoa
$CP $AC_DIST/ApCoCoAPackages/packages/platform-dependent/linux/*.cpkg $AC_APP_ROOT/Contents/MacOS/packages/apcocoa
$CP -R ApCoCoAPackages/modified-cocoa-files/* $AC_APP_ROOT/Contents/MacOS/packages
$CP ApCoCoAPackages/modified-cocoa-files/init.coc $AC_APP_ROOT/Contents/MacOS/init.coc
$CP ApCoCoAPackages/$AC_VERSION-release-files/StartApCoCoAServer.mac $APCOCOA_ROOT/StartApCoCoAServer
chmod 755 $APCOCOA_ROOT/StartApCoCoAServer

$CP ApCoCoAPackages/$AC_VERSION-release-files/README.release $APCOCOA_ROOT/README
$CP ApCoCoAPackages/$AC_VERSION-release-files/GPL.v3 $APCOCOA_ROOT/LICENSE

$CP ApCoCoAPackages/$AC_VERSION-release-files/cocoa.mac $APCOCOA_ROOT/cocoa
chmod 755 $APCOCOA_ROOT/cocoa

###############################################################################
# Get the latest help into doc and man
###############################################################################
echo "############################"
echo "# Getting Manual           #"
echo "############################"
svn co svn://apcocoa.org/Manual/trunk Manual > /dev/null

echo "# AC_APP_ROOT=$AC_APP_ROOT"
rm -rf $AC_APP_ROOT/Contents/MacOS/doc/*
rm -rf $AC_APP_ROOT/Contents/MacOS/packages/man/*

echo "# AC_DIST=$AC_DIST"
$CP -R $AC_DIST/Manual/doc/* $AC_APP_ROOT/Contents/MacOS/doc
$CP -R $AC_DIST/Manual/man/* $AC_APP_ROOT/Contents/MacOS/packages/man

###############################################################################
# (Get and) Copy the external executables.
###############################################################################
echo "Copy external executables..."
echo "GLPK..."
cd $AC_APP_ROOT/Contents/MacOS
mkdir Glpk
$CP -R $GLPK_ROOT Glpk

echo "Gnuplot..."
mkdir -p gnuplot/bin
echo "#!/bin/bash" > gnuplot/bin/pgnuplot
echo "gnuplot \$*" >> gnuplot/bin/pgnuplot
chmod 755 gnuplot/bin/pgnuplot

echo "Latte..."
mkdir Latte
$CP -R $LATTE_ROOT Latte

echo "Minisat..."
mkdir -p minisat/bin
$CP $MINISAT_ROOT/minisat minisat/bin
chmod 755 minisat/bin/minisat

###############################################################################
# Change icons
###############################################################################

echo "Changing icons..."
# Can we change the images? Yes, we can...
# Syntax: changeIcon <iconfile> <target>
COCOA_ICON=$AC_APP_ROOT/Contents/Resources/MacCoCoA.icns
# Change .app folders icon
$CHICN $COCOA_ICON $AC_APP_ROOT
# And the applications
$CHICN $COCOA_ICON $APCOCOA_ROOT/StartApCoCoAServer
$CHICN $COCOA_ICON $APCOCOA_ROOT/ApCoCoABBFServer
$CHICN $COCOA_ICON $APCOCOA_ROOT/cocoa


# Can we automatically build the dmg or whatever is suitable?
# We have to build a package and place this package in a dmg.
