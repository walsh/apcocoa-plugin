#!/bin/bash

# This script looks for a GMP installation in a standard location.
# If found, print out the full path of the (static) library.
# If not found, print out nothing.

##################################################################
# Here is our list of standard locations for the static library.
# We just use the first we find (check /usr/local/lib *before*
# /usr/lib expecting there might be a more recent "local" version).


STD_GMP="/usr/lib64/libgmp.a:/usr/local/gmp-4.2.1-static/lib/libgmp.a:/usr/local/gmp-4.2-static/lib/libgmp.a:/usr/local/gmp-4.1.4-static/lib/libgmp.a:/usr/local/lib/libgmp.a:/usr/lib/libgmp.a:/sw/lib/libgmp.a"

# Loop through list of candidates: accept candidate if both library
# and header file exist and are readable.
IFS=":"
for candidate in $STD_GMP
do
  GMP_DIR=`dirname $candidate`
  GMP_HDR="$GMP_DIR/../include/gmp.h"
  if [ -f "$candidate" -a -r "$candidate" -a -f "$GMP_HDR" -a -r "$GMP_HDR" ]
  then
    # Candidate is acceptable; print it out and exit.
    echo "$candidate"
    exit
  fi
done

# Did not find any plausible GMP installation, so return empty handed.
