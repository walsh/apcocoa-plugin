# file: cmake-config.linux32.sk
#
# Initial cmake cache setup file.
#
# This file provides an overview of
#   1) the variables that must be set in order to build the basic ApCoCoALib
#      library.
#   2) optional variables that enable different optional features of the
#      ApCoCoALib library.
#
# Please refer to cmake and/or a cmake GUI to find out which additional
# variables can be set on your platform. Setting only the variables in this
# file should allow you to build the ApCoCoALib library without the help of a
# cmake GUI, i.e. just by running
#
#   cmake -C <path-to-this-file> <path-to-ApCoCoALib-source>
#   make
#
# Of course you can also use this file when using a cmake GUI to preload cache
# variable values, e.g. ccmake
#
#   ccmake -C <path-to-this-file> <path-to-ApCoCoALib-source>
# 
# The syntax used in this file is the cmake syntax. Only values denoted by
# "<>" (e.g. "<path>") must be set up by user. Use "#" to comment out lines.
# 
# Setup description keywords:
#   MANDATORY  - This variable must be set by the user
#   FIND-BASED - This variable is usually determined automatically by
#                cmake; set if automatic determination fails
#   OPTIONAL   - Setting up this variable is optional

#-------------------------------------------------------------------------------

#
# Additional settings for static linking of executables
#

# Create statically linked executables
option(DO_STATIC_BUILD
       "Create statically linked executables."
       ON)

# Additional linker flags to enable static linking
set(ADD_LINK_FLAGS
    "-static -static-libgcc"
    CACHE
    STRING
    "Additional linker flags when linking executables.")

# Additional libraries for static linking of executables
set(ADD_LINK_LIBS
    "/lib/gcc/i686-pc-cygwin/4.3.4/libgfortran.a"
    CACHE
    STRING
    "Additional libraries for static linking of executables.")

#-------------------------------------------------------------------------------

#
# Additional settings for cygwin
# 

# If e.g. Matlab is installed make is not found correctly
set(CMAKE_MAKE_PROGRAM
    "/usr/bin/make.exe"
    CACHE
    PATH
    "Additional libraries for static linking of executables.")

#-------------------------------------------------------------------------------

#
# CoCoALib setup
#

# MANDATORY: path to CoCoALib root directory
set(COCOALIB_ROOT
    /home/BeppoB/CoCoALib-0.99
    CACHE
    PATH
    "Path to CoCoALib source tree.")

#-------------------------------------------------------------------------------

#
# GMP setup
#

# FIND-BASED: GMP include directory
#set(GMP_INCLUDE_DIR
#    <path>
#    CACHE
#    PATH
#    "Path to GMP include directory.")

# FIND-BASED: GMP library
#set(GMP_LIBRARY
#    <path-to-file>
#    CACHE
#    FILEPATH
#    "GMP library used for linking.")

#-------------------------------------------------------------------------------

#
# Boost setup
#

# FIND-BASED: path to Boost installation root directory
#set(BOOST_ROOT
#    <path>
#    CACHE
#    PATH
#    "Path to Boost installation root directory.")

# OPTIONAL: enable linking against static Boost libraries;
#           default = OFF
set(Boost_USE_STATIC_LIBS
    OFF
    CACHE
    BOOL
    "Link executables against static Boost libraries.")

#-------------------------------------------------------------------------------

# OPTIONAL: build the Border Basis Framework;
#           default = OFF
option(APCOCOALIB_BUILD_BBF
       "Build the Border Basis Framework"
       ON)

#-------------------------------------------------------------------------------

#
# Optional: IML setup
#

# OPTIONAL: enable IML support;
#           if enabled, please also check the variables below;
#           default = OFF
option(APCOCOALIB_USE_IML
       "Enable IML support"
       ON)

# FIND-BASED: path to IML include directory
set(IML_INCLUDE_DIR
    /usr/local/include
    CACHE
    PATH
    "Path to IML include directory.")


# FIND-BASED: IML library
set(IML_LIBRARY
    /usr/local/lib/libiml.a
    CACHE
    FILEPATH
    "IML library used for linking.")

# FIND-BASED: path to ATLAS include directory
#set(ATLAS_INCLUDE_DIR
#    <path>
#    CACHE
#    PATH
#    "Path to ATLAS include directory.")

# FIND-BASED: ATLAS library
#set(ATLAS_LIBRARY
#    <path-to-file>
#    CACHE
#    FILEPATH
#    "ATLAS library.")

# FIND-BASED: cblas library
#set(CBLAS_LIBRARY
#    <path-to-file>
#    CACHE
#    FILEPATH
#    "cblas library.")

#-------------------------------------------------------------------------------

#
# Optional: LinBox setup
#

# OPTIONAL: enable LinBox support;
#           if enabled, please also check the variables below;
#           default = OFF
option(APCOCOALIB_USE_LINBOX
       "Enable LinBox support"
       ON)

# FIND-BASED: path to LinBox include directory
set(LINBOX_INCLUDE_DIR
    /usr/local/include
    CACHE
    PATH
    "Path to LinBox include directory.")

# FIND-BASED: LinBox library
set(LINBOX_LIBRARY
    /usr/local/lib/liblinbox.a
    CACHE
    FILEPATH
    "LinBox library used for linking.")

# FIND-BASED: path to Givaro include directoriy
set(GIVARO_INCLUDE_DIR
    /usr/local/include
    CACHE
    PATH
    "Path to Givaro include directory.")

# FIND-BASED: Givaro library
set(GIVARO_LIBRARY
    /usr/local/lib/libgivaro.a
    CACHE
    FILEPATH
    "Givaro library used for linking.")

# FIND-BASED: path to ATLAS include directory
#set(ATLAS_INCLUDE_DIR
#    <path>
#    CACHE
#    PATH
#    "Path to ATLAS include directory.")

# FIND-BASED: ATLAS library
#set(ATLAS_LIBRARY
#    <path-to-file>
#    CACHE
#    FILEPATH
#    "ATLAS library.")

# FIND-BASED: cblas library
#set(CBLAS_LIBRARY
#    <path-to-file>
#    CACHE
#    FILEPATH
#    "cblas library.")

#-------------------------------------------------------------------------------

#
# Optional: Numerical computations support
#

# OPTIONAL: enable numerical computations support;
#           if enabled, please also check the variables below;
#           default = OFF
option(APCOCOALIB_ENABLE_NUMERICAL
       "Enable numerical computations"
       ON)

# FIND-BASED (LINUX/CYGWIN only): path to ATLAS include directory
#set(ATLAS_INCLUDE_DIR
#    <path>
#    CACHE
#    PATH
#    "Path to ATLAS include directory.")

# FIND-BASED (LINUX/CYGWIN only): ATLAS library
#set(ATLAS_LIBRARY
#    <path-to-file>
#    CACHE
#    FILEPATH
#    "ATLAS library.")

#-------------------------------------------------------------------------------