######################
# Mandatory settings #
######################
 
 BOOSTFLAGS=-I"/usr/local/include/boost/"
 
 BOOSTLIBS= "/usr/local/lib/libboost_thread.a" \
            "/usr/local/lib/libboost_system.a" \
			"/usr/local/lib/libboost_filesystem.a" \
			"/usr/local/lib/libboost_iostreams.a" \
			-lpthread
 
#####################
# Optional features #
#####################

 # Uncomment the following flag if you want to use the
 # Border Basis Framework
 BBFFLAGS=-DUSEBBFramework
 
 # Linker flags of your Boost program_options and thread
 # libraries plus linker flags of additional libraries needed
 # by Boost, e.g.
 BBFLIBS="/usr/local/lib/libboost_program_options.a" \
         "/usr/local/lib/libboost_thread.a" \
         -lpthread
 
 # Uncomment the following flag if you want to use
 # IML-driven computations    
 IMLFLAGS=-DUSEIML
 
 # Path to your IML include directory, e.g.
 IMLFLAGS:=$(IMLFLAGS) -I"/usr/local/include/"
 
 # Linker flags of your IML library plus linker flags of
 # additional libraries needed by IML, e.g.
 IMLLIBS="/usr/local/lib/libiml.a" "/usr/local/lib/libcblas.a" "/usr/local/lib/libgmp.a"
 
 # Uncomment the following flag if you want to use LinBox-
 # driven computations
 LINBOXFLAGS=-DUSELinBox
 
 # Path to your LinBox include directory plus path to
 # additional include directories needed by LinBox, e.g.
 LINBOXFLAGS:=$(LINBOXFLAGS) \
             -I"/usr/local/include/linbox"
 #LINBOXFLAGS:=$(LINBOXFLAGS)   # Can be left empty if you
                               # do not want to use LinBox-
                               # driven computations
 
 # Linker flags of your LinBox library plus linker flags
 # of additional libraries needed by LinBox, e.g. 
 LINBOXLIBS=/usr/local/lib/liblinbox.a \
          /usr/local/lib/libgivaro.a /usr/local/lib/libgmp.a  /usr/local/lib/libgmpxx.a
 
 # Uncomment the following flag if you do not want
 # to use the BLAS and Lapack libraries
 # NUMFLAGS=-DUSENoLapack
 
 # Uncomment the following flag if you want to use
 # the BLAS and Lapack libraries and you are using
 # Linux or Cygwin
 NUMFLAGS=-DUSEF77Lapack

 
 # Linker flags of your BLAS and Lapack libraries
 # plus linker flags of additional libraries needed
 # by BLAS and/or Lapack.
 
 NUMLIBS=-llapack_CYGWIN -lf77blas -lblas_CYGWIN -lf2c -latlas -L/usr/local/lib
