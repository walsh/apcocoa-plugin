#!/bin/bash
if [ ! -f ./CoCoALibCompileTest ]
then
	exit -1
fi
./CoCoALibCompileTest
if [ $? -ne 0 ]
then
	echo "Oops. Execution failed. Please recheck your configuration."
        exit -1
fi
echo "Good, CoCoALib is present and works as expected."
echo
rm -f ./CoCoALibCompileTest
exit 0
