#!/bin/bash

# This script checks that GMP is at least version 3; better still 4.1.4 or later.
# If all checks pass, print GMP version number and exit with code 0.
# If GMP is too old, or some other problem arises, print error mesg and return with code 1.

if [ "$#" != "1" ]
then
  echo "$0: ERROR: requires 1 arg (full path of libgmp.a) "
  exit 1
fi

GMP_LIB="$1"
GMP_LIBDIR=`dirname "$GMP_LIB"`
GMP_HDRDIR=`dirname "$GMP_LIBDIR"`/include
GMP_HDR="$GMP_HDRDIR/gmp.h"

# Check for existence and readability of library and header file.
# Not very readable because "test" has peculiar syntax.
if [ ! \( -f "$GMP_LIB" -a -r "$GMP_LIB" -a -f "$GMP_HDR" -a -r "$GMP_HDR" \) ]
then
  echo "ERROR: GMP header file or library does not exist or is unreadable." > /dev/stderr
  echo "     : Sought header file in \"$GMP_HDR\""                          > /dev/stderr
  echo "     : Sought library in \"$GMP_LIB\""                              > /dev/stderr
  exit 1
fi

# Line below assumes that the GMP header file defines the version macros
# in the order major-minor-patch, and that __GNU_MP_VERSION appears only on those lines.
GMP_HDR_VERSION=`egrep __GNU_MP_VERSION $GMP_HDR | cut -f 3 -d " " | tr "\n" "."`


# Get version number from the library.  A bit convoluted but should work.
# Create tmp directory, put C prog in it, compile, run, get output, delete directory.
TMP_DIR=/tmp/CoCoALib-$USER-$$
PROG=$TMP_DIR/gmp-print-version
/bin/rm -rf $TMP_DIR
mkdir $TMP_DIR
cat > $PROG.c <<EOF
#include <stdio.h>

extern const char* const __gmp_version;

int main()
{
  printf("%s\n", __gmp_version);
  return 0;
}
EOF


# Use c++ compiler found by the main configue script to compile $PROG.c
$CXX $CXXFLAGS $PROG.c -o $PROG "$GMP_LIB"

# Check whether compilation failed; if so, complain.
if [ $? -ne 0 ]
then
  echo "ERROR: unable to determine version of GMP library $GMP_LIB"         > /dev/stderr
  echo "     : (compilation failed in gmp-version-check.sh)"                > /dev/stderr
  /bin/rm -rf $TMP_DIR
  exit 1
fi

# Compilation succeeded, so run $PROG which will print out the version.
GMP_LIB_VERSION=`$PROG`

# Check whether execution failed; if so, complain (probably linker problems).
if [ $? -ne 0 ]
then
  echo "ERROR: unable to determine version of GMP library $GMP_LIB"         > /dev/stderr
  echo "     : (execution of test program failed in gmp-version-check.sh)"  > /dev/stderr
  echo "     : Check that LD_LIBRARY_PATH the GMP library directory."       > /dev/stderr
  /bin/rm -rf $TMP_DIR
  exit 1
fi

/bin/rm -rf $TMP_DIR

# NB in the following line the extra dot after GMP_LIB_VERSION is needed.
if [ "$GMP_HDR_VERSION" != "$GMP_LIB_VERSION." -a "$GMP_HDR_VERSION" != "$GMP_LIB_VERSION.0." \
     -a "$GMP_HDR_VERSION" != "$GMP_LIB_VERSION.(__GNU_MP_VERSION." ]
then
  echo "ERROR: $GMP_LIB appears to be from versions: $GMP_LIB_VERSION"      > /dev/stderr
  echo "     : while $GMP_HDR appears to be from version: $GMP_HDR_VERSION" > /dev/stderr
  exit 1
fi

# CoCoALib source assumes existence of some fns which appeared only in GMP 3.0.
if [ "$GMP_LIB_VERSION" \< "3.0.0" ]
then
  echo "ERROR: Your version of GMP is too old: $GMP_LIB_VERSION"            > /dev/stderr
  echo "     : Header file is $GMP_HDR"                                     > /dev/stderr
  echo "     : Library file is $GMP_LIB"                                    > /dev/stderr
  exit 1
fi

# CoCoALib should really be compiled only with GMP-4.1.4 or later, so warn if
# if the version found is older than 4.1.4.
if [ "$GMP_LIB_VERSION" \< "4.1.4" ]
then
  echo
  echo "WARNING: You have GMP version $GMP_LIB_VERSION."                    > /dev/stderr
  echo "       : A bug in versions of GMP prior to 4.1.4 may lead to"       > /dev/stderr
  echo "       : incorrect results when using RingFloat from CoCoALib."     > /dev/stderr
  echo
fi

# If we get here, all tests have passed, so print version number and exit with code 0.
echo $GMP_LIB_VERSION
