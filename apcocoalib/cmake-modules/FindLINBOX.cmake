# Try to find the LinBox library and set up the following variables.
#
#   LINBOX_FOUND - system has LinBox library
#   LINBOX_INCLUDE_DIR - the LinBox include directory
#   LINBOX_LIBRARY - LinBox library used for linking

if(LINBOX_INCLUDE_DIR AND LINBOX_LIBRARY)
  # Already in cache, be silent
  set(LINBOX_FIND_QUIETLY TRUE)
endif(LINBOX_INCLUDE_DIR AND LINBOX_LIBRARY)

find_path(LINBOX_INCLUDE_DIR NAMES linbox/integer.h)
# Check if path is correct
if(LINBOX_INCLUDE_DIR)
  if(NOT EXISTS ${LINBOX_INCLUDE_DIR}/linbox/integer.h)
    message(FATAL_ERROR "LinBox include directory ${LINBOX_INCLUDE_DIR} does not seem to be correct.")
  endif(NOT EXISTS ${LINBOX_INCLUDE_DIR}/linbox/integer.h)
endif(LINBOX_INCLUDE_DIR)

find_library(LINBOX_LIBRARY NAMES linbox liblinbox)

include(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(LINBOX "Could NOT find LinBox. Please check variables LINBOX_INCLUDE_DIR and LINBOX_LIBRARY." LINBOX_INCLUDE_DIR LINBOX_LIBRARY)

mark_as_advanced(LINBOX_INCLUDE_DIR LINBOX_LIBRARY)
