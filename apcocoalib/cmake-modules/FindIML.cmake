# Try to find the IML library and set up the following variables.
#
#   IML_FOUND - system has IML library
#   IML_INCLUDE_DIR - the IML include directory
#   IML_LIBRARY - IML library used for linking

if(IML_INCLUDE_DIR AND IML_LIBRARY)
  # Already in cache, be silent
  set(IML_FIND_QUIETLY TRUE)
endif(IML_INCLUDE_DIR AND IML_LIBRARY)

find_path(IML_INCLUDE_DIR NAMES iml.h)
# Check if path is correct
if(IML_INCLUDE_DIR)
  if(NOT EXISTS ${IML_INCLUDE_DIR}/iml.h)
    message(FATAL_ERROR "IML include directory ${IML_INCLUDE_DIR} does not seem to be correct.")
  endif(NOT EXISTS ${IML_INCLUDE_DIR}/iml.h)
endif(IML_INCLUDE_DIR)

find_library(IML_LIBRARY NAMES iml libiml)

include(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(IML "Could NOT find IML. Please check variables IML_INCLUDE_DIR and IML_LIBRARY." IML_INCLUDE_DIR IML_LIBRARY)

mark_as_advanced(IML_INCLUDE_DIR IML_LIBRARY)
