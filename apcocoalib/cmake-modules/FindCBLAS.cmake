# Try to find the cblas library and set up the following variables.
#
#   CBLAS_FOUND - system has cblas library
#   CBLAS_LIBRARY - cblas library used for linking

if(CBLAS_LIBRARY)
  # Already in cache, be silent
  set(CBLAS_FIND_QUIETLY TRUE)
endif(CBLAS_LIBRARY)

# added the PATH_SUFFIXES option to extend the search to the default ATLAS installation directory (by Markus, 05.05.2013)
find_library(CBLAS_LIBRARY NAMES cblas libcblas PATH_SUFFIXES atlas/lib)

include(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(CBLAS "Could NOT find cblas library. Please check variable CBLAS_LIBRARY." CBLAS_LIBRARY)

mark_as_advanced(CBLAS_LIBRARY)
