# Try to find the GMP library and set the following variables.
#
#  GMP_FOUND - system has GMP library
#  GMP_INCLUDE_DIR - the GMP include directory
#  GMP_LIBRARY - GMP library used for linking

if(GMP_INCLUDE_DIR AND GMP_LIBRARY)
  # Already in cache, be silent
  set(GMP_FIND_QUIETLY TRUE)
endif(GMP_INCLUDE_DIR AND GMP_LIBRARY)

find_path(GMP_INCLUDE_DIR NAMES gmp.h)
# Check if path is correct
if(GMP_INCLUDE_DIR)
  if(NOT EXISTS ${GMP_INCLUDE_DIR}/gmp.h)
    message(FATAL_ERROR "GMP include directory ${GMP_INCLUDE_DIR} does not seem to be correct.")
  endif(NOT EXISTS ${GMP_INCLUDE_DIR}/gmp.h)
endif(GMP_INCLUDE_DIR)

find_library(GMP_LIBRARY NAMES gmp libgmp)

include(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(GMP "Could NOT find GMP. Please check variables GMP_INCLUDE_DIR and GMP_LIBRARY." GMP_INCLUDE_DIR GMP_LIBRARY)

mark_as_advanced(GMP_INCLUDE_DIR GMP_LIBRARY)
