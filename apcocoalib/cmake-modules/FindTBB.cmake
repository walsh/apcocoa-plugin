# Try to find the TBB library and set up the following variables.
#
#   TBB_FOUND - system has TBB library
#   TBB_INCLUDE_DIR - the TBB include directory
#   TBB_LIBRARY - TBB library used for linking

if(TBB_INCLUDE_DIR AND TBB_LIBRARY)
  # Already in cache, be silent
  set(TBB_FIND_QUIETLY TRUE)
endif(TBB_INCLUDE_DIR AND TBB_LIBRARY)

find_path(TBB_INCLUDE_DIR NAMES tbb/tbb.h)
# Check if path is correct
if(TBB_INCLUDE_DIR)
  if(NOT EXISTS ${TBB_INCLUDE_DIR}/tbb/tbb.h)
    message(FATAL_ERROR "TBB include directory ${TBB_INCLUDE_DIR} does not seem to be correct.")
  endif(NOT EXISTS ${TBB_INCLUDE_DIR}/tbb/tbb.h)
endif(TBB_INCLUDE_DIR)

find_library(TBB_LIBRARY NAMES tbb libtbb)

include(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(TBB "Could NOT find TBB. Please check variables TBB_INCLUDE_DIR and TBB_LIBRARY." TBB_INCLUDE_DIR TBB_LIBRARY)

mark_as_advanced(TBB_INCLUDE_DIR TBB_LIBRARY)
