# This snippet checks if the CoCoALib root directory is set up correctly
# and sets the following variables.
#
#  COCOALIB_INCLUDE_DIR - path to CoCoALib includes
#  COCOALIB_LIBRARY - CoCoALib library used for linking

# Check if COCOALIB_ROOT is set to the correct directory
if(EXISTS ${COCOALIB_ROOT}/src/server/SocketStream.C)
  message("* CoCoALib source tree seems to be present in ${COCOALIB_ROOT}.")
  
  # Check if libcocoa.a exists
  if(NOT EXISTS ${COCOALIB_ROOT}/lib/libcocoa.a)
    message(FATAL_ERROR "libcocoa.a does not seem to be present in ${COCOALIB_ROOT}/lib. Please check your CoCoALib installation.")
  endif(NOT EXISTS ${COCOALIB_ROOT}/lib/libcocoa.a)
  
  # CoCoALib include directories
  set(COCOALIB_INCLUDE_DIR ${COCOALIB_ROOT}/include CACHE PATH "Path to CoCoALib include directory.")
  mark_as_advanced(COCOALIB_INCLUDE_DIR)
  message("* Using ${COCOALIB_INCLUDE_DIR} as CoCoALib include directory.")
  
  # CoCoALib library
  set(COCOALIB_LIBRARY ${COCOALIB_ROOT}/lib/libcocoa.a CACHE FILEPATH "Path to libcocoa.a.")
  mark_as_advanced(COCOALIB_LIBRARY)
  message("* Using ${COCOALIB_LIBRARY} as CoCoALib library.")
else(EXISTS ${COCOALIB_ROOT}/include/CoCoA)
  message(FATAL_ERROR "CoCoALib source root directory not found. Please specify path to CoCoALib source root directory in COCOALIB_ROOT.")
endif(EXISTS ${COCOALIB_ROOT}/src/server/SocketStream.C)
