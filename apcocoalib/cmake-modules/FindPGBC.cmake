# Try to find the PGBC library and set up the following variables.
#
#   PGBC_FOUND - system has PGBC library
#   PGBC_INCLUDE_DIR - the PGBC include directory
#   PGBC_LIBRARY - PGBC library used for linking

if(PGBC_INCLUDE_DIR AND PGBC_LIBRARY)
  # Already in cache, be silent
  set(PGBC_FIND_QUIETLY TRUE)
endif(PGBC_INCLUDE_DIR AND PGBC_LIBRARY)

find_path(PGBC_INCLUDE_DIR NAMES F4.H)
# Check if path is correct
if(PGBC_INCLUDE_DIR)
  if(NOT EXISTS ${PGBC_INCLUDE_DIR}/F4.H)
    message(FATAL_ERROR "PGBC include directory ${PGBC_INCLUDE_DIR} does not seem to be correct.")
  endif(NOT EXISTS ${PGBC_INCLUDE_DIR}/F4.H)
endif(PGBC_INCLUDE_DIR)

find_library(PGBC_LIBRARY NAMES f4 libf4)

include(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(PGBC "Could NOT find parallelGBC. Please check variables PGBC_INCLUDE_DIR and PGBC_LIBRARY." PGBC_INCLUDE_DIR PGBC_LIBRARY)

mark_as_advanced(PGBC_INCLUDE_DIR PGBC_LIBRARY)
