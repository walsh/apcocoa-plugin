# Try to find the ATLAS library and set up the following variables.
#
#   ATLAS_FOUND - system has ATLAS library
#   ATLAS_INCLUDE_DIR - the ATLAS include directory
#   ATLAS_LIBRARY - ATLAS library used for linking

if(ATLAS_INCLUDE_DIR AND ATLAS_LIBRARY)
  # Already in cache, be silent
  set(ATLAS_FIND_QUIETLY TRUE)
endif(ATLAS_INCLUDE_DIR AND ATLAS_LIBRARY)

# added the PATH_SUFFIXES option to extend the search to the default ATLAS installation directory (by Markus, 05.05.2013)
find_path(ATLAS_INCLUDE_DIR NAMES cblas.h PATH_SUFFIXES atlas/include)
# Check if path is correct
if(ATLAS_INCLUDE_DIR)
  if(NOT EXISTS ${ATLAS_INCLUDE_DIR}/cblas.h)
    message(FATAL_ERROR "${ATLAS_INCLUDE_DIR}/cblas.h does not exist. ATLAS include directory ${ATLAS_INCLUDE_DIR} does not seem to be correct.")
  endif(NOT EXISTS ${ATLAS_INCLUDE_DIR}/cblas.h)
endif(ATLAS_INCLUDE_DIR)

# added the PATH_SUFFIXES option to extend the search to the default ATLAS installation directory (by Markus, 05.05.2013)
find_library(ATLAS_LIBRARY NAMES atlas libatlas PATH_SUFFIXES atlas/lib)

include(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(ATLAS "Could NOT find ATLAS. Please check variables ATLAS_INCLUDE_DIR and ATLAS_LIBRARY." ATLAS_INCLUDE_DIR ATLAS_LIBRARY)

mark_as_advanced(ATLAS_INCLUDE_DIR ATLAS_LIBRARY)
