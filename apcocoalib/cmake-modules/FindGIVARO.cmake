# Try to find the Givaro library and set up the following variables.
#
#   GIVARO_FOUND - system has Givaro library
#   GIVARO_INCLUDE_DIR - the Givaro include directory
#   GIVARO_LIBRARY - Givaro library used for linking

if(GIVARO_INCLUDE_DIR AND GIVARO_LIBRARY)
  # Already in cache, be silent
  set(GIVARO_FIND_QUIETLY TRUE)
endif(GIVARO_INCLUDE_DIR AND GIVARO_LIBRARY)

find_path(GIVARO_INCLUDE_DIR NAMES givaro/giverror.h)
# Check if path is correct
if(GIVARO_INCLUDE_DIR)
  if(NOT EXISTS ${GIVARO_INCLUDE_DIR}/givaro/giverror.h)
    message(FATAL_ERROR "Givaro include directory ${GIVARO_INCLUDE_DIR} does not seem to be correct.")
  endif(NOT EXISTS ${GIVARO_INCLUDE_DIR}/givaro/giverror.h)
endif(GIVARO_INCLUDE_DIR)

find_library(GIVARO_LIBRARY NAMES givaro libgivaro)

include(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(GIVARO "Could NOT find Givaro. Please check variables GIVARO_INCLUDE_DIR and GIVARO_LIBRARY." GIVARO_INCLUDE_DIR GIVARO_LIBRARY)

mark_as_advanced(GIVARO_INCLUDE_DIR GIVARO_LIBRARY)
