######################
# Mandatory settings #
######################

# Path to your Boost library include directory, e.g.
#BOOSTFLAGS=-I"/home/skaspar/lib/boost_1_38_0/include/boost-1_38"
BOOSTFLAGS=   # Mandatory, cannot be left empty

# Linker flags of your Boost thread libraries plus linker flags of
# additional libraries needed by Boost, e.g.
#BOOSTLIBS="/home/skaspar/lib/boost_1_38_0/lib/libboost_thread-gcc43-mt.a" \
#          "/home/skaspar/lib/boost_1_38_0/lib/libboost_system-gcc43-mt.a" \
#          "/home/skaspar/lib/boost_1_38_0/lib/libboost_filesystem-gcc43-mt.a" \
#          "/home/skaspar/lib/boost_1_38_0/lib/libboost_iostreams-gcc43-mt.a" \
#          -lpthread
BOOSTLIBS=   # Mandatory, cannot be left empty

#####################
# Optional features #
#####################

# Uncomment the following flag if you want to use the
# Border Basis Framework
#BBFFLAGS=-DUSEBBFramework

# Linker flags of your Boost program_options and thread
# libraries plus linker flags of additional libraries needed
# by Boost, e.g.
#BBFLIBS="/home/skaspar/lib/boost_1_36_0/lib/libboost_program_options-gcc41-mt.a" \
#        "/home/skaspar/lib/boost_1_36_0/lib/libboost_thread-gcc41-mt.a" \
#        -lpthread
BBFLIBS=   # Can be left empty if you do not want to use
           # the Border Basis Framework

# Uncomment the following flag if you want to use
# IML-driven computations
#IMLFLAGS=-DUSEIML

# Path to your IML include directory, e.g.
#IMLFLAGS:=$(IMLFLAGS) -I"/home/skaspar/lib/iml-1.0.2/include"

IMLFLAGS:=$(IMLFLAGS)   # Can be left empty if you do not
                        # want to use IML-driven
                        # computations
 
# Linker flags of your IML library plus linker flags of
# additional libraries needed by IML, e.g.
#IMLLIBS="/home/skaspar/lib/iml-1.0.2/lib/libiml.a" \
#        -lcblas \
#        -latlas \
#        "/usr/lib/libgmp.a"
IMLLIBS=  # Can be left empty if you do not want to use
          # IML driven computations
 
# Uncomment the following flag if you want to use LinBox-
# driven computations
#LINBOXFLAGS=-DUSELinBox
 
# Path to your LinBox include directory plus path to
# additional include directories needed by LinBox, e.g.
#LINBOXFLAGS:=$(LINBOXFLAGS) \
#            -I"/home/skaspar/lib/linbox-1.1.6/include" \
#            -I"/home/skaspar/lib/givaro-3.2.13rc1/include"
LINBOXFLAGS:=$(LINBOXFLAGS)   # Can be left empty if you
                              # do not want to use LinBox-
                              # driven computations
 
# Linker flags of your LinBox library plus linker flags
# of additional libraries needed by LinBox, e.g. 
#LINBOXLIBS=/home/skaspar/lib/linbox-1.1.6/lib/liblinbox.a \
#           -lgmpxx \
#           /home/skaspar/lib/givaro-3.2.13rc1/lib/libgivaro.a
LINBOXLIBS=   # Can be left empty if you do not
              # want to use LinBox-driven
              # computations
 
# Uncomment the following flag if you do not want
# to use the BLAS and Lapack libraries
NUMFLAGS=-DUSENoLapack
 
# Uncomment the following flag if you want to use
# the BLAS and Lapack libraries and you are using
# Linux or Cygwin
#NUMFLAGS=-DUSEF77Lapack
 
# Uncomment the following flag if you want to use
# the BLAS and Lapack libraries provided by the
# Accelerate Framework on MacOSX
#NUMFLAGS=-DUSEAccFW
 
# Linker flags of your BLAS and Lapack libraries
# plus linker flags of additional libraries needed
# by BLAS and/or Lapack.
#
# Example flags on a Linux or cygwin system
#NUMLIBS=-llapack -lblas -latlas
#  
# Example flags on a MacOSX system using the
# the Accelerate Framework
#NUMLIBS=-bind_at_load -framework accelerate
NUMLIBS=   # Can be left empty if you do
           # not want to use the BLAS and
           # Lapack libraries

# Uncomment the following flag if you want to use
# parallelGBC to compute parallel Groebner Bases
# over finite fields. (EXPERIMENTAL)
#PGBCFLAGS=-DUsePGBC -fopenmp -std=c++0x -I/path/to/parallelGBC/include/F4.H
#PGBCLIBS=/path/to/parallelGBC/lib/libf4.a
