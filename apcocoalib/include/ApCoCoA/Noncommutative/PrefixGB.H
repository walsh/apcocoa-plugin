//
// This file is part of the source of ApCoCoALib, the ApCoCoA Library.
//
//   Copyright (c) ApCoCoA Project (Prof. Dr. Martin Kreuzer, Uni Passau)
//
//   Author: 2009, 2010 X. XIU
//
// Visit http://apcocoa.org/ for more information regarding ApCoCoA
// and ApCoCoALib.
// Visit http://www.apcocoa.org/wiki/ApCoCoA:KnownIssues for bugs, problems
// and known issues.
//
// ApCoCoALib is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License (version 3 or later)
// as published by the Free Software Foundation. A copy of the full
// licence may be found in the file COPYING in this directory.
//
// ApCoCoALib is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ApCoCoALib; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

#ifndef ApCoCoA_PrefixGB_H
#define ApCoCoA_PrefixGB_H

#include "ApCoCoA/Noncommutative/MPolynomial.H"
#include <iostream>
#include <set>
#include <vector>
using std::cout;
using std::endl;
using std::set;
using std::size_t;
using std::vector;


namespace ApCoCoA {
    namespace NoncommutativeAlgorithms {


        ////////////////////////////////////////////////////////////
        //	prefix Groebner basis computations
        ////////////////////////////////////////////////////////////
        
        //! =============== PrefixNormalForm ===============
        /*! \brief prefix normal form given by prefix division algorithm.
         * Note that normal form nf of p w.r.t. a set of polynomials S if
         * there is no terms in nf can be prefix reduced by any element in G.
         * Strictly speaking, nf is a normal prefix remainder of p w.r.t. S.
         * Termination and uniqueness are guaranteed by admissible ordering
         * and ordering of elements in S.
         */
        template<typename T>
        bool PrefixNormalForm(const MPolynomial<T> & p,
                              const set<MPolynomial<T> > & S,
                              MPolynomial<T> & nf)
        {
            nf = p;
            if (p.isZero() || S.empty()) return false; // nf=p
            bool bRet = false;
            MPolynomial<T> f = p;
            MPolynomial<T> q; // q=0
            while (!f.isZero())
            {
                string str_ltf = f.LW().getTerm();
                typename set<MPolynomial<T> >::const_reverse_iterator crit; //place to play strategy
                for (crit = S.rbegin(); crit != S.rend(); )
                {
                    string str_crit = crit->LW().getTerm();
                    if (0 == str_ltf.find(str_crit)) //prefix division (or reduction) f=f-c*crit*w
                    {
                        T c = f.LC() / crit->LC();
                        Monomial m(p.LW().getOMPtr(),
                                           str_ltf.substr(str_crit.length(),string::npos));
                        f = f - ((*crit) * m * c);
                        bRet = true;
                        break;
                    }
                    if (++crit == S.rend()) // f=f-LC(f)LT(f); nf=nf+LC(f)LT(f)
                    {
                        map<Monomial,T> supp;
                        supp[f.LW()] = f.LC();
                        MPolynomial<T> h(supp);
                        supp.clear();
                        f = f - h;
                        q = q + h;
                    }
                } // end : for (crit = S.rbegin(); crit != S.rend(); )
            } // end : while (!f.IsZero())
            if (!q.isZero()) q = q / q.LC();
            nf = q;
            return bRet;
        }
        
        //! =============================== PrefixSpol ===============================
        /*! \brief prefix S-polynomial.
         * Spol(p1,p2)=
         * (1)LC(p2)p1-LC(p1)p2*w, if LT(p1)=LT(p2)*w, or
         * (2)LC(p2)p1*w-LC(p1)p2, if LT(p1)*w=LT(p2).
         * The function returns false, when p1=0 or p2=0 or p1 is unit or p2 is unit
         * or p1=p2, or even worse when prefix S-polynomial does not exist.
         */
        template<typename T>
        bool PrefixSpol(const MPolynomial<T> & p1, const MPolynomial<T> & p2,
                        MPolynomial<T> & sp)
        {
            if (p1.isZero() || p2.isZero() ||
                p1.isUnit() || p2.isUnit() || p1 == p2)
                return false;
            string str_lt_p1 = p1.LW().getTerm();
            string str_lt_p2 = p2.LW().getTerm();
            size_t len_1 = str_lt_p1.length();
            size_t len_2 = str_lt_p2.length();
            if (len_1 >= len_2)
            {
                if (str_lt_p1.substr(0,len_2) == str_lt_p2) // LC(p2)p1-LC(p1)p2*w
                {
                    Monomial w(p1.LW().getOMPtr(),
                                     str_lt_p1.substr(len_2,string::npos));
                    sp = (p1 * p2.LC()) - (p2 * w * p1.LC());
                    if (!sp.isZero()) sp = sp / sp.LC();
                    return true;
                }
            }
            else
            {
                if (str_lt_p2.substr(0,len_1) == str_lt_p1) // LC(p2)p1*w-LC(p1)p2
                {
                    Monomial w(p1.LW().getOMPtr(),
                                     str_lt_p2.substr(len_1,string::npos));
                    sp = (p1 * w * p2.LC()) - (p2 * p1.LC());
                    if (!sp.isZero()) sp = sp / sp.LC();
                    return true;
                }
            }
            return false;
        }
        
        //! =============================== PrefixInterreduce ===============================
        /*! \brief prefix interreduce a set F of polynomials.
         * A set of polynomials F is prefix interreduced if for any p in F, no terms of p
         * can be prefix reduced by any other polynomials in F.
         */
        template<typename T>
        void PrefixInterreduce(vector<MPolynomial<T> > & vF)
        {
            vector<MPolynomial<T> > vec_F = vF;
            size_t len_F = vec_F.size();
            bool bReducible = true;
            while(bReducible)
            {
                bReducible = false;
                for (size_t i = 0; i != len_F; ++i)
                {
                    MPolynomial<T> p = vec_F.at(i);
                    if (p.isZero()) continue;
                    set<MPolynomial<T> > S;
                    for (size_t j = 0; j != len_F; ++j)
                    {
                        if (j != i && !vec_F.at(j).isZero())
                            S.insert(vec_F.at(j));
                    }
                    bReducible = PrefixNormalForm(p,S,p);
                    S.clear();
                    if (bReducible)
                    {
                        vec_F.at(i) = p;
                        break;
                    }
                } // end: for (size_t i = 0; i != len_F; ++i)
            } // end: while(bReducible)
            vF.clear();
            for (size_t i = 0; i != len_F; ++i)
            {
                if (!vec_F.at(i).isZero())
                    vF.push_back(vec_F.at(i));
            }
            vec_F.clear();
            return;
        }
        template<typename T>
        void PrefixInterreduce(set<MPolynomial<T> > & sF)
        {
            vector<MPolynomial<T> > vF;
            vF.insert(vF.begin(),sF.begin(),sF.end());
            PrefixInterreduce(vF);
            sF.clear();
            sF.insert(vF.begin(),vF.end());
            vF.clear();
            return;
        }
        
        //! =============================== PrefixSaturate ===============================
        /*! \brief compute saturate of a polynomial.
         * Sat(p) is a set of non-zero polynomials such that p*w can be prefix reduced to 0
         * in one step for any w in M.
         * Note that termination of this function is not guaranteed.
         * Hypothesis: p is non-zero and non-unit.
         *
         * Question: how to enumerate the procedure?
         */
        template<typename T>
        void PrefixSaturate(const MPolynomial<T> & p,
                            set<MPolynomial<T> > & sat,
                            const size_t loopbound = 2048, const size_t iFlag = 0)
        {
            if (0 != iFlag)
            {
                cout << "Sat(p), where p = " << p << endl;
                vector<pair<string,string> > rels = p.LW().getOMPtr()->getRels();
                if (!rels.empty()) cout << "rewriting rules: ";
                for (size_t i = 0; i != rels.size(); )
                {
                    cout << rels.at(i).first << "-->" << rels.at(i).second;
                    if (++i != rels.size()) cout << ", "; else cout << endl;
                }
            }
            set<MPolynomial<T> > S;
            set<MPolynomial<T> > H;
            S.insert(p);
            H.insert(p);
            typedef typename set<MPolynomial<T> >::iterator It_P;
            size_t iCount = 0;
            while (!H.empty())
            {
                if (++iCount > loopbound) { H.clear(); break; }
                MPolynomial<T> h = *(H.begin()); H.erase(H.begin());
                set<Monomial> C = coverlaps(h.LW()); //it is always finite
                if (0 != iFlag)
                {
                    cout << "=============" << endl;
                    cout << iCount << "th loop:" << endl;
                    cout << "Sat(p) has " << S.size() << " elements, and H has "
                    << H.size() << " elements" << endl;
                    cout << "C(h) has " << C.size() << " elements, where h = " << h << endl;
                    cout << "C(h): ";
                    for (set<Monomial>::iterator it = C.begin(); it != C.end();)
                    { cout << *it << endl; if (++it != C.end()) cout << ","; else cout << endl; }
                }
                for (set<Monomial>::iterator it_m = C.begin(); it_m != C.end(); ++it_m)
                {
                    MPolynomial<T> hm = h * (*it_m);
                    if (0 != iFlag)
                    {
                        cout << "=====" << endl;
                        cout << "hm = h * " << *it_m << endl; cout << " = " << hm << endl;
                        if (!hm.isZero())
                        {
                            cout << "S has " << S.size() << " elements" << endl;
                            for (It_P its = S.begin(); its != S.end(); ++its)
                                cout << *its << endl;
                        }
                    }
                    if (!hm.isZero())
                    {
                        string str_lt_hm = hm.LW().getTerm();
                        size_t len_lt_hm = str_lt_hm.length();
                        size_t i = 0; bool rto0 = false;
                        for (It_P it_ps = S.begin() ; it_ps != S.end(); )
                        {
                            string  str_lt_ps = it_ps->LW().getTerm();
                            size_t len_lt_ps = str_lt_ps.length();
                            if (len_lt_hm >= len_lt_ps && 0 == str_lt_hm.find(str_lt_ps))
                            {
                                Monomial w(hm.LW().getOMPtr(),str_lt_hm.substr(str_lt_ps.length(),string::npos));
                                MPolynomial<T> r = (hm * it_ps->LC()) - (*it_ps * w * hm.LC());
                                if (r.isZero())
                                {
                                    if (0 != iFlag)
                                    {
                                        cout << "hm is reduced to 0 in one step by S(" << i << ")*"
                                        << w << endl;
                                    }
                                    rto0 = true;
                                    break;
                                }
                            }
                            if (++it_ps == S.end()) { S.insert(hm/hm.LC()); H.insert(hm/hm.LC()); }
                            ++i;
                        }
                        if (0 != iFlag && !rto0) { cout << "hm cannot be reduced to 0 in one step w.r.t. S" << endl; }
                    } // end: if (!hm.isZero())
                } // end: for (set<Monomial>::iterator it_m = C.begin();
                C.clear();
            } // end: while (!H.empty())
            sat = S;
            S.clear();
            return;
        }
        
        //! =============================== PrefixGB ===============================
        /*! \brief compute a prefix Groebner basis and hence right Groebner basis
         * finitely generated ideal <F>.
         */
        template<typename T>
        bool PrefixGB(const set<MPolynomial<T> > & F, set<MPolynomial<T> > & GBr)
        {
            set<MPolynomial<T> > set_G;
            vector<MPolynomial<T> > vec_G; // for the sake of easily accessing the elements
            set<MPolynomial<T> > sat;
            typedef typename set<MPolynomial<T> >::iterator It_P;
            typename set<MPolynomial<T> >::const_iterator cit;
            for (cit = F.begin(); cit != F.end(); ++cit)
            {
                PrefixSaturate(*cit, sat);
                for (It_P it = sat.begin(); it != sat.end(); ++it) set_G.insert(*it);
                sat.clear();
            }
            // put the elements first in set and then vector in order to deal will repeating elements
            for (It_P it = set_G.begin(); it != set_G.end(); ++it) vec_G.push_back(*it);
            vector<pair<size_t, size_t> > B; // B = {(q1,q2) | q1,q2 in vec_G, and q1 <> q2}
            pair<size_t, size_t> b;
            size_t size_G = set_G.size();
            for (size_t i = 0; i != size_G; ++i)
                for ( size_t j = i+1; j != size_G; ++j )
                { b.first = i; b.second = j; B.push_back(b);}
            while (!B.empty())
            {
                b = B.back(); B.pop_back();
                MPolynomial<T> spol;
                // Spol(p1,p2) requires that p1 <> 0 and p2 <> 0 and p1 <> p2.
                if (PrefixSpol(vec_G.at(b.first),vec_G.at(b.second),spol) && !spol.isZero())
                {
                    PrefixNormalForm(spol,set_G,spol);
                    if (!spol.isZero())
                    {
                        PrefixSaturate(spol,sat);
                        // set_G = set_G + sat; B = B + {(f,h), (h,f) | f in set_G, g in Sat}
                        for (It_P it = sat.begin(); it != sat.end(); ++it)
                        {
                            if (set_G.end() == set_G.find(*it))
                            {
                                set_G.insert(*it); vec_G.push_back(*it);
                                for (size_t k = 0; k != size_G; ++k)
                                { b.first = k; b.second = size_G; B.push_back(b); }
                                ++size_G;
                            }
                        } // end: for (It_P it = sat.begin();
                        sat.clear();
                    } // end: if (!spol.isZero())
                } // end: if (PrefixSpol(vec_G.at(b.first),vec_G.at(b.second),spol)
            } // end: while (!B.empty())
            GBr = set_G;
            vec_G.clear();
            set_G.clear();
            return true;
        }
        
        //! =============================== PrefixReducedGBR ===============================
        /*! \brief compute a prefix reduced Groebner basis and hence right Groebner basis
         * finitely generated ideal <F>.
         */
        template<typename T>
        bool PrefixReducedGB(const set<MPolynomial<T> > & F, set<MPolynomial<T> > & redGBr)
        {
            /* step 1 : G0 = empty set */
            set<MPolynomial<T> > G0;
            /* step 2 : S = F */
            set<MPolynomial<T> > S = F;
            /* step 4 : while loop */
            while(!S.empty())
            {
                /* step 6 : q = remove(S) */
                MPolynomial<T> q = *(S.begin()); S.erase(S.begin()); // some strategy can be used here.
                /* step 7 : q = NF_{G0}(q) */
                PrefixNormalForm(q,G0,q);
                /* step 8 */
                if (!q.isZero())
                {
                    /* step 9 : H = \{ g in G0 | LT(g) is prefix reducible using q \}*/
                    string str_lt_q = q.LW().getTerm();
                    set<MPolynomial<T> > H;
                    typedef typename set<MPolynomial<T> >::iterator It_P;
                    for (It_P it = G0.begin(); it != G0.end(); ++it)
                    {
                        string str_lt_it = it->LW().getTerm();
                        if (0 == str_lt_it.find(str_lt_q)) H.insert(*it);
                    }
                    /* step 10 : G1 = PrefixInterreduce(G0-H\cup q) */
                    set<MPolynomial<T> > G1 = G0;
                    for (It_P it = H.begin(); it != H.end(); ++it)
                    {
                        It_P fi = G1.find(*it);
                        if (fi != G1.end()) G1.erase(fi);
                        /* step 11 : S = S\cup H.
                         * move this step here, because i want to use H
                         * as a temporary parameter later. */
                        S.insert(*it);
                    } // end: for (It_P it = H.begin(); i
                    H.clear();
                    G1.insert(q);
                    PrefixInterreduce(G1);
                    /* step 11 S = S\cup_{g\in G1-G0} Sat(g)-\{g\}*/
                    H = G1; /* H = G1 - G0 */
                    for (It_P it = G0.begin(); it != G0.end(); ++it)
                    {
                        It_P fi = H.find(*it);
                        if (fi != H.end()) H.erase(fi);
                    }
                    for (It_P it = H.begin(); it != H.end(); ++it)
                    {
                        set<MPolynomial<T> > sat;
                        PrefixSaturate(*it, sat);
                        It_P fi = sat.find(*it);
                        if (fi != sat.end()) sat.erase(fi);
                        for (It_P jt = sat.begin(); jt != sat.end(); ++jt) S.insert(*jt);
                        sat.clear();
                    }
                    G0 = G1;
                    G1.clear(); H.clear();
                } // end: if (!q.isZero())
            } // end: while(!S.empty())
            redGBr = G0;
            S.clear(); G0.clear();
            return true;
        }



  } // end of namespace NoncommutativeAlgorithms
} // end of namespace ApCoCoA



#endif /* ApCoCoA_PrefixGB_H */
