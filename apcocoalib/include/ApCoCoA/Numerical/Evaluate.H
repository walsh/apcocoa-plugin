//
// This file is part of the source of ApCoCoALib, the ApCoCoA Library.
//
//   Copyright (c) ApCoCoA Project (Prof. Dr. Martin Kreuzer, Uni Passau)
//
//   Author: 2006, 2007 Daniel Heldt
//
// Visit http://apcocoa.org/ for more information regarding ApCoCoA
// and ApCoCoALib.
// Visit http://www.apcocoa.org/wiki/ApCoCoA:KnownIssues for bugs, problems 
// and known issues.
//
// ApCoCoALib is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License (version 3 or later)
// as published by the Free Software Foundation. A copy of the full
// licence may be found in the file COPYING in this directory.
//
// ApCoCoALib is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ApCoCoALib; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 US

#ifndef ApCoCoA_Evaluate_H 
#define ApCoCoA_Evaluate_H 

#include <set>
#include <complex>

#include "ApCoCoA/Numerical/Internal/DoubleDenseMatrix.H"
#include "ApCoCoA/Numerical/Internal/ComplexDenseMatrix.H"
#include "CoCoA/DenseMatrix.H"

namespace ApCoCoA {

namespace NumericalAlgorithms{
  
  /**
  * \brief EvaluatedPolynomials
   *
   * EvaluatedPolynomials ... long explanation - unimplemented.
   * @param Polynomials
   * @param Points
   */
  
  CoCoA::matrix * EvaluatePolynomials(std::vector<CoCoA::ConstRefRingElem> Polynomials,
                                      CoCoA::matrix Points);

  /**
  * \brief EvaluatedPolynomials
   *
   * EvaluatedPolynomials ... long explanation - unimplemented.
   * @param Polynomials
   * @param Points
   */
  
  CoCoA::matrix * EvaluatePolynomials(std::vector<CoCoA::ConstRefRingElem> Polynomials,
                                      ABM::DoubleDenseMatrix Points);
  
  /**
  * \brief EvaluatedPolynomialsNumerical
   *
   * EvaluatedPolynomialsNumerical ... long explanation - unimplemented.
   * @param Polynomials
   * @param Points
   */
  
  ABM::DoubleDenseMatrix * EvaluatePolynomialsNumerical(std::vector<CoCoA::ConstRefRingElem> Polynomials,
                                                   CoCoA::matrix Points);
  /**
  * \brief EvaluatedPolynomialsNumerical
   *
   * EvaluatedPolynomialsNumerical ... long explanation - unimplemented.
   * @param Polynomials
   * @param Points
   */
  
  ABM::DoubleDenseMatrix * EvaluatePolynomialsNumerical(std::vector<CoCoA::ConstRefRingElem> Polynomials,
                                                   ABM::DoubleDenseMatrix Points);

  /**
  * \brief EvaluatedPolynomialsNumerical
   *
   * EvaluatedPolynomialsNumerical - This function evaluates a set of polynomials on a set of points. 
   * These evaluations happen on the set of floats, so the result is not algebraicaly correct, but is an
   * approximation. It may be useful to get a feeling for polynomials' behaviour on a set of points
   * or inside numerical routines. The result should NOT be used for further symbolical computations!
   *
   * Up to now the implementation is slow, and there is lot of space to improve this method. This will happen
   * later on. 
   * 
   * Change third paramter to vector of double*, so that we can evaluate a vector of polynomials
   * @param Polynomials  - a vector of CoCoA::RingElems, being polynomials over Q. They are being evaluated.
   * @param Points - the point set, the method will evaluate the polynomials on. The number of columns has to correspond
   * with the number of indeterminates, the number of rows equals the number of points (each row represents a point).
   * @param EvaluatedPolynomials - a list of valarrays containing the polynomials evaluations on the point-set. 
   */
  
  void EvaluatePolynomialsNumerical(std::vector<CoCoA::RingElem>* Polynomials,
                                    ABM::DoubleDenseMatrix* Points,
                                    double* EvaluatedPolynomials);
									
  void EvaluatePolynomialsNumerical(std::vector<CoCoA::RingElem>* Polynomials,
                                    ABM::ComplexDenseMatrix* Points,
                                    std::complex<double>* EvaluatedPolynomials);
									
  void EvaluatePolynomials(std::vector<CoCoA::RingElem>* Polynomials,
                           CoCoA::matrix* Points,
                           CoCoA::matrix* EvaluatedPolynomials);

} // end of namespace NumericalAlgorithms

} // end of namespace ApCoCoA

#endif 
