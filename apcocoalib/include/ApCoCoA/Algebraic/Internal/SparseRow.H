//
// This file is part of the source of ApCoCoALib, the ApCoCoA Library.
//
//   Copyright (c) ApCoCoA Project (Prof. Dr. Martin Kreuzer, Uni Passau)
//
//   Author: Mischa Lusteck
//           2008 Daniel Heldt
//
// Visit http://apcocoa.org/ for more information regarding ApCoCoA
// and ApCoCoALib.
// Visit http://www.apcocoa.org/wiki/ApCoCoA:KnownIssues for bugs, problems 
// and known issues.
//
// ApCoCoALib is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License (version 3 or later)
// as published by the Free Software Foundation. A copy of the full
// licence may be found in the file COPYING in this directory.
//
// ApCoCoALib is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ApCoCoALib; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 US

#ifndef  ApCoCoA_SparseRow_H
#define  ApCoCoA_SparseRow_H

#include "CoCoA/TmpGTypes.H" // for VectorList!
#include "CoCoA/ring.H"

#include "ApCoCoA/Algebraic/Internal/Mat.H"

namespace ApCoCoA{
namespace AlgebraicAlgorithms{
namespace LinSyz{

////////////////////////////////////////////////////////////////////////////////

inline int Vorz(std::size_t &r);

////////////////////////////////////////////////////////////////////////////////

class SparseRow
{
private:
//   CoCoA::RingElem** v;
	 std::vector<std::vector<CoCoA::RingElem > > v;
   std::size_t len,cols;
	 CoCoA::ring my_CoeffField;
	 
public:

   std::size_t posy,row;

   SparseRow(CoCoA::ring p,
	           std::size_t r0,
						 std::size_t len0,
						 std::vector<Mat> & m,
						 std::size_t *pos,
						 std::size_t orow,
						 std::size_t py);
			
	 SparseRow(CoCoA::ring r):my_CoeffField(r){ 
		// ooops..... *wasntme*
	 }					
	 
	SparseRow	operator=(SparseRow b);
											
   SparseRow(CoCoA::ring p,
	           std::size_t len0,
						 std::size_t cols0);
	 
   ~SparseRow(void);
	 
   void SetBlock(const std::size_t posx,
								  Mat &m,
								 const std::size_t orow,
								 const std::size_t posy); 
	
   void Add(SparseRow &u,CoCoA::RingElem a);
   void ColTrans(std::size_t pos,std::size_t j1,std::size_t j2,CoCoA::RingElem a);
   void Mul(CoCoA::RingElem a);
   CoCoA::RingElem Coef(std::size_t pos,std::size_t j){return v[pos][j];}
   void SetCoef(std::size_t pos,std::size_t j,CoCoA::RingElem a){v[pos][j]=a;}
   void MulCol(std::size_t pos,std::size_t j,CoCoA::RingElem a)
	 {
			if (v[pos].size() != 0 )  // warum hier parameter checken und in den anderen
//			if(v[pos]!=0)  // methoden nicht????
				v[pos][j]*=a;
	 } 
   bool NotZero(std::size_t pos){return (v[pos].size() !=0);}
   void Clear(std::size_t pos);
   void TestClear(std::size_t pos);
};
 
////////////////////////////////////////////////////////////////////////////////

} // namespace LinSyz
} // namespace AlgebraicAlgorithms
} // namespace ApCoCoA

#endif

 ////////////////////////////////////////////////////////////////////////////////














