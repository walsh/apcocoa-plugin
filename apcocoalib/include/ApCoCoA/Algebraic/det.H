//
// This file is part of the source of ApCoCoALib, the ApCoCoA Library.
//
//   Copyright (c) ApCoCoA Project (Prof. Dr. Martin Kreuzer, Uni Passau)
//
//   Author: 2007 Stefan Kaspar
//
// Visit http://apcocoa.org/ for more information regarding ApCoCoA
// and ApCoCoALib.
// Visit http://www.apcocoa.org/wiki/ApCoCoA:KnownIssues for bugs, problems 
// and known issues.
//
// ApCoCoALib is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License (version 3 or later)
// as published by the Free Software Foundation. A copy of the full
// licence may be found in the file COPYING in this directory.
//
// ApCoCoALib is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ApCoCoALib; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA


#ifndef ApCoCoA_det_H
#define ApCoCoA_det_H

#include "CoCoA/matrix.H"

namespace ApCoCoA
{
  namespace AlgebraicAlgorithms
  {
    namespace LinearAlgebraAlgorithms
    {
      // TODO: Replace "DET_LINBOX" with something more precise
      enum ComputationMethod { DET_GAUSS, DET_LINBOX };

      /*! \brief Compute determinant.
       *
       *  Compute the determinant of a square matrix <EM>M</EM> by using a
       *  specified method. Currently the methods <EM>DET_GAUSS</EM>
       *  (computation by using Gauss' algorithm) and <EM>DET_LINBOX</EM>
       *  (computation by using a LinBox determinant computation function)
       *  are available. The computed determinant will be stored in <EM>d</EM>.
       *
       *  @param d Will hold the computed determinant.
       *  @param M A square matrix.
       *  @param method Computation method. Default: DET_GAUSS.
       */
      void det(CoCoA::RingElem d,
               const CoCoA::ConstMatrixView& M,
               const ComputationMethod method = DET_GAUSS);
    } // end of namespace LinearAlgebraAlgorithms
  } // end of namespace AlgebraicAlgorithms
} // end of namespace ApCoCoA

#endif
