//
// This file is part of the source of ApCoCoALib, the ApCoCoA Library.
//
//   Copyright (c) ApCoCoA Project (Prof. Dr. Martin Kreuzer, Uni Passau)
//
//   Author: 2011 Ehsan Ullah
//
// Visit http://apcocoa.org/ for more information regarding ApCoCoA
// and ApCoCoALib.
// Visit http://www.apcocoa.org/wiki/ApCoCoA:KnownIssues for bugs, problems
// and known issues.
//
// ApCoCoALib is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License (version 3 or later)
// as published by the Free Software Foundation. A copy of the full
// licence may be found in the file COPYING in this directory.
//
// ApCoCoALib is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ApCoCoALib; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA


#ifndef ApCoCoA_LA_H
#define ApCoCoA_LA_H

#include "ApCoCoA/Algebraic/LESystemSolver.H"
#include "CoCoA/PPMonoid.H"
#include "CoCoA/ring.H"

// #include <vector> // Included by PPMonoid.H
// using std::vector;

namespace ApCoCoA
{
  namespace AlgebraicAlgorithms
  {
    /*-----------------------------------------------------------------*/
    /** \include fglm.txt  */
    /*-----------------------------------------------------------------*/

    /*! \brief Convert a Groebner basis into a Groebner basis w.r.t. NewOrdering.
     *
     *  Convert a Groebner basis G into a Groebner basis w.r.t. NewOrdering.<br>
     *  NOTE: Id(G) must be zero dimensional!
     *
     *  \param NewGB Will hold the new Groebner basis w.r.t. NewOrdering. Note:
     *               The elements of NewGB belong to a different polynomial ring
     *               than the elements of OldGB!
     *  \param OldGB Old Groebner basis.
     *  \param NewOrdering A PPOrdering representing the target term ordering.
     *  \param method Solving the occurring linear equation systems with this
     *                method. Please see documentation of LESystemSolver for
     *                allowed values. (default = <em>LSS_GAUSS</em>)
     */
    void LAAlgorithm( std::vector<CoCoA::RingElem>& FPoly );


  } // end of namespace AlgebraicAlgorithms
} // end of namespace ApCoCoA

#endif
