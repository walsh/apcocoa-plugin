Copyright (c) ApCoCoA Project (Prof. Dr. Martin Kreuzer, Uni Passau)

Author: 2010 Rashid Ali

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.2;
with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
A copy of the license is included in the file COPYING in this directory.



User documentation for files Weyl.C and Weyl.H
==========================================================================

In these files several functions are implemented for working with Weyl 
algebras. In particular algorithm for Weyl multiplication and Weyl normal 
remainder are implemented. These implementations are then used for reducing 
a list of Weyl Groebner basis computed by an implementation in Weyl code 
of CoCoALib.

Currently, there are following functions available for computations in
Weyl algebras:

  CoCoA::PolyList WeylMul(CoCoA::PolyList s);
  
  CoCoA::RingElem WMulByMonomial(CoCoA::RingElem m, 
				 CoCoA::RingElem f); // returns m*f
  
  CoCoA::PolyList WMul(CoCoA::PolyList& ResPL,CoCoA::RingElem& f,
			CoCoA::RingElem& g); //returns f*g

  CoCoA::RingElem WNR(CoCoA::RingElem f, CoCoA::PolyList& l);
  
  CoCoA::PolyList WRedGB(CoCoA::PolyList& rgb, CoCoA::PolyList GB);


Maintainer documentation for files Weyl.C and Weyl.H
=====================================================

[WMulByMonomial] Computes the left-Weyl product of a monomial m and a 
polynomial [f] of Weyl algebra. Here both m and f should be of type 
CoCoA::RingElem and the output will also be of type CoCoA::RingElem. 
Let An=K[x,y] be the nth Weyl algebra over the field K where 
x=x_1,\ldots,x_n and y=y_1,\ldots,y_n. The following formula is 
implemented for computing the product y^a*x^b: 
	
y^a*x^b =\sigma{(a(a-1)...(a-k+1)b(b-1)...(b-k+1).x^(b-k).y^(a-k))/factorial(k)}

	where k is running from 0 to min(a,b) when characteristic(K)=0

[WeylMul] Computes the product of Weyl polynomials [f] and [g] using above 
implementation [WMulByMonomial] for the left product of a monomial with a 
polynomial. The function have three parameters [ResPL], [f] and [g]. Both [f] 
and [g] are of type CoCoA::RingElem and the product f*g is stored in in the 
list [ResPL] which is of type CoCoA::PolyList. 

[WNR] is an implementation of Normal remainder algorithm for Weyl algebras. It 
computes the normal remainder of a Weyl polynomial [f] with respect to a list 
[l] of Weyl polynomials. The parameter [f] should be of type CoCoA::RingElem 
and the parameter [l] should be of type CoCoA::PolyList and each of its entry 
should be of type CoCoA::RingElem.

[WRedGB] has two parameters [rgb] and [GB] both of type CoCoA::PolyList. All 
the entries in the list [GB] should be of type CoCoA::RingElem. The function 
converts the list [GB] into the list [rgb] such that every polynomial in the 
list [rgb] is reduced with respect to the other polynomials and leading 
coefficient of each polynomial is 1. Thus, if [GB] is a Groebner basis of an 
ideal I than [rgb] will be its reduced Groebner basis.


Bugs, Shortcomings and other ideas
==================================

First straight forward implementation of all the functions. Some improvements 
might be achieved by using advanced programming techniques. 
Function for Weyl multiplication is much faster as compared to the one 
implemented in CoCoALib. I believe that computation in CoCoALib for Weyl GB 
can be made faster by using this algorithm for Weyl multiplication.


