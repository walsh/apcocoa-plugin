Copyright (c) ApCoCoA Project (Prof. Dr. Martin Kreuzer, Uni Passau)

Author: 2007, 2008 Stefan Kaspar

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.2;
with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
A copy of the license is included in the file COPYING in this directory.



User documentation for files RREF.C and RREF.H
==============================================

These files contain an interface to several different algorithms which
allow to compute a (reduced) row echelon form of a Matrix over a
field K. There are two ApCoCoALib naive internal implementations available
that work over every field. Additionally, if you have enabled LinBox library
or IML support, and if you are computing over a finite field, then you can
choose to try one of the (faster) LinBox or IML computation methods.

The interface to the different algorithms works as follows:
  [R]REF(R, M, [R]REFMethod)
    Compute a row echelon form (REF) or the reduced row echelon form (RREF)
    of the input matrix M and store the result in R. The parameter
    [R]REFMethod specifies which of the supported algorithms will be used
    for the computation.

The following values for [R]REFMethod are currently available:
  [R]REF_APCOCOA   Use an unoptimized Gaussian elemination method that
                   works over every field K.
  
  [R]REF_APCOCOA_SPARSE   Use a computation method optimized for sparse
                          input matrices. Also works over every field K.
                          Please not that currently there is no real
                          sparse matrix representation available in
                          (Ap)CoCoALib, i.e. you still have to call
                          the [R]REF method with a "dense" input matrix.

  [R]REF_LINBOX   Use a LinBox method to compute a (reduced)
                  row echelon form. Currently you can use this
                  method for computations over finite fields with
                  characteristic p < 2^30, p prime, only.

  [R]REF_IML   Use an IML method to compute a (reduced) row echelon form.
               Currently you can use this method for computations over finite
               fields with characteristic p < ULONG_MAX, p prime, only.


Maintainer documentation for files RREF.C and RREF.H
====================================================

For the integration of the LinBox library it was necessary to use
several distinct convert functions.

The naive Gaussian elimination implementation is straight forward.

The method optimized for sparse input matrices uses the following
internal representation of a matrix.

Internal structure of the sparse matrix representation
------------------------------------------------------
Column map: Key = Col index, Value = List of component list iterators
Row list: Value = List of component lists

Column map: Key --> [ (CompIter1), (CompIter2), ..., (CompIterN) ]
Row list: Iterator --> [ (CompList1), (CompList2), ..., (CompListM) ]
Component list: Iterator --> [ (Comp1), (Comp2), ..., (CompK) ]
Component: Row index, column index, element,
           ColumnListIterator, ColumnMapIterator, RowListIterator

Example
-------
Stored matrix:
[ [1, 0, 0],
  [2, 3, 4] ]


Column map:
[1, ColList1]  [2, ColList2]  [3, ColList3]
     |              |              |
(CompIter11)        |              |
     |              |              |
(CompIter12)   (CompIter21)   (CompIter31)

Row list:
Row1 - (Comp11)
Row2 - (Comp21) - (Comp22) - (Comp23)

Component iterators:
CompIter11->Comp11
CompIter12->Comp21
CompIter21->Comp22
CompIter31->Comp23

Components:
Comp11: 0, 0, 1, Iterator->CompIter11, Iterator->[1, ColList1], Iterator->Row1
Comp21: 1, 0, 2, Iterator->CompIter12, Iterator->[1, ColList1], Iterator->Row2
Comp22: 1, 1, 3, Iterator->CompIter22, Iterator->[2, ColList2], Iterator->Row2
Comp23: 1, 2, 4, Iterator->CompIter31, Iterator->[3, ColList3], Iterator->Row2

The computation of the (reduced) row echelon form is then carried
out by traversing the rows and columns of the matrix via the stored
iterators.


Bugs, Shortcomings and other ideas
==================================

The ApCoCoALib internal implementations should work over integral
domains as well. Therefore the restriction to input matrices over a
field could be removed.

Is the sparse version really faster for sparse matrices than the
naive version? Exhausting benchmarks should be created.

For avoiding the use of pointers, the implementation of the sparse
matrix representation makes excessive use of nested STL containers.
This makes the code a bit hard to read and leads to non-trivial
"dereferencing adventures".
