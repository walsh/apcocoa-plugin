//
// This file is part of the source of ApCoCoALib, the ApCoCoA Library.
//
//   Copyright (c) ApCoCoA Project (Prof. Dr. Martin Kreuzer, Uni Passau)
//
//   Author: 2008 Daniel Heldt, Jan Limbeck
//
// Visit http://apcocoa.org/ for more information regarding ApCoCoA
// and ApCoCoALib.
// Visit http://www.apcocoa.org/wiki/ApCoCoA:KnownIssues for bugs, problems 
// and known issues.
//
// ApCoCoALib is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License (version 3 or later)
// as published by the Free Software Foundation. A copy of the full
// licence may be found in the file COPYING in this directory.
//
// ApCoCoALib is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ApCoCoALib; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 US

#include "ApCoCoA/Library.H"
#include "CoCoA/library.H"

//----------------------------------------------------------------------
const std::string ShortDescription =
  "Example, computing an approximate vanishing sub-ideal using AVI.\n";
//----------------------------------------------------------------------


void program()
{
  CoCoA::GlobalManager CoCoAFoundations;

  CoCoA::GlobalOutput() << ShortDescription << std::endl;

  // in this version, AVI computes an 
  // approximate vanishing ideal INSIDE another, given ideal.
  // 
  // this is algebraically the same as first computing the
  // vanishing ideal and then intersecting it with the given ideal,
  // but in the numerical case this is 'slightly' more complicated.
  
  // first, we have to choose our points:
  
   double coordinates[] = {   1, -1, 0,     0,    0,  0,  0,  0.001, 
                              0,  0, 1, -0.99, -1.0,  0,  0, -0.001, 
                              0,  0, 0,     0,     0, 1, -1,  0.999
                           };
                                     
  // so we have 8 points in R^3.
  // they are all 'almost' on the unit sphere, but not exactly.
  // we try to 'find' now an approximate vanishing ideal, containing the 
  // unit circle (or almost the unit circle). 
               
  // fist we initialize a new container matrix with number of points, dimension and the points coordinates        
  ApCoCoA::NumericalAlgorithms::ABM::DoubleDenseMatrix Points(8, 3, coordinates);  

  CoCoA::GlobalOutput() << Points << std::endl;
  
  // here we set how normalization of the the points is done, and how Gauss Elimination is performed
  Points.setRREFOptions(ApCoCoA::NumericalAlgorithms::ABM::SGauss, 
                        ApCoCoA::NumericalAlgorithms::ABM::DoNothing, 
						false);   

  // second, we create a polynomial ring over Q, having 3 indeterminates: 
  CoCoA::SparsePolyRing P = NewPolyRing(CoCoA::RingQ(),CoCoA::symbols("x", "y", "z"));

  CoCoA::GlobalOutput() << P << std::endl << std::endl;

  // third, we need a Groebner basis of an ideal to intersect with. 
  // in this case, we simply take the x,y - plane
  CoCoA::PolyList GivenBasis;
  GivenBasis.push_back(CoCoA::indet(P,2));


  // now, we can start the computation:
  CoCoA::PolyList Basis, OrderIdeal; 
  // PolyList is a std::vector<CoCoA::RingElem> .
  // it is defined in CocoA/CoCoA4io.H and rather useful!
  // here we use it, to store our computed set of polynomials.
  
  // here we set the options for AVI, namely epsilon = 0.1
  ApCoCoA::NumericalAlgorithms::OptionsAVI options(0.1);
  ApCoCoA::NumericalAlgorithms::ComputeSubIdealOfPointsAVI(Points, P, Basis, 
                                                           GivenBasis, &OrderIdeal, options);
  
  CoCoA::GlobalOutput() << std::endl << std::endl;
  // print each computed polynomial:
  for (std::size_t i=0; i<Basis.size(); ++i)
  {
	CoCoA::GlobalOutput() << Basis[i] << std::endl <<std::endl;
  }

}
    
//----------------------------------------------------------------------
// Use main() to handle any uncaught exceptions and warn the user about them.

int main()
{
  try
  {
    program();
    return 0;
  }
  catch (const CoCoA::ErrorInfo& err)
  {
    CoCoA::GlobalErrput() << "***ERROR***  UNCAUGHT CoCoA error" << std::endl;
    CoCoA::ANNOUNCE(CoCoA::GlobalErrput(), err);
  }
  catch (const std::exception& exc)
  {
    CoCoA::GlobalErrput() << "***ERROR***  UNCAUGHT std::exception: " << exc.what() << std::endl;
  }
  catch(...)
  {
    CoCoA::GlobalErrput() << "***ERROR***  UNCAUGHT UNKNOWN EXCEPTION" << std::endl;
  }
  return 1;
}
