# ApCoCoA

This project contains software for the ApCoCoA Computer Algebra System. You can find a detailed description of ApCoCoA
in our [wiki](https://apcocoa.uni-passau.de). This repository consists of the following parts:

* The IntelliJ plugin provides CoCoA support for the development environment IntelliJ. For documentation on how to build
 and use the plugin please visit https://apcocoa.uni-passau.de/wiki/index.php?title=ApCoCoA:Developer_Documentation
* apcocoalib contains the ApCoCoA library and the server. It has not been completely migrated to the new CoCoA5 and
 the the current version of CoCoALib
* mediawiki_plugin contains an extension for Mediawiki which renders CoCoA XML-tags 

## How to Contribute

If you find a bug please let us know by creating an issue or sending an email to info@apcocoa.org.
If you like to contribute something to the IntelliJ plugin you can create a pull request or contact us at info@apcocoa.org
