<?xml version="1.0" encoding="ascii"?>

<!--TODO: -->
<!--I changed of name of first chapter from "version" to "preamble" in listing of chapter titles below.  What effect does this have?-->
<!--fix white space with strip command-->

<xsl:stylesheet version="1.0"
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:output method="text"/>
  <xsl:strip-space elements="*"/>	
  <xsl:preserve-space elements="quotes"/>

  <xsl:template match="help">
-- $Id: help.cpkg, v<xsl:value-of
select="version/man_version"/><xsl:text> </xsl:text><xsl:value-of select="version/date"/> cocoa Exp $
----------------------------------------------------------------------
---------------- STRUCTURE OF THE HELP PACKAGE -----------------------
----------------------------------------------------------------------
<xsl:apply-templates select="description">
</xsl:apply-templates>
---------------------CHANGES IN THE HELP PACKAGE ----------------------
<xsl:value-of select="/help/changes"/>
----------------------------------------------------------------------
------------------ BEGINNING OF HELP PACKAGE -------------------------
----------------------------------------------------------------------
Package $man/help

Alias H := $man/help;

Define About()
  PrintLn "
    Author  : David Perkinson
    Version : <xsl:value-of select="/help/version/man_version"/>
    Date    : <xsl:value-of select="/help/version/date"/>
";
EndDefine;

----------------------------------------------------------------------
---------------------- CREATE THE MANUAL -----------------------------
----------------------------------------------------------------------

Define Initialize()
  MEMORY.Doc := Record[];
  H.CreateDoc();
  MEMORY.Doc.Pointer := [&quot;manual&quot;,[1,1,0]];
  MEMORY.Doc.More := False;
  MEMORY.Doc.ChapterTitles := Diff([<xsl:for-each select="manual_parts/part">
  <xsl:variable name="part_num" select="position()"/>
  <xsl:for-each select="chapter"> 
    <xsl:variable name="chap_num" select="position()"/>
    [[<xsl:value-of select="$part_num"/>,<xsl:value-of select="$chap_num"/>,1],&quot;<xsl:value-of select="translate(title,'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz')"/>&quot;],</xsl:for-each>
</xsl:for-each>
    [] ], [[]] );
EndDefine;

Define CreateDoc()
    --&gt; create the list of CoCoA commands &lt;--
  MEMORY.Doc.Commands := H.Comms();

    --&gt; construct the main manual &lt;--
  MEMORY.Doc.Parts := NewList(<xsl:value-of select="count(/help/manual_parts/part)"/>,Record[]); -- there are <xsl:value-of select="count(/help/manual_parts/part)"/> parts
<xsl:for-each select="manual_parts/part">
  <xsl:variable name="part_num" select="position()"/>
  -- PART <xsl:value-of select="$part_num"/> --

  MEMORY.Doc.Parts[<xsl:value-of select="$part_num"/>].Name := &quot;<xsl:value-of select="title"/>&quot;;
  MEMORY.Doc.Parts[<xsl:value-of select="$part_num"/>].Chapters := [
  <xsl:for-each select="chapter"> 
    <xsl:variable name="chap_num"
  select="position()"/>H.Chap<xsl:value-of
  select="$part_num"/>_<xsl:value-of select="$chap_num"/>()<xsl:if
  test="not(position()=last())">,
  </xsl:if>
  </xsl:for-each>];
</xsl:for-each>
EndDefine;

----------------------------------------------------------------------
-------------------------- CoCoA COMMANDS ----------------------------
----------------------------------------------------------------------

Define Comms()
  C := [];  -- the list of CoCoA commands
  <xsl:for-each select="cocoa_commands/chapter_letter/command">
    <xsl:sort select="title"/>
----&gt; next command &lt;----
  R := Record[];
  R.Title := &quot;<xsl:apply-templates select="title"/>&quot;;
  R.ShortDescr := &quot;<xsl:apply-templates select="short_description"/>&quot;;
  R.See := [<xsl:for-each select="seealso/see">
      &quot;<xsl:apply-templates select="."/>&quot;<xsl:if test="not(position()=last())">, </xsl:if>
    </xsl:for-each>
    ];
  R.Types := [<xsl:for-each select="type|types/type">
      &quot;<xsl:apply-templates select="."/>&quot;<xsl:if test="not(position()=last())">, </xsl:if>
    </xsl:for-each>
    ];
  R.Keys := [<xsl:for-each select="key">
      &quot;<xsl:apply-templates select="."/>&quot;<xsl:if test="not(position()=last())">, </xsl:if>
    </xsl:for-each>
    ];
  Append(R.Keys, H.Decap(R.Title));
  Append(C,R);
  </xsl:for-each>
--====================--
  Return C;
EndDefine;  -- end of function for creating commands list
  
----------------------------------------------------------------------
----------------------- CHAPTERS OF THE MANUAL -----------------------
----------------------------------------------------------------------
<xsl:for-each select="manual_parts/part">
  <xsl:variable name="part_num" select="position()"/>
  <xsl:for-each select="chapter"> 
  <xsl:variable name="chap_num" select="position()"/>
--- HERE IS CHAPTER <xsl:value-of select="$part_num"/>_<xsl:value-of select="$chap_num"/>---
Define Chap<xsl:value-of select="$part_num"/>_<xsl:value-of select="$chap_num"/>()
  Chapter:= Record[Name := &quot;<xsl:value-of select="title"/>&quot;];
  L := [];
  R := Record[];

------------------------------------------------------------
    <xsl:for-each select="section">
  R.SectionType := <xsl:value-of select="type|types/type"/>;
  R.Title := &quot;<xsl:apply-templates select="title"/>&quot;;
  R.Keys := [<xsl:for-each select="key">
      &quot;<xsl:apply-templates select="."/>&quot;<xsl:if test="not(position()=last())">, </xsl:if>
    </xsl:for-each>
    ];
  Append(R.Keys, H.Decap(R.Title));
  R.See := [<xsl:for-each select="seealso/see">
      &quot;<xsl:apply-templates select="."/>&quot;<xsl:if test="not(position()=last())">, </xsl:if>
    </xsl:for-each>
    ];

  Append(L,R); 
------------------------------------------------------------
    </xsl:for-each>
  Chapter.Sections := L;
  Return Chapter;
EndDefine;
  </xsl:for-each>
</xsl:for-each>

-----------------------------------------------------------------------
------------------ THE ONLINE HELP FUNCTIONS --------------------------
-----------------------------------------------------------------------

Define Man(...)
  If Len(ARGV) = 0 Then
    Out := H.ManIntroStr();
  Elif Type(ARGV[1])&lt;&gt;STRING Then
    Out := &quot;Something is wrong.  Is the keyword placed in quotation marks?&quot;;
  Elif Len(ARGV) = 1 Or (Len(ARGV) = 2 And ARGV[2] = 1) Then
    Out := H.KeyWord(ARGV[1],1);  -- search for help related to keyword
  Elif Len(ARGV) = 2 And ARGV[2] = 0 Then
    Out := H.KeyWord(ARGV[1],0);  -- list matching titles only
  Else
    Out :=
&quot;Something is wrong.
 
 USAGE:
   Man()             introductory message;
   Man(\&quot;KeyWord\&quot;)    help on the topic \&quot;KeyWord\&quot;;
   Man(\&quot;KeyWord\&quot;,0)  list topics containing \&quot;KeyWord\&quot; as a substring.&quot;;
  EndIf;
  If MEMORY.Doc.More Then More(Out); Else Print Out; EndIf;
EndDefine;

-- print introductory info about online help
Define ManIntroStr()
  Return
&quot;           &gt;&gt;&gt; Welcome to CoCoA online help. &lt;&lt;&lt; 

Please send comments to: cocoa@dima.unige.it.

  ?tutorial              CoCoA tutorial;

  ?                      this message;
  ?keyword               help on the topic \&quot;keyword\&quot;;
  ??keyword              entries in the manual matching part of \&quot;keyword\&quot;;

  About()                generalities about CoCoA;
  RelNotes()             notes about this particular release of CoCoA;

  H.Commands()           help with the \&quot;Commands\&quot; command;
  H.Commands(\&quot;Topic\&quot;)    summarize all commands associated with \&quot;Topic\&quot;;
  H.Syntax(\&quot;Keyword\&quot;)    syntax of command matching \&quot;Keyword\&quot;;
  H.Tips()               quick tips for using the online help system;
  H.Toc()                table of contents of the online manual;

  H.OutManual(\&quot;Manual\&quot;)  print manual to the text file named \&quot;Manual\&quot;;
  H.OutCommands(\&quot;Comm\&quot;)  print command documentation to the file, \&quot;Comm\&quot;;

  H.SetMore(N)           scroll results from online help, N lines at a time
  H.UnSetMore()          return to normal output from online help

  Quit                   quit CoCoA.

To start learning about CoCoA, enter the command \&quot;?tutorial\&quot; (without
the quotes).  For more help with online help, try \&quot;?online help\&quot;.&quot;;
EndDefine;

-- find and print all info pertaining to a keyword
-- if the match is not unique, print the choices

Define KeyWord(Key,Exact) 
  -- first collect list of hits (matches) in the manual itself:
  -- a hit can occur in a section of the manual, among the commands, or
  -- among the chapter titles commands.  We will represent a hit as a
  -- list with 2 elements: the first will be the section or command, the
  -- second will be &quot;manual&quot;  for a section, &quot;commands&quot; for a command,
  -- or &quot;chapter&quot;.
  Hits := [];
  Key := H.Decap(Key); -- case insensitive

  -- If Exact = 0, only look for keys containing Key as a 
  -- substring; don&quot;t worry about exact matches.
  -- If Exact = 1 and there is an exact match between Key
  -- and one of the keys, print information about that match.
  -- The latter mode would be the one usually used.
  If Exact = 0 Then Exact := FALSE Else Exact := TRUE EndIf;

  NL :=&quot;
&quot;;        -- newline character

  -- search manual sections
  For P := 1 To Len(MEMORY.Doc.Parts) Do 
    For C := 1 To Len(MEMORY.Doc.Parts[P].Chapters) Do
      For S := 1 To Len(MEMORY.Doc.Parts[P].Chapters[C].Sections) Do
        Sect := MEMORY.Doc.Parts[P].Chapters[C].Sections[S]; 
        PotentialFlag := TRUE;
        Foreach K In Sect.Keys Do
          If PotentialFlag And Key IsIn K Then
            If Key = H.Decap(Sect.Title) And Exact Then
              MEMORY.Doc.Pointer := [&quot;manual&quot;,[P,C,S]];
              Return H.SectionStr([P,C,S]);
            EndIf;
            PotentialRef := [&quot;manual&quot;,[P,C,S]]; -- ref. if only 1 hit
            Append(Hits,[&quot;manual&quot;,[P,C,S]]);
            PotentialFlag := FALSE;
          EndIf;
        EndForeach;  -- each key
      EndFor; -- each section
    EndFor; -- each chapter
  EndFor; -- each part

  -- search the commands
  For C:= 1 To Len(MEMORY.Doc.Commands) Do
    Keys := MEMORY.Doc.Commands[C].Keys;
    PotentialFlag := TRUE;  
    Foreach K In Keys Do
      If PotentialFlag And Key IsIn K Then
        If Key = H.Decap(MEMORY.Doc.Commands[C].Title) And Exact Then
          MEMORY.Doc.Pointer := [&quot;commands&quot;,C];
          Return H.CommandStr(C);
        EndIf;
        PotentialRef := [&quot;commands&quot;,C]; -- ref. if only 1 hit
        Append(Hits,[&quot;commands&quot;,C]); 
        PotentialFlag := FALSE;
      EndIf;
    EndForeach; -- each key
  EndFor; -- each command
  
  -- search chapter titles
  Foreach T In MEMORY.Doc.ChapterTitles Do
    If Key IsIn T[2] Then
      If Key = T[2] And Exact Then
        P := T[1,1]; C := T[1,2];
        MEMORY.Doc.Pointer := [&quot;manual&quot;,[P,C,1]];
        Title := MEMORY.Doc.Parts[P].Chapters[C].Name;
        Return &quot;Chapter &quot;+Sprint(P)+&quot;.&quot;+Sprint(C)+&quot;. &quot;+
               Title+NL+H.SectionStr([P,C,1]);
      EndIf;
      PotentialRef := [&quot;manual&quot;,T[1]]; -- ref. if only 1 hit
      Append(Hits,[&quot;chapter&quot;,T[1]]);
    EndIf;  
  EndForeach;

  -- report the results in the case of a non-exact match
  If Hits = [] Then
    Return NL+&quot;No matches.&quot;;    
  Elif Len(Hits) = 1 And Exact Then
    MEMORY.Doc.Pointer := PotentialRef;
    If Hits[1][1] = &quot;manual&quot; Then
      Return H.SectionStr(Hits[1][2]);
    Elif Hits[1][1] = &quot;chapter&quot; Then
      P := Hits[1][2][1]; 
      C := Hits[1][2][2];
      Return &quot;Chapter &quot;+Sprint(P)+&quot;.&quot;+Sprint(C)+&quot;. &quot;+
             MEMORY.Doc.Parts[P].Chapters[C].Name+NL+
             H.SectionStr(Hits[1][2]);
    Else
      Return H.CommandStr(Hits[1][2]);
    EndIf;
  Else
    -- collect the names of the hits 
    HNames := [];
    Foreach N In Hits Do
        M := N[2]; -- the reference
      If N[1] = &quot;manual&quot; Then
        Title := MEMORY.Doc.Parts[M[1]].Chapters[M[2]].Sections[M[3]].Title;
      Elif N[1] = &quot;chapter&quot; Then
        Title := MEMORY.Doc.Parts[M[1]].Chapters[M[2]].Name;   
      Else
        Title := MEMORY.Doc.Commands[M].Title;        
      EndIf;
      Append(HNames,Title);
    EndForeach;  -- each hit
    HNames := Sorted(MakeSet(HNames)); -- print names in alphabetical order
    Out := NL+&quot;See:&quot;;    
    Foreach N In HNames Do
      Out := Out+NL+&quot; ? &quot;+N;
    EndForeach;
    Return Out;
  EndIf;
EndDefine;  -- KeyWord

-- print a section of the manual
Define SectionStr(Ref)
  Filename := CocoaPackagePath()+&quot;/man/p&quot;+Sprint(Ref[1])+&quot;c&quot;+Sprint(Ref[2])+
               &quot;s&quot;+Sprint(Ref[3]);
  Source(Filename);
  Sect := MEMORY.Doc.Parts[Ref[1]].Chapters[Ref[2]].Sections[Ref[3]];
  NL :=&quot;
&quot;;      -- newline character
  Out := NL+&quot;==============[ &quot;+Sect.Title+&quot; ]===============&quot;+NL+NL+
         MEMORY.Doc.Descr + NL;
  H.SeeAlso(Sect.See, Out);

  -- print preceding topic and next topic
  Out := Out+NL;
  N := H.SectToNum(MEMORY.Doc.Pointer[2]);
  If N &gt; 1 Then
    Ref := H.NumToSect(N-1);
    Out := Out+NL+&quot;Preceding topic: &quot;+
           MEMORY.Doc.Parts[Ref[1]].Chapters[Ref[2]].Sections[Ref[3]].Title;
  EndIf;
  Ref := H.NumToSect(N+1);  -- NumToSect will not go past end of manual
  If Ref &lt;&gt; MEMORY.Doc.Pointer[2]  Then  -- if not at end of manual
    Out := Out+NL+&quot;Next topic: &quot;+
           MEMORY.Doc.Parts[Ref[1]].Chapters[Ref[2]].Sections[Ref[3]].Title;
  EndIf;
  Return Out;
EndDefine; -- SectionStr

Define Tutorial()
  H.Man(&quot;A Tutorial Introduction to CoCoA&quot;);
EndDefine;

-- print a list summarizing the commands pertaining to a keyword/type
Define Commands(...)
  If Len(ARGV) = 0 Then
    Out := H.CommandsIntroStr();
  Elif Len(ARGV) = 1 Then
    Out := H.ListCommands(ARGV[1]);
  Else  -- for use only with the recursive call in CommandsIntroStr
    Return H.ListCommands(ARGV[1]);  
  EndIf;
  If MEMORY.Doc.More Then More(Out) Else Out EndIf;
EndDefine;

-- intro to commands function
Define CommandsIntroStr()
  NL :=&quot;
&quot;;   -- newline character
  Out :=NL+
&quot;The function \&quot;Commands\&quot; prints a list of functions associated with a
keyword.  The keywords are: 

&quot;;
  Count := 0;
  Out := Out+&quot;  &quot;; 
  Foreach K In Sorted(H.ListCommandTypes(&quot; &quot;)) Do
    Count := Count+1;
    If Mod(Count,5) = 0 Then Out:=Out+NL+&quot;  &quot;; EndIf;
    Out:=Out+K+&quot;  &quot;;
  EndForeach;
  Out := Out+NL+
&quot;
If \&quot;Topic\&quot; exactly matches a keyword, the functions associated with
that keyword will be summarized.  Otherwise, \&quot;Commands\&quot; will
summarize the commands whose keywords contain \&quot;Topics\&quot; as a substring.
Thus, to save on typing, one need only type a part of a keyword.  Also
note that \&quot;Commands()\&quot; will produce a list of *all* CoCoA
commands in the online help system. (This will still not be an
exhaustive list of CoCoA commands.  For instance, the assignment
command is not associated with a keyword in the online manual.)  Also,
note that the search for commands is case insensitive: you do not need
to worry about the capitalization of the keyword.

&gt;EXAMPLE&lt;
Commands(\&quot;bo\&quot;);  -- list of commands associated with booleans
&quot;+ H.Commands(&quot;&quot;bo&quot;&quot;,1);  -- recursive call
  Return Out;
EndDefine; -- of CommandsIntroStr 

-- print all info having to do with a given command
Define CommandStr(C)
  NL :=&quot;
&quot;; 
  Com := MEMORY.Doc.Commands[C];
  Filename := CocoaPackagePath()+&quot;/man/cmd&quot;+Sprint(C);
  Source(Filename);
  Out := NL+&quot;==============[ &quot;+Com.Title+&quot; ]===============&quot;+NL+
         &quot;SUMMARY: &quot;+Com.ShortDescr+&quot; &quot;+NL+NL+
         &quot;== SYNTAX ==&quot;+MEMORY.Doc.Syntax+NL+
         &quot;== DESCRIPTION ==&quot;+MEMORY.Doc.Descr+NL; 
  H.SeeAlso(Com.See, Out);
  Out := Out+NL;
  If MEMORY.Doc.Pointer[2] &gt; 1 Then
    Out := Out+&quot;Preceding topic: &quot;+
           MEMORY.Doc.Commands[MEMORY.Doc.Pointer[2]-1].Title+NL;
  EndIf;
  If MEMORY.Doc.Pointer[2] &lt; Len(MEMORY.Doc.Commands) Then
    Out := Out+&quot;Next topic: &quot;+
           MEMORY.Doc.Commands[MEMORY.Doc.Pointer[2]+1].Title;
  EndIf;
  Return Out;
EndDefine; -- of CommandStr

-- Make a list of Types of commands that match a given search
-- string.  These are the strings that can be passed to the function
-- &quot;Commands&quot;, sometimes refered to as &quot;keywords&quot;.  They are *not*
-- stored in the &quot;Keys&quot; field, but in the &quot;Types&quot; field.   
-- Case insentsitive.
Define ListCommandTypes(S)
  Hits := [];
  S := H.Decap(S);  -- case insensitive

  -- compile list of matches --
  Foreach M In MEMORY.Doc.Commands Do
    Foreach N In M.Types Do
      If S = N Then Return [S];
      Elif S IsIn N Then Append(Hits,N);
      EndIf;
    EndForeach;
  EndForeach;
  Return MakeSet(Hits);
EndDefine; -- ListCommandTypes

-- return string listing commands that have to do with the string S
Define ListCommands(S)
  Hits := H.ListCommandTypes(S);
  Hits := [X | X In Hits];
  NL :=   
&quot;
&quot;;  -- newline character
  Out := NL;        -- the list of commands so far
  If Hits = [] Then Out := Out+&quot;No matches&quot;+NL;
  Else
    Foreach M In MEMORY.Doc.Commands Do
      -- if any of the hits is a keyword for M ... --
      DT := [X | X In M.Types];                    -- Some of this could
      If (Len(Diff(DT,Hits)) &lt;&gt; Len(DT)) Then      -- be saved in a global
                                                   -- variable.
        Out := Out + &quot;  * &quot; + M.Title + &quot; -- &quot; + 
                 M.ShortDescr + NL; 
      EndIf;
    EndForeach;
  EndIf;
  Return Out;
EndDefine; -- ListCommands

-- Search for a command matching the string S.  For a unique match,
-- print the short description and the syntax, moving
-- MEMORY.Doc.Pointer so that H.Browser() will display more
-- information.  If match is not unique, print a summary.
Define Syntax(...)
  If Len(ARGV) = 0 Then
    H.Man(&quot;H.Syntax&quot;);
    Return;
    Elif Type(ARGV[1])&lt;&gt;STRING Then
    PrintLn(&quot;Something is wrong.  Is the keyword placed in quotation marks?&quot;);
    Return;
  EndIf;
  Key := H.Decap(ARGV[1]);
  Hits := [];

  -- search the commands
  ExactHit := False;
  For C := 1 To Len(MEMORY.Doc.Commands) Do
    Com := MEMORY.Doc.Commands[C];
    Foreach K In Com.Keys Do
      If Key = K Then
        PotentialRef := [&quot;commands&quot;,C-1];
        Hits := [C];
        ExactHit := True;
        Break;  -- out of key-loop
      Else
        If Key IsIn K Then 
          PotentialRef := [&quot;commands&quot;,C-1]; -- ref. if only 1 hit
          Append(Hits,C);
          Break;  -- go to next command
	EndIf;
      EndIf;
    EndForeach; -- key
    If ExactHit Then Break EndIf;  -- go to report results section
  EndFor; -- command

  
  -- report results
  NL :=&quot;
&quot;;  -- newline character
  If Hits = [] Then
    Out := NL+&quot;No matches.&quot;+NL;
  Elif Len(Hits) = 1 Then
    MEMORY.Doc.Pointer := PotentialRef;
    Out := H.SyntaxStr(Hits[1]);
  Else
    -- print the names and short descriptions 
    Out := NL;
    Foreach N In Hits Do
      Out := Out+&quot;  * &quot;+MEMORY.Doc.Commands[N].Title+&quot; -- &quot;+
             MEMORY.Doc.Commands[N].ShortDescr+NL;
    EndForeach;
  EndIf;
  If MEMORY.Doc.More Then More(Out) Else Out EndIf;
EndDefine; -- Syntax

-- print the name, syntax, and short description of a command
Define SyntaxStr(C)
  Filename := CocoaPackagePath()+&quot;/man/cmd&quot; + Sprint(C);
  Source(Filename);
  NL :=&quot;
&quot;;  -- newline
  Return NL+&quot;============ &quot;+MEMORY.Doc.Commands[C].Title+
         &quot;=============&quot;+NL+&quot;SYNTAX&quot;+NL+MEMORY.Doc.Syntax+NL+ 
         NL+&quot;SUMMARY: &quot;+MEMORY.Doc.Commands[C].ShortDescr+NL+NL+
         &quot;--&gt; \&quot;H.Browse();\&quot; for more information. &lt;--&quot;+NL;
EndDefine; -- SyntaxStr


-- print quick tips for using online help 
Define Tips()
  H.Man(&quot;Quick Tips for Using Online Help&quot;);
EndDefine;

---------- BROWSING FUNCTIONS --------------

-- browse the manual or the commands
Define Browse(...)
  If Len(ARGV) = 0 Then N := 1
  Elif Type(ARGV[1])&lt;&gt;INT Then
    PrintLn(&quot;Something is wrong.  Is the argument an integer?&quot;);
    Return;
  Else N := ARGV[1] EndIf; -- how far to jump
  If MEMORY.Doc.Pointer[1] = &quot;manual&quot; Then
    M := H.SectToNum(MEMORY.Doc.Pointer[2]); -- current section number
    M := M + N; -- section to display next
    Ref := H.NumToSect(M);
    MEMORY.Doc.Pointer := [&quot;manual&quot;,Ref];
    Out := H.SectionStr(Ref);
  Else -- otherwise, print the next command
    M := MEMORY.Doc.Pointer[2]; -- current command number
    M := M + N;
    -- check we are within bounds, this was done by NumToSect for
    -- the manual browsing above
    If M &lt; 1 Then 
      M := 1  -- first command
    Elif M &gt; Len(MEMORY.Doc.Commands) Then 
      M := Len(MEMORY.Doc.Commands)  -- last command
    EndIf;
    MEMORY.Doc.Pointer := [&quot;commands&quot;,M];
    Out := H.CommandStr(M);
  EndIf;
  If MEMORY.Doc.More Then More(Out) Else Out EndIf;
EndDefine; -- Browse


--------------------------------------------------------
-- Print the table of contents of the online manual ----
--------------------------------------------------------
Define Toc(...)
  NL :=&quot;
&quot;;  -- newline character
  If Len(ARGV) = 0 Then  -- print just part and chapter titles
    Out := H.Chapters();
  Elif H.Decap(ARGV[1]) = &quot;all&quot; Then
    Out:=&quot; &quot;;
    For N := 1 To Len(MEMORY.Doc.Parts) Do   -- print whole toc
      Out := Out+H.TocPart(N);  -- print the toc for part number N
    EndFor; -- part
  Elif Len(ARGV) = 1 And Type(ARGV[1]) = INT Then 
        -- print toc for a single part
    Out := H.TocPart(ARGV[1]);
  Elif Len(ARGV) = 2 And Type(ARGV[1]) = INT And Type(ARGV[2]) = INT Then
                           -- print toc for a single chapter 
    Out := NL+&quot;Part &quot;+Sprint(ARGV[1])+&quot;. &quot;+
           MEMORY.Doc.Parts[ARGV[1]].Name+NL+
           H.TocChapter(ARGV[1],ARGV[2]);
  Else
    Out :=
    &quot;ERROR: bad arguments to \&quot;H.Toc\&quot;.  For help, type \&quot;Man(\&quot;H.Toc\&quot;)\&quot;.&quot;;
  EndIf;
  If MEMORY.Doc.More Then More(Out) Else Out EndIf;
EndDefine; -- Toc

-- print the table of contents of Part N of the manual
Define TocPart(N)
  NL :=&quot;
&quot;;  -- newline character
  P:=MEMORY.Doc.Parts[N];     
  Out :=&quot;Part &quot;+Sprint(N)+&quot;. &quot;+P.Name+NL;
  For M:=1 To Len(P.Chapters) Do             -- list chapter names
    Out := Out + H.TocChapter(N,M);  -- list sections
  EndFor;
  Return Out;
EndDefine; -- TocPart
  

-- print the table of contents of a single chapter
Define TocChapter(N,M)
  NL :=&quot;
&quot;;  -- newline character
  C := MEMORY.Doc.Parts[N].Chapters[M];
  Out :=&quot;  &quot;+Sprint(M)+&quot;. &quot;+C.Name+NL;
  For I := 1 To Len(C.Sections) Do
    Sect := C.Sections[I];
    For J:= 1 To Sect.SectionType Do  
      Out := Out+&quot;  &quot;;
    EndFor;
    If MEMORY.Doc.Pointer[1]=&quot;manual&quot; 
       And [N,M,I] = MEMORY.Doc.Pointer[2] Then
      Out := Out+&quot;--&gt; &quot;+Sect.Title+NL;
    Else 
      Out := Out+&quot;    &quot;+Sect.Title+NL;
    EndIf;
  EndFor;
  Return Out;
EndDefine; -- TocChapter

-- print the chapters of the online manual
Define Chapters()
  NL :=&quot;
&quot;;  -- newline character
  Out := NL+&quot;    COCOA ONLINE MANUAL&quot;+NL;
  For I := 1 To Len(MEMORY.Doc.Parts) Do
    Part := MEMORY.Doc.Parts[I];
    Out := Out+&quot;Part &quot;+Sprint(I)+&quot;. &quot;+Part.Name+NL;
    For J := 1 To Len(Part.Chapters) Do
      Out := Out+&quot;  &quot;+Sprint(I)+&quot;.&quot;+Sprint(J)+&quot;. &quot;+
             Part.Chapters[J].Name+NL;
    EndFor;
  EndFor;
  Out := Out+NL+
&quot;Tips for table of contents (toc): H.Toc(P): toc for part P;
H.Toc(P,C): toc for part P, chapter C; H.Toc(\&quot;all\&quot;): complete toc.&quot;+NL;
  Return Out;
EndDefine; -- Chapters

---------------------------------------------
----------- Printing Functions --------------
---------------------------------------------

-- The following are simple functions for printing parts of the 
-- manual or collections of commands to text files.

-- print a section of the manual to a file
Define OutSection(D,P,C,S)
  Filename := CocoaPackagePath()+&quot;/man/p&quot;+Sprint(P)+&quot;c&quot;+Sprint(C)+&quot;s&quot;+Sprint(S);
  Source(Filename);
  Sect := MEMORY.Doc.Parts[P].Chapters[C].Sections[S];
  Newline :=
&quot;
&quot;;
  Out := Newline + &quot;--------------- &quot; + Sprint(P) + &quot;.&quot; +
         + Sprint(C) + &quot;.&quot; + Sprint(S) + &quot;. &quot; + 
         Sect.Title + &quot; ---------------&quot; +
         Newline + Newline + MEMORY.Doc.Descr + Newline;
  H.SeeAlso(Sect.See, Out);
  Out := Out + Newline;
  Print Out On D;
EndDefine; -- OutSection

-- print a chapter of the manual to a file
Define OutChapter(D,P,C) 
  Chap := MEMORY.Doc.Parts[P].Chapters[C];
  Newline :=
&quot;
&quot;;
  Header :=    Newline + &quot;\\\\\ &quot; + Sprint(P) + &quot;.&quot; + Sprint(C) +
               &quot;. &quot; + Chap.Name + &quot; /////&quot; + Newline;
  Print Header On D;
  For I := 1 To Len(Chap.Sections) Do
    H.OutSection(D,P,C,I);
  EndFor;
EndDefine; -- OutChapter

-- print a part of the manual to a file
Define OutPart(D,P)
  Newline :=
&quot;
&quot;;
  Out := &quot;====================================&quot; + Newline +
         &quot;Part &quot;+ Sprint(P) + &quot;. &quot; + MEMORY.Doc.Parts[P].Name
         + Newline + &quot;====================================&quot; + 
          Newline + Newline;
  Print Out On D;
  For I := 1 To Len(MEMORY.Doc.Parts[P].Chapters) Do
    H.OutChapter(D,P,I);
  EndFor;
EndDefine; -- OutPart

-- print the manual to a file
Define OutManual(...)
  If (Len(ARGV) = 0) Or (Len(ARGV) &gt; 4) Then 
    Return &quot;ERROR: Bad Arguments&quot; 
  EndIf;
  If Type(ARGV[1])&lt;&gt; STRING Then
    Return &quot;ERROR: Bad Arguments&quot; 
  EndIf;

  D := OpenOFile(ARGV[1]);
  If Len(ARGV) = 1 Then
    For I := 1 To Len(MEMORY.Doc.Parts) Do
      H.OutPart(D,I);
    EndFor;
  Elif Len(ARGV) = 2 Then
    H.OutPart(D,ARGV[2]);
  Elif Len(ARGV) = 3 Then
    H.OutChapter(D,ARGV[2],ARGV[3]);
  Elif Len(ARGV) = 4 Then
    H.OutSection(D,ARGV[2],ARGV[3],ARGV[4]);
  Else
    Return &quot;ERROR: Bad Arguments&quot;;
  EndIf;
  Close(D);
EndDefine; -- OutManual

-- print the commands to a file
Define OutCommands(...)
  If (Len(ARGV) = 0) Or (Len(ARGV) &gt; 4) Then 
    Return &quot;ERROR: Bad Arguments&quot; 
  EndIf;
  If Type(ARGV[1])&lt;&gt; STRING Then
    Return &quot;ERROR: Bad Arguments&quot; 
  EndIf;

  D := OpenOFile(ARGV[1]);
  If Len(ARGV) = 1 Then
    For I := 1 To Len(MEMORY.Doc.Commands) Do
      H.OutCommand(D,I);
    EndFor;
  Elif Len(ARGV) = 2 Then
    H.OutCommand(D,ARGV[2]);
  Elif Len(ARGV) = 3 Then
    For I := ARGV[2] To ARGV[3] Do
      H.OutCommand(D,I);
    EndFor;
  EndIf;
  Close(D);
EndDefine; -- OutCommands

-- print a single command to a file
Define OutCommand(D,I)
  Filename := CocoaPackagePath()+&quot;/man/cmd&quot; + Sprint(I);
  Source(Filename);
  C := MEMORY.Doc.Commands[I];
  Newline :=
&quot;
&quot;;
  Out := Newline + C.Title + Newline + Newline
         + &quot;SYNTAX&quot; + Newline + MEMORY.Doc.Syntax + Newline + Newline
         + &quot;SUMMARY&quot; + Newline + C.ShortDescr + Newline + Newline 
         + &quot;DESCRIPTION&quot; + Newline + MEMORY.Doc.Descr + Newline
         + Newline;
  H.SeeAlso(C.See, Out);
  Out := Out + &quot;===================================&quot; + Newline;
  Print Out On D;
EndDefine; -- OutCommand


Define SeeAlso(L, Var Out)
  If L &lt;&gt; [] Then
    Newline := &quot;
&quot;;
    Out := Out + Newline + &quot;== SEE ALSO ==&quot; + Newline;
    Foreach S In L Do
      Out := Out + &quot; ? &quot; + S + Newline;
    EndForeach;
  EndIf;
EndDefine; -- SeeAlso

--------------------------------------------
--------------------------------------------
----------- Functions Related to -----------
-----------    the More Device   -----------
--------------------------------------------

Define SetMore(...)
  MEMORY.Doc.More := True;
  If Len(ARGV)&gt;0 And Type(ARGV[1])=INT Then
    MEMORY.MoreCount := ARGV[1];
  EndIf;
EndDefine; -- SetMore


Define UnSetMore()
  MEMORY.Doc.More := False;
EndDefine; -- UnSetMore
 
--------------------------------------------
----------- Utility Functions --------------
--------------------------------------------
-- convert a number to a manual section reference: 
-- [Part #, Chapter #, Section #]
Define NumToSect(N)
  If N &lt; 1 Then Return [1,1,1] -- beginning of manual
  Else
    Ref := [0,0,0];
    M := 0;  -- counter
    Foreach P In MEMORY.Doc.Parts Do
      Ref[1] := Ref[1]+1;
      Ref[2] := 0;
      Foreach C In P.Chapters Do
        Ref[2] := Ref[2]+1;
        Ref[3] := 0;
        Foreach S In C.Sections Do
          Ref[3] := Ref[3]+1;
          M := M+1;
          If M = N Then Return Ref EndIf;
	EndForeach; -- section
      EndForeach; -- chapter
    EndForeach; -- part
  EndIf;
  Return Ref;  -- if N refers to a number past the end of the manual
               -- return the last section of the manual
EndDefine; -- NumToSect


-- convert a manual section reference, [Part #, Chapter #, Section #] 
-- into a number
-- Assumes input is a valid reference.
Define SectToNum(Ref)
  Num := 0;
  For P := 1 To Ref[1] Do  -- each part
    For C := 1 To Len(MEMORY.Doc.Parts[P].Chapters) Do  -- each chapter
      If (P &lt; Ref[1]) 
         Or (C &lt; Ref[2]) Then
      Num := Num +
         Len(MEMORY.Doc.Parts[P].Chapters[C].Sections);
      EndIf;
    EndFor; -- chapter 
  EndFor; -- part
  Num := Num+Ref[3];
  Return Num;
EndDefine; -- SectToNum


  -- Decap a string, also remove leading/trailing spaces.
Define Decap(S)
  If Type(S)&lt;&gt;STRING Then Return S EndIf;
  L := Ascii(S); -- list of ascii numbers
  A := Comp(Ascii(&quot;A&quot;),1);
  AA := Comp(Ascii(&quot;a&quot;),1);
  Z := Comp(Ascii(&quot;Z&quot;),1);
  For C := 1 To Len(L) Do
    If (L[C] &gt;= A) And (L[C] &lt;= Z) Then
      L[C] := L[C] + (AA - A);
    EndIf;
  EndFor;
  Return H.HeadAndTail(Ascii(L));
EndDefine; -- Decap


-- remove leading And trailing spaces from a string
Define HeadAndTail(S)
  If Type(S) &lt;&gt; STRING Then Error(&quot;HeadAndTail: argument must be a string&quot;); EndIf;
  L := Ascii(S);
  Space := Comp(Ascii(&quot; &quot;), 1);
  I := 1;
  N := Len(L);
  While I &lt;= N And L[I] = Space Do L[I] := 0; I := I+1; EndWhile;
  I := N;
  While I &gt;= 1 And L[I] = Space Do L[I] := 0; I := I-1; EndWhile;
  Return Ascii(L);
EndDefine; -- HeadAndTail
  
EndPackage; -- $man/help
  </xsl:template>

  <xsl:template match="description">
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="coc_version">
    <xsl:value-of select="/help/version/cocoa_version"/>
  </xsl:template>

  <xsl:template match="less_eq">&lt;=</xsl:template>
  <xsl:template match="times"> x </xsl:template>


  <xsl:template match="em">\&quot;<xsl:value-of select="."/>\&quot;</xsl:template>

  <xsl:template match="quotes">\&quot;<xsl:apply-templates select="."/>\&quot;</xsl:template>

  <xsl:template match="sup">^<xsl:apply-templates/></xsl:template>
  <xsl:template match="tt">\&quot;<xsl:apply-templates/>\&quot;</xsl:template>
  <xsl:template match="ref">\&quot;<xsl:apply-templates/>\&quot;</xsl:template>
  <xsl:template match="ttref">\&quot;<xsl:apply-templates/>\&quot;</xsl:template>

  <xsl:template match="coc_date">
    <xsl:value-of select="/help/version/date"/>
  </xsl:template>

</xsl:stylesheet> 
