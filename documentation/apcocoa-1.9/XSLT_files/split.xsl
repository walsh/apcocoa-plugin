<?xml version="1.0" encoding="ISO-8859-1"?>

<xsl:stylesheet version="1.1"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:saxon="http://icl.com/saxon">

<!-- XSLT to make easy changes to the CoCoA XML manual -->

<xsl:output method="text"/>
<xsl:preserve-space elements="*"/>


<!-- ____ TEMPLATES ______________________________________________ -->

<!-- empty elements -->
<xsl:template name="emptytag">
  <xsl:text>&lt;</xsl:text>
  <xsl:value-of select="name()"/>
  <xsl:text>/&gt;</xsl:text>
</xsl:template>

<xsl:template match="_"><xsl:call-template name="emptytag"/></xsl:template>
<xsl:template match="ccb"><xsl:call-template name="emptytag"/></xsl:template>
<xsl:template match="coc_date"><xsl:call-template name="emptytag"/></xsl:template>
<xsl:template match="coc_version"><xsl:call-template name="emptytag"/></xsl:template>
<xsl:template match="dollar"><xsl:call-template name="emptytag"/></xsl:template>
<xsl:template match="less_eq"><xsl:call-template name="emptytag"/></xsl:template>
<xsl:template match="ocb"><xsl:call-template name="emptytag"/></xsl:template>
<xsl:template match="par"><xsl:call-template name="emptytag"/></xsl:template>
<xsl:template match="tilde"><xsl:call-template name="emptytag"/></xsl:template>
<xsl:template match="times"><xsl:call-template name="emptytag"/></xsl:template>

<xsl:template match="commands_and_functions_for">&lt;commands_and_functions_for type=&quot;<xsl:value-of select="@type"/>&quot;/&gt;</xsl:template>

<!-- start/end-tag elements -->
<xsl:template name="startendtag">
  <xsl:text>&lt;</xsl:text>
  <xsl:value-of select="name()"/>
  <xsl:text>&gt;</xsl:text>
  <xsl:apply-templates/>
  <xsl:text>&lt;/</xsl:text>
  <xsl:value-of select="name()"/>
  <xsl:text>&gt;</xsl:text>
</xsl:template>

<xsl:template name="startendtag-withnumber">
  <xsl:text>&lt;</xsl:text>
  <xsl:value-of select="name()"/>
  <xsl:text> number=&quot;</xsl:text>
  <xsl:value-of select="@number"/>
  <xsl:text>&quot;&gt;</xsl:text>
  <xsl:apply-templates/>
  <xsl:text>&lt;/</xsl:text>
  <xsl:value-of select="name()"/>
  <xsl:text>&gt;</xsl:text>
</xsl:template>

<xsl:template match="changes"><xsl:call-template name="startendtag"/></xsl:template>
<xsl:template match="chapter_letter"><xsl:call-template name="startendtag"/></xsl:template>
<xsl:template match="chapter_name"><xsl:call-template name="startendtag"/></xsl:template>
<xsl:template match="coc-quotes"><xsl:call-template name="startendtag"/></xsl:template>
<xsl:template match="cocoa_commands"><xsl:call-template name="startendtag"/></xsl:template>
<xsl:template match="cocoa_version"><xsl:call-template name="startendtag"/></xsl:template>
<xsl:template match="code"><xsl:call-template name="startendtag"/></xsl:template>
<xsl:template match="coderef"><xsl:call-template name="startendtag"/></xsl:template>
<xsl:template match="command"><xsl:call-template name="startendtag"/></xsl:template>
<xsl:template match="description"><xsl:call-template name="startendtag"/></xsl:template>
<xsl:template match="em"><xsl:call-template name="startendtag"/></xsl:template>
<xsl:template match="example"><xsl:call-template name="startendtag"/></xsl:template>
<xsl:template match="formula"><xsl:call-template name="startendtag"/></xsl:template>
<xsl:template match="help"><xsl:call-template name="startendtag"/></xsl:template>
<xsl:template match="key"><xsl:call-template name="startendtag"/></xsl:template>
<xsl:template match="manual_parts"><xsl:call-template name="startendtag"/></xsl:template>
<xsl:template match="name"><xsl:call-template name="startendtag"/></xsl:template>
<xsl:template match="quotes"><xsl:call-template name="startendtag"/></xsl:template>
<xsl:template match="ref"><xsl:call-template name="startendtag"/></xsl:template>
<xsl:template match="see"><xsl:call-template name="startendtag"/></xsl:template>
<xsl:template match="short_description"><xsl:call-template name="startendtag"/></xsl:template>
<xsl:template match="syntax"><xsl:call-template name="startendtag"/></xsl:template>
<xsl:template match="title"><xsl:call-template name="startendtag"/></xsl:template>
<xsl:template match="type"><xsl:call-template name="startendtag"/></xsl:template>
<xsl:template match="verbatim"><xsl:call-template name="startendtag"/></xsl:template>
<xsl:template match="version"><xsl:call-template name="startendtag"/></xsl:template>

<xsl:template match="part"><xsl:call-template name="startendtag-withnumber"/></xsl:template>
<xsl:template match="chapter"><xsl:call-template name="startendtag-withnumber"/></xsl:template>
<xsl:template match="section"><xsl:call-template name="startendtag-withnumber"/></xsl:template>

<!-- ____ direct hyperlink references ___________________________ -->

<!-- since we can't build associative arrays we'll have to collect key/value pairs for the links -->

<xsl:variable name="command_hrefs">
  <xsl:for-each select="//cocoa_commands/chapter_letter">
    <xsl:variable name="initial" select="chapter_name"/>
  <xsl:for-each select="command">
    <xsl:sort select="title"/>
    <link>
      <key><xsl:value-of select="title"/></key>
      <value>
        <xsl:choose>
          <xsl:when test="$initial != 'Symbol' and function-available('translate')">
            <xsl:value-of select="concat('Cmd',translate(title,',.:/() ',''))"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="concat('Cmd',$initial,position())"/>
          </xsl:otherwise>
        </xsl:choose>
      </value>
    </link>
  </xsl:for-each>
  </xsl:for-each>
</xsl:variable>

<xsl:variable name="parts_hrefs">
  <xsl:for-each select="//manual_parts/part">
    <xsl:sort select="@number" data-type="number"/>
    <xsl:variable name="part_num" select="@number"/>
    <xsl:for-each select="chapter">
      <xsl:sort select="@number" data-type="number"/>
      <xsl:variable name="chap_num" select="@number"/>
      <xsl:for-each select="section">
        <xsl:sort select="@number" data-type="number"/>
        <xsl:variable name="sect_num" select="@number"/>
        <link>
          <key><xsl:value-of select="title"/></key>
          <value>
            <xsl:choose>
              <xsl:when test="function-available('translate')">
                <xsl:value-of select="concat('Part',translate(title,',.:/() ',''))"/>
              </xsl:when>
              <xsl:otherwise>
                <xsl:value-of select="concat('P',$part_num,'c',$chap_num, 's',$sect_num)"/>
              </xsl:otherwise>
            </xsl:choose>
          </value>
        </link>
      </xsl:for-each>
    </xsl:for-each>
  </xsl:for-each>
</xsl:variable>

<xsl:template name="create_href">
  <xsl:param name="key"/> <!-- it is assumed that the key is unique... -->
  <xsl:choose>
    <xsl:when test="$command_hrefs/*[key=$key]">
      <xsl:for-each select="$command_hrefs/*[key=$key]">
        <xsl:value-of select="value"/>
      </xsl:for-each>
    </xsl:when>
    <xsl:when test="$parts_hrefs/*[key=$key]">
      <xsl:for-each select="$parts_hrefs/*[key=$key]">
        <xsl:value-of select="value"/>
      </xsl:for-each>
    </xsl:when>
    <xsl:otherwise>
      <xsl:text>toc.html#</xsl:text>
      <xsl:value-of select="$key"/>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

<xsl:template name="previous_href">
  <xsl:param name="key"/>
  <xsl:choose>
    <xsl:when test="$command_hrefs/*[key=$key]">
      <xsl:for-each select="$command_hrefs/*[key=$key]">
        <xsl:value-of select="preceding-sibling::*[1]/value"/>
      </xsl:for-each>
    </xsl:when>
    <xsl:when test="$parts_hrefs/*[key=$key]">
      <xsl:for-each select="$parts_hrefs/*[key=$key]">
        <xsl:value-of select="preceding-sibling::*[1]/value"/>
      </xsl:for-each>
    </xsl:when>
  </xsl:choose>
</xsl:template>

<xsl:template name="next_href">
  <xsl:param name="key"/>
  <xsl:choose>
    <xsl:when test="$command_hrefs/*[key=$key]">
      <xsl:for-each select="$command_hrefs/*[key=$key]">
        <xsl:value-of select="following-sibling::*[1]/value"/>
      </xsl:for-each>
    </xsl:when>
    <xsl:when test="$parts_hrefs/*[key=$key]">
      <xsl:for-each select="$parts_hrefs/*[key=$key]">
        <xsl:value-of select="following-sibling::*[1]/value"/>
      </xsl:for-each>
    </xsl:when>
  </xsl:choose>
</xsl:template>

</xsl:stylesheet>
