<?xml version="1.0" encoding="ISO-8859-1"?>

<xsl:stylesheet version="1.1"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<!-- xslt document for creating the QT Assistant Document Profile for CoCoA -->

<xsl:output method="text"/>
<xsl:strip-space elements="*"/>

<!-- line breaks -->

<xsl:variable name="endl">
<xsl:text>
</xsl:text>
</xsl:variable>

<xsl:template match="help">

<!-- ____ TOC ___________________________ -->
<xsl:variable name="file" select="'apcocoa_eclipse_toc.xml'"/>
<xsl:document href="{$file}">

<xsl:text>
&lt;toc label=&quot;ApCoCoA Handbook&quot;&gt;
</xsl:text>

<!-- ____ Parts/Chapters/topics ____________________________ -->

<!-- Eclipse TOC for Doing Mathematics with ApCoCoA -->

<xsl:text>  </xsl:text>
<xsl:text>&lt;topic href=&quot;doc/apcocoa/toc_doingmath.html</xsl:text>
<xsl:text>&quot; label=&quot;Doing Mathematics with ApCoCoA&quot;&gt;</xsl:text>
<xsl:value-of select="$endl"/>

<xsl:for-each select="/help/manual_parts/part">
  <xsl:variable name="part_num" select="6"/>

  <xsl:for-each select="chapter">
    <xsl:variable name="chap_num" select="position()"/>

    <xsl:text>    </xsl:text>
    <xsl:text>&lt;topic href=&quot;doc/apcocoa/toc_doingmath.html#</xsl:text>
    <xsl:value-of select="concat('p',$part_num,'c',$chap_num)"/>
    <xsl:text>&quot; label=&quot;</xsl:text>
    <xsl:apply-templates select="title"/>
    <xsl:text>&quot;&gt;</xsl:text>
    <xsl:value-of select="$endl"/>

    <xsl:for-each select="section">
      <xsl:variable name="sect_num" select="position()"/>

      <xsl:text>      </xsl:text>
      <xsl:text>&lt;topic href=&quot;doc/apcocoa/</xsl:text>
      <xsl:call-template name="create_href">
        <xsl:with-param name="key" select="title"/>
      </xsl:call-template>
      <xsl:text>&quot; label=&quot;</xsl:text>
      <xsl:apply-templates select="title"/>
      <xsl:text>&quot;/&gt;</xsl:text>
      <xsl:value-of select="$endl"/>
    </xsl:for-each> <!-- end for each section -->
         
    <xsl:text>    &lt;/topic&gt;</xsl:text>
    <xsl:value-of select="$endl"/>
  </xsl:for-each> <!-- end for each chapter -->
           
</xsl:for-each> <!-- end for each part -->

<xsl:text>  &lt;/topic&gt;</xsl:text>
<xsl:value-of select="$endl"/>



<!-- ____ alphabetical list of commands ___________________________ -->

<xsl:text>  </xsl:text>
<xsl:text>&lt;topic href=&quot;doc/apcocoa/toc_cmdref.html&quot; label=&quot;ApCoCoA Command Reference&quot;&gt;</xsl:text>
<xsl:value-of select="$endl"/>

<xsl:for-each select="cocoa_commands/chapter_letter">
  <xsl:for-each select="command">
    <xsl:sort select="title"/>

    <xsl:text>    </xsl:text>
    <xsl:text>&lt;topic href=&quot;doc/apcocoa/</xsl:text>
    <xsl:call-template name="create_href">
      <xsl:with-param name="key" select="title"/>
    </xsl:call-template>
    <xsl:text>&quot; label=&quot;</xsl:text>
    <xsl:apply-templates select="title"/>
    <xsl:text>&quot;/&gt;</xsl:text>
    <xsl:value-of select="$endl"/>

  </xsl:for-each>
</xsl:for-each>
<xsl:text>  &lt;/topic&gt;</xsl:text>
<xsl:value-of select="$endl"/>

<!--
<xsl:text>  &lt;topic href=&quot;html/eclipse_gui_help/gui_help.html&quot; label=&quot;User Interface Guide&quot;&gt;</xsl:text>
<xsl:value-of select="$endl"/>
<xsl:text>  &lt;/topic&gt;</xsl:text>
<xsl:value-of select="$endl"/>-->

<xsl:text>&lt;/toc&gt;</xsl:text>
</xsl:document>
</xsl:template>


<!-- ____ TEMPLATES ______________________________________________ -->

<xsl:template match="em"><xsl:value-of select="."/></xsl:template>
<!-- <xsl:template match="formula"><xsl:value-of select="."/></xsl:template> -->
<xsl:template match="underscore">_</xsl:template>

<!-- ____ direct hyperlink references ___________________________ -->

<!-- since we can't build associative arrays we'll have to collect key/value pairs for the links -->

<xsl:variable name="command_hrefs">
  <xsl:for-each select="//cocoa_commands/chapter_letter">
    <xsl:variable name="initial" select="title"/>
  <xsl:for-each select="command">
    <xsl:sort select="title"/>
    <link>
      <key><xsl:value-of select="title"/></key>
      <value>
        <xsl:choose>
          <xsl:when test="$initial != 'Symbol' and function-available('translate')">
            <xsl:value-of select="concat('cmd',translate(title,',.:/()- ',''),'.html')"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="concat('cmd',$initial,position(),'.html')"/>
          </xsl:otherwise>
        </xsl:choose>
      </value>
    </link>
  </xsl:for-each>
  </xsl:for-each>
</xsl:variable>

<xsl:variable name="parts_hrefs">
  <xsl:for-each select="//manual_parts/part">
    <xsl:variable name="part_num" select="position()"/>
    <xsl:for-each select="chapter">
      <xsl:variable name="chap_num" select="position()"/>
      <xsl:for-each select="section">
        <xsl:variable name="sect_num" select="position()"/>
        <link>
          <key><xsl:value-of select="title"/></key>
          <value>
            <xsl:choose>
              <xsl:when test="function-available('translate')">
                <xsl:value-of select="concat('part',translate(title,',.:/()- ',''),'.html')"/>
              </xsl:when>
              <xsl:otherwise>
                <xsl:value-of select="concat('p',$part_num,'c',$chap_num, 's',$sect_num,'.html')"/>
              </xsl:otherwise>
            </xsl:choose>
          </value>
        </link>
      </xsl:for-each>
    </xsl:for-each>
  </xsl:for-each>
</xsl:variable>

<xsl:template name="create_href">
  <xsl:param name="key"/> <!-- it is assumed that the key is unique... -->
  <xsl:choose>
    <xsl:when test="$command_hrefs/*[key=$key]">
      <xsl:for-each select="$command_hrefs/*[key=$key]">
        <xsl:value-of select="value"/>
      </xsl:for-each>
    </xsl:when>
    <xsl:when test="$parts_hrefs/*[key=$key]">
      <xsl:for-each select="$parts_hrefs/*[key=$key]">
        <xsl:value-of select="value"/>
      </xsl:for-each>
    </xsl:when>
    <xsl:otherwise>
      <xsl:text>toc.html#</xsl:text>
      <xsl:value-of select="$key"/>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

<xsl:template name="previous_href">
  <xsl:param name="key"/>
  <xsl:choose>
    <xsl:when test="$command_hrefs/*[key=$key]">
      <xsl:for-each select="$command_hrefs/*[key=$key]">
        <xsl:value-of select="preceding-sibling::*[1]/value"/>
      </xsl:for-each>
    </xsl:when>
    <xsl:when test="$parts_hrefs/*[key=$key]">
      <xsl:for-each select="$parts_hrefs/*[key=$key]">
        <xsl:value-of select="preceding-sibling::*[1]/value"/>
      </xsl:for-each>
    </xsl:when>
  </xsl:choose>
</xsl:template>

<xsl:template name="next_href">
  <xsl:param name="key"/>
  <xsl:choose>
    <xsl:when test="$command_hrefs/*[key=$key]">
      <xsl:for-each select="$command_hrefs/*[key=$key]">
        <xsl:value-of select="following-sibling::*[1]/value"/>
      </xsl:for-each>
    </xsl:when>
    <xsl:when test="$parts_hrefs/*[key=$key]">
      <xsl:for-each select="$parts_hrefs/*[key=$key]">
        <xsl:value-of select="following-sibling::*[1]/value"/>
      </xsl:for-each>
    </xsl:when>
  </xsl:choose>
</xsl:template>

</xsl:stylesheet>
