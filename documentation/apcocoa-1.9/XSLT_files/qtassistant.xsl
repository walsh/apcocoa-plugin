<?xml version="1.0" encoding="ISO-8859-1"?>

<xsl:stylesheet version="1.1"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<!-- xslt document for creating the QT Assistant Document Profile for CoCoA -->

<xsl:output method="text"/>
<xsl:strip-space elements="*"/>

<!-- line breaks -->

<xsl:variable name="endl">
<xsl:text>
</xsl:text>
</xsl:variable>

<xsl:template match="help">

<!-- ____ TOC ___________________________ -->

<xsl:text>
&lt;!DOCTYPE DCF&gt;

&lt;assistantconfig version=&quot;3.2.0&quot;&gt;

  &lt;profile>
    &lt;property name=&quot;name&quot;&gt;ApCoCoAHelp&lt;/property&gt;
    &lt;property name=&quot;title&quot;&gt;ApCoCoA help&lt;/property&gt;
    &lt;property name=&quot;applicationicon&quot;&gt;images/handbook.png&lt;/property&gt;
    &lt;property name=&quot;startpage&quot;&gt;index.html&lt;/property&gt;
    &lt;property name=&quot;assistantdocs&quot;&gt;./help&lt;/property&gt;
  &lt;/profile>

  &lt;DCF ref=&quot;toc.html&quot; icon=&quot;handbook.png&quot; imagedir=&quot;images&quot; title=&quot;CoCoA Handbook&quot;&gt;
</xsl:text>

<!-- ____ Parts/Chapters/Sections ____________________________ -->

  <xsl:for-each select="/help/manual_parts/part">
    <xsl:variable name="part_num" select="position()"/>

    <xsl:text>    </xsl:text>
    <xsl:text>&lt;section ref=&quot;toc.html#</xsl:text>
    <xsl:value-of select="$part_num"/>
    <xsl:text>&quot; title=&quot;</xsl:text>
    <xsl:apply-templates select="title"/>
    <xsl:text>&quot;&gt;</xsl:text>
    <xsl:value-of select="$endl"/>

    <xsl:for-each select="chapter">
      <xsl:variable name="chap_num" select="position()"/>

      <xsl:text>      </xsl:text>
      <xsl:text>&lt;section ref=&quot;toc.html#</xsl:text>
      <xsl:value-of select="concat('p',$part_num,'c',$chap_num)"/>
      <xsl:text>&quot; title=&quot;</xsl:text>
      <xsl:apply-templates select="title"/>
      <xsl:text>&quot;&gt;</xsl:text>
      <xsl:value-of select="$endl"/>

      <xsl:for-each select="section">
        <xsl:variable name="sect_num" select="position()"/>

        <xsl:text>        </xsl:text>
        <xsl:text>&lt;section ref=&quot;</xsl:text>
        <xsl:call-template name="create_href">
          <xsl:with-param name="key" select="title"/>
        </xsl:call-template>
        <xsl:text>&quot; title=&quot;</xsl:text>
        <xsl:apply-templates select="title"/>
        <xsl:text>&quot;/&gt;</xsl:text>
        <xsl:value-of select="$endl"/>

      </xsl:for-each>
      <xsl:text>      &lt;/section&gt;</xsl:text>
      <xsl:value-of select="$endl"/>

    </xsl:for-each>
    <xsl:text>    &lt;/section&gt;</xsl:text>
    <xsl:value-of select="$endl"/>

  </xsl:for-each>

<xsl:text>  &lt;/DCF&gt;
</xsl:text>

<!-- ____ alphabetical list of commands ___________________________ -->

<xsl:text>
  &lt;DCF ref=&quot;toc.html#alphalist&quot; icon=&quot;handbook.png&quot; imagedir=&quot;images&quot; title=&quot;CoCoA Commands Handbook&quot;&gt;
</xsl:text>

  <xsl:text>    </xsl:text>
  <xsl:text>&lt;section ref=&quot;toc.html#alphalist&quot; title=&quot;All Commands&quot;&gt;</xsl:text>
  <xsl:value-of select="$endl"/>

  <xsl:for-each select="cocoa_commands/chapter_letter">
    <xsl:for-each select="command">
      <xsl:sort select="title"/>

      <xsl:text>      </xsl:text>
      <xsl:text>&lt;section ref=&quot;</xsl:text>
      <xsl:call-template name="create_href">
        <xsl:with-param name="key" select="title"/>
      </xsl:call-template>
      <xsl:text>&quot; title=&quot;</xsl:text>
      <xsl:apply-templates select="title"/>
      <xsl:text>&quot;/&gt;</xsl:text>
      <xsl:value-of select="$endl"/>

    </xsl:for-each>
  </xsl:for-each>
  <xsl:text>    &lt;/section&gt;</xsl:text>
  <xsl:value-of select="$endl"/>

  <xsl:for-each select="//commands_and_functions_for">
    <xsl:sort select="../../title"/>
    <xsl:variable name="type" select="@type"/>

    <xsl:text>    </xsl:text>
    <xsl:text>&lt;section ref=&quot;</xsl:text>
    <!-- assume that the commands_and_functions_for element is in a section... -->
    <xsl:call-template name="create_href">
      <xsl:with-param name="key" select="../../title"/>
    </xsl:call-template>
    <xsl:text>&quot; title=&quot;</xsl:text>
    <xsl:apply-templates select="../../title"/>
    <xsl:text>&quot;&gt;</xsl:text>
    <xsl:value-of select="$endl"/>

    <xsl:for-each select="//command[type=$type]|//command[types/type=$type]">
      <xsl:sort select="title"/>

      <xsl:text>      </xsl:text>
      <xsl:text>&lt;section ref=&quot;</xsl:text>
      <xsl:call-template name="create_href">
        <xsl:with-param name="key" select="title"/>
      </xsl:call-template>
      <xsl:text>&quot; title=&quot;</xsl:text>
      <xsl:apply-templates select="title"/>
      <xsl:text>&quot;/&gt;</xsl:text>
      <xsl:value-of select="$endl"/>

    </xsl:for-each>
    <xsl:text>    &lt;/section&gt;</xsl:text>
    <xsl:value-of select="$endl"/>
  </xsl:for-each>

<xsl:text>  &lt;/DCF&gt;
&lt;/assistantconfig&gt;
</xsl:text>

</xsl:template>


<!-- ____ TEMPLATES ______________________________________________ -->

<xsl:template match="em"><xsl:value-of select="."/></xsl:template>
<!-- <xsl:template match="formula"><xsl:value-of select="."/></xsl:template> -->
<xsl:template match="underscore">_</xsl:template>

<!-- ____ direct hyperlink references ___________________________ -->

<!-- since we can't build associative arrays we'll have to collect key/value pairs for the links -->

<xsl:variable name="command_hrefs">
  <xsl:for-each select="//cocoa_commands/chapter_letter">
    <xsl:variable name="initial" select="title"/>
  <xsl:for-each select="command">
    <xsl:sort select="title"/>
    <link>
      <key><xsl:value-of select="title"/></key>
      <value>
        <xsl:choose>
          <xsl:when test="$initial != 'Symbol' and function-available('translate')">
            <xsl:value-of select="concat('cmd',translate(title,',.:/()- ',''),'.html')"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="concat('cmd',$initial,position(),'.html')"/>
          </xsl:otherwise>
        </xsl:choose>
      </value>
    </link>
  </xsl:for-each>
  </xsl:for-each>
</xsl:variable>

<xsl:variable name="parts_hrefs">
  <xsl:for-each select="//manual_parts/part">
    <xsl:variable name="part_num" select="position()"/>
    <xsl:for-each select="chapter">
      <xsl:variable name="chap_num" select="position()"/>
      <xsl:for-each select="section">
        <xsl:variable name="sect_num" select="position()"/>
        <link>
          <key><xsl:value-of select="title"/></key>
          <value>
            <xsl:choose>
              <xsl:when test="function-available('translate')">
                <xsl:value-of select="concat('part',translate(title,',.:/()- ',''),'.html')"/>
              </xsl:when>
              <xsl:otherwise>
                <xsl:value-of select="concat('p',$part_num,'c',$chap_num, 's',$sect_num,'.html')"/>
              </xsl:otherwise>
            </xsl:choose>
          </value>
        </link>
      </xsl:for-each>
    </xsl:for-each>
  </xsl:for-each>
</xsl:variable>

<xsl:template name="create_href">
  <xsl:param name="key"/> <!-- it is assumed that the key is unique... -->
  <xsl:choose>
    <xsl:when test="$command_hrefs/*[key=$key]">
      <xsl:for-each select="$command_hrefs/*[key=$key]">
        <xsl:value-of select="value"/>
      </xsl:for-each>
    </xsl:when>
    <xsl:when test="$parts_hrefs/*[key=$key]">
      <xsl:for-each select="$parts_hrefs/*[key=$key]">
        <xsl:value-of select="value"/>
      </xsl:for-each>
    </xsl:when>
    <xsl:otherwise>
      <xsl:text>toc.html#</xsl:text>
      <xsl:value-of select="$key"/>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

<xsl:template name="previous_href">
  <xsl:param name="key"/>
  <xsl:choose>
    <xsl:when test="$command_hrefs/*[key=$key]">
      <xsl:for-each select="$command_hrefs/*[key=$key]">
        <xsl:value-of select="preceding-sibling::*[1]/value"/>
      </xsl:for-each>
    </xsl:when>
    <xsl:when test="$parts_hrefs/*[key=$key]">
      <xsl:for-each select="$parts_hrefs/*[key=$key]">
        <xsl:value-of select="preceding-sibling::*[1]/value"/>
      </xsl:for-each>
    </xsl:when>
  </xsl:choose>
</xsl:template>

<xsl:template name="next_href">
  <xsl:param name="key"/>
  <xsl:choose>
    <xsl:when test="$command_hrefs/*[key=$key]">
      <xsl:for-each select="$command_hrefs/*[key=$key]">
        <xsl:value-of select="following-sibling::*[1]/value"/>
      </xsl:for-each>
    </xsl:when>
    <xsl:when test="$parts_hrefs/*[key=$key]">
      <xsl:for-each select="$parts_hrefs/*[key=$key]">
        <xsl:value-of select="following-sibling::*[1]/value"/>
      </xsl:for-each>
    </xsl:when>
  </xsl:choose>
</xsl:template>

</xsl:stylesheet>
