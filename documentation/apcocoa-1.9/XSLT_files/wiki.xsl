<?xml version="1.0" encoding="ISO-8859-1"?>

<xsl:stylesheet version="1.1"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:saxon="http://icl.com/saxon">

<xsl:preserve-space elements="quotes"/>
<xsl:preserve-space elements="code"/>

<!-- xslt document for exporting the CoCoA command documentation to Wiki text -->

<xsl:output method="text"/>
<xsl:strip-space elements="*"/>

<!-- line breaks -->

<xsl:variable name="endl">
<xsl:text>
</xsl:text>
</xsl:variable>

<!-- alphanumeric characters -->
<xsl:variable name="alphanum">
  <xsl:text>abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789</xsl:text>
</xsl:variable>

<xsl:template match="command">
        <xsl:apply-templates select="short_description"/>
        <xsl:value-of select="$endl"/>

        <xsl:call-template name="h2">
          <xsl:with-param name="header">Syntax</xsl:with-param>
        </xsl:call-template>

        <xsl:apply-templates select="syntax"/>

        <xsl:call-template name="h2">
          <xsl:with-param name="header">Description</xsl:with-param>
        </xsl:call-template>
        <xsl:apply-templates select="description"/>
        <xsl:value-of select="$endl"/>

        <!-- new style "See also" -->
        <xsl:if test="seealso">
          <xsl:apply-templates select="seealso"/>
        </xsl:if>

        <!-- old style "See also" -->
        <xsl:if test="see">
          <xsl:call-template name="seealso"/>
        </xsl:if>

        <!-- new style "Types" -->
        <!-- <xsl:apply-templates select="types"/> -->

        <!-- <xsl:apply-templates select="wiki-category"/> -->
        <xsl:if test="wiki-category">
          <xsl:apply-templates select="wiki-category"/>
          <xsl:value-of select="$endl"/>
        </xsl:if>
</xsl:template>

<!-- ____ TEMPLATES ______________________________________________ -->

<xsl:template match="wiki-category">
  <xsl:text>[[Category:</xsl:text>
  <xsl:value-of select="normalize-space()"/>
  <xsl:if test="not(contains(., '|'))">
      <xsl:text>|{{PAGENAME}}</xsl:text>
  </xsl:if>
  <xsl:text>]]</xsl:text>
</xsl:template>

<xsl:template match="commands_and_functions_for">

<xsl:text>
{| border="1" align="center"
</xsl:text>

  <xsl:variable name="my_type" select="@type"/>
  <xsl:for-each select="//command[type=$my_type]|//command[types/type=$my_type]">
    <xsl:sort select="title"/>

<xsl:text>|-
| </xsl:text>

    <!-- from ref / coderef -->
    <xsl:text>[[CoCoA4.7:</xsl:text>
    <xsl:call-template name="create_href">
      <xsl:with-param name="key" select="title"/>
    </xsl:call-template>
    <xsl:text>|</xsl:text>
    <xsl:value-of select="normalize-space(title)"/>
    <xsl:text>]]</xsl:text>

    <xsl:text> || </xsl:text>

    <xsl:apply-templates select="short_description"/>

    <xsl:value-of select="$endl"/>

  </xsl:for-each>

  <xsl:text>|}</xsl:text>
  <xsl:value-of select="$endl"/>
  <xsl:value-of select="$endl"/>

</xsl:template>

<xsl:template match="coc_version">
  <xsl:value-of select="/help/version/cocoa_version"/>
</xsl:template>


<xsl:template match="em">'''<xsl:value-of select="."/>'''</xsl:template>

<xsl:template match="quotes">&quot;<xsl:apply-templates/>&quot;</xsl:template>
<xsl:template match="sup">^{<xsl:apply-templates/>}</xsl:template>
<xsl:template match="formula">&lt;math&gt;<xsl:apply-templates/>&lt;/math&gt;</xsl:template>
<xsl:template match="tt">&lt;tt&gt;<xsl:apply-templates/>&lt;/tt&gt;</xsl:template>

<xsl:template match="less_eq">\le </xsl:template>
<xsl:template match="times">\times </xsl:template>
<xsl:template match="underscore">_</xsl:template>

<xsl:template match="ref" name="ref">
  <xsl:text>&lt;commandref&gt;</xsl:text>
  <xsl:value-of select="normalize-space()"/>
  <xsl:text>&lt;/commandref&gt;</xsl:text>
</xsl:template>

<xsl:template match="ttref" name="ttref">
  <xsl:text>&lt;commandref&gt;</xsl:text>
  <xsl:value-of select="normalize-space()"/>
  <xsl:text>&lt;/commandref&gt;</xsl:text>
</xsl:template>

<xsl:template match="par"><xsl:value-of select="$endl"/></xsl:template>

<xsl:template match="example">
  <xsl:call-template name="h3">
    <xsl:with-param name="header">Example</xsl:with-param>
  </xsl:call-template>
  <xsl:call-template name="verbatim"/>
</xsl:template>

<xsl:template match="coc_date">
  <xsl:value-of select="/help/version/date"/>
</xsl:template>

<xsl:template match="syntax"><xsl:call-template name="verbatim"/></xsl:template>

<xsl:template match="seealso" name="seealso">
  <xsl:call-template name="h2">
    <xsl:with-param name="header">See Also</xsl:with-param>
  </xsl:call-template>

  <xsl:for-each select="see">
    <xsl:text>*</xsl:text>
    <xsl:call-template name="ttref"/>
    <xsl:value-of select="$endl"/>
  </xsl:for-each>
</xsl:template>

<xsl:template match="types">
  <xsl:call-template name="h2">
    <xsl:with-param name="header">Types</xsl:with-param>
  </xsl:call-template>

  <xsl:for-each select="type">
    <xsl:text>*</xsl:text>
    <xsl:value-of select="."/>
    <xsl:value-of select="$endl"/>
  </xsl:for-each>
</xsl:template>

<xsl:template match="verbatim_OLDSOLUTION" name="verbatim_OLDSOLUTION">
  <!-- Prefix every line with a space to generate a wiki text comment -->
  <xsl:variable name="rendered">
    <xsl:apply-templates/>
  </xsl:variable>
  <xsl:for-each select="saxon:tokenize($rendered, '&#x0A;&#x0D;')">
    <xsl:text> </xsl:text>
    <xsl:value-of select="."/>
    <xsl:value-of select="$endl"/>
  </xsl:for-each>
</xsl:template>

<xsl:template match="verbatim" name="verbatim">
  <xsl:text>&lt;pre&gt;</xsl:text>
    <xsl:apply-templates/>
  <xsl:text>&lt;/pre&gt;</xsl:text>
  <xsl:value-of select="$endl"/>
</xsl:template>

<xsl:template name="h1">
  <xsl:param name="header" select="'H1'"/>
  <xsl:text>=</xsl:text><xsl:value-of select="$header"/><xsl:text>=</xsl:text>
  <xsl:value-of select="$endl"/>
</xsl:template>

<xsl:template name="h2">
  <xsl:param name="header" select="'H2'"/>
  <xsl:text>==</xsl:text><xsl:value-of select="$header"/><xsl:text>==</xsl:text>
  <xsl:value-of select="$endl"/>
</xsl:template>

<xsl:template name="h3">
  <xsl:param name="header" select="'H3'"/>
  <xsl:text>===</xsl:text><xsl:value-of select="$header"/><xsl:text>===</xsl:text>
  <xsl:value-of select="$endl"/>
</xsl:template>

<!-- ____ direct hyperlink references ___________________________ -->

<!-- since we can't build associative arrays we'll have to collect key/value pairs for the links -->

<xsl:variable name="command_hrefs">
  <xsl:for-each select="//cocoa_commands/chapter_letter">
    <xsl:variable name="initial" select="chapter_name"/>
  <xsl:for-each select="command">
    <xsl:sort select="title"/>
    <link>
      <key><xsl:value-of select="title"/></key>
      <value>
        <xsl:choose>
          <xsl:when test="$initial != 'Symbol' and function-available('translate')">
            <xsl:value-of select="concat('Cmd',translate(title,',.:/() ',''))"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="concat('Cmd',$initial,position())"/>
          </xsl:otherwise>
        </xsl:choose>
      </value>
    </link>
  </xsl:for-each>
  </xsl:for-each>
</xsl:variable>

<xsl:variable name="parts_hrefs">
  <xsl:for-each select="//manual_parts/part">
    <xsl:sort select="@number" data-type="number"/>
    <xsl:variable name="part_num" select="@number"/>
    <xsl:for-each select="chapter">
      <xsl:sort select="@number" data-type="number"/>
      <xsl:variable name="chap_num" select="@number"/>
      <xsl:for-each select="section">
        <xsl:sort select="@number" data-type="number"/>
        <xsl:variable name="sect_num" select="@number"/>
        <link>
          <key><xsl:value-of select="title"/></key>
          <value>
            <xsl:choose>
              <xsl:when test="function-available('translate')">
                <xsl:value-of select="concat('Part',translate(title,',.:/() ',''))"/>
              </xsl:when>
              <xsl:otherwise>
                <xsl:value-of select="concat('P',$part_num,'c',$chap_num, 's',$sect_num)"/>
              </xsl:otherwise>
            </xsl:choose>
          </value>
        </link>
      </xsl:for-each>
    </xsl:for-each>
  </xsl:for-each>
</xsl:variable>

<xsl:template name="create_href">
  <xsl:param name="key"/> <!-- it is assumed that the key is unique... -->
  <xsl:choose>
    <xsl:when test="$command_hrefs/*[key=$key]">
      <xsl:for-each select="$command_hrefs/*[key=$key]">
        <xsl:value-of select="value"/>
      </xsl:for-each>
    </xsl:when>
    <xsl:when test="$parts_hrefs/*[key=$key]">
      <xsl:for-each select="$parts_hrefs/*[key=$key]">
        <xsl:value-of select="value"/>
      </xsl:for-each>
    </xsl:when>
    <xsl:otherwise>
      <xsl:text>toc.html#</xsl:text>
      <xsl:value-of select="$key"/>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

<xsl:template name="previous_href">
  <xsl:param name="key"/>
  <xsl:choose>
    <xsl:when test="$command_hrefs/*[key=$key]">
      <xsl:for-each select="$command_hrefs/*[key=$key]">
        <xsl:value-of select="preceding-sibling::*[1]/value"/>
      </xsl:for-each>
    </xsl:when>
    <xsl:when test="$parts_hrefs/*[key=$key]">
      <xsl:for-each select="$parts_hrefs/*[key=$key]">
        <xsl:value-of select="preceding-sibling::*[1]/value"/>
      </xsl:for-each>
    </xsl:when>
  </xsl:choose>
</xsl:template>

<xsl:template name="next_href">
  <xsl:param name="key"/>
  <xsl:choose>
    <xsl:when test="$command_hrefs/*[key=$key]">
      <xsl:for-each select="$command_hrefs/*[key=$key]">
        <xsl:value-of select="following-sibling::*[1]/value"/>
      </xsl:for-each>
    </xsl:when>
    <xsl:when test="$parts_hrefs/*[key=$key]">
      <xsl:for-each select="$parts_hrefs/*[key=$key]">
        <xsl:value-of select="following-sibling::*[1]/value"/>
      </xsl:for-each>
    </xsl:when>
  </xsl:choose>
</xsl:template>

</xsl:stylesheet>
