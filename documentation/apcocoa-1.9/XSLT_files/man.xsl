<?xml version="1.0" encoding="ascii"?>

<xsl:stylesheet version="1.1"
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:preserve-space elements="quotes"/>
  <xsl:preserve-space elements="tt"/>

<!--xslt document for creating the man directory for CoCoA-->

  <xsl:output method="text"/>
  <xsl:strip-space elements="*"/>	
  
 
  <xsl:template match="help">
  <xsl:for-each select="cocoa_commands/chapter_letter/command">
      <xsl:sort select="title"/>
          
    <xsl:variable name="file" select="concat('cmd',position())"/>
    <xsl:document href="{$file}">
-- <xsl:apply-templates select="title"/>
MEMORY.Doc.Syntax := &quot;<xsl:apply-templates select="syntax"/>&quot;;
MEMORY.Doc.Descr := &quot;&#xa;<xsl:apply-templates select="description"/>&quot;;
    </xsl:document>
  </xsl:for-each>
  <xsl:for-each select="manual_parts/part">
    <xsl:variable name="part_num" select="position()"/>
    <xsl:for-each select="chapter"> 
      <xsl:variable name="chap_num" select="position()"/>
      <xsl:for-each select="section">
	<xsl:variable name="sect_num" select="position()"/>
	<xsl:variable name="file" select="concat('p',$part_num,'c',$chap_num,'s',$sect_num)"/> 
	<xsl:document href="{$file}">
-- <xsl:apply-templates select="title"/>
MEMORY.Doc.Descr := &quot;&#xa;<xsl:apply-templates select="description"/>&quot;;
    </xsl:document>
      </xsl:for-each>
    </xsl:for-each>
  </xsl:for-each>
  </xsl:template>
  
    <!-- Template itemize -->
  <xsl:template match="itemize" name="itemize">
    <xsl:text>&#xa;</xsl:text>
    <xsl:for-each select="item">
      <xsl:text>* </xsl:text>  
      <xsl:apply-templates/>
      <xsl:text>&#xa;</xsl:text>
    </xsl:for-each>
    <xsl:text>&#xa;</xsl:text>
  </xsl:template> <!-- End Template see also -->  

  <xsl:template match="commands_and_functions_for">  
   <xsl:variable name="my_type" select="@type"/>
   <xsl:for-each select="//command[type=$my_type]|//command[types/type=$my_type]">
     <xsl:sort select="title"/>
  * <xsl:apply-templates select="title"/> -- <xsl:apply-templates select="short_description"/>
   </xsl:for-each>
  </xsl:template>

  <xsl:template match="coc_version">
    <xsl:value-of select="/help/version/cocoa_version"/>
  </xsl:template>


  <xsl:template match="em">*<xsl:value-of select="."/>*</xsl:template>

  <xsl:template match="quotes">\&quot;<xsl:apply-templates/>\&quot;</xsl:template>

  <xsl:template match="sup">^<xsl:apply-templates/></xsl:template>
  <xsl:template match="tt">\&quot;<xsl:apply-templates/>\&quot;</xsl:template>
  <xsl:template match="ref">\&quot;<xsl:apply-templates/>\&quot;</xsl:template>
  <xsl:template match="ttref">\&quot;<xsl:apply-templates/>\&quot;</xsl:template>

  <xsl:template match="less_eq">&lt;=</xsl:template>
  <xsl:template match="times"> x </xsl:template>


  <xsl:template match="example">
     <xsl:text>//==========================  EXAMPLE  ==========================\\&#xa;</xsl:text>
     <xsl:apply-templates/>
     <xsl:text>\\==========================  o=o=o=o  ===========================//&#xa;&#xa;</xsl:text>
   </xsl:template>

  <xsl:template match="coc_date">
    <xsl:value-of select="/help/version/date"/>
  </xsl:template>
  
 <xsl:template name="replace">
    <xsl:param name="string"/>
    <xsl:param name="from"/>
    <xsl:param name="to"/>
    <xsl:choose>
       <xsl:when test="contains($string, $from)">
          <xsl:value-of select="substring-before($string, $from)"/>
          <xsl:copy-of select="$to"/>
          <xsl:call-template name="replace">
             <xsl:with-param name="string" select="substring-after($string, $from)"/>
             <xsl:with-param name="from" select="$from"/>
             <xsl:with-param name="to" select="$to" />
          </xsl:call-template>
       </xsl:when>
       <xsl:otherwise>
          <xsl:value-of select="$string" />
       </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
</xsl:stylesheet> 

