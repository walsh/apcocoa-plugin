<?xml version="1.0" encoding="UTF-8"?>
<!--<?xml version="1.0" encoding="ISO-8859-1"?>-->

<xsl:stylesheet version="1.1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:preserve-space elements="quotes"/>
  <xsl:preserve-space elements="tt"/>
  
  <!-- ######################################################################
      xslt document for creating the ApCoCoA-online help
      ###################################################################### -->
  
  <xsl:output method="text"/>
  <xsl:strip-space elements="*"/>
  
  <!-- line breaks -->
  <xsl:variable name="endl">
    <xsl:text>
    </xsl:text>
  </xsl:variable>
  
  
  <xsl:template match="help">
  
    <!-- ################### iterate through  COMMANDS ###################### -->
    <xsl:for-each select="cocoa_commands/chapter_letter">
      <xsl:for-each select="command">
        <xsl:sort select="title"/>
    
        <xsl:variable name="file_cmd">
          <xsl:call-template name="create_href">
            <xsl:with-param name="key" select="title"/>
          </xsl:call-template>
        </xsl:variable>
          
        <xsl:document href="{$file_cmd}">
      
          <xsl:text>
          &lt;html&gt;
          &lt;head&gt;
          &lt;link rel="stylesheet" type="text/css" href="gui.css"&gt;
          </xsl:text>
    
          <xsl:text>&lt;title&gt;</xsl:text>
          <xsl:apply-templates select="title"/>
          <xsl:text>&lt;/title&gt;</xsl:text>
    
          <xsl:text>
          &lt;/head&gt;
        
          &lt;body bgcolor=#eeffff&gt;
          &lt;div&gt;
          </xsl:text>
    
          <xsl:text>&lt;a href=&quot;toc_cmdref.html#</xsl:text>
          <xsl:value-of select="title"/>
          <xsl:text>&quot;&gt;up&lt;/a&gt;</xsl:text>
          <xsl:text> </xsl:text>
    
          <xsl:choose>
            <xsl:when test="position()!=1">
              <xsl:text>&lt;a href=&quot;</xsl:text>
              <xsl:call-template name="previous_href">
                <xsl:with-param name="key" select="title"/>
              </xsl:call-template>
              <xsl:text>&quot;&gt;previous&lt;/a&gt; </xsl:text>
            </xsl:when>
              
            <xsl:otherwise>
              <xsl:text>&lt;span class="grayed"&gt;previous&lt;/span&gt;</xsl:text>
            </xsl:otherwise>
          </xsl:choose>
            
          <xsl:text> </xsl:text>
    
          <xsl:choose>
            <xsl:when test="position()!=last()">
              <xsl:text>&lt;a href=&quot;</xsl:text>
                <xsl:call-template name="next_href">
                  <xsl:with-param name="key" select="title"/>
                </xsl:call-template>
              <xsl:text>&quot;&gt;next&lt;/A&gt;</xsl:text>
            </xsl:when>
            
            <xsl:otherwise>
              <xsl:text>&lt;span class="grayed"&gt;next&lt;/span&gt;</xsl:text>
            </xsl:otherwise>
          </xsl:choose>
  
          <xsl:call-template name="h1">
            <xsl:with-param name="header"><xsl:apply-templates select="title"/></xsl:with-param>
          </xsl:call-template>
      
          <xsl:text>&lt;BR&gt;</xsl:text>
          <xsl:value-of select="$endl"/>
          <xsl:apply-templates select="short_description"/>
    
          <xsl:call-template name="h2">
            <xsl:with-param name="header">Syntax</xsl:with-param>
          </xsl:call-template>
    
          <xsl:text>
          &lt;table bgcolor=#ccffff width=100%&gt;
          &lt;tr&gt;&lt;td&gt;&lt;pre&gt;
          </xsl:text>
  
          <xsl:apply-templates select="syntax"/>
    
          <xsl:text>
          &lt;/pre&gt;&lt;/td&gt;&lt;/tr&gt;
          &lt;/table&gt;
          </xsl:text>
    
          <xsl:call-template name="h2">
            <xsl:with-param name="header">Description</xsl:with-param>
          </xsl:call-template>
            
          <xsl:apply-templates select="description"/>
          <xsl:text>&lt;br&gt;</xsl:text>
      
          <!-- new style "See also" -->
          <xsl:if test="seealso">
            <xsl:apply-templates select="seealso"/>
          </xsl:if>
    
          <!-- old style "See also" -->
          <xsl:if test="see">
          <xsl:call-template name="seealso"/>
          </xsl:if>
    
          <xsl:text>
          &lt;/div&gt;
          
          &lt;/body&gt;
          &lt;/html&gt;
          </xsl:text>  
        </xsl:document> 
      </xsl:for-each> <!-- End "for each command" -->
    </xsl:for-each> <!-- End "cocoa_commands/chapter_letter" -->
  
  
  
    <!-- ################## iteration through CHAPTERS ###################### -->
    <xsl:for-each select="manual_parts/part">
      <xsl:variable name="part_num" select="6"/>
        
      <xsl:for-each select="chapter">
        <xsl:sort select="title"/>
        <xsl:variable name="chap_num" select="position()"/>
    
        <xsl:for-each select="section">
          <xsl:variable name="sect_num" select="position()"/>
          <xsl:variable name="file">
            <xsl:call-template name="create_href"><xsl:with-param name="key" select="title"/></xsl:call-template>
          </xsl:variable>
    
          <xsl:document href="{$file}">
    
  
            <xsl:text>
              &lt;HTML&gt;
              &lt;HEAD&gt;
              &lt;link REL="stylesheet" TYPE="text/css" href="gui.css"&gt;
            </xsl:text>
    
            <xsl:text>&lt;TITLE&gt;</xsl:text>
            <xsl:apply-templates select="title"/>
            <xsl:text>&lt;/TITLE&gt;</xsl:text>
    
            <xsl:text>
              &lt;/HEAD&gt;
              &lt;BODY bgcolor=#eeffff&gt;
              &lt;DIV&gt;
            </xsl:text>
    
            <!--<xsl:choose>
              <xsl:when test="$part_num=3">
                <xsl:text>&lt;A HREF=&quot;</xsl:text> 
                <xsl:value-of select="concat('toc_cocoal.html#p',$part_num,'c',$chap_num)"/>
                <xsl:text>&quot;&gt;up&lt;/A&gt;</xsl:text>
                <xsl:text> </xsl:text>
              </xsl:when>
              <xsl:when test="$part_num=4">
                <xsl:text>&lt;A HREF=&quot;#</xsl:text> 
                <xsl:value-of select="concat('toc_cmdref.html#p',$part_num,'c',$chap_num)"/>
                <xsl:text>&quot;&gt;up&lt;/A&gt;</xsl:text>
                <xsl:text> </xsl:text>                
              </xsl:when>
              <xsl:otherwise>
                <xsl:text>&lt;A HREF=&quot;</xsl:text> 
                <xsl:value-of select="concat('toc_cmdref.html#p',$part_num,'c',$chap_num)"/>
                <xsl:text>&quot;&gt;up&lt;/A&gt;</xsl:text>
                <xsl:text> </xsl:text>
              </xsl:otherwise>
            </xsl:choose>-->
            
            <xsl:text>&lt;A HREF=&quot;</xsl:text> 
            <xsl:value-of select="concat('toc_doingmath.html#p',$part_num,'c',$chap_num)"/>
            <xsl:text>&quot;&gt;up&lt;/A&gt;</xsl:text>
            <xsl:text> </xsl:text>            
    
            <xsl:choose>
              <xsl:when test="position()!=1">
                <xsl:text>&lt;A HREF=&quot;</xsl:text>
                
                <xsl:call-template name="previous_href">
                  <xsl:with-param name="key" select="title"/>
                </xsl:call-template>
    
                <xsl:text>&quot;&gt;previous&lt;/A&gt;</xsl:text>
              </xsl:when>
  
              <xsl:otherwise>
                <xsl:text>&lt;span class="grayed"&gt;previous&lt;/span&gt;</xsl:text>
              </xsl:otherwise>
            </xsl:choose>
              
            <xsl:text> </xsl:text>
    
            <xsl:choose>
              <xsl:when test="position()!=last()">
                <xsl:text>&lt;A HREF=&quot;</xsl:text>
    
                <xsl:call-template name="next_href">
                  <xsl:with-param name="key" select="title"/>
                </xsl:call-template>
    
                <xsl:text>&quot;&gt;next&lt;/A&gt;</xsl:text>
              </xsl:when>
                
              <xsl:otherwise>
                <xsl:text>&lt;span class="grayed"&gt;next&lt;/span&gt;</xsl:text>
              </xsl:otherwise>
            </xsl:choose>
    
            <xsl:call-template name="h1">
              <xsl:with-param name="header">
                <xsl:value-of select="concat($part_num,'.',$chap_num,'.',$sect_num,' ')"/>
                  
                <xsl:apply-templates select="title"/>
              </xsl:with-param>
            </xsl:call-template>
    
            <xsl:apply-templates select="description"/>
  
            <!-- new style "See also" -->
            <xsl:if test="seealso">
              <xsl:apply-templates select="seealso"/>
            </xsl:if>
  
            <xsl:text>
              &lt;/DIV&gt;
              &lt;/BODY&gt;
              &lt;/HTML&gt;
            </xsl:text>
          </xsl:document>
        </xsl:for-each> <!-- End "for each section -->
      </xsl:for-each> <!-- End "for each chapter" -->
    </xsl:for-each> <!-- End " for each manual_parts/part" -->
    
    
    
    <!-- #####################################################################
         ### Table of contents: Doing Mathematics with ApCoCoA ###############
         ##################################################################### -->
    <xsl:variable name="file_toc_doingmath" select="'toc_doingmath.html'"/>
    
    <xsl:document href="{$file_toc_doingmath}">
      
      <xsl:text>
        &lt;HTML&gt;
        &lt;HEAD&gt;
        &lt;link REL="stylesheet" TYPE="text/css" href="gui.css"&gt;
      </xsl:text>
      
      <xsl:text>&lt;TITLE&gt;</xsl:text>
      <xsl:text>Doing Mathematics with ApCoCoA: table of contents</xsl:text>
      <xsl:text>&lt;/TITLE&gt;</xsl:text>
      
      <xsl:text>
        &lt;/HEAD&gt;
        &lt;BODY bgcolor=#eeffff&gt;
        &lt;A NAME=&quot;top&quot;&gt;&lt;/A&gt;
      </xsl:text>
           
      
      <!-- #################### table of contents: Parts ################### -->
      <xsl:for-each select="/help/manual_parts/part">
        
            <xsl:variable name="part_num" select="6"/>
            
            <xsl:text>&lt;DIV CLASS=P</xsl:text>
            <xsl:value-of select="$part_num"/>
            <xsl:text>&gt;</xsl:text>
            <xsl:value-of select="$endl"/>
            <xsl:text>&lt;A NAME=&quot;</xsl:text>
            <xsl:value-of select="$part_num"/>
            <xsl:text>&quot;&gt;&lt;/A&gt;</xsl:text>
            
            <xsl:call-template name="h1">
              <xsl:with-param name="header">
                <xsl:text>Part </xsl:text><xsl:value-of select="$part_num"/>
                <xsl:text>:  </xsl:text><xsl:value-of select="title"/>
              </xsl:with-param>
            </xsl:call-template>
            
            <xsl:for-each select="chapter">
              <xsl:sort select="title"/>
              <xsl:variable name="chap_num" select="position()"/>             
              
              <xsl:text>&lt;A NAME=&quot;</xsl:text>
              <xsl:value-of select="concat('p',$part_num,'c',$chap_num)"/>
              <xsl:text>&quot;&gt;&lt;/A&gt;</xsl:text>
              <xsl:text>&lt;A NAME=&quot;</xsl:text>
              <xsl:apply-templates select="title"/>
              <xsl:text>&quot;&gt;&lt;/A&gt;</xsl:text>
              
              <xsl:call-template name="h3">
                <xsl:with-param name="header">
                  <xsl:text>Part </xsl:text><xsl:value-of select="$part_num"/>
                  <xsl:text> - </xsl:text>
                  <xsl:text>Chapter </xsl:text><xsl:value-of select="$chap_num"/>
                  <xsl:text> - </xsl:text>
                  <xsl:apply-templates select="title"/>
                </xsl:with-param>
              </xsl:call-template>
              
              <xsl:value-of select="$endl"/>
              <xsl:text>&lt;table bgcolor=#ccffff width=100%&gt;&lt;tr&gt;&lt;td&gt;</xsl:text>
              
              <xsl:for-each select="section">
                <xsl:variable name="sect_num" select="position()"/>
                
                <xsl:value-of select="$endl"/>
                <xsl:text>&lt;A NAME=&quot;</xsl:text>
                <xsl:value-of select="title"/>
                <xsl:text>&quot;&gt;&lt;/A&gt;</xsl:text>
                <xsl:value-of select="$endl"/>
                
                <xsl:value-of select="$part_num"/><xsl:text>.</xsl:text>
                <xsl:value-of select="$chap_num"/><xsl:text>.</xsl:text>
                <xsl:value-of select="$sect_num"/><xsl:text>.</xsl:text>
                
                <xsl:text> &lt;A HREF=&quot;</xsl:text>
                <xsl:call-template name="create_href">
                  <xsl:with-param name="key" select="title"/>
                </xsl:call-template>
                <xsl:text>&quot;&gt;</xsl:text>
                <xsl:apply-templates select="title"/>
                <xsl:text>&lt;/A&gt;</xsl:text>
                
                <xsl:value-of select="$endl"/>
                <xsl:text>&lt;BR&gt;</xsl:text>
              </xsl:for-each> <!-- End "for each section" -->
              
              
              <xsl:text>
                &lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;
                &lt;I&gt;&lt;A HREF=&quot;#top&quot;&gt;Back to the top&lt;/A&gt;&lt;/I&gt;
                &lt;BR&gt;
              </xsl:text>
            </xsl:for-each> <!-- End "for each chapter" -->
            
            <xsl:text>
              &lt;/DIV&gt;
              &lt;BR&gt;
            </xsl:text>
      </xsl:for-each> <!-- End "for each /help/manual_parts/part" -->
      
      <xsl:text>
        &lt;/DIV&gt;
        &lt;/BODY&gt;
        &lt;/HTML&gt;
      </xsl:text>      
    </xsl:document> <!-- End toc_doingmath.html -->      
  
    
    
    <!-- #####################################################################
         ### Table of contents: The ApCoCoA command reference ###
         ##################################################################### -->
    <xsl:variable name="file_toc_cmdref" select="'toc_cmdref.html'"/>
    
    <xsl:document href="{$file_toc_cmdref}">
      
      <xsl:text>
        &lt;HTML&gt;
        &lt;HEAD&gt;
        &lt;link REL="stylesheet" TYPE="text/css" href="gui.css"&gt;
      </xsl:text>
      
      <xsl:text>&lt;TITLE&gt;</xsl:text>
      <xsl:text>The ApCoCoA command reference: table of contents</xsl:text>
      <xsl:text>&lt;/TITLE&gt;</xsl:text>
      
      <xsl:text>
        &lt;/HEAD&gt;
        &lt;BODY bgcolor=#eeffff&gt;
        &lt;A NAME=&quot;top&quot;&gt;&lt;/A&gt;
      </xsl:text>
           
      
      <!-- ################### alphabetical list of COMMANDS ################ -->
      
      <xsl:text>
        &lt;A NAME=&quot;alphalist&quot;&gt;&lt;/A&gt;
        &lt;DIV CLASS=commands&gt;
      </xsl:text>
      
      <xsl:call-template name="h1">
        <xsl:with-param name="header">
          <xsl:text>Alphabetical list of commands</xsl:text>
        </xsl:with-param>
      </xsl:call-template>
      
      <xsl:call-template name="h2">
        <xsl:with-param name="header">
          <xsl:for-each select="/help/cocoa_commands/chapter_letter">
            <xsl:if test="not(string-length(title)>1)">
              <xsl:text>&lt;A HREF=&quot;#</xsl:text>
              <xsl:value-of select="title"/>
              <xsl:text>&quot;&gt;</xsl:text>
              <xsl:value-of select="title"/>
              <xsl:text>&lt;/A&gt;</xsl:text>
            </xsl:if>
          </xsl:for-each>
        </xsl:with-param>
      </xsl:call-template>
      
      <xsl:for-each select="cocoa_commands/chapter_letter">
        <xsl:text>&lt;A NAME=&quot;</xsl:text>
        <xsl:value-of select="title"/>
        <xsl:text>&quot;&gt;&lt;H3&gt;</xsl:text>
        <xsl:value-of select="title"/>
        <xsl:text>&lt;/H3&gt;&lt;/A&gt;</xsl:text>
        <xsl:value-of select="$endl"/>
        
        <xsl:text>&lt;table bgcolor=#ccffff width=100%&gt;&lt;tr&gt;&lt;td&gt;</xsl:text>
        
        <xsl:for-each select="command">
          <xsl:sort select="title"/>
          
          <xsl:value-of select="$endl"/>
          <xsl:text>&lt;A NAME=&quot;</xsl:text>
          <xsl:value-of select="title"/>
          <xsl:text>&quot;&gt;&lt;/A&gt;</xsl:text>
          <xsl:value-of select="$endl"/>
          
          <xsl:text>&lt;A HREF=&quot;</xsl:text>
          <xsl:call-template name="create_href">
            <xsl:with-param name="key" select="title"/>
          </xsl:call-template>
          <xsl:text>&quot;&gt;</xsl:text>
          
          <xsl:text>&lt;B&gt;</xsl:text>
          <xsl:apply-templates select="title"/>
          <xsl:text>&lt;/B&gt;&lt;/A&gt;</xsl:text>
          
          <xsl:text> -- </xsl:text>
          
          <xsl:apply-templates select="short_description"/>
          
          <xsl:value-of select="$endl"/>
          <xsl:text>&lt;BR&gt;</xsl:text>
        </xsl:for-each> <!-- End "for each command" -->
        
        <xsl:text>
          &lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;
          &lt;BR&gt;
          &lt;I&gt;
          &lt;A HREF=&quot;#top&quot;&gt;Back to the top&lt;/A&gt; ---
          &lt;A HREF=&quot;#alphalist&quot;&gt;Back to the alphabet&lt;/A&gt;
          &lt;/I&gt;
          &lt;BR&gt;
        </xsl:text>
      </xsl:for-each> <!-- End "for each cocoa_commands/chapter_letter" -->
      
      <xsl:text>
        &lt;/DIV&gt;
        &lt;/BODY&gt;
        &lt;/HTML&gt;
      </xsl:text>      
    </xsl:document> <!-- End toc_cmdref.html -->      
    
    
   <!-- #####################################################################
         ### Wordlist #######################################################
         ##################################################################### -->
    <xsl:variable name="file_wordlist" select="'apcocoa_wordlist.txt'"/>
    
    <xsl:document href="{$file_wordlist}">
      
      <xsl:for-each select="cocoa_commands/chapter_letter">
              
        <xsl:for-each select="command">
          <xsl:sort select="title"/>

          <xsl:value-of select="title"/>
          <xsl:text>&#xa;</xsl:text>
        </xsl:for-each> <!-- End "for each command" -->
      </xsl:for-each> <!-- End "for each cocoa_commands/chapter_letter" -->
          
    </xsl:document> <!-- End apcocoa_wordlist.txt -->        
  </xsl:template>
  
  
  
  
  
  <!-- ######################################################################
      ####################### TEMPLATES #################################### 
      ###################################################################### -->
  
  <!-- Template itemize -->
  <xsl:template match="itemize" name="itemize">
    <xsl:text>&lt;ul&gt;</xsl:text>
    <xsl:value-of select="$endl"/>
  
    <xsl:for-each select="item">
      <xsl:text>&lt;li&gt;</xsl:text>  
      <xsl:apply-templates/>
      <xsl:value-of select="$endl"/>
    </xsl:for-each>
  
    <xsl:text>&lt;/ul&gt;</xsl:text>
  </xsl:template> <!-- End Template see also -->  
  
  
  <!-- Template see also -->
  <xsl:template match="seealso" name="seealso">
    <xsl:call-template name="h2">
      <xsl:with-param name="header">See Also</xsl:with-param>
    </xsl:call-template>
  
    <xsl:text>&lt;ul&gt;</xsl:text>
    <xsl:value-of select="$endl"/>
  
    <xsl:for-each select="see">
      <xsl:text>&lt;li&gt;&lt;a href=&quot;</xsl:text>  
      <xsl:call-template name="create_href">
        <xsl:with-param name="key" select="normalize-space(.)"/>
      </xsl:call-template>
              
      <xsl:text>&quot;&gt;</xsl:text>  
      <xsl:value-of select="."/>
      <xsl:text>&lt;/A&gt;</xsl:text> 
      <xsl:value-of select="$endl"/>
    </xsl:for-each>
  
    <xsl:text>&lt;/ul&gt;</xsl:text>
  </xsl:template> <!-- End Template see also -->
  
  
  <!-- Template "commands and functions for" -->
  <xsl:template match="commands_and_functions_for">
    <xsl:text>
      &lt;CENTER&gt;
      &lt;TABLE BORDER=1&gt;
    </xsl:text>
  
    <xsl:variable name="my_type" select="@type"/>
    
    <xsl:for-each select="//command[type=$my_type]|//command[types/type=$my_type]">
      <xsl:sort select="title"/>
  
      <xsl:text>
        &lt;TR bgcolor=#ffffff&gt;
        &lt;TD&gt;
      </xsl:text>
  
      <xsl:text>&lt;A HREF=&quot;</xsl:text>
      
      <xsl:call-template name="create_href">
        <xsl:with-param name="key" select="title"/>
      </xsl:call-template>
      
      <xsl:text>&quot;&gt;</xsl:text>
      <xsl:value-of select="title"/>
      <xsl:text>&lt;/A&gt;</xsl:text>
  
      <xsl:text>
        &lt;/TD&gt;
        &lt;TD&gt;
      </xsl:text>
  
      <xsl:apply-templates select="short_description"/>
  
      <xsl:text>
        &lt;/TD&gt;
        &lt;/TR&gt;
      </xsl:text>
    </xsl:for-each>
  
    <xsl:text>
      &lt;/TABLE&gt;
      &lt;/CENTER&gt;
      &lt;BR&gt;&lt;BR&gt;
    </xsl:text>
  </xsl:template> <!-- End Template "commands and functions for" -->
  
  
  <!-- Template "commands and functions for package" -->
  <xsl:template match="commands_and_functions_for_package">
    <xsl:text>
      &lt;CENTER&gt;
      &lt;TABLE BORDER=1&gt;
    </xsl:text>
    
    <xsl:variable name="my_type" select="@type"/>
    
    <xsl:for-each select="//command[wiki-category=$my_type]">
      <xsl:sort select="title"/>
      
      <xsl:text>
        &lt;TR bgcolor=#ffffff&gt;
        &lt;TD&gt;
      </xsl:text>
      
      <xsl:text>&lt;A HREF=&quot;</xsl:text>
      
      <xsl:call-template name="create_href">
        <xsl:with-param name="key" select="title"/>
      </xsl:call-template>
      
      <xsl:text>&quot;&gt;</xsl:text>
      <xsl:value-of select="title"/>
      <xsl:text>&lt;/A&gt;</xsl:text>
      
      <xsl:text>
        &lt;/TD&gt;
        &lt;TD&gt;
      </xsl:text>
      
      <xsl:apply-templates select="short_description"/>
      
      <xsl:text>
        &lt;/TD&gt;
        &lt;/TR&gt;
      </xsl:text>
    </xsl:for-each>
    
    <xsl:text>
      &lt;/TABLE&gt;
      &lt;/CENTER&gt;
      &lt;BR&gt;&lt;BR&gt;
    </xsl:text>
  </xsl:template> <!-- End Template "commands and functions for" -->  
  
  
  <!-- Template CoCoA-Version -->
  <xsl:template match="coc_version">
    <xsl:value-of select="/help/version/cocoa_version"/>
  </xsl:template>
  
  
  <!-- Template em -->
  <xsl:template match="em">&lt;B&gt;<xsl:value-of select="."/>&lt;/B&gt;</xsl:template>
  
  
  <!-- Template quotes -->
  <xsl:template match="quotes">&quot;<xsl:apply-templates/>&quot;</xsl:template>
  
  
  <!-- Template sup -->
  <xsl:template match="sup">&lt;sup&gt;<xsl:apply-templates/>&lt;/sup&gt;</xsl:template>
  
  
  <!-- Template Latex-formula -->
  <xsl:template match="formula">&lt;font color=#0000aa&gt;<xsl:apply-templates/>&lt;/font&gt;</xsl:template>
  
  
  <!-- Template tt -->
  <xsl:template match="tt">&lt;tt&gt;<xsl:apply-templates/>&lt;/tt&gt;</xsl:template>
  
  
  <!-- Template less or equal -->
  <xsl:template match="less_eq">&amp;lt;=</xsl:template>
  
  
  <!-- Template times symbol -->
  <xsl:template match="times"> x </xsl:template>
  
  
  <!-- Template underscore -->
  <xsl:template match="underscore">_</xsl:template>

  
  <!-- Template ref -->
  <xsl:template match="ref">
    <xsl:text>&lt;A HREF=&quot;</xsl:text>
    <xsl:call-template name="create_href">
      <xsl:with-param name="key" select="."/>
    </xsl:call-template>
    <xsl:text>&quot;&gt;</xsl:text>
    <xsl:value-of select="."/>
    <xsl:text>&lt;/A&gt;</xsl:text>
  </xsl:template> <!-- End Template ref -->
  
  
  <!-- Template ttref -->
  <xsl:template match="ttref">
    <xsl:text>&lt;A HREF=&quot;</xsl:text>
    <xsl:call-template name="create_href">
      <xsl:with-param name="key" select="."/>
    </xsl:call-template>
    <xsl:text>&quot;&gt;</xsl:text>
      &lt;tt&gt;<xsl:value-of select="."/>&lt;/tt&gt;
    <xsl:text>&lt;/A&gt;</xsl:text>
  </xsl:template> <!-- End Template ttref -->
  
  
  <!-- Template paragraph -->
  <xsl:template match="par">&lt;BR&gt;&lt;BR&gt;</xsl:template>
  
  
  <!-- Template create example -->
  <xsl:template match="example">
    <xsl:text>&lt;BR&gt;</xsl:text>
    <xsl:value-of select="$endl"/>
  
    <xsl:call-template name="h2">
      <xsl:with-param name="header">Example</xsl:with-param>
    </xsl:call-template>
  
    <xsl:text>
      &lt;table bgcolor=#ccffff width=100%&gt;
      &lt;tr&gt;&lt;td&gt;
    </xsl:text>
  
    <xsl:text>&lt;pre&gt;</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>&lt;/pre&gt;</xsl:text>
  
    <xsl:text>
      &lt;/td&gt;&lt;/tr&gt;
      &lt;/table&gt;
    </xsl:text>
  </xsl:template> <!-- End Template example -->
  
  
  <!-- Template date -->
  <xsl:template match="coc_date">
    <xsl:value-of select="/help/version/date"/>
  </xsl:template>
  
  
  <!-- Template verbatim -->
  <xsl:template match="verbatim">&lt;pre&gt;<xsl:apply-templates/>&lt;/pre&gt;</xsl:template>
  
  
  <!-- Template h1 -->
  <xsl:template name="h1">
    <xsl:param name="header" select="A"/>
  
    <xsl:text>&lt;BR&gt;</xsl:text>
    <xsl:value-of select="$endl"/>
    <xsl:text>&lt;table bgcolor=#00dddd width=100%&gt;&lt;tr&gt;&lt;td&gt;</xsl:text>
    <xsl:value-of select="$endl"/>
  
    <xsl:text>&lt;font size=+3&gt;&lt;b&gt;</xsl:text>
    <xsl:value-of select="$header"/>
    <xsl:text>&lt;/b&gt;&lt;/font&gt;</xsl:text>
    <xsl:value-of select="$endl"/>
  
    <xsl:text>&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;</xsl:text>
  </xsl:template> <!-- End Template h1 -->
  
  
  <!-- Template h2 -->
  <xsl:template name="h2">
    <xsl:param name="header" select="A"/>
  
    <xsl:text>&lt;BR&gt;</xsl:text>
    <xsl:value-of select="$endl"/>
    <xsl:text>&lt;table bgcolor=#00dddd width=100%&gt;&lt;tr&gt;&lt;td&gt;</xsl:text>
    <xsl:value-of select="$endl"/>
  
    <xsl:text>&lt;font size=+2&gt;&lt;b&gt;</xsl:text>
    <xsl:value-of select="$header"/>
    <xsl:text>&lt;/b&gt;&lt;/font&gt;</xsl:text>
    <xsl:value-of select="$endl"/>
  
    <xsl:text>&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;</xsl:text>
  </xsl:template> <!-- End Template h2 -->
  
  
  <!-- Template h3 -->
  <xsl:template name="h3">
    <xsl:param name="header" select="A"/>
  
    <xsl:text>&lt;BR&gt;</xsl:text>
    <xsl:value-of select="$endl"/>
    <xsl:text>&lt;table bgcolor=#00dddd width=100%&gt;&lt;tr&gt;&lt;td&gt;</xsl:text>
    <xsl:value-of select="$endl"/>
  
    <xsl:text>&lt;font size=+1&gt;&lt;b&gt;</xsl:text>
    <xsl:value-of select="$header"/>
    <xsl:text>&lt;/b&gt;&lt;/font&gt;</xsl:text>
    <xsl:value-of select="$endl"/>
  
    <xsl:text>&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;</xsl:text>
  </xsl:template> <!-- End Template h3 -->
  
  
  <!-- ____ direct hyperlink references ___________________________ -->
  <!-- since we can't build associative arrays we'll have to collect key/value pairs for the links -->
  
  <!-- Variable command_hrefs -->
  <xsl:variable name="command_hrefs">
    <xsl:for-each select="//cocoa_commands/chapter_letter">
      <xsl:variable name="initial" select="title"/>
    
      <xsl:for-each select="command">
        <xsl:sort select="title"/>
        <link>
          <key><xsl:value-of select="title"/></key>
          <value>
            <xsl:choose>
              <xsl:when test="$initial != 'Symbol' and function-available('translate')">
                <xsl:value-of select="concat('cmd',translate(title,',.:/()- ',''),'.html')"/>
              </xsl:when>
              <xsl:otherwise>
                <xsl:value-of select="concat('cmd',$initial,position(),'.html')"/>
              </xsl:otherwise>
            </xsl:choose>
          </value>
        </link>
      </xsl:for-each>
    </xsl:for-each>
  </xsl:variable> <!-- End Variable command_hrefs -->
  
  
  <!-- Variable parts_hrefs -->
  <xsl:variable name="parts_hrefs">
    <xsl:for-each select="//manual_parts/part">
      <xsl:variable name="part_num" select="position()"/>
      
      <xsl:for-each select="chapter">
        <xsl:variable name="chap_num" select="position()"/>
        
        <xsl:for-each select="section">
          <xsl:variable name="sect_num" select="position()"/>
          <link>
            <key><xsl:value-of select="title"/></key>
            <value>
              <xsl:choose>
                <xsl:when test="function-available('translate')">
                  <xsl:value-of select="concat('part',translate(title,',.:/()- ',''),'.html')"/>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="concat('p',$part_num,'c',$chap_num, 's',$sect_num,'.html')"/>
                </xsl:otherwise>
              </xsl:choose>
            </value>
          </link>
        </xsl:for-each>
      </xsl:for-each>
    </xsl:for-each>
  </xsl:variable> <!-- End Variable parts_hrefs -->
  
  
  <!-- Template create_href -->
  <xsl:template name="create_href">
    <xsl:param name="key"/> <!-- it is assumed that the key is unique... -->
    
    <xsl:choose>
      <xsl:when test="$command_hrefs/*[key=$key]">
        <xsl:for-each select="$command_hrefs/*[key=$key]">
          <xsl:value-of select="value"/>
        </xsl:for-each>
      </xsl:when>
      <xsl:when test="$parts_hrefs/*[key=$key]">
        <xsl:for-each select="$parts_hrefs/*[key=$key]">
          <xsl:value-of select="value"/>
        </xsl:for-each>
      </xsl:when>
      <xsl:otherwise>
        <xsl:text>../cocoa/toc.html#</xsl:text>
        <xsl:value-of select="$key"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template> <!-- End Template create href -->
  
  
  <!-- Template previous_href -->
  <xsl:template name="previous_href">
    <xsl:param name="key"/>
    <xsl:choose>
      <xsl:when test="$command_hrefs/*[key=$key]">
        <xsl:for-each select="$command_hrefs/*[key=$key]">
          <xsl:value-of select="preceding-sibling::*[1]/value"/>
        </xsl:for-each>
      </xsl:when>
      <xsl:when test="$parts_hrefs/*[key=$key]">
        <xsl:for-each select="$parts_hrefs/*[key=$key]">
          <xsl:value-of select="preceding-sibling::*[1]/value"/>
        </xsl:for-each>
      </xsl:when>
    </xsl:choose>
  </xsl:template> <!-- End Template previous_href -->
  
  
  <!-- Template next_href -->
  <xsl:template name="next_href">
    <xsl:param name="key"/>
    
    <xsl:choose>
      <xsl:when test="$command_hrefs/*[key=$key]">
        <xsl:for-each select="$command_hrefs/*[key=$key]">
          <xsl:value-of select="following-sibling::*[1]/value"/>
        </xsl:for-each>
      </xsl:when>
      <xsl:when test="$parts_hrefs/*[key=$key]">
        <xsl:for-each select="$parts_hrefs/*[key=$key]">
          <xsl:value-of select="following-sibling::*[1]/value"/>
        </xsl:for-each>
      </xsl:when>
    </xsl:choose>
  </xsl:template>  <!-- End Template next_href -->

</xsl:stylesheet>
