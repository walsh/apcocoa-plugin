<?php

$options = array('help', 'recursive');

$MW='/home/apcocoa/htdocs/mediawiki-1.11.0/maintenance/';
require_once "$MW/commandLine.inc";
$wgShowExceptionDetails = true;

if (count($args) < 1 || isset($options['help'])) {
  showHelp();
  exit;
}

$filename = isset($args[1]) ? $args[1] : 'php://stdin';
$file = fopen($filename, 'w');
if (!$file) {
  print "Unable to open file, exiting\n";
  exit;
}

$dbw = wfGetDB(DB_MASTER);
$categories = array($args[0]);
$articles   = array();

// find subcategories and articles
while ($category = array_shift($categories)) {
  $result = $dbw->select(
    'categorylinks',
    'cl_from',
    'cl_to="'.$category.'"',
    'dumpManual',
    array('ORDER BY' => 'cl_sortkey')
  );

  while ($row = $dbw->fetchObject($result)) {
    $t = Title::newFromID($row->cl_from);

    if ($t->getNamespace() === NS_CATEGORY) {
      if (isset($options['recursive']))
        $categories[] = $t->getDBkey();
      continue;
    }

    $articles[] = $t->getPrefixedDBkey();
  }
}

// write CoCoA XML header
fwrite($file, "<help>\n");
fwrite($file, "<cocoa_commands>\n");

if (count($articles)) {
  // sort articles by title
  sort($articles);

  // open first chapter_letter
  $t1 = Title::newFromDBkey(reset($articles));
  $lastletter = strtoupper(substr($t1->getBaseText(), 0, 1));
  fwrite($file, "<chapter_letter>\n");
  fwrite($file, "  <title>$lastletter</title>\n");

  // print out article contents
  foreach ($articles as $article) {
    $t = Title::newFromDBkey($article);
    $a = new Article($t);

    // check if the first letters match
    $thisletter = strtoupper(substr($t->getBaseText(), 0, 1));
    if ($thisletter != $lastletter) {
      fwrite($file, "</chapter_letter>\n");
      fwrite($file, "<chapter_letter>\n");
      fwrite($file, "  <title>$thisletter</title>\n");
      $lastletter = $thisletter;
    }

    // dump article contents
    fwrite($file, rtrim($a->getContent())."\n");
  }

  // close last chapter_letter
  fwrite($file, "</chapter_letter>\n");
}

// write CoCoA XML footer
fwrite($file, "</cocoa_commands>\n");
fwrite($file, "</help>\n");

fclose($file);

function showHelp() {
  echo("Export the contents of a CoCoAXML category.\n");
  echo("USAGE: php dumpManual.php <options> <category> [<filename>]\n\n");
  echo("<category> : Name of the CoCoAXML category\n");
  echo("<filename> : Path to the output file\n\n");
  echo("Options:\n\n");
  echo("--recursive\n\tDump contents of subcategories aswell\n");
  echo("--help\n\tShow this information\n");
  echo("\n");
}

?>