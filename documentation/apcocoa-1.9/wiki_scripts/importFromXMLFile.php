<?php

$options = array('help', 'nooverwrite', 'norc', 'dryrun');
$optionsWithArgs = array('ns', 'user', 'comment');

$MW='/home/apcocoa/htdocs/mediawiki-1.11.0/maintenance';
require_once "$MW/commandLine.inc";
$wgShowExceptionDetails = true;

echo("Import from XML File\n\n");

if (count($args) < 1 || isset($options['help'])) {
  showHelp();
  exit;
}

// check XML file
$filename = $args[0];
echo("Using '{$filename}'... ");
if (!is_readable($filename))
  die("can't be read\n");

// check user
$user = isset($options['user']) ? $options['user'] : 'Maintenance script';
echo("\nUsing username '{$user}'... ");
$user = User::newFromName($user);
if (!is_object($user))
  die("invalid username.\n");
$wgUser =& $user;

// check namespace
$namespace = NS_MAIN;
if (isset($options['ns'])) {
  echo("\nUsing namespace '{$options['ns']}'... ");
  $namespace = @Namespace::getCanonicalIndex(strtolower($options['ns']));
  if (!is_int($namespace))
    die("invalid namespace.\n");
}

// set edit parameters
$comment = isset($options['comment']) ? $options['comment'] : 'Importing XML file';
$flags = isset($options['norc']) ? EDIT_SUPPRESS_RC : 0;

// load XML commands
$doc = DOMDocument::load($filename);
$coms = $doc->getElementsByTagName("command");

echo("\n");
foreach($coms as $com) {
  $xmltitle = $com->getElementsByTagName("title")->item(0)->nodeValue;
  $title = Title::makeTitleSafe($namespace, $xmltitle);
  echo("Importing '".$title->getFullText()."'... ");

  // check title object
  if (!is_object($title)) {
    echo("invalid title.\n");
    continue;
  }
  // check article
  if ($title->exists() && isset($options['nooverwrite'])) {
    echo("page exists.\n");
    continue;
  }

  $wgTitle =& $title;
  $content = trim($doc->saveXML($com));

  // find indentation
  $lastline = substr(strrchr($content, "\n"), 1);
  $indent = str_repeat(" ", strspn($lastline, " "));

  // edit article
  echo("\nPerforming edit... ");
  $article = new Article($title);
  if (!isset($options['dryrun']))
    $article->doEdit($indent.$content, $comment, $flags);

  echo("done.\n");
}

function showHelp() {
  echo("Import the contents of the CoCoA XML file commands into a wiki.\n");
  echo("USAGE: php importFromXMLFile.php <options> <filename>\n\n");
  echo("<filename> : Path to the CoCoA XML file\n\n");
  echo("Options:\n\n");
  echo("--ns <namespace>\n\tTarget namespace\n");
  echo("--user <user>\n\tUser to be associated with the edit\n");
  echo("--comment <comment>\n\tEdit summary\n");
  echo("--dryrun\n\tDon't commit any changes\n");
  echo("--nooverwrite\n\tDon't overwrite existing content\n");
  echo("--norc\n\tDon't update recent changes\n");
  echo("--help\n\tShow this information\n");
  echo("\n");
}

?>