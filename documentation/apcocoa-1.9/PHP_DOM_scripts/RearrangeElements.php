<?php

$xmlfile = 'CoCoAHelp.xml';
$doc = DOMDocument::load($xmlfile);
$xpath = new DOMXPath($doc);

$order = array("title","short_description","syntax","description","see","type","key");

// move elements inside a "command" to support the order given above
foreach($doc->getElementsByTagName("command") as $com) {
  $new = $doc->createElement("command");

  // select all children with a specific name
  foreach($order as $elem)
    foreach($xpath->query($elem, $com) as $e) {
      $new->appendChild($doc->createTextNode("\n"));
      if ($elem != "syntax")
        $new->appendChild($doc->createTextNode("    "));
      $new->appendChild($e->cloneNode(true));
    }

  $new->appendChild($doc->createTextNode("\n  "));
  $com->parentNode->replaceChild($new, $com);
}

// less memory intensive than DOMDocument->saveXML()
$doc->save('php://output');

?>