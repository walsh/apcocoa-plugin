<?php

$xmlfile = 'CoCoAHelp.xml';
$doc = DOMDocument::load($xmlfile);

$linebreak = $doc->createTextNode("\n");
$coms = $doc->getElementsByTagName("command");

foreach($coms as $com) {
  $title = preg_replace("/[^A-Za-z0-9]/", "", $com->getElementsByTagName("title")->item(0)->nodeValue);
  file_put_contents("out/Cmd$title", "  ".$doc->saveXML($com));
}

?>