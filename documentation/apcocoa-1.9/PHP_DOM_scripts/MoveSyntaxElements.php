<?php

$xmlfile = 'CoCoAHelp.xml';
$doc = DOMDocument::load($xmlfile);

$linebreak = $doc->createTextNode("\n");
$syns = $doc->getElementsByTagName("syntax");

foreach($syns as $syn) {
  if ($syn->parentNode->nodeName != "command") {
    print "Found some strange node with parent node ".$syn->parentNode->nodeName."!\n";
    continue;
  }

  $searchnode = $syn;
  while ($searchnode->previousSibling->nodeName != "short_description") {
    $searchnode = $searchnode->previousSibling;
  }

  $syn->parentNode->insertBefore($linebreak->cloneNode(true), $searchnode);
  $syn->parentNode->insertBefore($syn->cloneNode(true), $searchnode);
  $syn->parentNode->removeChild($syn);
}

$search = array("<syntax>", "</syntax>");
$replace = array("    <syntax>", "    </syntax>");
echo str_replace($search, $replace, $doc->saveXML());

?>