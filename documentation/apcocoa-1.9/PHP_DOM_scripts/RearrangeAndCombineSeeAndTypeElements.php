<?php

$xmlfile = 'CoCoAHelp.xml';
$doc = DOMDocument::load($xmlfile);

// move elements to support the order given as parameter
function orderElements(&$doc, &$node, &$order) {
  $xpath = new DOMXPath($doc);
  $new = $doc->createElement($node->nodeName);

  $indent = ($node->nodeName == "command") ? "  " : "      ";
  // select all children with a specific name
  foreach($order as $elem) {
    if ($elem == "see") {
      $newSeeContainer = $doc->createElement("seealso");

      $sees = $xpath->query("see", $node);
      if ($sees->length < 1) continue;
      foreach($sees as $s) {
        $newSeeContainer->appendChild($doc->createTextNode("\n    ".$indent));
        $newSeeContainer->appendChild($s->cloneNode(true));
      }

      $new->appendChild($doc->createTextNode("\n  ".$indent));
      $newSeeContainer->appendChild($doc->createTextNode("\n  ".$indent));
      $new->appendChild($newSeeContainer);
      continue;
    }
    if ($elem == "type") {
      $newTypeContainer = $doc->createElement("types");

      $sees = $xpath->query("type", $node);
      if ($sees->length < 1) continue;
      foreach($sees as $s) {
        $newTypeContainer->appendChild($doc->createTextNode("\n    ".$indent));
        $newTypeContainer->appendChild($s->cloneNode(true));
      }

      $new->appendChild($doc->createTextNode("\n  ".$indent));
      $newTypeContainer->appendChild($doc->createTextNode("\n  ".$indent));
      $new->appendChild($newTypeContainer);
      continue;
    }

    // else
    foreach($xpath->query($elem, $node) as $e) {
      $new->appendChild($doc->createTextNode("\n"));
      switch ($elem) {
        case "syntax":
          // don't indent
          $new->appendChild($e->cloneNode(true));
          break;
        case "description":
          // fix indentation (it's looking weird for section elements)
          $clone = $e->cloneNode(true);
          if ($clone->lastChild->nodeType == XML_TEXT_NODE)
            $clone->lastChild->nodeValue = rtrim($clone->lastChild->nodeValue);
          $clone->appendChild($doc->createTextNode("\n  ".$indent));
          $new->appendChild($doc->createTextNode("  ".$indent));
          $new->appendChild($clone);
          break;
        default:
          $new->appendChild($doc->createTextNode("  ".$indent));
          $new->appendChild($e->cloneNode(true));
      }
    }
  }

  $new->appendChild($doc->createTextNode("\n".$indent));
  $node->parentNode->replaceChild($new, $node);
}

$order = array("title","short_description","syntax","description","see","type","key");
foreach($doc->getElementsByTagName("command") as $com) orderElements($doc, $com, $order);
foreach($doc->getElementsByTagName("section") as $sec) orderElements($doc, $sec, $order);

// less memory intensive than DOMDocument->saveXML()
$doc->save('php://output');

?>