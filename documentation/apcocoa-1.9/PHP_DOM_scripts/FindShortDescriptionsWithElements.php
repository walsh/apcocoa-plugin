<?php

$xmlfile = 'CoCoAHelp.xml';
$doc = DOMDocument::load($xmlfile);

$xpath = new DOMXPath($doc);
$hasElements = $xpath->query("//short_description[*]");

foreach($hasElements as $shortdesc) {
  print "The command ".$shortdesc->parentNode->getElementsByTagName("title")->item(0)->nodeValue." has a short description which contains additional elements.\n";
}

?>