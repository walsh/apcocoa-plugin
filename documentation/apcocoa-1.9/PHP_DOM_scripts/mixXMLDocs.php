<?php

if ($argc < 2 || in_array($argv[1], array('--help', '-help', '-h', '-?'))) {
  echo("Update the contents of a CoCoA XML file with data in other XML files.\n");
  echo("USAGE: php ".basename(__FILE__)." <xmlfile> [<xmlupdates> ...]\n\n");
  echo("<xmlfile> :\tPath to the CoCoA XML file\n");
  echo("<xmlupdates> :\tPath to the update files which use the same XML structure\n\n");
  echo("(can only update cocoa_commands right now)\n\n");
  exit;
}

// input validation
foreach ($argv as $file) {
  if (!is_file($file))
    die("$file is not a file.\n");
  if (!is_readable($file))
    die("$file cannot be read.\n");
}

// load original XML
$original = DOMDocument::load($argv[1]);
$o_xpath = new DOMXPath($original);

// perform updates
//
// okay, because of the structure in the CoCoAHelp.xml I have to specialize here:
// if cocoa_commands would not need chapter_letter's or if those would use an
// attribute instead of the title element, I could do this in an abstract way
for ($arg = 2; @$file = $argv[$arg]; ++$arg) {
  $update = DOMDocument::load($file);
  $u_xpath = new DOMXPath($update);

  $chapters = $update->getElementsByTagName("chapter_letter");

  foreach ($chapters as $u_chapter) {
    $chapter_letter = $u_xpath->query("title", $u_chapter)->item(0)->nodeValue;

    // update XPaths (this is important! :P)
    $o_xpath = new DOMXPath($original);

    // find chapter_letter element in original document, if possible
    $o_chapter = $o_xpath->query("//chapter_letter[title='".$chapter_letter."']");

    // TODO: I have to add a new chapter_letter container
    if (!$o_chapter->length) {
      print ("No chapter_letter container for '$chapter_letter' found, ignoring");
      continue;
    }
    $o_chapter = $o_chapter->item(0);

    // update all commands in there
    foreach ($u_xpath->query("command", $u_chapter) as $u_command) {
      $u_title = $u_xpath->query("title", $u_command)->item(0)/*->nodeValue*/;
      $u_command = $original->importNode($u_command, true);

      // update XPaths (this is important! :P)
      $o_xpath = new DOMXPath($original);

      // find command element with the same title, if possible
      // checking this is faster than waiting for strcmp = 0
      $o_command = $o_xpath->query("command[title='{$u_title->nodeValue}']", $o_chapter);

      // original exists: replace it
      if ($o_command->length) {
        $o_command = $o_command->item(0);
        $o_chapter->replaceChild($u_command, $o_command);
        continue;
      }

      // get list of commands for that letter
      $commandlist = $o_xpath->query("command", $o_chapter);

      // find right position to insert, I assume the list is sorted
      foreach ($commandlist as $o_command) {
        $o_title = $o_xpath->query("title", $o_command)->item(0)/*->nodeValue*/;

        // compare the entire contents of the title tag, including XML tags - ugh
        $cmp = strcmp($original->saveXML($o_title), $update->saveXML($u_title));
        if ($cmp > 0) {
          // found an element with "bigger" title, inserting before
          $o_chapter->insertBefore($u_command, $o_command);
          continue 2;
        }
        elseif ($cmp = 0) {
          // found an element with matching title, replacing
          // this can (only) happen if the title element includes extra XML tags
          $o_chapter->replaceChild($u_command, $o_command);
          continue 2;
        }
      }

      // insert at the end
      $o_chapter->appendChild($u_command);
    }
  }
}

echo $original->saveXML();

?>