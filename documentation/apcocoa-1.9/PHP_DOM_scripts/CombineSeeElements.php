<?php

$xmlfile = 'CoCoAHelp.xml';
$doc = DOMDocument::load($xmlfile);

$xpath = new DOMXPath($doc);
$haveSeeElements = $xpath->query("//*[see]");

foreach($haveSeeElements as $parent) {
  // select all see elements that are direct child nodes (no non-child descendants)
  $xpath = new DOMXPath($doc);
  $mySees = $xpath->query('see', $parent);

  switch ($mySees->length) {
    // no see elements: do nothing (shouldn't happen!)
    case 0:
      print "Somehow ".$parent->getElementsByTagName("title")->item(0)->nodeValue." got selected\n";
      continue(2);
    // one see element: just align the data
    case 1:
      $mySees->item(0)->nodeValue = "\n      ".trim($mySees->item(0)->nodeValue);
      continue(2);
  }

  // more than one see element: combine them
  $seestr = "\n      ";
  $seearr = array();
  foreach($mySees as $see)
    $seearr[] = $see->nodeValue;
  $seestr .= implode($seestr, $seearr);

  $new = $doc->createElement("see", $seestr);
  $parent->insertBefore($new, $mySees->item(0));

  // remove all see's and all the text (whitespace) in-between
  for ($node = $mySees->item(0), $next = $node->nextSibling;
       !$next->isSameNode($mySees->item($mySees->length-1));
       $node = $next, $next = $node->nextSibling)
    if ($node->nodeName == "see" or $node->nodeType == XML_TEXT_NODE)
      $parent->removeChild($node);
    else
      print "Something is fishy at ".$parent->getElementsByTagName("title")->item(0)->nodeValue."\n";

  $parent->removeChild($node);
  $parent->removeChild($next);

}

$search = array("<see>", "</see>");
$replace = array("\n    <see>", "\n    </see>");
echo str_replace($search, $replace, $doc->saveXML());

?>