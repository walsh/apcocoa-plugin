<?php

$xmlfile = 'CoCoAHelp.xml';
$doc = DOMDocument::load($xmlfile);
$xpath = new DOMXPath($doc);

// print out the parent element names for all see elements
foreach($doc->getElementsByTagName("see") as $com) {
  print $com->parentNode->nodeName;
  print " --see--> ";
  print $com->nodeValue;
  print "\n";
}

?>