
-- Num.SavGol
MEMORY.Doc.Syntax := "
SavGol(Points: MAT, MaxDiffOrd:INT, Nl:INT, Nr:INT, Deg:INT, Mode, StepWidth:RAT)

";
MEMORY.Doc.Descr := "
*Please note:* The function(s) explained on this page is/are using the *ApCoCoAServer*. You will have to start the ApCoCoAServer in order to use it/them.

This command computes numerical derivatives of given data series assuming that the data points are equally spaced.

Each data series is considered as a column of the matrix Points. It is assumed that the spacing StepWidth is the same for all columns. The polynomial degree \"Deg\" must be less than the window size \"Nl+Nr+1\". The derivatives are computed up to order \"MaxDiffOrd\".
The resulting matrix has \"Nl+Nr\" rows less than Points and has \"MaxDiffOrd+1\" times more columns as Points. The first \"MaxDiffOrd+1\" columns correspond to the 0-th, 1-st,...,\"MaxDiffOrd\"-th derivatives of the first column of Points, and so on.

* @param *Points* The data points for which numerical derivatives are computed, each data series is considered as a column.
* @param *MaxDiffOrd* The order up to which derivatives are computed.
* @param *Nl* Number of left data points to be considered.
* @param *Nr* Number of right data points to be considered.
* @param *Deg* Maximum degree of the interpolation polynomal. Must be greater or equal MaxOrd.

* @return A matrix that represents the derivatives of Points up to order \"MaxDiffOrd\".

//==========================  EXAMPLE  ==========================\\

Points := Mat([[ Fn.SinN(I/2, 10), Fn.CosN(I/2, 10) ] | I In 0..40 ]);

MaxDiffOrd := 3;
Nl := 4;
Nr := 4;
Deg := 3;
Mode := 0; -- Mode is currently not used!
StepWidth := 0.5;

Result := Num.SavGol(Points, MaxDiffOrd, Nl, Nr, Deg, Mode, StepWidth);
Dec(Result, 2); ]])

-- (Result) --
Mat([
  [0.84, 0.40, 0.63, 0.31, 0.38, 0.87, 0.29, 0.69]
  [0.55, 0.77, 0.41, 0.61, 0.74, 0.57, 0.56, 0.45]
[...]

\\==========================  o=o=o=o  ===========================//

";
    