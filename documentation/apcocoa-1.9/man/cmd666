
-- Slinalg.SEF
MEMORY.Doc.Syntax := "
Slinalg.SEF(NRow:INT, NCol:INT, M:LIST): LIST 
Slinalg.SEF_v2(NRow:INT, NCol:INT, M:LIST): LIST 
";
MEMORY.Doc.Descr := "
*Please note:* The function(s) explained on this page is/are using the *ApCoCoAServer*. You will have to start the ApCoCoAServer in order to use it/them.

Both functions compute the row echelon form of a sparse matrix. The first one performs usual gaussian elimination. The second one collects all the rows which contain the pivot element and then select the row with fewer number of non zero elements to perform elimination. Therefore, the second one is more efficient than the first one. 


* @param *NRow*: Number of rows of the matrix.
* @param *NCol*: Number of Columns of the matrix.
* @param *M*: List of lists containing positions of non zero elements.
* @return A list of lists containing the row echelon form of the matrix M.

//==========================  EXAMPLE  ==========================\\

Use ZZ/(2)[x];
NRow:=10;
NCol:=13;
M := [[1, 2, 6, 7],
      [1, 2, 4, 5, 6], 
      [2, 3], 
      [2, 3, 10, 11], 
      [2, 4, 6, 7, 9, 10], 
      [2, 10, 11, 13], 
      [5, 6, 8],
      [ 6, 8, 9,10,12],
      [6, 10, 12], 
      [10, 13]];

Slinalg.SEF(NRow, NCol, M);
[[1,2,6,7],
 [2,3],
 [3,4,6,7,9,10],
 [4,5,7],
 [5,6,8],
 [6,8,9,10,12],
 [8,9,11,13],
 [10,11],
 [11,13]]
	
-------------------------------
\\==========================  o=o=o=o  ===========================//

//==========================  EXAMPLE  ==========================\\

Use ZZ/(2)[x];
NRow:=10;
NCol:=13;
M := [[1, 2, 6, 7],
      [1, 2, 4, 5, 6], 
      [2, 3], 
      [2, 3, 10, 11], 
      [2, 4, 6, 7, 9, 10], 
      [2, 10, 11, 13], 
      [5, 6, 8],
      [ 6, 8, 9,10,12],
      [6, 10, 12], 
      [10, 13]];

Slinalg.SEF_v2(NRow, NCol, M);
[[1, 2, 6, 7],
 [2, 3],
 [3, 10, 11, 13],
 [4, 5, 7],
 [5, 6, 8],
 [6, 10, 12],
 [8, 9],
 [10, 11],
 [11, 13]]

-------------------------------
\\==========================  o=o=o=o  ===========================//

";
    