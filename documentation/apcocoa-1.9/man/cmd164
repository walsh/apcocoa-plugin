
-- Dec
MEMORY.Doc.Syntax := "
Dec(X:Object, Digits:INT):Object
";
MEMORY.Doc.Descr := "

This function prints (rational) numbers within the object \"X\" as a floating point number up to \"Digits\" Digits. The floating point numbers are represented as strings.


* @param *X* The object whose numbers should be printed as floating points. \"X\" can be \"MAT\", \"LIST\", \"POLY\", \"INT\", \"RAT\" or iteration thereof.
* @param *Digits* Gives the exactness of printing.
* @return The given object with floating point numbers represented as strings instead of (rational) numbers.

//==========================  EXAMPLE  ==========================\\

Use P::=Q[x,y,z];

Dec(1,4);
1
-------------------------------
Dec(1/2,4);
0.5
-------------------------------
A:=Mat([[17/13, 6/11], [5/3, 1/9]]);
Dec(A,4);
Mat([
  [\"1.3076\", \"0.5454\"],
  [\"1.6666\", \"0.1111\"]
])
-------------------------------
\\==========================  o=o=o=o  ===========================//

";
    