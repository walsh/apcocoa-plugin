
-- Bin
MEMORY.Doc.Syntax := "
Bin(N:INT or POLY, K:INT):INT
";
MEMORY.Doc.Descr := "

This function computes the binomial coefficient, *N choose K*
according to the formula

      (N)(N-1)(N-2)...(N-K+1)/ K!

The same formula is used if N is a polynomial.  The integer K cannot
be negative.

//==========================  EXAMPLE  ==========================\\

  Bin(4,2);
6
-------------------------------
  Bin(-4,3);
-20
-------------------------------
  Bin(x^2+2y,3);
1/6x^6 + x^4y - 1/2x^4 + 2x^2y^2 - 2x^2y + 4/3y^3 + 1/3x^2 - 2y^2 + 2/3y
-------------------------------
  It = (x^2+2y)(x^2+2y-1)(x^2+2y-2)/6;
True
-------------------------------
\\==========================  o=o=o=o  ===========================//

";
    