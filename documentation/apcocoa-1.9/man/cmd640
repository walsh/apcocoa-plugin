
-- SAT.ConvertToCNF
MEMORY.Doc.Syntax := "
SAT.ConvertToCNF(SPE:LIST, CuttingNumber:INT, QStrategy:INT, CStrategy:INT)
";
MEMORY.Doc.Descr := "

This function starts the conversion algorithm.


* @param *SPE*: A List containing the polynomial equations of the system.
* @param *CuttingNumber*: Maximal support-length of the linear polynomials when their corresponding CNF is written to the file. Could be 3 - 6. 
* @param *QStrategy*: Strategy for quadratic substitution. 0 - Standard; 1 - Linear Partner; 2 - Adv. Lin. Partner;
* @param *CStrategy*: Strategy for cubic substitution. 0 - Standard; 1 - Quadratic Partner;

//==========================  EXAMPLE  ==========================\\

-- quadratic system:
Use R::=ZZ/(2)[x[1..3]];
F1:= x[1]x[2] + x[1]x[3] + x[2]x[3] + x[3];
F2:= x[2] + 1;
F3:= x[1]x[2] + x[3];
SPE:=[F1,F2,F3]; 
SAT.ConvertToCNF(SPE,4,0,0);
SAT.LaunchMiniSat(\"sat.cnf\");
SAT.GetResult();
--Result: [0,1,0] Test with: Eval(SPE,[0,1,0]);
\\==========================  o=o=o=o  ===========================//

//==========================  EXAMPLE  ==========================\\

-- cubic system:
Use ZZ/(2)[x[1..3]];
F1:=x[1]x[2]x[3] + x[1]x[2] + x[2]x[3] + x[1] + x[3] +1;
F2:=x[1]x[2]x[3] + x[1]x[2] + x[2]x[3] + x[1] + x[2];
F3:=x[1]x[2] + x[2]x[3] + x[2];
SPE:=[F1,F2,F3];
SAT.ConvertToCNF(SPE,4,0,0);
SAT.LaunchMiniSat(\"sat.cnf\");
SAT.GetResult();
--Result: [0,0,1] Test with: Eval(SPE,[0,0,1]);
\\==========================  o=o=o=o  ===========================//

";
    