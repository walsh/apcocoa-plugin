
-- Introduction to Rational Functions
MEMORY.Doc.Descr := "

An object of type RATFUN in CoCoA represents a rational function,
i.e., a quotient of polynomials.  Each rational function is
represented as P/Q where P and Q are polynomials (of type POLY) and
deg(Q) > 0.  Common factors of the numerator and denominator are
automatically simplified.  At present, rational functions in CoCoA are
only available over a field.

//==========================  EXAMPLE  ==========================\\

  Use R ::= QQ[x,y];
  F := x/(x+y);  -- a rational function
  F*(x+y);
x
-------------------------------
  (x^2-y^2)/(x+y);  -- the result here is a polynomial
x - y
-------------------------------
\\==========================  o=o=o=o  ===========================//



The following algebraic operations on rational functions are supported:

  F^N, +F, -F, F*G, F/G, F+G, F-G,

where F, G are rational functions and N is an integer.

For details look up each item by name.  Online, try
\"?ItemName\" or \"H.Syntax(\"ItemName\")\".
    ";
    