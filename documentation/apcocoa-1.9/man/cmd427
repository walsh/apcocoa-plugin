
-- Memory
MEMORY.Doc.Syntax := "
Memory():TAGGED(\"Memory\")
Memory(R:RING):TAGGED(\"Memory\")
";
MEMORY.Doc.Descr := "

The first form of this function prints the contents of the working
memory, i.e, all non-global variables. The second form lists all
global variables bound to the ring R.

For more information about memory in CoCoA, see the chapter entitled
\"Memory Management\".

//==========================  EXAMPLE  ==========================\\

  Use R ::= QQ[x,y,z];
  I := Ideal(x-y^2, xy-z^3);
  X := 14;
  ENV.R.Y := 5;  --  a global variable bound to R
                 -- \"ENV.R\" is equivalent to \"MEMORY.ENV.R\"
  Use S ::= QQ[a,b];
  J := Ideal(a,b);
  ENV.S.Z := 7;
  Memory();
[\"I\", \"J\", \"X\"]
-------------------------------
  Memory(R);
[\"Y\"]
-------------------------------
\\==========================  o=o=o=o  ===========================//

";
    