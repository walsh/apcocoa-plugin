
-- Introduction to Rings
MEMORY.Doc.Descr := "

Polynomial rings play a central role in CoCoA. Indeed, every object
in CoCoA is defined over a base ring which is a polynomial ring. The
user can define many rings, but at any time a *current ring* is
active within the system.  Most commands use the current ring as the
base ring.

Once a ring has been defined, the system can handle the following
mathematical objects defined over that ring:

 * numbers (integers, rationals, modular integers);
 * polynomials;
 * vectors of polynomials;
 * rational functions;
 * ideals;
 * modules (submodules of a free module);
 * lists of objects;
 * matrices of objects.

Variables containing ring-dependent objects such as polynomials,
ideals, and modules are *labeled* by their ring.  Variables
containing objects such as integers which are not dependent on a
particular ring are not labeled.

IMPORTANT NOTE: Starting with CoCoA 3.5, variables are no longer local
to specific rings, i.e., all variables are accessible from all rings.

The next sections explains how to create a ring.
    ";
    