
-- NCo.GB
MEMORY.Doc.Syntax := "
NCo.GB(G:LIST[, DB:INT, LB:INT, OFlag:INT]):LIST
";
MEMORY.Doc.Descr := "

Given a two-sided ideal \"I\" and a word ordering \"Ordering\", a set of non-zero polynomials \"G\" is called a *Groebner basis* of \"I\" with respect to \"Ordering\" if the leading word set \"LW{G}\" generates the leading word ideal \"LW(I)\". Note that a two-sided ideal may not have finite Groebner bases.
*Please note:* The function(s) explained on this page is/are using the *ApCoCoAServer*. You will have to start the ApCoCoAServer in order to use it/them.

Please set ring environment *coefficient field*\" K\", *alphabet* (or set of indeterminates) \"X\" and *ordering* via the functions \"NCo.SetFp\", \"NCo.SetX\" and \"NCo.SetOrdering\", respectively, before using this function. The default coefficient field is \"Q\", and the default ordering is the length-lexicographic ordering (\"LLEX\"). For more information, please check the relevant functions.

* @param *G*: a LIST of non-zero polynomials generating a two-sided ideal in \"K<X>\". Each polynomial is represented as a LIST of monomials, which are LISTs of the form [C, W] where W is a word in \"<X>\" and C is the coefficient of W. For example, the polynomial \"f=xy-y+1\" is represented as F:=[[1,\"xy\"], [-1, \"y\"], [1,\"\"]].
* @return: a LIST of polynomials, which is a Groebner basis with respect to the current word ordering if there exists a finite Groebner basis and the enumerating procedure doesn't terminate due to reaching the degree bound \"DB\" or the loop bound LB, and is a partial Groebner basis otherwise.


About 3 optional parameters: for most cases we do not know whether or not there exists a finite Groebner basis beforehand. Thus, the function offers 3 optional parameters for the enumerating procedure. Note that at the moment *all* of the following 3 optional parameters must be used at the same time.

* @param *DB:* a positive INT, which gives a degree bound of S-polynomials (or obstructions) during the enumerating procedure. When the degree bound is reached, the procedure will be interrupted and return a partial Groebner basis.
* @param *LB:* a positive INT, which gives a loop bound of enumerating steps. When the LB-th enumerating step finishes, the procedure will be interrupted and return a partial Groebner basis.
* @param *OFlag:* a non-negative INT, which is a multi-switch for the output of the ApCoCoAServer. If OFlag=0, the server displaces nothing on the screen. If OFlag=1, the server prints basic information on the enumerating procedure, such as the number of enumerating steps has been proceeded, the number of elements in partial Groebner basis, the number of unselected obstructions; the total number of obstructions, the number of selected obstructions, and the number of unnecessary obstructions. If OFlag=2, besides the information as OFlag=1, the server also displays explicitly the elements in partial Groebner basis and the current selected S-polynonial. Note that the initial idea of using OFlag is to trace and debug the enumerating procedure.

//==========================  EXAMPLE  ==========================\\

NCo.SetX(\"xyzt\"); 
F1 := [[1,\"xx\"], [-1,\"yx\"]];
F2 := [[1,\"xy\"], [-1,\"ty\"]];
F3 := [[1,\"xt\"], [-1, \"tx\"]];
F4 := [[1,\"yt\"], [-1, \"ty\"]];
G := [F1, F2,F3,F4]; 
NCo.GB(G); -- over Q (default field), LLEX ordering (default ordering)

[[[1, \"yt\"], [-1, \"ty\"]], [[1, \"xt\"], [-1, \"tx\"]], [[1, \"xy\"], [-1, \"ty\"]], 
[[1, \"xx\"], [-1, \"yx\"]], [[1, \"tyy\"], [-1, \"tty\"]], [[1, \"yyx\"], [-1, \"tyx\"]], 
[[1, \"ttyy\"], [-1, \"ttty\"]], [[1, \"tyyx\"], [-1, \"ttyx\"]]]
-------------------------------
NCo.SetFp(); -- set default Fp=F2
NCo.GB(G); -- over F2, LLEX ordering

[[[1, \"yt\"], [1, \"ty\"]], [[1, \"xt\"], [1, \"tx\"]], [[1, \"xy\"], [1, \"ty\"]], 
[[1, \"xx\"], [1, \"yx\"]], [[1, \"tyy\"], [1, \"tty\"]], [[1, \"yyx\"], [1, \"tyx\"]], 
[[1, \"ttyy\"], [1, \"ttty\"]], [[1, \"tyyx\"], [1, \"ttyx\"]]]
-------------------------------
NCo.SetFp(3);
NCo.GB(G); -- over F3, LLEX ordering

[[[1, \"yt\"], [2, \"ty\"]], [[1, \"xt\"], [2, \"tx\"]], [[1, \"xy\"], [2, \"ty\"]], 
[[1, \"xx\"], [2, \"yx\"]], [[1, \"tyy\"], [2, \"tty\"]], [[1, \"yyx\"], [2, \"tyx\"]], 
[[1, \"ttyy\"], [2, \"ttty\"]], [[1, \"tyyx\"], [2, \"ttyx\"]]]
-------------------------------
NCo.SetX(\"txyz\"); 
NCo.SetOrdering(\"ELIM\"); -- ELIM will eliminate t, x, y, z one after another
Gb:=NCo.GB(G);
NCo.FindPolynomials(\"xyz\",Gb); -- compute GB of the intersection of <G> and F3<x,y,z>

[[[1, \"xx\"], [2, \"yx\"]], [[1, \"xyx\"], [2, \"yyx\"]], [[1, \"xyy\"], [2, \"yxy\"]]]
-------------------------------
\\==========================  o=o=o=o  ===========================//

";
    