
-- GLPK.RIPCSolve
MEMORY.Doc.Syntax := "
GLPK.RIPCSolve(F:LIST, Rule1:INT, Rule2:INT, MinMax:STRING):LIST
";
MEMORY.Doc.Descr := "
*Please note:* The function(s) explained on this page is/are using the *ApCoCoAServer*. You will have to start the ApCoCoAServer in order to use it/them.

This function finds one solution (if exists) in \"F_2^n\" of a system of polynomial equations over the field \"F_2\". This function uses Integer Polynomial Conversion (IPC) along with some standard rules, for linearizein 0-1 nonlinear polynomial functions into 0-1 linear polynomials, to model the solution of system F as a mixed integer linear programming problem. The linearization involves adding two kind of 0-1 linear constraints. Rule1 and Rule2 handle the first and the second kind of constriants respectively. Afterwards, the mixed integer linear programming problem is solved using glpk. Finally, an inverse conversion is applied to obtain the solution of the system F. 



* @param *F*: A List containing the polynomials of the given system.
* @param *Rule1*: Strategy for the first kind of constraints. 0 - standard; and 1 - non-standard;
* @param *Rule2*: Strategy for the second kind of constraints. 0 - standard; and 0 to 3 - non-standard;
* @param *MinMax*: Optimization direction i.e. minimization (\"Min\") or maximization (\"Max\").
* @return A list containing a zero of the polynomial system F.

//==========================  EXAMPLE  ==========================\\

Use Z/(2)[x[1..4]];
F:=[
    x[1]x[2] + x[2]x[3] + x[2]x[4] + x[3]x[4] + x[1] + x[3] + 1, 
    x[1]x[2] + x[1]x[3] + x[1]x[4] + x[3]x[4] + x[2] + x[3] + 1, 
    x[1]x[2] + x[1]x[3] + x[2]x[3] + x[3]x[4] + x[1] + x[4] + 1, 
    x[1]x[3] + x[2]x[3] + x[1]x[4] + x[2]x[4] + 1
    ];

Rule1:=0;
Rule2:=0;
MinMax:=\"Max\";

-- Then we compute the solution with

GLPK.RIPCSolve(F, Rule1, Rule2, MinMax);

-- The result will be the following:
Input ok...
Modelling the system as a mixed integer programming problem. 
Rule1: 0, Rule2: 0.
Model is ready to solve with GLPK...

Solution Status: INTEGER OPTIMAL
Value of objective function: 2

[0, 1, 0, 1]
-------------------------------
\\==========================  o=o=o=o  ===========================//

//==========================  EXAMPLE  ==========================\\

Use S::=Z/(2)[x[1..5]];
F:=[
 x[1]x[5] + x[3]x[5] + x[4]x[5] + x[1] + x[4],
 x[1]x[2] + x[1]x[4] + x[3]x[4] + x[1]x[5] + x[2]x[5] + x[3]x[5] + x[1] + x[4] + x[5] + 1,
 x[1]x[2] + x[4]x[5] + x[1] + x[2] + x[4],
 x[1]x[4] + x[3]x[4] + x[2]x[5] + x[1] + x[2] + x[4] + x[5] + 1,
 x[1]x[4] + x[2]x[4] + x[3]x[4] + x[2]x[5] + x[4]x[5] + x[1] + x[2] + x[4] + x[5]
];


Rule1:=1;
Rule2:=0;
MinMax:=\"Max\";

-- Then we compute the solution with

GLPK.RIPCSolve(F, Rule1, Rule2, MinMax);

-- The result will be the following:
Input ok...
Modelling the system as a mixed integer programming problem. 
Rule1: 1, Rule2: 0.
Model is ready to solve with GLPK...
Solution Status: INTEGER OPTIMAL
Value of objective function: 4

[1, 1, 1, 1, 0]
-------------------------------
\\==========================  o=o=o=o  ===========================//

//==========================  EXAMPLE  ==========================\\

Use ZZ/(2)[x[1..3]];
F := [ x[1]x[2]x[3] + x[1]x[2] + x[2]x[3] + x[1] + x[3] +1,
       x[1]x[2]x[3] + x[1]x[2] + x[2]x[3] + x[1] + x[2],
       x[1]x[2] + x[2]x[3] + x[2]
     ];


Rule1:=0;
Rule2:=1;
MinMax:=\"Max\";

-- Then we compute the solution with

GLPK.RIPCSolve(F, Rule1, Rule2, MinMax);

-- The result will be the following:
Input ok...
Modelling the system as a mixed integer programming problem. 
Rule1: 0, Rule2: 1.
Model is ready to solve with GLPK...

Solution Status: INTEGER OPTIMAL
Value of objective function: 1

[0, 0, 1]
-------------------------------
\\==========================  o=o=o=o  ===========================//

";
    