
-- IsInRadical
MEMORY.Doc.Syntax := "
IsInRadical(F:POLY, I:IDEAL):BOOL
IsInRadical(J:IDEAL, I:IDEAL):BOOL
";
MEMORY.Doc.Descr := "

This function tests whether the first argument, a polynomial or an
ideal, is contained in the radical of the second argument, an ideal.


This function is faster than asking \"F IsIn Radical(I);\".

//==========================  EXAMPLE  ==========================\\

  Use QQ[x,y,z];
  I := Ideal(x^6y^4, z);
  IsInRadical(xy, I);
True
-------------------------------
  IsInRadical(Ideal(x,y), I);
False
-------------------------------
  MinPowerInIdeal(xy, I);
6
-------------------------------
\\==========================  o=o=o=o  ===========================//

";
    