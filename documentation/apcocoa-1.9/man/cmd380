
-- Latte.Count
MEMORY.Doc.Syntax := "
Latte.Count(Equations: LIST, LesserEq: LIST, GreaterEq: LIST):INT
Latte.Count(Equations: LIST, LesserEq: LIST, GreaterEq: LIST, Dil: INT):INT
";
MEMORY.Doc.Descr := "
*Please note:* The function(s) explained on this page is/are using the *ApCoCoAServer*. You will have to start the ApCoCoAServer in order to use it/them.


* @param *Equations*: A list of linear polynomials, which are equivalent to the equality-part of the polyhedral constraints
* @param *LesserEq*: A list of linear polynomials, which are equivalent to the lower or equal-part of the polyhedral constraints
* @param *GreaterEq*: A list of linear polynomials, which are equivalent to the greater or equal-part of the polyhedral constraints
* @return The number of lattice points in the given polyhedral P 


The following parameter is optional:

* @param *Dil*: Integer > 0, factor for dilation of the polyhedral P, to count the lattice points of the polyhedral n*P

*IMPORTANT:* If the given polyhedral is unbound, the output of LattE is zero, as for an empty polyhedral.

//==========================  EXAMPLE  ==========================\\

Use S ::= QQ[x,y];
Equations := [];
LesserEq := [1/2*x-1, x+1/3y-1];
GreaterEq := [x,y];
Latte.Count(Equations, LesserEq, GreaterEq);

5
-------------------------------
\\==========================  o=o=o=o  ===========================//

";
    