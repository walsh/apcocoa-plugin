
-- Weyl.WRedGB
MEMORY.Doc.Syntax := "
Weyl.WRedGB(GB:LIST):LIST
";
MEMORY.Doc.Descr := "

This function converts Groebner basis \"GB\" computed by ApCoCoAServer into the reduced Groebner Basis. If \"GB\" is not a Groebner basis then the output will not be the reduced Groebner basis. In fact, this function reduces a list \"GB\" of Weyl polynomials using \"Weyl.WNR\" into a new list \"L\" such that \"Ideal(L) = Ideal(GB)\".
*Note:* This function is faster than \"Weyl.WRGB\" for a list \"GB\" of large size.

* @param *GB* Groebner Basis of an ideal in the Weyl algebra.
* @result The reduced Groebner Basis of the given ideal.

//==========================  EXAMPLE  ==========================\\

A1::=QQ[x,d];	--Define appropriate ring
Use A1;
L:=[x,d,1];
Weyl.WRedGB(L);
[1]
-------------------------------
-- Done.
-------------------------------
\\==========================  o=o=o=o  ===========================//

//==========================  EXAMPLE  ==========================\\

A2::=ZZ/(7)[x[1..2],y[1..2]]; -- define appropriate ring
Use A2;
I:=Ideal(2x[1]^14y[1]^7,x[1]^2y[1]^3+x[1]^2-1,y[2]^7-1,x[2]^3y[2]^2-x[2]y[2]-3x[2]-1);
GbI:=Weyl.WGB(I,0);Len(GbI);

-------------------------------
-- CoCoAServer: computing Cpu Time = 0.485
-------------------------------
42   -- size of complete GB of the ideal I
-------------------------------
Time GbI:=Weyl.WRedGB(GbI);

Cpu time = 10.89, User time = 11
-------------------------------
11  -- GbI is now reduced Groebner Basis of the ideal I.
-------------------------------
-- Done.
-------------------------------
\\==========================  o=o=o=o  ===========================//

";
    