
-- Introduction to Records
MEMORY.Doc.Descr := "

A record is a data type in CoCoA representing a list of bindings of
the form *name to object*.

//==========================  EXAMPLE  ==========================\\

  Use R ::= QQ[x,y,z];
  P := Record[ I := Ideal(x,y^2-z), F := x^2 + y, Misc := [1,3,4]];
  P.I;
Ideal(x, y^2 - z)
-------------------------------
  P.F;
x^2 + y
-------------------------------
  P.Misc;
[1, 3, 4]
-------------------------------
  P.Misc[2];
3
-------------------------------
  P.Date := \"1/1/98\";
  P;
Record[Date := \"1/1/98\", F := x^2 + y, I := Ideal(x, y^2 - z), Misc := [1, 3, 4]]
-------------------------------
  P[\"I\"];  -- equivalent to P.I
Ideal(x, y^2 - z)
-------------------------------
  P[\"Misc\",3];  -- equivalent to P.Misc[3]
4
-------------------------------
\\==========================  o=o=o=o  ===========================//



Each entry in a record is called a *field*.  Note that records are
*open* in the sense that their fields can be extended, as shown in
the previous example.  At present, there is no function for deleting
fields from a record, one must rewrite the record, selecting the
fields to retain:

//==========================  EXAMPLE  ==========================\\

  P := Record[A := 2, B := 3, C := 5, D := 7];
  Q := Record[];

  Foreach F In Fields(P) Do
    If F <> \"C\" Then Q.Var(F) := P.Var(F) EndIf; -- \"Q.F\" would not work here
  EndForeach;
  P := Q;
  Delete Q;  -- get rid of the variable Q

  P;
Record[A := 2, B := 3, D := 7]
-------------------------------
\\==========================  o=o=o=o  ===========================//

";
    