
-- NC.LC
MEMORY.Doc.Syntax := "
NC.LC(F:LIST):INT or RAT
";
MEMORY.Doc.Descr := "
*Please note:* The function(s) explained on this page is/are using the *ApCoCoAServer*. You will have to start the ApCoCoAServer in order to use it/them.

Please set non-commutative polynomial ring (via the command \"Use\") and word ordering (via the function \"NC.SetOrdering\") before calling this function. The default word ordering is the length-lexicographic ordering (\"LLEX\"). For more information, please check the relevant commands and functions.

* @param *F*: a non-zero non-commutative polynomial. Each polynomial is represented as a LIST of LISTs, and each element in every inner LIST involves only one indeterminate or none (a constant). For example, the polynomial \"f=2x[2]y[1]x[2]^2-9y[2]x[1]^2x[2]^3+5\" is represented as F:=[[2x[1],y[1],x[2]^2], [-9y[2],x[1]^2,x[2]^3], [5]]. The zero polynomial \"0\" is represented as the empty LIST [].
* @return: an INT or a RAT, whhich is the leading coefficient of F with respect to the current word ordering.

//==========================  EXAMPLE  ==========================\\

USE QQ[x[1..2]];
F:= [[x[1]^2], [2x[1],x[2]], [3x[2],x[1]],[4x[2]^2]]; -- x[1]^2+2x[1]x[2]+3x[2]x[1]+4x[2]^2
NC.SetOrdering(\"LLEX\");
NC.LC(F);

[1]
-------------------------------
NC.SetOrdering(\"LRLEX\");
NC.LC(F);

[4]
-------------------------------
NC.SetOrdering(\"ELIM\");
NC.LC(F);

[1]
-------------------------------
NC.SetOrdering(\"DEGRLEX\");
NC.LC(F);

[1]
-------------------------------
\\==========================  o=o=o=o  ===========================//

";
    