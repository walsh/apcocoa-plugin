
-- DA.LD
MEMORY.Doc.Syntax := "
DA.LD(F:POLY):POLY
";
MEMORY.Doc.Descr := "
\"DA.LD\" computes the leading derivative of polynomial \"F\" wrt. the current differential term ordering, or the hereby induced ranking respectively.

* @param *F* A differential polynomial.
* @return The leading derivative of \"F\".

//==========================  EXAMPLE  ==========================\\

Use QQ[x[1..2,0..20]];
Use QQ[x[1..2,0..20]], Ord(DA.DiffTO(\"DegOrd\"));
F:=x[1,2]^2*x[1,1]-x[2,4]^3;
DA.LD(F);
-------------------------------
x[2,4]
-------------------------------
\\==========================  o=o=o=o  ===========================//

";
    