
-- Weyl.InIw
MEMORY.Doc.Syntax := "
Weyl.InIw(I:IDEAL,W:LIST):IDEAL
";
MEMORY.Doc.Descr := "
*Please note:* The function(s) explained on this page is/are using the *ApCoCoAServer*. You will have to start the ApCoCoAServer in order to use it/them.

Computes the initial ideal of a D-ideal \"I\" in the Weyl algebra D with respect to weight vector \"W:=[u,v]\" such that \"u+v > 0\". Here \"u=(u1,...,un)\" and \"v=(v1,...,vn)\" are weights of indeterminates \"[x1,...,xn]\" and \"[y1,...,yn]\" respectively. Note that \"InIw(P,W)\" is an ideal of graded ring of D with respect to weight vector \"W\". Due to limitations in CoCoA4, all \"u_i\" and \"v_i\" should be positive integers. Computation of initial ideal if \"u+v= 0\" is not implemented yet.

* @param *I* An ideal in the Weyl algebra.
* @param *W* A list of n positive integers, where n = number of indeterminates.
* @return An ideal, which is the initial ideal of \"I\" with respect to \"W\".

*Beta Warning:* This method, package or class is a beta version. It may not work as intended or its interface may change in the next version! So please be careful when you're intending to use it.

//==========================  EXAMPLE  ==========================\\

Use A4::=QQ[x[1..4],d[1..4]];
I4:=Ideal(d[2]d[3]-d[1]d[4],x[1]d[1]-x[4]d[4]-1,x[2]d[2]+x[4]d[4]+1,x[3]d[3]+x[4]d[4]+2);
Weyl.InIw(I4,[0,0,0,0,1,1,1,1]);  --This function is implemented only for positive weights!
ERROR: All weights shoud be strictly positive
CONTEXT: Error(\"All weights shoud be strictly positive\")
-------------------------------
Weyl.InIw(I4,[1,1,1,1,2,2,1,1]);
-- CoCoAServer: computing Cpu Time = 0.016
-------------------------------
Ideal(x[2]x[3]x[4]d[4]^2 - x[1]x[4]^2d[4]^2, x[1]x[4]d[3]d[4] + x[2]x[4]d[4]^2, x[1]x[4]d[2]d[4], x[2]d[1]d[4],
x[3]d[1]d[4] + x[4]d[2]d[4], x[1]d[1], x[2]d[2], x[3]d[3] + x[4]d[4], d[2]d[3] - d[1]d[4])
-------------------------------
Weyl.InIw(I4,[1,2,1,1,2,2,1,1]);
-- CoCoAServer: computing Cpu Time = 0.032
-------------------------------
Ideal(x[2]x[4]d[4]^2, x[1]x[4]d[2]d[4], x[2]d[1]d[4], x[3]d[1]d[4] + x[4]d[2]d[4], x[1]d[1], x[2]d[2],
x[3]d[3] + x[4]d[4], d[2]d[3] - d[1]d[4])
-------------------------------

Weyl.InIw(I4,[2,2,2,2,1,1,1,1]);
-- CoCoAServer: computing Cpu Time = 0.031
-------------------------------
Ideal(x[2]x[3]x[4]d[4]^2 - x[1]x[4]^2d[4]^2, x[1]x[4]d[3]d[4] + x[2]x[4]d[4]^2, x[1]x[4]d[2]d[4] + x[3]x[4]d[4]^2,
x[2]d[1]d[4] + x[4]d[3]d[4], x[3]d[1]d[4] + x[4]d[2]d[4], x[1]d[1] - x[4]d[4], x[2]d[2] + x[4]d[4],
x[3]d[3] + x[4]d[4], d[2]d[3] - d[1]d[4])
-------------------------------


\\==========================  o=o=o=o  ===========================//

";
    