
-- More
MEMORY.Doc.Syntax := "
More(S:STRING):NULL
More():NULL
";
MEMORY.Doc.Descr := "

The purpose of this function is to print the string S without
scrolling off of the screen.

The first form of this function stores the string S in a \"MoreDevice\",
then prints the first N lines from the MoreDevice where N is the
integer stored in the global variable MEMORY.MoreCount.  Subsequent
calls to \"More\" print the next N lines from the MoreDevice, (each time
removing the lines from the device) until the MoreDevice is empty.
After each call to \"More\" a line with \"More();\" is printed as long as
the MoreDevice is not empty.  This line is easily cut-and-pasted.

The user may set the number of lines to print with the command
\"MEMORY.MoreCount := X\", where X is an integer.  The default is value
is 20.

If E is *any* CoCoA object, the command \"Sprint(E)\"
converts E into a string.  The output may then be given
to \"More\" for printing.

To use the more device with the online help system, see
\"H.SetMore\", \"H.UnSetMore\".
    ";
    