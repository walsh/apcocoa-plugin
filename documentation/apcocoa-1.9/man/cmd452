
-- NC.Interreduction
MEMORY.Doc.Syntax := "
NC.Interreduction(G:LIST):LIST
";
MEMORY.Doc.Descr := "

Note that, given a word ordering, a set of non-zero polynomial \"G\" is called *interreduced* if, for all \"g\" in \"G\", no element of \"Supp(g)\" is a multiple of any element in \"LW{G\{g}}\".
*Please note:* The function(s) explained on this page is/are using the *ApCoCoAServer*. You will have to start the ApCoCoAServer in order to use it/them.

Please set non-commutative polynomial ring (via the command \"Use\") and word ordering (via the function \"NC.SetOrdering\") before calling this function. The default word ordering is the length-lexicographic ordering (\"LLEX\"). For more information, please check the relevant commands and functions.

* @param *G*: a LIST of non-commutative polynomials. Each polynomial is represented as a LIST of LISTs, and each element in every inner LIST involves only one indeterminate or none (a constant). For example, the polynomial \"f=2x[2]y[1]x[2]^2-9y[2]x[1]^2x[2]^3+5\" is represented as F:=[[2x[1],y[1],x[2]^2], [-9y[2],x[1]^2,x[2]^3], [5]]. The zero polynomial \"0\" is represented as the empty LIST [].
* @return: a LIST, which is an interreduced set of G.

//==========================  EXAMPLE  ==========================\\

USE QQ[x[1..2],y[1..2]];
NC.SetOrdering(\"LLEX\");
F1:= [[x[1],y[1],x[2]^2], [-9y[2],x[1]^2,x[2]^3],[5]]; -- x[1]y[1]x[2]^2-9y[2]x[1]^2x[2]^3+5
F2:= [[y[1],x[2]^2], [y[2],x[2]^2]]; -- y[1]x[2]^2+y[2]x[2]^2
F3:= [[x[1],y[1]],[x[2]]]; -- x[1]y[1]+x[2]
NC.Interreduction([F1,F2,F3]);

[[[y[2], x[1]^2, x[2]^3], [1/9x[1], y[2], x[2]^2], [-5/9]], [[y[1], x[2]^2], [y[2], x[2]^2]], [[x[1], y[1]], [x[2]]]]
-------------------------------
\\==========================  o=o=o=o  ===========================//

";
    