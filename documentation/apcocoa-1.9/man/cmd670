
-- SortBy
MEMORY.Doc.Syntax := "
SortBy(V:LIST, F:FUNCTION):NULL

where V is a variable containing a list and F is a boolean-valued
comparison function of two arguments (e.g. representing *less than*).
";
MEMORY.Doc.Descr := "

This function sorts the elements of the list in V with respect
to the comparisons made by F; it overwrites V and returns NULL.

The comparison function F takes two arguments and returns True if the
first argument is less than the second, otherwise it returns False.
The sorted list is in increasing order.

Note that if both F(A, B) and F(B, A) return True, then A and B are
viewed as being equal.

//==========================  EXAMPLE  ==========================\\

  Define ByLength(S, T)    -- define the sorting function
    Return Len(S) > Len(T);
  EndDefine;
  M := [\"dog\",\"mouse\",\"cat\"];
  SortBy(M, Function(\"ByLength\"));
  M;
[\"mouse\", \"dog\", \"cat\"]
-------------------------------
\\==========================  o=o=o=o  ===========================//

";
    