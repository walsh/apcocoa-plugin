
-- ImplicitPlot
MEMORY.Doc.Syntax := "
ImplicitPlot(F: POLY, Xrange: LIST, Yrange: LIST)
";
MEMORY.Doc.Descr := "

This function evaluates the first argument, a bivariate polynomial, at
a grid of points in the range given by the second and third arguments.
The coordinates of the approximate zeroes are output to a file
called \"CoCoAPlot\".
See \"ImplicitPlotOn\" for outputting to another file.

This result can be plotted using your preferred plotting program.
For example, start \"gnuplot\" and then give it the command
  plot \"CoCoAPlot\"
to see the plot.

//==========================  EXAMPLE  ==========================\\

  ImplicitPlot(x^2 + y^2 - 200^2, [-256,256], [-256,256]);
\\==========================  o=o=o=o  ===========================//

";
    