
-- BB.LiftHomND
MEMORY.Doc.Syntax := "
BB.LiftHomND(OO:LIST):LIST
";
MEMORY.Doc.Descr := "

This command computes the generators of the border basis scheme ideal \"I(B^hom_O)\" that result from the lifting of next-door (ND) neighbors. 

* @param *OO* A list of terms representing an order ideal. The second element is of type \"POLY\".
* @return A list of generators of the homogeneous border basis scheme ideal. The polynomials will belong to the ring \"BBS=K[c_{ij}]\".

//==========================  EXAMPLE  ==========================\\

Use QQ[x,y,z], DegRevLex;
BB.LiftHomND([Poly(1), x, y, xy]);

[BBS :: c[3,1]c[4,4] + c[2,1] - c[4,2],
 BBS :: c[2,1]c[4,5] + c[3,1] - c[4,3]]
-------------------------------
\\==========================  o=o=o=o  ===========================//

";
    