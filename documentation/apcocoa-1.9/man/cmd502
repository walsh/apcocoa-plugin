
-- NCo.MRInterreduction
MEMORY.Doc.Syntax := "
NCo.MRInterreduction(X:STRING, Ordering:STRING, Relations:LIST, G:LIST):LIST
";
MEMORY.Doc.Descr := "

Given a word ordering, a set \"G\" of non-zero polynomials is called *interreduced* if no element of \"Supp(g)\" is a multiply of any element in the leading word set \"MRLW{G\{g}}\" for all \"g\" in \"G\".
*Please note:* The function(s) explained on this page is/are using the *ApCoCoAServer*. You will have to start the ApCoCoAServer in order to use it/them.

Please set coefficient field via the function \"NCo.SetFp\" (or \"NCo.UnsetFp\") before calling this function. The default coefficient field is the field of rational numbers, i.e. RAT in CoCoAL. For more information, please check the relevant functions.

* @param *X:* a finite alphabet (or set of indeterminates). It is of STRING type. Note that every letter in X MUST appear only once.The order of letters in X induces word orderings.
* @param *Ordering:* a word ordering induced by the order of letters in X. It is a STRING, which is a shortened-form of the name of corresponding word ordering. Note that \"LLEX\" (the length-lexicographic ordering), \"ELIM\" (an elimination ordering) and \"LRLEX\" (the length-reverse-lexicographic ordering) are supported currently. See \"NCo.SetOrdering\" for more details.
* @param *Relations:* a finite set of relations. It is of LIST type. Each element in Relations is of the form [W1, W2], where W1 and W2 are words in \"<X>\". Each word is represented as a STRING. For example, the word \"xy^2x\" is represented as \"xyyx\", and the identity is represented as the empty string \"\". Thus, the relation \"(yx, xy)\" is represented as [\"yx\", \"xy\"], and the set of relations \"{(yx, xy),(zx,xz),(zy,yz)}\" is represented as [[\"yx\", \"xy\"],[\"zx\", \"xz\"],[\"zy\", \"yz\"]].
* @param *G:* a LIST of polynomials in the finitely generated monoid ring. Each polynomial is represented as a LIST of monomials, which are pairs of the form [C, W] where W is a word in \"<X>\" and C is the coefficient of W. For example, the polynomial \"f=xy-y+1\" is represented as F:=[[1,\"xy\"], [-1, \"y\"], [1,\"\"]]. The zero polynomial \"0\" is represented as the empty LIST [].
* @return: a LIST of interreduced polynomials.

//==========================  EXAMPLE  ==========================\\

X := \"abc\";
Ordering := \"LLEX\";
Relations := [[\"aa\",\"\"], [\"bb\",\"\"], [\"ab\",\"c\"], [\"ac\", \"b\"], [\"cb\", \"a\"]];
G:=[[[1,\"ba\"]], [[1,\"b\"],[1,\"\"]], [[1,\"c\"]]];
NCo.MRInterreduction(X, Ordering, Relations, G);

[[[1, \"a\"]], [[1, \"b\"], [1, \"\"]], [[1, \"c\"]]]
-------------------------------
\\==========================  o=o=o=o  ===========================//

";
    