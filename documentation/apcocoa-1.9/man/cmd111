
-- CharP.GBasisF8
MEMORY.Doc.Syntax := "
CharP.GBasisF8(Ideal:IDEAL):LIST
";
MEMORY.Doc.Descr := "
*Please note:* The function(s) explained on this page is/are using the *ApCoCoAServer*. You will have to start the ApCoCoAServer in order to use it/them.

This command computes a Groebner basis in the field \"F_8 = (Z/(2))[x]/(x^3 + x +1)\". 


* @param *Ideal* An Ideal in a Ring over \"Z\", where the elements \"0,...,7\" represent the elements of the field \"F_8\". For short, the binary representation of the number represents the coefficient vector if the polynomial in the field, e.g. \"11 = 8 + 2 + 1 = 2^3 + 2^1 + 2^0\". So the number \"11\" corresponds to the polynomial \"x^3 + x + 1\".
* @return A Groebner Basis of the given ideal.

//==========================  EXAMPLE  ==========================\\

Use R::=QQ[x,y,z];
I:=Ideal(x-y^2,x^2+xy,y^3);
GBasis(I);

[x^2 + xy, -y^2 + x, -xy]
-------------------------------
Use Z::=ZZ[x,y,z];
-- WARNING: Coeffs are not in a field
-- GBasis-related computations could fail to terminate or be wrong

-------------------------------
I:=Ideal(x-y^2,x^2+xy,y^3);
CharP.GBasisF8(I);
-- WARNING: Coeffs are not in a field
-- GBasis-related computations could fail to terminate or be wrong
-- CoCoAServer: computing Cpu Time = 0
-------------------------------
[y^2 + 4x, x^2, xy]
-------------------------------
\\==========================  o=o=o=o  ===========================//

";
    