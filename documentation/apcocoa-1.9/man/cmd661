
-- Shape
MEMORY.Doc.Syntax := "
Shape(E:LIST):LIST (of TYPE)
Shape(E:MAT):MAT (of TYPE)
Shape(E:RECORD):RECORD (of TYPE)
Shape(E:OTHER):TYPE

where OTHER stands for a type which is not LIST, MAT, or RECORD.
";
MEMORY.Doc.Descr := "

This function returns the extended list of types involved in the
expression E as outlined below:

Type(E) = LIST
  In this case, Shape(E) is the list whose i-th component is the type
  of  the i-th component of E.

Type(E) = MAT
  In this case, Shape(E) is a matrix with (i,j)-th entry equal to the
  type of the (i,j)-th entry of E.

Type(E) = RECORD
  In this case, Shape(E) is a record whose fields are the types of the
  fields of E.

Otherwise, \"Shape(E)\" is the type of E.

//==========================  EXAMPLE  ==========================\\

  Use R ::= QQ[x];
  L := [1,[1,\"a\"], x^2-x];
  Shape(L);
[INT, [INT, STRING], POLY]
-------------------------------
  R := Record[Name := \"test\", Contents := L];
  Shape(R);
Record[Contents := [INT, [INT, STRING], POLY], Name := STRING]
-------------------------------
  It.Name;
STRING
-------------------------------
\\==========================  o=o=o=o  ===========================//



There are undocumented functions, \"IsSubShape\" and \"IsSubShapeOfSome\",
for determining if the \"shape\" of a CoCoA expression is a \"subshape\"
of another.  To see the code for these functions, enter

  Describe Function(\"$misc.IsSubShape\");
  Describe Function(\"$misc.IsSubShapeOfSome\");
";
    