
-- NCo.IsGB
MEMORY.Doc.Syntax := "
NCo.IsGB(G:LIST):BOOL
";
MEMORY.Doc.Descr := "

Note that, given an ideal \"I\" and a word ordering \"Ordering\", a set of non-zero polynomials \"G\" is called a *Groebner basis* of \"I\" with respect to \"Ordering\" if the leading word set \"LW{G}\" generates the leading word ideal \"LW(I)\". The function checks whether a given finite LIST of non-zero polynomials \"G\" is a Groebner basis by using the \"Buchberger Criterion\", i.e. \"G\" is a Groebner basis if the S-polynomials of all obstructions have the zero normal remainder with respect to \"G\".
*Please note:* The function(s) explained on this page is/are using the *ApCoCoAServer*. You will have to start the ApCoCoAServer in order to use it/them.

Please set ring environment *coefficient field*\" K\", *alphabet* (or set of indeterminates) \"X\" and *ordering* via the functions \"NCo.SetFp\", \"NCo.SetX\" and \"NCo.SetOrdering\", respectively, before using this function. The default coefficient field is \"Q\", and the default ordering is the length-lexicographic ordering (\"LLEX\"). For more information, please check the relevant functions.

* @param *G*: a LIST of non-zero polynomials in \"K<X>\". Each polynomial is represented as a LIST of monomials, which are LISTs of the form [C, W] where W is a word in \"<X>\" and C is the coefficient of W. For example, the polynomial \"f=xy-y+1\" is represented as F:=[[1,\"xy\"], [-1, \"y\"], [1,\"\"]].
* @return: a BOOL, which is True if \"G\" is a Groebner basis with respect to the current word ordering and False otherwise.

//==========================  EXAMPLE  ==========================\\

NCo.SetX(\"xyt\");  
F1 := [[1,\"xx\"], [-1,\"yx\"]];   
F2 := [[1,\"xy\"], [-1,\"ty\"]];  
F3 := [[1,\"xt\"], [-1, \"tx\"]];  
F4 := [[1,\"yt\"], [-1, \"ty\"]];  
G := [F1, F2,F3,F4]; 
NCo.IsGB(G); -- LLEX ordering (default ordering)

False
-------------------------------
NCo.SetOrdering(\"ELIM\");
NCo.IsGB(G);

False
-------------------------------
\\==========================  o=o=o=o  ===========================//

";
    