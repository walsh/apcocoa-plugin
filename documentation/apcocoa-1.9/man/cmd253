
-- GB.GetNthSyz
MEMORY.Doc.Syntax := "
$gb.GetNthSyz(M:IDEAL or MODULE, N:INT):MODULE
";
MEMORY.Doc.Descr := "

This function, if used after executing \"Res(M)\", returns the Nth
syzygy module for M.  Within the Interactive Groebner Framework, in
which resolutions may be computed one step at a time, the function
returns the part of the Nth syzygy module computed so far.  In
contrast, the function \"Syz\" always determines the complete syzygy
module even from within the Interactive Groebner Framework.

//==========================  EXAMPLE  ==========================\\

  Use R ::= QQ[t,x,y,z];
  I := Ideal(x^2-yt, xy-zt, xy);
  $gb.Start_Res(I);
  $gb.Steps(I,1);
  $gb.GetNthSyz(I,1);  $gb.GetNthSyz(I,2);
Module([[0]])
-------------------------------
Module([[0]])
-------------------------------
  $gb.Steps(I,1);
  $gb.GetNthSyz(I,1);  $gb.GetNthSyz(I,2);
Module([0, 0])
-------------------------------
Module([[0]])
-------------------------------
  $gb.Steps(I,5);
  $gb.GetNthSyz(I,1);  $gb.GetNthSyz(I,2);
Module([-xz, -y^2, yz])
-------------------------------
Module([[0]])
-------------------------------
  $gb.Complete(I);
  $gb.GetNthSyz(I,1);  $gb.GetNthSyz(I,2);
Module([-xz, -y^2, yz], [tz, xy, 0], [0, -x^2 + ty, -tz], [-x^2 + ty, 0, xy])
-------------------------------
Module([-x, -y, 0, z], [-t, -x, -y, 0])
-------------------------------
\\==========================  o=o=o=o  ===========================//

";
    