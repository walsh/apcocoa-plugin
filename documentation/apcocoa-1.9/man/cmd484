
-- NCo.Deg
MEMORY.Doc.Syntax := "
NCo.Deg(F:LIST):INT
";
MEMORY.Doc.Descr := "

Please set ring environment *alphabet* (or set of indeterminates) \"X\" via the function \"NCo.SetX\" before using this function. For more information, please check the relevant functions.

* @param *F*: a polynomial in \"K<X>\". Each polynomial is represented as a LIST of monomials, which are LISTs of the form [C, W] where W is a word in \"<X>\" and C is the coefficient of W. For example, the polynomial \"f=xy-y+1\" is represented as F:=[[1,\"xy\"], [-1, \"y\"], [1,\"\"]]. The zero polynomial \"0\" is represented as the empty LIST [].
* @return: an INT which represents the standard degree of F. If F=0, the function returns \"0\". 

//==========================  EXAMPLE  ==========================\\

NCo.SetX(\"abc\");
F:=[[1,\"ab\"],[2,\"aa\"],[3,\"bb\"],[4,\"bab\"]];
NCo.Deg(F);
3
-------------------------------
NCo.Deg([]); -- 0 polynomial
0
-------------------------------
\\==========================  o=o=o=o  ===========================//

";
    