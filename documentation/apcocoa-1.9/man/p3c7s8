
-- Printing a Tagged Object
MEMORY.Doc.Descr := "

Suppose the object E is tagged with the string S.  When one tries to
print E---say with \"Print E\" or just \"E;\"--- CoCoA looks for a
user-defined function with name \"Print_S\".  If no such function is
available, CoCoA prints E as if it were not tagged, otherwise, it
executes \"Print_S\".

//==========================  EXAMPLE  ==========================\\

  L := [\"Dave\",\"March 14, 1959\",372];  -- continuing with the previous example
  M := Tagged(L,\"MiscData\");
  M; -- M is printed as normal in the absence of a function \"Print_MiscData\"
[\"Dave\", \"March 14, 1959\", 372]
-------------------------------
  Define Print_MiscData(X) -- Exactly one parameter is required.
    M := Untagged(X);
    Print M[1];
  EndDefine;
  Print M; -- Now, any object tagged with the string \"MiscData\" will be
     -- printed using Print_MiscData
Dave
-------------------------------
  M;  -- Whenever printing of M is called for, \"Print_MiscData\" is executed.
Dave
-------------------------------
\\==========================  o=o=o=o  ===========================//


The line \"M := Untagged(X)\" is actually not necessary here, but in
general one may get into an infinite loop trying to print X, a tagged
object, from within the function that is being defined in order to
print X, if that makes sense.  Untagging X prevents this problem.
    ";
    