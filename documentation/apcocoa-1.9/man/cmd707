
-- Using
MEMORY.Doc.Syntax := "
Using R Do Cmds EndUsing

where R is the identifier for a ring and Cmds is a sequence of commands.
";
MEMORY.Doc.Descr := "

Suppose S is the current ring and R is another ring, then


  Using R Do
    Command1;
    Command2;
  EndUsing;

is equivalent to

  Use R;
  Command1;
  Command2;
  Use S;
//==========================  EXAMPLE  ==========================\\

  Use S ::= QQ[x,y];          -- the current ring is S
  R ::= QQ[a,b,c];            -- another ring
  Using R Do PrintLn Indets(); EndUsing;
[a, b, c]
-------------------------------
\\==========================  o=o=o=o  ===========================//



Note that you cannot create a new ring with this command:
for instance \"Using QQ[a,b] Do ... EndUsing;\" will produce an error.
Instead you need to name the ring first, like this:
\"MyRing ::= QQ[a,b]; Using MyRing Do ... EndUsing;\"";
    