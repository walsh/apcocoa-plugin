
-- CharP.IMXLSolve
MEMORY.Doc.Syntax := "
CharP.IMXLSolve(F:LIST):LIST
";
MEMORY.Doc.Descr := "
*Please note:* The function(s) explained on this page is/are using the *ApCoCoAServer*. You will have to start the ApCoCoAServer in order to use it/them.


This function computes the unique zero in \"F_2^n\" of a polynomial system over \"F_2 \". It uses Improved Mutant XL-Algorithm to find the unique zero. The idea is to linearize the polynomial system by considering terms as indeterminates and then apply gaussian elimination to find a univariate polynomial. If no univariate polynomial is found then the system is extended by generating more polynomials in the ideal and gaussian elimination is applied again. In this way by applying gaussian elimination repeatedly we find the zero of the system. In fact Improved Mutant XL-Algorithm is the XL-Algorithm with improved mutant strategy. The Improved Mutant XL-Algorithm is implemented only to find the unique zero. If the given polynomial system has more than one zeros in \"F_2^n \" then this function does not find any zero. In this case a massage for non-uniqueness will be displayed to the screen after reaching the maximum degree bound. 



* @param *F:* List of polynomials of given system.
* @return The unique solution of the given system in \"F_2^n\". 

//==========================  EXAMPLE  ==========================\\

Use Z/(2)[x[1..4]];
F:=[
    x[1]x[2] + x[2]x[3] + x[2]x[4] + x[3]x[4] + x[1] + x[3] + 1, 
    x[1]x[2] + x[1]x[3] + x[1]x[4] + x[3]x[4] + x[2] + x[3] + 1, 
    x[1]x[2] + x[1]x[3] + x[2]x[3] + x[3]x[4] + x[1] + x[4] + 1, 
    x[1]x[3] + x[2]x[3] + x[1]x[4] + x[2]x[4] + 1
    ];

-- Then we compute the solution with
CharP.IMXLSolve(F);

-- And we achieve the following information on the screen together with the solution at the end.
----------------------------------------
         The size of Matrix is:
		No. of Rows=4
		No. of Columns=11
Appling Gaussian Elimination...
-- CoCoAServer: computing Cpu Time = 0
-------------------------------
Gaussian Elimination Completed.
	The size of Matrix is:
		No. of Rows=4
		No. of Columns=11
Appling Gaussian Elimination...
-- CoCoAServer: computing Cpu Time = 0
-------------------------------
Gaussian Elimination Completed.
	The variables found till now are:
	[x[1], x[2], x[3], x[4]]
	No. of New Mutants found = 0
	The size of Matrix is:
		No. of Rows=4
		No. of Columns=11
Appling Gaussian Elimination...
-- CoCoAServer: computing Cpu Time = 0
-------------------------------
Gaussian Elimination Completed.
	The variables found till now are:
	[x[1], x[2], x[3], x[4]]
	No. of New Mutants found = 0
	The size of Matrix is:
		No. of Rows=8
		No. of Columns=11
Appling Gaussian Elimination...
-- CoCoAServer: computing Cpu Time = 0
-------------------------------
Gaussian Elimination Completed.
	The variables found till now are:
	[x[1], x[2], x[3], x[4]]
	No. of New Mutants found = 1
	The total No. of Mutants found are = 1
	The No. of Mutants of Minimum degree (Mutants used) are = 1
	The size of Matrix is:
		No. of Rows=11
		No. of Columns=11
Appling Gaussian Elimination...
-- CoCoAServer: computing Cpu Time = 0
-------------------------------
Gaussian Elimination Completed.
	The variables found till now are:
	[0, 1, 0, 1]
[0, 1, 0, 1]


\\==========================  o=o=o=o  ===========================//

//==========================  EXAMPLE  ==========================\\

Use Z/(2)[x[1..4]];
F:=[ 
    x[2]x[3] + x[1]x[4] + x[2]x[4] + x[3]x[4] + x[1] + x[2] + x[3] + x[4], 
    x[2]x[3] + x[2]x[4] + x[3]x[4] + x[2] + x[3] + x[4],  
    x[1]x[2] + x[2]x[3] + x[2]x[4] + x[3]x[4] + x[1] + x[2],  
    x[1]x[2] + x[2]x[3] + x[2]x[4] + x[3]x[4] + x[1] + x[2]
   ];

-- Solution is not unique i.e. [0, 1, 1, 1], [0, 0, 0, 0], and [1, 1, 1, 1] are solutions 

-- Then we compute the solution with
CharP.IMXLSolve(F);

-- And we achieve the following information on the screen.
----------------------------------------
       
	The size of Matrix is:
		No. of Rows=4
		No. of Columns=9
Appling Gaussian Elimination...
-- CoCoAServer: computing Cpu Time = 0
-------------------------------
Gaussian Elimination Completed.
	The size of Matrix is:
		No. of Rows=3
		No. of Columns=9
Appling Gaussian Elimination...
-- CoCoAServer: computing Cpu Time = 0
-------------------------------
Gaussian Elimination Completed.
	The variables found till now are:
	[x[1], x[2], x[3], x[4]]
	No. of New Mutants found = 0
	The size of Matrix is:
		No. of Rows=7
		No. of Columns=14
Appling Gaussian Elimination...
-- CoCoAServer: computing Cpu Time = 0
-------------------------------
Gaussian Elimination Completed.
	The variables found till now are:
	[x[1], x[2], x[3], x[4]]
	No. of New Mutants found = 0
	The size of Matrix is:
		No. of Rows=14
		No. of Columns=14
Appling Gaussian Elimination...
-- CoCoAServer: computing Cpu Time = 0
-------------------------------
Gaussian Elimination Completed.
	The variables found till now are:
	[x[1], x[2], x[3], x[4]]
	No. of New Mutants found = 4
	The total No. of Mutants found are = 4
	The No. of Mutants of Minimum degree (Mutants used) are = 2
	The size of Matrix is:
		No. of Rows=15
		No. of Columns=10
Appling Gaussian Elimination...
-- CoCoAServer: computing Cpu Time = 0
-------------------------------
Gaussian Elimination Completed.
	The variables found till now are:
	[x[1], x[2], x[3], x[4]]
	No. of New Mutants found = 0
	The total No. of Mutants found are = 2
	The No. of Mutants of Minimum degree (Mutants used) are = 2
	The size of Matrix is:
		No. of Rows=15
		No. of Columns=14
Appling Gaussian Elimination...
-- CoCoAServer: computing Cpu Time = 0
-------------------------------
Gaussian Elimination Completed.
	The variables found till now are:
	[x[1], x[2], x[3], x[4]]
	No. of New Mutants found = 0
	The size of Matrix is:
		No. of Rows=12
		No. of Columns=14
Appling Gaussian Elimination...
-- CoCoAServer: computing Cpu Time = 0
-------------------------------
Gaussian Elimination Completed.
	The variables found till now are:
	[x[1], x[2], x[3], x[4]]
	No. of New Mutants found = 0
	The size of Matrix is:
		No. of Rows=17
		No. of Columns=14
Appling Gaussian Elimination...
-- CoCoAServer: computing Cpu Time = 0
-------------------------------
Gaussian Elimination Completed.
	The variables found till now are:
	[x[1], x[2], x[3], x[4]]
	No. of New Mutants found = 0
	The size of Matrix is:
		No. of Rows=25
		No. of Columns=15
Appling Gaussian Elimination...
-- CoCoAServer: computing Cpu Time = 0
-------------------------------
Gaussian Elimination Completed.
	The variables found till now are:
	[x[1], x[2], x[3], x[4]]
	No. of New Mutants found = 0
	The size of Matrix is:
		No. of Rows=13
		No. of Columns=15
Appling Gaussian Elimination...
-- CoCoAServer: computing Cpu Time = 0
-------------------------------
Gaussian Elimination Completed.
	The variables found till now are:
	[x[1], x[2], x[3], x[4]]
	Please Check the uniqueness of solution.
	The Given system of polynomials does not
	seem to have a unique solution.
\\==========================  o=o=o=o  ===========================//

";
    