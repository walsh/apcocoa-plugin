
-- Hom.LRSolve
MEMORY.Doc.Syntax := "
Hom.LRSolve(P:LIST,HomTyp:INT)
";
MEMORY.Doc.Descr := "
*Please note:* The function(s) explained on this page is/are using the *ApCoCoAServer*. You will have to start the ApCoCoAServer in order to use it/them.

This function computes isolated solutions of a non-square polynomial system by using the idea of randomization. Consider the non-square polynomial system \"F:=[ x^2-1, xy-1, x^2-x ]\". After randomization this system will be converted into a  system \"G := [ x^2-1+a(x^2-x), xy-1+b(x^2-x) ]\", where a and b are complex numbers having absolute value near one. The system G is a randomization of system F. Sometimes the system G may have more solutions than F. This function first randomizes the given system to make it square and then call HOM4PS to solve it. This function and the function \"Hom.SRSolve\" do the same job but with a different technique of randomization. 

This functions provides two different kinds of computation depending on the input that you provide in ApCoCoAServer during execution. After passing the command \"Hom.LRSolve(P,HomTyp)\" in ApCoCoA you need to interact with ApCoCoAServer. At this stage ApCoCoAServer asks you to enter 1 for the polyhedral homotopy and enter 2 for the classical linear homotopy. As a result this function provides all isolated solutions of a zero dimensional system of polynomial equations. The system of polynomials may be homogeneous or non-homogeneous.

* @param *P*: List of polynomials of the given system.
* @param *HomTyp*: set it to 1 for polyhedral homotopy and to 2 for classical linear homotopy.
* @return A list of lists containing the finite solutions of the system P.

//==========================  EXAMPLE  ==========================\\

-- An example of zero dimensional Non-Homogeneous Solving using the classical linear homotopy.
-- We want to find isolated solutions of the following system. 

Use QQ[x[1..3]];              
P := [
  x[1]x[2]x[3] - x[1]x[2]-15, 
  3x[1]x[2]-x[1]+5, 
  7x[1]x[3] - x[1],
  24x[1]x[2]+x[3] - 3x[1]x[3] - 1, 
  x[1]^2 - x[1] 
];
HomTyp:=1;

-- Then we compute the solution with
Hom.LRSolve(P,HomTyp);

-- Now you have to interact with ApCoCoAServer
-- Enter 1 for the polyhedral homotopy and 2 for the classical linear homotopy.
-- Since we want to use the classical linear homotopy therefore we enter 2.
-- The all finite solutions are:

----------------------------------------
[
 [[9455327382203569/5000000000000000, -25208009777282481/10000000000000000],
  [13172347071045859/1000000000000000000, 780259255441451/10000000000000000],
  [50662103933981573/100000000000000000, 24894084616179979/50000000000000000]],
 [[94045825811783779/10000000000000000000, -18561325122258089/500000000000000000],
  [-11252856171103929/500000000000000, 53756347909614881/10000000000000000],
  [43866568184785617/10000000000000000, -970984718484509/40000000000000]],
 [[23564339009933287/1000000000000000000, 37422202697036111/1000000000000000000],
  [-20929334925895049/1000000000000000, -24991129623196171/2500000000000000],
  [26847721395327557/10000000000000000, 20456859352398073/1000000000000000]],
 [[-5340666810400797/10000000000000000, 7138058708108771/2500000000000000],
  [157412137424673/4000000000000000, -15131835631465503/250000000000000000],
  [45533206002984217/1000000000000000000, -67237130550938307/100000000000000000]],
 [[2223557602823067/10000000000000, -19326230622413977/250000000000000],
  [-15392736087963673/2000000000000000, 18511778667155307/200000000000000000],
  [-25906948948013323/1000000000000000, 3338667600178357/50000000000000]]
]


-- The smallest list represents a complex number.

\\==========================  o=o=o=o  ===========================//

//==========================  EXAMPLE  ==========================\\

-- An example of zero dimensional Non-Homogeneous Solving using the polyhedral homotopy.
-- We want to find isolated solutions of non-homogeneous polynomial system x[1]^2-1=0, x[1]x[2]-1=0, x[1]^2-x[1]=0. 

Use QQ[x[1..2]];           
P := [x[1]^2-1, x[1]x[2]-1,x[1]^2-x[1]];
HomTyp:=1;

-- Then we compute the solution with
Hom.LRSolve(P,HomTyp);

-- Now you have to interact with ApCoCoAServer
-- Enter 1 for the polyhedral homotopy and 2 for the classical linear homotopy.
-- Since we want to use polyhedral homotopy therefore we enter 1.
-- The all finite solutions are:

----------------------------------------
[
[[-9143436298249491/20000000000000000, 9937657539108147/50000000000000000],
 [-24282046571107613/50000000000000000, 18641461485865229/100000000000000000]],
 [[1, 0], [1, 0]]
]



-- The smallest list represents a complex number. For example above system has 2 solutions the second solution is [[1, 0], [1, 0]] 
-- and we read it as x=2+0i, y=1+0i. Since imaginary part is zero therefore its a real solution. 

\\==========================  o=o=o=o  ===========================//

";
    