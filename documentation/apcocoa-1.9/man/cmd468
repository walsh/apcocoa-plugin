
-- NCo.Add
MEMORY.Doc.Syntax := "
NCo.Add(F1:LIST, F2:LIST):LIST
";
MEMORY.Doc.Descr := "
*Please note:* The function(s) explained on this page is/are using the *ApCoCoAServer*. You will have to start the ApCoCoAServer in order to use it/them.

Please set ring environment *coefficient field*\" K\", *alphabet* (or set of indeterminates) \"X\" and the *word ordering* via the functions \"NCo.SetFp\", \"NCo.SetX\" and \"NCo.SetOrdering\", respectively, before using this function. The default coefficient field is \"Q\", and the default ordering is the length-lexicographic ordering (\"LLEX\"). For more information, please check the relevant functions.

* @param *F1, F2:* two polynomials in \"K<X>\", which are left and right operands of addition respectively. Each polynomial is represented as a LIST of monomials, which are LISTs of the form [C, W] where W is a word in \"<X>\" and C is the coefficient of W. For example, the polynomial \"f=xy-y+1\" is represented as F:=[[1,\"xy\"], [-1, \"y\"], [1,\"\"]]. The zero polynomial \"0\" is represented as the empty LIST [].
* @return: a LIST, which represents the polynomial equal to \"F1+F2\".

//==========================  EXAMPLE  ==========================\\

NCo.SetX(\"abc\"); 				
NCo.SetOrdering(\"ELIM\"); 
NCo.RingEnv();
Coefficient ring : Q
Alphabet : abc
Ordering : ELIE
-------------------------------	
F1 := [[1,\"a\"],[1,\"\"]];
F2 := [[1,\"b\"],[1,\"ba\"]];
NCo.Add(F1,F2); -- over Q
[[1, \"ba\"], [1, \"a\"], [1, \"b\"], [1, \"\"]]
-------------------------------
NCo.SetFp(); -- set default Fp = F2
NCo.RingEnv();
Coefficient ring : Fp = Z/(2)
Alphabet : abc
Ordering : ELIM
-------------------------------
NCo.Add(F1,F2); -- over F2
[[1, \"ba\"], [1, \"a\"], [1, \"b\"], [1, \"\"]]
-------------------------------
NCo.Add(F1,F1); -- over F2
[ ]
-------------------------------
\\==========================  o=o=o=o  ===========================//

";
    