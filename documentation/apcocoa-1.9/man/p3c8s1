
-- Introduction to Memory
MEMORY.Doc.Descr := "

CoCoA has three types of memory: *working*, *global*, and
*ring-bound* memory.  Unlike previous versions of CoCoA, starting
with CoCoA 3.5, variables defined during a session are by default
assigned to a *working memory*, accessible from all rings (but not
from user-defined functions).  There are no longer variables that are
local to a particular ring.  However, as in previous versions of
CoCoA, one may define variables in the *global memory* by using the
prefix \"MEMORY\".

The word *global* now refers to the fact that these
variables are accessible not only to all rings but also to
user-defined functions.  A special class of global variables can be
stored in what is called the *ring-bound memory*. 
Most users will never need the ring-bound memory!
These variables are formed with the prefix \"MEMORY.ENV.R\"
where \"R\" is a ring identifier; they are *bound* to the
ring, which means that they are automatically destroyed when their
corresponding rings cease to exist.  Otherwise, variables in the
ring-bound memory behave exactly as all other global variables.

These three types of memory are discussed separately, below.
    ";
    