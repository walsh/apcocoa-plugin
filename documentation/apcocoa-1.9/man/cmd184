
-- Div
MEMORY.Doc.Syntax := "
Div(N:INT, D:INT):INT
";
MEMORY.Doc.Descr := "

If N = Q*D + R, and 0 <= R < |D|, then \"Div(N, D)\" returns Q and
\"Mod(N, D)\" returns R.

NOTE: To perform the division algorithm on a polynomial or vector, use
\"NR\" (normal remainder) to find the remainder, or \"DivAlg\" to get both
the quotients and the remainder.  To determine if a polynomial is in a
given ideal or a vector is in a given module, use \"NF\" or \"IsIn\", and
to find a representation in terms of the generators use \"GenRepr\".

//==========================  EXAMPLE  ==========================\\

  Div(10,3);
3
-------------------------------
  Mod(10,3);
1
-------------------------------
\\==========================  o=o=o=o  ===========================//

";
    