
-- Sorted
MEMORY.Doc.Syntax := "
Sorted(L:LIST):LIST

where V is a variable containing a list.
";
MEMORY.Doc.Descr := "

This function returns the list of the sorted elements of L without
affecting L, itself.

For more on the default comparisons, see \"Relational Operators\"
in the chapter on operators.
For more complicated sorting, see \"SortBy\", \"SortedBy\".

//==========================  EXAMPLE  ==========================\\

  L := [3,2,1];
  Sorted(L);
[1, 2, 3]
-------------------------------
  Use R ::= QQ[x,y,z];
  L := [x,y,z];
  Sorted(L);
[z, y, x]
-------------------------------
  Sorted([y, x, z, x^2]);
[z, y, x, x^2]
-------------------------------
  Sorted([3, 1, 1, 2]);
[1, 1, 2, 3]
-------------------------------
  Sorted([\"b\",\"c\",\"a\"]);
[\"a\", \"b\", \"c\"]
-------------------------------
  Sorted([Ideal(x,y), Ideal(x)]); -- ideals are ordered by containment
[Ideal(x), Ideal(x, y)]
-------------------------------
\\==========================  o=o=o=o  ===========================//

";
    