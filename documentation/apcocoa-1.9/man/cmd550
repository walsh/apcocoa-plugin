
-- Num.EigenValuesAndAllVectors
MEMORY.Doc.Syntax := "
Num.EigenValuesAndAllVectors(A:MAT):[B:MAT, C:MAT, D:MAT, E:MAT , F:MAT]
";
MEMORY.Doc.Descr := "
*Please note:* The function(s) explained on this page is/are using the *ApCoCoAServer*. You will have to start the ApCoCoAServer in order to use it/them.

This function returns a list of five matrices, containing numerical approximation of the eigenvalues of the matrix \"A\" and right and left eigenvectors. 


* @param *A* A quadratic matrix with rational entries.
* @return The output is a list of five matrices \"[B:MAT, C:MAT, D:MAT, E:MAT, F:MAT]\". The first matrix \"B\" contains the complex eigenvalues of the matrix \"A\", i.e. the first entry of a column is the real part and the second entry of the same column is the imaginary part of the eigenvalue. The matrices \"C\" and \"D\" represent the right eigenvectors of \"A\", i.e. the \"j\"-th column of \"C\" contains the real part of the right eigenvector corresponding to eigenvalue \"j\" and the \"j\"-th column of D contains the imaginary part of the same right eigenvector corresponding to eigenvalue \"j\". The matrices \"E\" and \"F\" store the left eigenvectors analogue to \"C\" and \"D\".

//==========================  EXAMPLE  ==========================\\

A:=Mat([[1,2,7,18],[2,4,9,12],[23,8,9,10],[7,5,3,2]]); 
Dec(Num.EigenValuesAndAllVectors(A),3);
-- CoCoAServer: computing Cpu Time = 0.016
-------------------------------
[Mat([
  [\"28.970\", \"-13.677\", \"0.353\", \"0.353\"],
  [\"0\", \"0\", \"3.051\", \"-3.051\"]
]), Mat([
  [\"0.538\", \"-0.600\", \"0.389\", \"0.389\"],
  [\"0.311\", \"-0.222\", \"-0.442\", \"-0.442\"],
  [\"0.427\", \"0.174\", \"0.050\", \"0.050\"],
  [\"0.656\", \"0.748\", \"0\", \"0\"]
]), Mat([
  [\"0\", \"0\", \"-0.174\", \"0.174\"],
  [\"0\", \"0\", \"0.139\", \"-0.139\"],
  [\"0\", \"0\", \"0.265\", \"-0.265\"],
  [\"0\", \"0\", \"-0.727\", \"0.727\"]
]), Mat([
  [\"0.394\", \"-0.581\", \"0.260\", \"0.260\"],
  [\"0.435\", \"-0.442\", \"-0.547\", \"-0.547\"],
  [\"0.763\", \"0.621\", \"0\", \"0\"],
  [\"0.268\", \"0.281\", \"0.046\", \"0.046\"]
]), Mat([
  [\"0\", \"0\", \"-0.031\", \"0.031\"],
  [\"0\", \"0\", \"-0.301\", \"0.301\"],
  [\"0\", \"0\", \"0.680\", \"-0.680\"],
  [\"0\", \"0\", \"-0.274\", \"0.274\"]
])]
-------------------------------
\\==========================  o=o=o=o  ===========================//

";
    