
-- Other Help
MEMORY.Doc.Descr := "

1. A user may provide help and sometimes (rarely) get help for a
user-defined function using the \"Help\" feature of the \"Define\"
command.

//==========================  EXAMPLE  ==========================\\

  Help();
Type Help(< Op >) to get help on operator < Op >
-------------------------------
  Help(\"GBasis\");  -- note the typical response, one of the main
                 -- motivations for the author of the online manual.
No help available for GBasis
-------------------------------
\\==========================  o=o=o=o  ===========================//



2. The command, \"Describe\", can be used to find more information about
functions.

//==========================  EXAMPLE  ==========================\\

  Describe Function(\"Insert\");
Define Insert(L, I, O)
  $list.Insert(L, I, O)
EndDefine;
-------------------------------
  Describe Function(\"\"$list.Insert\"\");
Define Insert(L, I, O)
  If NOT(Type(L) = LIST) Then
    Return(Error(ERR.BAD_PARAMS,\": expected LIST\"))
  Elif I = Len(L) + 1 Then
    Append(L, O)
  Elif I > Len(L) Then
    Return(Error(ERR.INDEX_TOO_BIG, I))
  Elif I <= 0 Then
    Return(Error(ERR.INDEX_NEG, I))
  Else
    L := Concat([L[J]|J In 1..(I - 1)],[O],[L[J]|J In I..Len(L)]);
  EndIf;
EndDefine;
-------------------------------
\\==========================  o=o=o=o  ===========================//



3. The function, \"Functions\", may be used to list all functions
defined in a package.
Note: \"Functions(\"$user\")\"
lists all current user-defined functions.

//==========================  EXAMPLE  ==========================\\

  Functions(\"$mat\");
[About(), Man(), Identity(N), Transposed(M), Submat(M, Rows, Cols),
Jacobian(S), Resultant(F, G, X), DirectSum(M1, M2), BlockMatrix(LL),
ColumnVectors(M), Complement(M, I, J), Bip(M, J), Pfaffian(M),
Sylvester(F, G, X), ExtendRows(M, N), PushRows(M, N), ConcatRows(L),
PkgName()]
-------------------------------
\\==========================  o=o=o=o  ===========================//



The list of packages is given by \"Packages()\".

4. The function \"Starting(S)\" where S is a string returns a list of all
functions starting with the string S.

//==========================  EXAMPLE  ==========================\\

  Starting(\"Su\");
[\"SubstPoly\", \"SubSet\", \"Submat\", \"Sum\", \"Subst\", \"Support\"]
-------------------------------
\\==========================  o=o=o=o  ===========================//

";
    