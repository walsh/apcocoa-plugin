
-- Fn.RootN
MEMORY.Doc.Syntax := "
Fn.RootN(Val:RAT,N:INT,Acc:INT):RAT
";
MEMORY.Doc.Descr := "

This functions returns the \"N\"-th root of \"Val\" rounded
to the given accuracy \"Acc\"
* @param *Val* The value of which to take the \"N\"-th root.
* @param *N* The degree of the root to be taken.
* @param *Acc* The desired accuracy.
* @return Returns the \"N\"-th root of \"Val\" rounded to accuracy \"Acc\".

//==========================  EXAMPLE  ==========================\\

R := Fn.RootN(8, 3, 30);
Dec(R, 40);

2
-------------------------------

\\==========================  o=o=o=o  ===========================//

//==========================  EXAMPLE  ==========================\\

R := Fn.RootN(-8, 3, 30);
Dec(R, 40);

-2
-------------------------------

\\==========================  o=o=o=o  ===========================//

//==========================  EXAMPLE  ==========================\\

R := Fn.RootN(9, 3, 30);
Dec(R, 40);

2.080083823051904114530056824358
-------------------------------

\\==========================  o=o=o=o  ===========================//

";
    