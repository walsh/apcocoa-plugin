
-- BB.Box
MEMORY.Doc.Syntax := "
BB.Box(D:LIST):LIST
";
MEMORY.Doc.Descr := "

Computes the box order ideal of type \"D=[D_1,..,D_N]\". The input is a list of integers \"D\" of length \"NumIndets\"(). The output is a list of terms sorted in ascending order with respect to the current term ordering.

* @param *D* List of integer values representing an exponent vector of a term. The order ideal spanned by the term represented by this exponent vector will be computed.
* @return A list of terms of the order ideal spanned by the term represented by the exponent vector \"D\", sorted in ascending order w.r.t. the current term ordering.

//==========================  EXAMPLE  ==========================\\

Use QQ[x,y,z];
BB.Box([2,1,1]);

[1, z, y, x, yz, xz, xy, x^2, xyz, x^2z, x^2y, x^2yz]
-------------------------------
\\==========================  o=o=o=o  ===========================//

";
    