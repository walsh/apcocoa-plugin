
-- Predefined Term-Orderings
MEMORY.Doc.Descr := "

The predefined term-orderings are:

 * degree reverse lexicographic: \"DegRevLex\"  (the default ordering)

 * degree lexicographic: \"DegLex\"
 * pure lexicographic: \"Lex\"
 * pure xel: \"Xel\"
 * elimination term-ordering: \"Elim(X:INDETS)\"
The first two term-orderings use the weights of the indeterminates for
computing the degree of a monomial. If the indeterminates are given in
the order \"x_1, ...,x_n\", then \"x_1 > ... > x_n\" with respect to Lex, but
\"x_1 < ... < x_n\" with respect to Xel.

In the last ordering, X specifies the variables that are to be
eliminated.  It may be a single indeterminate or a range of
indeterminates.  However, X may not be an arbitrary list of
indeterminates; for that, see the command \"Elim\" (as opposed to the
modifier \"Elim\" being discussed here).  A range of indeterminates can
be specified using the syntax \"< first-indet >..< last-indet >\".
Another shortcut: if there are indexed variables of the form, say,  \"x[i,j]\",
then \"Elim(x)\" specifies a term-ordering for eliminating all of the
\"x[i,j]\".

//==========================  EXAMPLE  ==========================\\

  Use R ::= QQ[x,y,z], Lex;
  x+y+z;
x + y + z
-------------------------------
  Use R ::= QQ[x,y,z], Xel;
  x+y+z;
z + y + x
-------------------------------
  Use R ::= QQ[t,x,y,z], Elim(t);
  I := Ideal(t-x, t-y^2, t^2-xz^3);
  GBasis(I);
[t - x, -y^2 + x, xz^3 - x^2]
-------------------------------
  Use R ::= QQ[x[1..5],y,z], Elim(x[5]..y); -- eliminating x[5] and y
  Use R ::= QQ[x[1..5],y,z], Elim(x); -- term-ordering for eliminating all
                                      -- of the x[i]'s
  Ord();
Mat([
  [1, 1, 1, 1, 1, 0, 0],
  [0, 0, 0, 0, 0, 1, 1],
  [0, 0, 0, 0, 0, 0, -1],
  [0, 0, 0, 0, -1, 0, 0],
  [0, 0, 0, -1, 0, 0, 0],
  [0, 0, -1, 0, 0, 0, 0],
  [0, -1, 0, 0, 0, 0, 0]
])
-------------------------------
\\==========================  o=o=o=o  ===========================//

";
    