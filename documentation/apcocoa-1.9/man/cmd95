
-- Cast
MEMORY.Doc.Syntax := "
Cast(E:OBJECT, T:TYPE):TYPE
";
MEMORY.Doc.Descr := "

This function returns the value of the expression E after converting
it to type T.  If S and T are types with S < T, then casting from S to
T is usually possible.

//==========================  EXAMPLE  ==========================\\

  L := [[1,2],[3,4]];
  Type(L);
LIST
-------------------------------
  Cast(L, MAT);
Mat([
  [1, 2],
  [3, 4]
])
-------------------------------
  L;  -- L is unchanged; it is still a list.
[[1, 2], [3, 4]]
-------------------------------
  Use ZZ/(5)[t];
  A := 8;
  A;  -- A has type INT
8
-------------------------------
  Cast(A, POLY);  -- cast as a polynomial, A = -2 since the coefficient
                  -- ring is ZZ/5ZZ
-2
-------------------------------
\\==========================  o=o=o=o  ===========================//

";
    