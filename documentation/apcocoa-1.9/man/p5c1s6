
-- Trace
MEMORY.Doc.Descr := "

If the Trace option is on, then the system echoes every command at
every level.  This is useful for debugging programs.  The following
(toy) user-defined function returns the same error message for N = 3
and N = 6.  Turning the Trace option on makes the sources of the
trouble clear.

//==========================  EXAMPLE  ==========================\\

  Define T(N)
    M := 1/(N-3);
    If N = 6 Then N := 3 EndIf;
    M := 1/(N-3);
    Return M;
  EndDefine;
  T(3);
ERROR: Division by zero
CONTEXT: 1 / (N - 3)
-------------------------------
  T(6);
ERROR: Division by zero
CONTEXT: 1 / (N - 3)
-------------------------------
  Set Trace;
  T(3);
T(3)
M := 1 / (N - 3); If N = 6 Then N := 3 EndIf; M := 1 / (N - 3); Return(M);
M := 1 / (N - 3)
ERROR: Division by zero
CONTEXT: 1 / (N - 3)
-------------------------------
  T(6);
T(6)
M := 1 / (N - 3); If N = 6 Then N := 3 EndIf; M := 1 / (N - 3); Return(M);
M := 1 / (N - 3)
If N = 6 Then N := 3 EndIf
N := 3
M := 1 / (N - 3)
ERROR: Division by zero
CONTEXT: 1 / (N - 3)
-------------------------------
\\==========================  o=o=o=o  ===========================//

";
    