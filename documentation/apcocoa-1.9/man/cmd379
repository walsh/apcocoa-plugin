
-- Latex
MEMORY.Doc.Syntax := "
Latex(X:OBJECT):STRING
";
MEMORY.Doc.Descr := "

This function returns a string containing the argument formatted in LaTeX.
From version 4.7.5 it returns a string, so it can be printed on a file.
Can also be called as \"LaTeX\".

//==========================  EXAMPLE  ==========================\\

  Use R ::= QQ[x,y,z];
  F := x^3+2y^2z;
  Latex(F);
x^{3} + 2y^{2}z
-------------------------------
  M := Mat([[1,2],[3,4]]);
  LaTeX(M);
left( begin{array}{cc} 
  1 & 2 
  3 & 4 end{array}right)
-------------------------------
  F := (x+y)/(1-z)^3;
  Latex(F);
frac{ - x - y} {z^{3}-3z^{2} + 3z-1}
-------------------------------
  Latex(Ideal(x^2,y+z));
( x^{2},
  y + z)
-------------------------------
  R := Res(Ideal(x^2,y+z));
  D := OpenOFile(\"my-test\");
  Print Latex(R) On D;
\\==========================  o=o=o=o  ===========================//

";
    