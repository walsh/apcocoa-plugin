
-- GB.GetNthSyzShifts
MEMORY.Doc.Syntax := "
$gb.GetNthSyzShifts(M:IDEAL or MODULE, N:INT):TAGGED(\"shifts\")
";
MEMORY.Doc.Descr := "

This function, if used after executing \"Res(M)\", returns the shifts
for the Nth syzygy module for M.  Within the Interactive Groebner
Framework, in which resolutions may be computed one step at a time,
the function returns shifts of the part of the Nth syzygy module
computed so far.

//==========================  EXAMPLE  ==========================\\

  Use R ::= QQ[t,x,y,z];
  I := Ideal(x^2-yt, xy-zt, xy);
  $gb.Start_Res(I);
  $gb.Steps(I,6);
  $gb.GetNthSyzShifts(I,2);
Shifts([x^2yz])
-------------------------------
  $gb.Complete(I);
  $gb.GetNthSyzShifts(I,2);
Shifts([x^2yz, txyz, tx^2z, x^3y])
-------------------------------
  J := Ideal(t,x)^3;
  Res(J);
0 --> R^3(-4) --> R^4(-3)
-------------------------------
  $gb.GetNthSyzShifts(J,1);
Shifts([x^3, tx^2, t^2x, t^3])
-------------------------------
  $gb.GetNthSyzShifts(J,2);
Shifts([tx^3, t^2x^2, t^3x])
-------------------------------
  SS := It;
  SS[1];
tx^3
-------------------------------
\\==========================  o=o=o=o  ===========================//

";
    