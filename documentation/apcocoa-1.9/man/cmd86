
-- BettiMatrix
MEMORY.Doc.Syntax := "
BettiMatrix(M:IDEAL or MODULE):TAGGED(\"$io.Matrix\")
";
MEMORY.Doc.Descr := "

This function returns the Betti matrix for M.

//==========================  EXAMPLE  ==========================\\

  Use R ::= QQ[t,x,y,z];
  I := Ideal(x^2-yt, xy-zt, xy);
  Res(I);
0 --> R^2(-5) --> R^4(-4) --> R^3(-2)
-------------------------------
  BettiMatrix(I);
--------------

--------------
   0    0    0
   0    0    3
   0    0    0
   0    4    0
   2    0    0
--------------

-------------------------------
  Untagged(BettiMatrix(I));
Mat([
  [0, 0, 0],
  [0, 0, 3],
  [0, 0, 0],
  [0, 4, 0],
  [2, 0, 0]
])
-------------------------------
\\==========================  o=o=o=o  ===========================//

";
    