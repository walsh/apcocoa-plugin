
-- NCo.BInterreduction
MEMORY.Doc.Syntax := "
NCo.BInterreduction(G:LIST):LIST
";
MEMORY.Doc.Descr := "

Note that, given a word ordering, a set \"G\" of non-zero polynomials is called *interreduced* if no element of \"Supp(g)\" is contained in the leading word ideal \"BLW(G\{g})\" for all \"g\" in \"G\".
*Please note:* The function(s) explained on this page is/are using the *ApCoCoAServer*. You will have to start the ApCoCoAServer in order to use it/them.

Please set ring environment *alphabet* (or set of indeterminates) \"X\" and *word ordering* via the functions \"NCo.SetX\" and \"NCo.SetOrdering\", respectively, before calling this function. The default ordering is length-lexicographic ordering (\"LLEX\"). For more information, please check the relevant functions.

* @param *G:* a LIST of polynomials in the free monoid ring \"F_{2}<X>\". Each polynomial is represented as a LIST of words (or terms) in \"<X>\". Each word is represented as a STRING. For example, \"xy^2x\" is represented as \"xyyx\", and the identity is represented as the empty string \"\". Thus, the polynomial \"f=xy-y+1\" is represented as F:=[\"xy\", \"y\", \"\"]. The zero polynomial \"0\" is represented as the empty LIST [].
* @return: a LIST of interreduced polynomials.

//==========================  EXAMPLE  ==========================\\

NCo.SetX(\"abc\");
NCo.SetOrdering(\"ELIM\");
Polynomials:=[[\"ba\",\"c\"],[\"b\",\"\"], [\"c\"]];
NCo.BInterreduction(Polynomials);

[[\"a\"], [\"b\", \"\"], [\"c\"]]
-------------------------------
\\==========================  o=o=o=o  ===========================//

";
    