===============================================================================
HOWTO Create the Online Help and Manual of ApCoCoA
===============================================================================


Result:
- directory doc 
- directory packages/man 
for using within the ApCoCoA-directory.


Requirements:
- saxon, see here: http://saxon.sourceforge.net/
PLEASE NOTE: You have to use the same version of saxon for all xml-files!!!
- php with xml and xsl support
- you have to be XML-Admin of the ApCoCoA-Wiki
- you have to able to access the CoCoA-CVS

(0) Preparation

- Delete all files in 'man' and 'doc/apcocoa', from the repo, too.
rm man/*
rm doc/apcocoa/*
svn st | grep ^! | awk '{print " --force "$2}' | xargs svn rm
svn ci -m "[GenDoc] Deleting all documentation files."

-------------------------------------------------------------------------------
(1) ApCoCoA Online Help
-------------------------------------------------------------------------------

- [Checkout the CoCoAHelp.xml from CoCoA-CVS (this is now generally ommitted
   since CoCoA 4 is not any more actively developed [ST])]

- Export the ApCoCoA-Manual from the ApCoCoA-Wiki using the special page
http://www.apcocoa.org/wiki?title=Special:XMLImportExport
checking "include subcategories" and save it as ApCoCoAHelp.xml

- (has to be improved!) Copy the content of XML/apcocoa_basics.xml into 
ApCoCoAHelp.xml after the list of commands
  Note: If there are new categories in the wiki, these categories have to be
        added manually to apcocoa_basics.xml

- Copy all xml-files into the directory XML

- Copy the CoCoA GUI-help files of the CoCoA-CVS into doc/gui_help

- Create the CoCoA Online Help:
cd doc/cocoa
java -jar ../../SAXON/saxon.jar ../../XML/CoCoAHelp.xml ../../XSLT_files/cocoa_help.xsl

- Create the ApCoCoA Online Help:
cd ../apcocoa
java -jar ../../SAXON/saxon.jar ../../XML/ApCoCoAHelp.xml ../../XSLT_files/apcocoa_help.xsl


The Online Help is complete now. Copy the directory doc/ into your ApCoCoA-directory.



-------------------------------------------------------------------------------
(2) ApCoCoA Manual
-------------------------------------------------------------------------------

- Merge CoCoAHelp.xml and ApCoCoAHelp.xml to Manul.xml by using mixXMLDocs.php:
cd ../../XML
php ../PHP_DOM_scripts/mixXMLDocs.php CoCoAHelp.xml ApCoCoAHelp.xml > Manual.xml

- Create the help.cpkg:
cd ../man
java -jar ../SAXON/saxon.jar ../XML/Manual.xml ../XSLT_files/help.xsl > help.cpkg

- Create all "man-files":
java -jar ../SAXON/saxon.jar ../XML/Manual.xml ../XSLT_files/man.xsl



-------------------------------------------------------------------------------
(3) Eclipse TOC
-------------------------------------------------------------------------------

cd ../eclipse
- Create the file "cocoa_eclipse_toc.xml":
java -jar ../SAXON/saxon.jar ../XML/CoCoAHelp.xml ../XSLT_files/cocoa_eclipse.xsl

- Create the file "apcocoa_eclipse_toc.xml":
java -jar ../SAXON/saxon.jar ../XML/ApCoCoAHelp.xml ../XSLT_files/apcocoa_eclipse.xsl

- Merge the files cocoa_eclipse_toc.xml and apcocoa_eclipse_toc.xml to "apcocoa_toc.xml



-------------------------------------------------------------------------------
(4) wordlist.txt
-------------------------------------------------------------------------------

- Check out the current CoCoA wordlist.txt from CoCoACVS
- Copy the CoCoA wordlist and apcocoa_wordlist.txt (in doc/apcocoa) into e.g. home directory
touch wordlist_tmp.txt
cat wordlist.txt >> wordlist_tmp.txt
cat apcocoa_wordlist.txt >> wordlist_tmp.txt 
touch wordlist.txt
cat wordlist_tmp.txt | sort -u > wordlist.txt

- Copy the new wordlist.txt into ApCoCoAPackages/trunk/modified-cocoa-files


