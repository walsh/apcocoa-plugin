<?php

//This is a MediaWiki extension that renders the CoCoA-XML tags
//This version of the plugin has been tested with MediaWiki 1.33
//
//Author: Florian Walsh

class CoCoAXML {
    // Register any render callbacks with the parser
    public static function onParserFirstCallInit( Parser $parser ) {
        $parser->setHook( 'command', [ self::class, 'renderCommand' ] );
        $parser->setHook( 'title', [ self::class, 'renderTitle' ] );
        $parser->setHook( 'short_description', [ self::class, 'renderShortDescription' ] );
        $parser->setHook( 'syntax', [ self::class, 'renderSyntax' ] );
        $parser->setHook( 'description', [ self::class, 'renderDescription' ] );
        $parser->setHook( 'example', [ self::class, 'renderExample' ] );
        $parser->setHook( 'itemize', [ self::class, 'renderItemize' ] );
        $parser->setHook( 'item', [ self::class, 'renderItem' ] );
        $parser->setHook( 'types', [ self::class, 'renderTypes' ] );
        $parser->setHook( 'see', [ self::class, 'renderSee' ] );
        $parser->setHook( 'seealso', [ self::class, 'renderSeealso' ] );
        $parser->setHook( 'key', [ self::class, 'renderKey' ] );
        $parser->setHook( 'wiki-category', [ self::class, 'renderCategory' ] );
        $parser->setHook( 'par/', [ self::class, 'renderPar' ] );
        $parser->setHook( 'ref', [ self::class, 'renderRef' ] );
        $parser->setHook( 'quotes', [ self::class, 'renderQuotes' ] );
        $parser->setHook( 'cocoa', [ self::class, 'renderCocoa' ] );
    }

    public static function renderCommand( $input, array $args, Parser $parser, PPFrame $frame ) {
        $output = $parser->recursiveTagParseFully( $input, $frame );
        return $output;
    }

    public static function renderTitle( $input, array $args, Parser $parser, PPFrame $frame ) {
        return  '<h2>' . htmlspecialchars($input) . '</h2>';
    }

    public static function renderShortDescription( $input, array $args, Parser $parser, PPFrame $frame ) {
        $output = $parser->recursiveTagParseFully( $input, $frame );
        return '<p>'. $output .' </p>';
    }

    public static function renderSyntax( $input, array $args, Parser $parser, PPFrame $frame ) {
        //remove possible newline at the beginning of the string
        $output = ltrim($input, "\n");
        return  '<h3>Syntax</h3> <pre><tt>'. htmlspecialchars($output) .'</tt></pre>';
    }

    public static function renderDescription( $input, array $args, Parser $parser, PPFrame $frame ) {
        $output = '<h3>Description</h3> <p>';
        $output .= $parser->recursiveTagParseFully( $input, $frame );
        $output .= '</p>';
        return  $output;
    }

    public static function renderExample( $input, array $args, Parser $parser, PPFrame $frame ) {
        //remove possible newline at the beginning of the string
        $output = ltrim($input, "\n");
        return '<h4>Example</h4> <pre><tt>'. htmlspecialchars($output) .'</tt></pre>';
    }

    public static function renderItemize( $input, array $args, Parser $parser, PPFrame $frame ) {
        $output = $parser->recursiveTagParseFully( $input, $frame );
        return '<ul>' . $output . '</ul>';
    }

    public static function renderSeealso( $input, array $args, Parser $parser, PPFrame $frame ) {
        $output = $parser->recursiveTagParseFully( $input, $frame );
        return '<h3>See also</h3>'. $output;
    }

    public static function renderItem( $input, array $args, Parser $parser, PPFrame $frame ) {
        $output = $parser->recursiveTagParseFully( $input, $frame );
        return '<li>' . $output . '</li>';
    }

    public static function renderSee( $input, array $args, Parser $parser, PPFrame $frame ) {
        $output = $parser->recursiveTagParseFully( '[['.$input.']]', $frame );
        return '<p>'.$output.'</p>';
    }

    public static function renderRef( $input, array $args, Parser $parser, PPFrame $frame ) {
        $output = $parser->recursiveTagParse( '[['.htmlspecialchars($input).']]', $frame );
        return $output;
    }

    public static function renderPar( $input, array $args, Parser $parser, PPFrame $frame ) {
        // This tag is no longer necessary, we return the empty string to avoid double newlines
        // when this tag appears in a separate line. Notice that in this way an inline <par/> is not
        // handled correctly
        return '';
    }

    public static function renderQuotes( $input, array $args, Parser $parser, PPFrame $frame ) {
        return '"'. htmlspecialchars($input) .'"';
    }

    public static function renderKey( $input, array $args, Parser $parser, PPFrame $frame ) {
        return '';
    }

    public static function renderTypes( $input, array $args, Parser $parser, PPFrame $frame ) {
        return '';
    }

    public static function renderCategory( $input, array $args, Parser $parser, PPFrame $frame ) {
        $parser->recursiveTagParseFully( '[[Category:'.$input.']]', $frame );
        return '';
    }

    public static function renderCocoa( $input, array $args, Parser $parser, PPFrame $frame ) {
        //remove possible newline at the beginning of the string
        $output = ltrim($input, "\n");
        return '<pre><tt>'. htmlspecialchars($output) .'</tt></pre>';
    }
}
